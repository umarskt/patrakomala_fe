<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Kolom following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Kolom :attribute must be accepted.',
    'active_url'           => 'Kolom :attribute is not a valid URL.',
    'after'                => 'Kolom :attribute must be a date after :date.',
    'alpha'                => 'Kolom :attribute may only contain letters.',
    'alpha_dash'           => 'Kolom :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'Kolom :attribute may only contain letters and numbers.',
    'array'                => 'Kolom :attribute must be an array.',
    'before'               => 'Kolom :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'Kolom :attribute must be between :min and :max.',
        'file'    => 'Kolom :attribute must be between :min and :max kilobytes.',
        'string'  => 'Kolom :attribute must be between :min and :max characters.',
        'array'   => 'Kolom :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'Kolom :attribute field must be true or false.',
    'confirmed'            => 'Kolom :attribute confirmation does not match.',
    'date'                 => 'Kolom :attribute is not a valid date.',
    'date_format'          => 'Kolom :attribute does not match the format :format.',
    'different'            => 'Kolom :attribute and :other must be different.',
    'digits'               => 'Kolom :attribute must be :digits digits.',
    'digits_between'       => 'Kolom :attribute must be between :min and :max digits.',
    'distinct'             => 'Kolom :attribute field has a duplicate value.',
    'email'                => 'Kolom :attribute tidak valid.',
    'exists'               => 'Kolom selected :attribute is invalid.',
    'filled'               => 'Kolom :attribute field is required.',
    'image'                => 'Kolom :attribute must be an image.',
    'in'                   => 'Kolom selected :attribute is invalid.',
    'in_array'             => 'Kolom :attribute field does not exist in :other.',
    'integer'              => 'Kolom :attribute must be an integer.',
    'ip'                   => 'Kolom :attribute must be a valid IP address.',
    'json'                 => 'Kolom :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'Kolom :attribute tidak boleh melebihi :max.',
        'file'    => 'Kolom :attribute tidak boleh melebihi :max kb.',
        'string'  => 'Kolom :attribute tidak boleh melebihi :max karakter.',
        'array'   => 'Kolom :attribute tidak boleh melebihi :max buah.',
    ],
    'mimes'                => 'Kolom :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'Kolom :attribute harus diisi minimal :min.',
        'file'    => 'Kolom :attribute harus diisi minimal :min kb.',
        'string'  => 'Kolom :attribute harus diisi minimal :min karakter.',
        'array'   => 'Kolom :attribute harus diisi minimal :min buah.',
    ],
    'not_in'               => 'Kolom selected :attribute is invalid.',
    'numeric'              => 'Kolom :attribute hanya boleh diisi angka.',
    'present'              => 'Kolom :attribute field must be present.',
    'regex'                => 'Kolom :attribute format is invalid.',
    'required'             => 'Kolom :attribute harus diisi.',
    'required_if'          => 'Kolom :attribute field is required when :other is :value.',
    'required_unless'      => 'Kolom :attribute field is required unless :other is in :values.',
    'required_with'        => 'Kolom :attribute field is required when :values is present.',
    'required_with_all'    => 'Kolom :attribute field is required when :values is present.',
    'required_without'     => 'Kolom :attribute field is required when :values is not present.',
    'required_without_all' => 'Kolom :attribute field is required when none of :values are present.',
    'same'                 => 'Kolom :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'Kolom :attribute must be :size.',
        'file'    => 'Kolom :attribute must be :size kilobytes.',
        'string'  => 'Kolom :attribute must be :size characters.',
        'array'   => 'Kolom :attribute must contain :size items.',
    ],
    'string'               => 'Kolom :attribute must be a string.',
    'timezone'             => 'Kolom :attribute must be a valid zone.',
    'unique'               => 'Kolom :attribute has already been taken.',
    'url'                  => 'Kolom :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | Kolom following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
