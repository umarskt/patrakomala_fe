@extends('layouts.application2')

@section('title')
    @lang('general.title.login')
@stop

@section('custom_meta')
@stop

@section('custom_styles')
@stop

@section('content')
<!-- 	<div class="container-fluid full">
		<div class="jumbotron-sign">
			
		</div>
	</div> -->
	<div class="container-fluid login-page">
	{!! Form::open(['route'=>'users.reset_password.post','method'=>'post','class'=>'']) !!}
		{{csrf_field()}}
			<div class="col-xs-12 col-sm-8 col-md-6 col-md-offset-3 col-sm-offset-2 login-form">
				<div class="login-form-box">
					<h3>Reset your password</h3>
					<div class="form-group">
						{!! Form::label('pin', 'Kode Keamanan') !!}
						{!! Form::text('pin',null,['class'=>'form-control']) !!}
						<span class="help-block">{{@$errors->first('pin')}}</span>
					</div>
					<div class="form-group">
						{!! Form::label('password', 'Password Baru') !!}
						{!! Form::password('password',['class'=>'form-control']) !!}
						<span class="help-block">{{@$errors->first('password')}}</span>
					</div>
					<div class="form-group">
						{!! Form::label('password_confirmation', 'Konfirmasi Password Baru') !!}
						{!! Form::password('password_confirmation',['class'=>'form-control']) !!}
						<span class="help-block">{{@$errors->first('password_confirmation')}}</span>
					</div>
					<div class="form-group right">
						<button type="submit" class="btn btn-orange" style="width:auto;">Reset Password</button>
					</div>
					<div class="form-group center">
						<p>Login <a href="{{route('users.sign_in')}}">disini</a>.</p>
					</div>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
@stop

@section('custom_scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			@if(@$errors->first('email'))
				$('#resendEmail').modal('show');
			@endif
			@if(@$errors->first('forgot_email'))
				$('#forgotPassword').modal('show');
			@endif
		});
	</script>
@stop