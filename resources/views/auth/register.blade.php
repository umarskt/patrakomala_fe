@extends('layouts.application2')

@section('title')
    @lang('general.title.registeruser')
@stop

@section('custom_meta')
@stop

@section('custom_styles')
@stop

@section('content')
<!-- 	<div class="container-fluid full">
		<div class="jumbotron-sign">
			
		</div>
	</div> -->
	<div class="container-fluid regis-page">
		<div class="page-detail blue">
			<h2 class="page-title center" style="margin-top: 80px;font-family: 'Poppins Bold';color:#fe7e41;">@lang('general.title.user.registration')</h2>
			<p class="center small" style="font-family: 'Poppins Light';margin-top:20px;">
				@lang('general.desc.regis-user')
			</p>
		</div>
		{!! Form::open(['route'=>'users.sign_up.post','method'=>'post','class'=>'']) !!}
		<div class="col-xs-12 col-sm-12 col-md-12 regis-form">
			<div class="regis-form-box">
				<h3>@lang('general.desc.login.3')</h3>
				<div class="row regis-form-box2">
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							{!! Form::label('client_first_name', 'First Name') !!}
							{!! Form::text('client_first_name',null,['class'=>'form-control','autofocus'=>true,'placeholder'=>'First Name']) !!}
							<span class="help-block">{{$errors->first('client_first_name')}}</span>
						</div>
						<div class="form-group">
							{!! Form::label('client_last_name', 'Last Name') !!}
							{!! Form::text('client_last_name',null,['class'=>'form-control','autofocus'=>true,'placeholder'=>'Last Name']) !!}
							<span class="help-block">{{$errors->first('client_last_name')}}</span>
						</div>
						<div class="form-group">
							{!! Form::label('company_name', 'Company Name') !!}
							{!! Form::text('company_name',null,['class'=>'form-control','autofocus'=>true,'placeholder'=>'Company Name']) !!}
							<span class="help-block">{{$errors->first('company_name')}}</span>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="form-group">
							{!! Form::label('client_email', 'Email Address') !!}
							{!! Form::text('client_email',null,['class'=>'form-control','placeholder'=>'Email Address']) !!}
							<span class="help-block">{{$errors->first('client_email')}}</span>
						</div>
						<div class="form-group">
							{!! Form::label('client_password', 'Password') !!}
							{!! Form::password('client_password',['class'=>'form-control','placeholder'=>'Password']) !!}
							<span class="help-block">{{$errors->first('client_password')}}</span>
						</div>
						<div class="form-group">
							{!! Form::label('client_password_confirmation', 'Confirm Password') !!}
							{!! Form::password('client_password_confirmation',['class'=>'form-control','placeholder'=>'Confirm Password']) !!}
							<span class="help-block">{{$errors->first('client_password_confirmation')}}</span>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="form-group right regis-submit">
							<label class="checkbox-inline">
								<input type="checkbox" name="term" value="1">
								<span class="checkboxmark"></span>
								<a href="#" data-toggle="modal" data-target="#modal-term" style="text-decoration: none;color:#000;">I accept Terms and Conditions</a>
							</label>
							<button type="submit" class="btn btn-orange btn-submit" disabled="true">@lang('general.title.user.registration')</button>
							<!-- <button type="button" class="btn btn-orange">Register</button> -->
						</div>
					</div>
				</div>
			</div>
		</div>
		{!! Form::close() !!}
			<div class="divider">
			    <hr class="pull-left"/>OR<hr class="pull-right" />
			</div><br/>
		<div class="col-xs-12 col-sm-12 col-md-12 regis-form">
			<div class="regis-form-box ">
				<h3 class="center">@lang('general.desc.login.4')</h3>
				<div class="row regis-form-box2">
					<div class="col-xs-12 col-sm-12 col-md-8">
						<p style="font-family: 'Poppins Light';">@lang('general.desc.regis-tenant')</p>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 center">
						<p class="regis-item"><a href="{{route('tennants.sign_up')}}" class="btn btn-orange">@lang('general.title.user.registration2')</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('auth.modals._terms')
@stop

@section('custom_scripts')
	{!! JsValidator::formRequest('App\Http\Requests\ClientRegisterRequest') !!}
	<script type="text/javascript">
		$('input[type=checkbox][name=term]').click(function(){
			if($(this).is(':checked')){
				$('.btn-submit').removeAttr('disabled');
			}
			else{
				$('.btn-submit').attr('disabled',true);	
			}
		});
		$('.oke').click(function(){
			// console.log($('input[name=term]').is(':checked'));
			if(!$('input[name=term]').is(':checked')){
				$('input[name=term]').trigger('click');
			}
		});
		$('input[name=term]').click(function(event){
			// $(this).removeAttr('checked');
			// event.preventDefault();
			if($('input[name=term]').is(':checked')){
				$('#modal-term').modal('show');
			}
		});
	</script>
@stop