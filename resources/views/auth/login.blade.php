@extends('layouts.application2')

@section('title')
    @lang('general.title.login')
@stop

@section('custom_meta')
@stop

@section('custom_styles')
@stop

@section('content')
<!-- 	<div class="container-fluid full">
		<div class="jumbotron-sign">
			
		</div>
	</div> -->
	<div class="container-fluid login-page">
	{!! Form::open(['route'=>'users.sign_in.post','method'=>'post','class'=>'']) !!}
			<div class="col-xs-12 col-sm-12 col-md-6 login-form">
				<div class="login-form-box">
					<h3>@lang('general.header.menu.login')</h3>
					<div class="form-group">
						{!! Form::label('username', 'Email / Username') !!}
						{!! Form::text('username',null,['class'=>'form-control','autofocus'=>true,'placeholder'=>'Username/Email']) !!}
						<span class="help-block">{{@$errors->first('username')}}</span>
					</div>
					<div class="form-group">
						{!! Form::label('password', 'Password') !!}
						{!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
						<span class="help-block">{{@$errors->first('password')}}</span>
					</div>
					<div class="form-group auth-link-right right">
						<a href='#' data-toggle="modal" data-target="#forgotPassword">@lang('general.desc.login.10')</a>
					</div>
					<div class="divider">
					    <!-- <hr class="pull-left"/>OR<hr class="pull-right" /> -->
					</div>
					<!-- <div class="form-group social">
						{!! Form::label('social', trans('general.desc.login.11')) !!}
						<br/>
						<div class="row">
							<div class="col-xs-4 social-item">
								<a href="{{route('auth.provider','facebook')}}">
									<img src="{{asset('assets/images/login/fb.png')}}">
								</a>
							</div>
							<div class="col-xs-4 social-item">
								<a href="{{route('auth.provider','google')}}">
									<img src="{{asset('assets/images/login/go.png')}}">
								</a>
							</div>
							<div class="col-xs-4 social-item">
								<a href="{{route('auth.provider','twitter')}}">
									<img src="{{asset('assets/images/login/tw.png')}}">
								</a>
							</div>
						</div>
					</div> -->
					<div class="form-group right">
						<button type="submit" class="btn btn-orange">@lang('general.header.menu.login')</button>
						<!-- <button type="button" class="btn btn-orange">Log in</button> -->
					</div>
					<div class="form-group center">
						<hr style="border-bottom:1px solid #000;" />
						<p>@lang('general.desc.login.5')</p>
						<a href="#" class="" style="color:#fe7e41;font-size: 16px;" data-toggle="modal" data-target="#resendEmail">@lang('general.desc.login.6')</a>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 login-form">
				<h3 class="center">@lang('general.desc.login.1')</h3>
				<p><!-- Lorem ipsum lorem nor ipsume ipsum lorem nor ipsume ipsum lorem nor ipsume ipsum lorem nor ipsume ipsum lorem nor ipsume ipsum lorem nor ipsume ipsum lorem nor ipsume ipsum lorem nor ipsume --></p>
				<br/>
				<p class="regis-item"><a href="{{route('users.sign_up')}}" class="btn btn-orange">@lang('general.desc.login.3')</a></p>
				<p class="regis-item"><a href="{{route('tennants.sign_up')}}" class="btn btn-orange">@lang('general.desc.login.4')</a></p>
			</div>
		{!! Form::close() !!}
	</div>
	<div id="resendEmail" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
					<h4 class="modal-title">@lang('general.desc.login.6')</h4>
				</div>
				<form method="post" action="{{route('auth.resend-email')}}" name='resend_email'>
					{{csrf_field()}}
					<div class="modal-body">
						<div class="form-group {{(@$errors->first('email')) ? 'has-error': ''}}">
							<label for='email' style="font-weight:normal;">Email</label>
							<input type="text" name="email" class="form-control" style="border-radius:0px;border:1px solid #000;" autofocus>
							<span class="help-block">{{@$errors->first('email')}}</span>
						</div>
					</div>
					<div class="modal-footer">
						<div class="form-group right">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							<input type="submit" name="new-job-sbm" value="Kirim" class="btn btn-orange">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="forgotPassword" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
					<h4 class="modal-title">@lang('general.desc.login.9')</h4>
				</div>
				<form method="post" action="{{route('users.forgot_password.post')}}" name='forgot_password'>
					{{csrf_field()}}
					<div class="modal-body">
						<div class="form-group {{(@$errors->first('forgot_email')) ? 'has-error': ''}}">
							<label for='forgot_email' style="font-weight:normal;">Email</label>
							<input type="text" name="forgot_email" class="form-control" style="border-radius:0px;border:1px solid #000;" autofocus>
							<span class="help-block">{{@$errors->first('forgot_email')}}</span>
						</div>
					</div>
					<div class="modal-footer">
						<div class="form-group right">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							<input type="submit" name="new-job-sbm" value="Kirim" class="btn btn-orange">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			@if(@$errors->first('email'))
				$('#resendEmail').modal('show');
			@endif
			@if(@$errors->first('forgot_email'))
				$('#forgotPassword').modal('show');
			@endif
		});
	</script>
@stop