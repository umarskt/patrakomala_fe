@extends('layouts.application2')

@section('title')
    @lang('general.title.news.detail')
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container news-detail">
		<hr>
		<div class="news-header row">
			<div class="col-xs-12 col-sm-4 header-image">
				<img src="{{@$news['image'][0]}}" alt="{{@$news['title']}}">
			</div>
			<div class="col-xs-12 col-sm-8">
				<h3>{{@$news['title']}}</h3>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-xs-12 col-sm-8">
				<div class="news-description">
					{!!@$news['description']!!}
				</div>
			</div>
			<div class="col-xs-12 col-sm-4">
				<ul class="list-news-side">
				@if(@$news['other_news'])
					@foreach($news['other_news'] as $key => $value)
						@if((@$value['id'] != @$news['id']))
							<li>
								<a href="{{route('news.detail',@$value['id'])}}">
									<img src="{{@$value['image']}}" alt="{{@$value['title']}}">
									<p>{{@$value['title']}}</p>
								</a>
							</li>
						@endif
					@endforeach
					<li><a href="{{route('home')}}?type=news" class="see-more">See More ...</a></li>
				@endif
				</ul>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')

@stop