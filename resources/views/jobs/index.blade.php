@extends('layouts.application')

@section('title')
    Job
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container">
		<div class="page-detail blue">
			<h3 class="page-title">WHAT YOU NEED</h3>
			<p>Proin in tellus sit amet nibh dignissim sagittis. Donec iaculis gravida nulla. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Pellentesque pretium lectus id turpis. Mauris metus.</p>
		</div>
	</div>
	<div class="container job-page">
		<div class="col-sm-12 row">
			{!! Form::open(['route'=>'tennants.sign_in.post','method'=>'post','class'=>'']) !!}
				<div class="col-xs-12 col-sm-5 login-form blue-bg">
					<h3>Already have account</h3>
					<div class="form-group">
						{!! Form::label('username', 'Username') !!}
						{!! Form::text('username',null,['class'=>'form-control','placeholder'=>'Username/Email','id'=>'username-login']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('password', 'Password') !!}
						{!! Form::password('password',['class'=>'form-control','placeholder'=>'Password','id'=>'password-login']) !!}
					</div>
					<div class="form-group auth-link-right">
						<a href="{{route('users.forgot_password')}}">forgot password?</a>
					</div>
					<div class="divider">
					    <hr class="pull-left"/>OR<hr class="pull-right" />
					</div>
					<div class="form-group social">
						{!! Form::label('social', 'Sign in with social media') !!}
						<br><br><br><br>
						<div class="row">
							<div class="col-xs-4">
								<a href="#">
									<img src="{{asset('assets/images/Button-social-media_01.png')}}">
								</a>
							</div>
							<div class="col-xs-4">
								<a href="#">
									<img src="{{asset('assets/images/Button-social-media_02.png')}}">
								</a>
							</div>
							<div class="col-xs-4">
								<a href="#">
									<img src="{{asset('assets/images/Button-social-media_03.png')}}">
								</a>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-red-full">SIGN IN</button>
				</div>
			{!! Form::close() !!}
			<div class="col-sm-2"><br/></div>
			{!! Form::open(['route'=>'tennants.sign_up.post','method'=>'post','class'=>'']) !!}
				<div class="col-xs-12 col-sm-5 login-form blue-bg">
					<h3>Create a new account</h3>
					<div class="form-group">
						{!! Form::label('full_name', 'Full Name') !!}
						{!! Form::text('full_name',null,['class'=>'form-control','placeholder'=>'Full Name']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('company_name', 'Company Name') !!}
						{!! Form::text('company_name',null,['class'=>'form-control','placeholder'=>'Company Name']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('email', 'Email Address') !!}
						{!! Form::email('email',null,['class'=>'form-control','placeholder'=>'Email Address']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('password', 'Password') !!}
						{!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('password_confirmation', 'Confirm Password') !!}
						{!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Confirm Password']) !!}
					</div>
					<button type="submit" class="btn btn-red-full">SIGN UP</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop

@section('custom_scripts')

@stop