@extends('layouts.hackathon')

@section('title')
    @lang('general.title.hackathon')
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_map1.css')}}">
	<link type="text/css" rel="stylesheet" href="{{asset('vendor/lightbox/style.css')}}" />
	<style>
		.lSSlideOuter {
			width: 100%;
		}
    	ul{
			list-style: none outside none;
		    padding-left: 0;
            margin: 0;
		}
		.content-slider li{
		    /*background-color: #ed3020;*/
		    text-align: center;
		}
		.content-slider img{
			width: 100%;
			height: auto;
		}
		.lSPager.lSpg{
			display: none;
		}
		@media(max-width: 450px) AND (max-height: 850px){
			.countdown td{
				padding: 10px !important;
			}
			.col-xs-12{
				padding-left: 0px !important;
				padding-right: 0px !important;
			}
			/*#logo-bandung{
				width: auto !important;
				height: 200px !important;
			}*/
		}
    </style>
@stop

@section('head_scripts')
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCEKkG9jlRbavkuE6unDPFGjZ6Ur5cYjHM&libraries=geometry"></script>
	@include('tennants.scripts._detail_tenant_maps')
	<script type="text/javascript">
		$(document).ready(function() {
		    // $("#content-slider").lightSlider({
      //           loop:true,
      //           keyPress:true
      //       });
	    });
	</script>
@stop

@section('content')
	<div class="container-fluid full">
		<!-- <div class="jumbotron jumbotron-tenant-detail" style="background:url('{{asset('assets/images/tenant/BANNER.png')}}');background-size:auto 100%;background-repeat: no-repeat;background-position:center;"> -->
		<div class="jumbotron jumbotron-hackathon center" style="background:url('{{asset('assets/images/hackathon/BG_header.png')}}');background-size:100% 100%;background-repeat: no-repeat;background-position:top center;">
			<img src="{{asset('assets/images/hackathon/Logo-Bandung-Lautan-Api.png')}}" alt="Logo Bandung Lautan Api" style="width:300px;height:auto;">
			<h3 class="text-uppercase">
				Hackathon 2018
			</h3>
			<h4>
				@lang('general.desc.hackathon.14')<br/>@lang('general.desc.hackathon.15')
			</h4>
			<table class="countdown">
				<tr>
					<td>
						<span class="pink" id="day"></span>
						<br/>@lang('general.desc.hackathon.16')
					</td>
					<td>
						<span class="pink" id="hour"></span>
						<br/>@lang('general.desc.hackathon.17')
					</td>
					<td>
						<span class="pink" id="minute"></span>
						<br/>@lang('general.desc.hackathon.18')
					</td>
					<td>
						<span class="pink" id="second"></span>
						<br/>@lang('general.desc.hackathon.19')
					</td>
				</tr>
			</table>
			<h4>
				<u>Pendaftaran telah ditutup</u>
			</h4>
		</div>
	</div>
	<div class="container detail-hackathon">
		<div class="col-xs-12 row">
			<div class="thank-hackathon">
				<div class="col-xs-12 col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
					<h4 class="text-uppercase">terima kasih telah melakukan pendaftaran</h4>
					<div class="col-xs-12 thank-box">
						<p>
							Pendaftaran anda akan melewati tahap verifikasi.
						</p>
						<p>
							Setelah dilakukan verifikasi, kami akan memberitahukan anda<br/>melalui email yang terdaftar.
						</p>
						<br/>
						<p class="reg">
							<a href="{{route('hackathon.index')}}" id="btn-stay-register" class="btn btn-square btn-orange">BACK TO EVENT DETAIL</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop

@section('custom_scripts')
	<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
	<script src="{{asset('vendor/lightbox/script.js')}}"></script>
	<script type="text/javascript">
		$('.select2').select2();
	</script>
	
@stop