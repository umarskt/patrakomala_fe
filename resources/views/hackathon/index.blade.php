@extends('layouts.hackathon')

@section('title')
    @lang('general.title.hackathon')
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_map1.css')}}">
	<link type="text/css" rel="stylesheet" href="{{asset('vendor/lightbox/style.css')}}" />
	<link type="text/css" rel="stylesheet" href="{{asset('vendor/grid/component.css')}}" />
	<link type="text/css" rel="stylesheet" href="{{asset('vendor/sliderengine/amazingslider-1.css')}}" />
	<style>
		#btn-scroll-register{
			color: #000;
			/*height: 100px;*/
			/*width: 100px;*/
			/*font-size: 0;*/
		}
		#btn-scroll-register p{
			font-family: 'Poppins Semibold';
			font-size: small;
		}
		#btn-scroll-register img{
			/*height: 100%;*/
			/*width: 100%;*/
			/*font-size: 0;*/
		}
		@media(max-width: 450px) AND (max-height: 850px){
			#btn-scroll-register{
				/*background-image: url("/assets/images/hackathon/icon-registration.png");*/
				background-color: transparent;
				top: 480px !important;
				right: 10px !important;
				/*height: 90px;*/
				/*width: 90px;*/
				/*font-size: 0;*/
			}
			#btn-scroll-register:after{
				content: "";
			}
			.countdown td{
				padding: 0px 10px !important;
			}
			/*#logo-bandung{
				width: auto !important;
				height: 200px !important;
			}*/
		}
		@media(max-width: 350px) AND (max-height: 600px){
			#btn-scroll-register{
				/*background-image: url("/assets/images/hackathon/icon-registration.png");*/
				background-color: transparent;
				top: 420px !important;
				right: 10px !important;
				/*height: 80px;*/
				/*width: 80px;*/
				/*font-size: 0;*/
			}
		}
    </style>
@stop

@section('head_scripts')
	<!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCEKkG9jlRbavkuE6unDPFGjZ6Ur5cYjHM&libraries=geometry"></script> -->
	<!-- @include('tennants.scripts._detail_tenant_maps') -->
	<script src="{{asset('vendor/grid/modernizr.custom.js')}}"></script>

	<script type="text/javascript">
		$(document).ready(function() {
		    // $("#content-slider").lightSlider({
      //           loop:true,
      //           keyPress:true
      //       });
	    });
	</script>
@stop

@section('content')

	<!-- BANNER -->
	<div class="container-fluid full">
		<!-- <div class="jumbotron jumbotron-tenant-detail" style="background:url('{{asset('assets/images/tenant/BANNER.png')}}');background-size:auto 100%;background-repeat: no-repeat;background-position:center;"> -->
		<div class="jumbotron jumbotron-hackathon center" style="background:url('{{asset('assets/images/hackathon/BG_header.png')}}');background-size:100% 100%;background-repeat: no-repeat;background-position:top center;">
			<img src="{{asset('assets/images/hackathon/Logo-Bandung-Lautan-Api.png')}}" alt="Logo Bandung Lautan Api" id="logo-bandung" style="width:300px;height:auto;">
			<h3 class="text-uppercase">
				Hackathon 2018
			</h3>
			<h4>
				@lang('general.desc.hackathon.14')<br/>@lang('general.desc.hackathon.15')
			</h4>
			<table class="countdown">
				<tr>
					<td>
						<div class="pink" id="day">43</div>
						Tim
					</td>
					<td>
						<div class="pink" id="hour">163</div>
						Orang
					</td>
					<td>
						<div class="pink" id="minute">43</div>
						Aplikasi
					</td>
				</tr>
			</table>
		</div>
	</div>

	<!-- INTRO -->
	<div class="container detail-hackathon">
		<div class="col-xs-12 row">
			<div class="page-detail2 blue">
				<p>Bandung Lautan API Hackathon — Double Diamond</p>
			</div>
		</div>
		<div class="row col-sm-10 col-md-10 col-lg-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2" style="margin-bottom: 20px">
			<div class="col-xs-12">
				<div class="work-image">
					<!-- <p>Design Process</p> -->
					 <p style="text-align: justify;" class="p-header">
						Amplifying Bandung Creative Industry Using Patrakomala Database to Increase Tourism. 
					</p>
					<p style="font-family: 'Poppins Regular';text-align: justify;">
						Ada bermacam masalah yang dihadapi pelaku industri kreatif dan UMKM di Kota Bandung. Lalu, bagaimana cara developers mengetahui masalah-masalah ini dan memberikan solusi aplikasi yang tepat? <br/>Di Bandung Lautan API Hackathon, developers akan mengidentifikasi masalah tersebut dengan bertemu langsung dengan pengusaha-pengusaha UMKM. Pengusaha UMKM akan membagikan masalah-masalah yang mereka hadapi sehari-hari. Developers akan diberi kebebasan untuk berinovasi dan mewujudkan aplikasi yang dapat sehingga aplikasi yang nantinya dikembangkan akan memiliki dampak yang nyata untuk mengakselerasi ekonomi kreatif.
					</p>
				</div>
			</div>
		</div>
		<div class="row col-sm-10 col-md-10 col-lg-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2" style="margin-bottom: 20px">
			<div class="col-xs-12">
				<div class="work-image">
					<p style="text-align: justify;" class="p-header">Desain Proses “Double Diamond”</p>
				</div>
			</div>
		</div>
		<div class="row col-sm-10 col-md-10 col-lg-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2" style="margin-bottom: 20px;">
			<div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">
				<div class="work-image div-image">
					<span class="span-image">
						<img src="{{asset('assets/images/hackathon/double-hack-01.png')}}" class="image-left" style="">
					</span>
				</div>
			</div>
			<div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
				<div class="work-image">
					<p style="text-align: left;" class="p-header text-uppercase">Discover</p>
					<p style="font-family: 'Poppins Regular';text-align: justify;">Developers dipertemukan dengan penguasaha UMKM untuk mengidenfikasi masalah-masalah nyata yang menghambat pengusaha UMKM untuk mengembangkan usahanya.</p>
				</div>
			</div>
		</div>
		<div class="row col-sm-10 col-md-10 col-lg-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2" style="margin-bottom: 20px;">
			<div class="col-xs-12 col-sm-5 col-md-4 col-lg-4 pull-right">
				<div class="work-image div-image">
					<span class="span-image">
						<img src="{{asset('assets/images/hackathon/double-hack-02.png')}}" class="image-right" style="">
					</span>
				</div>
			</div>
			<div class="col-xs-12 col-sm-7 col-md-8 col-lg-8 pull-left">
				<div class="work-image">
					<p style="text-align: right;" class="p-header text-uppercase">Define</p>
					<p style="font-family: 'Poppins Regular';text-align: justify;">
						Tahap kedua adalah kelanjutan dari tahap pertama, di mana developers mengideasikan kemungkinan yang muncul dari tahap Discover. Tujuannya adalah agar mendapatkan kerangka pemikiran yang jelas yang menjadi dasar pemecahan masalah. 
					</p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<div class="work-image">
					<p style="font-family: 'Poppins Regular';text-align: justify;">
						Di tahap Define, developers mendefinisikan masalah yang sudah diidentifikasi dari tahap Discover lalu mengideasikannya sebagai ide-ide yang kreatif, solutif, dan orisinal. Ide-ide inilah yang akan menjadi kerangka pemikiran yang jelas sebagai dasar dari pemecahan masalah.
					</p>
				</div>
			</div>
		</div>
		<div class="row col-sm-10 col-md-10 col-lg-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2" style="margin-bottom: 20px;">
			<div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">
				<div class="work-image div-image">
					<span class="span-image">
						<img src="{{asset('assets/images/hackathon/double-hack-03.png')}}" class="image-left" style="">
					</span>
				</div>
			</div>
			<div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
				<div class="work-image">
					<p style="text-align: left;" class="p-header text-uppercase">Develop</p>
					<p style="font-family: 'Poppins Regular';text-align: justify;">
						Tahap selanjutnya adalah Develop, yaitu konsep-konsep ideasi yang muncul dari tahap Develop mulai dikembangkan dan diuji. Ide-ide baru yang muncul di tahap ini juga bisa ikut diaplikasikan di iterasi-iterasi selanjutnya. 
					</p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<div class="work-image">
					<p style="font-family: 'Poppins Regular';text-align: justify;">
						Di tahap Develop, ide paling kreatif, solutif, dan orisinil yang muncul dari tahap Define mulai dikembangkan secara nyata sehingga nantinya akan menjadi aplikasi yang nyata dan dapat didemonstrasikan.
					</p>
				</div>
			</div>
		</div>
		<div class="row col-sm-10 col-md-10 col-lg-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2" style="margin-bottom: 20px;">
			<div class="col-xs-12 col-sm-5 col-md-4 col-lg-4 pull-right">
				<div class="work-image div-image">
					<span class="span-image">
						<img src="{{asset('assets/images/hackathon/double-hack-04.png')}}" class="image-right" style="">
					</span>
				</div>
			</div>
			<div class="col-xs-12 col-sm-7 col-md-8 col-lg-8 pull-left">
				<div class="work-image">
					<p style="text-align: right;" class="p-header text-uppercase">Delivery</p>
					<p style="font-family: 'Poppins Regular';text-align: justify;">
						Di tahap terakhir ini, aplikasi nyata yang sudah sukses dikembangkan sebagai buah hasil tiga tahap sebelumnya difinalisasikan dan didemonstrasikan.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<hr class="dt">
	</div>

	<!-- PEMENANG -->
	<div class="container-fluid full">
		<header class="clearfix">
			<h1>Pemenang</h1>	
		</header>
		<div class="container detail-hackathon detail-finalis">
			<div class="row col-sm-10 col-md-10 col-lg-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2" style="margin-bottom: 75px;">
				<div class="col-xs-12 col-sm-5 col-md-4 col-lg-5">
					<div class="div-image">
						<span class="span-image">
							<img src="{{asset('assets/images/winner/winner1.jpeg')}}" class="image-left" style="width:100%">
						</span>
					</div>
				</div>
				<div class="col-xs-12 col-sm-7 col-md-8 col-lg-7">
					<div class="work-image">
						<h2 style="text-align: left; color: #fe7e41" class="p-header text-uppercase">JUARA 1</h2>
						<h4 style="text-align: left;" class="p-header text-uppercase">MANDALA</h4>
						<p style="font-family: 'Poppins Regular';text-align: justify;">Aplikasi mobile end-to-end micro-event management dan direktori event untuk industri kreatif di kota Bandung.</p>
					</div>
				</div>
			</div>
			<div class="row col-sm-10 col-md-10 col-lg-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2" style="margin-bottom: 75px;">
				<div class="col-xs-12 col-sm-5 col-md-4 col-lg-5 pull-right">
					<div class="div-image">
						<span class="span-image">
							<img src="{{asset('assets/images/winner/winner2.jpeg')}}" class="image-right" style="width:100%">
						</span>
					</div>
				</div>
				<div class="col-xs-12 col-sm-7 col-md-8 col-lg-7 pull-left">
					<div class="work-image">
						<h2 style="text-align: right; color: #fe7e41" class="p-header text-uppercase">JUARA 2</h2>
						<h4 style="text-align: right;" class="p-header text-uppercase">SELLUTION PROJECT</h4>
						<p style="font-family: 'Poppins Regular';text-align: justify;">
							One stop sollution marketing apps for SME dengan fitur smart create post and marketing performance control. 
						</p>
					</div>
				</div>
			</div>
			<div class="row col-sm-10 col-md-10 col-lg-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2" style="margin-bottom: 75px;">
				<div class="col-xs-12 col-sm-5 col-md-4 col-lg-5">
					<div class="div-image">
						<span class="span-image">
							<img src="{{asset('assets/images/winner/winner3.jpeg')}}" class="image-left" style="width:100%">
						</span>
					</div>
				</div>
				<div class="col-xs-12 col-sm-7 col-md-8 col-lg-7">
					<div class="work-image">
						<h2 style="text-align: left; color: #fe7e41" class="p-header text-uppercase">JUARA 3</h2>
						<h4 style="text-align: left;" class="p-header text-uppercase">SADIKIN</h4>
						<p style="font-family: 'Poppins Regular';text-align: justify;">New way to spend your time. Perencanaan wisata dengan personalisasi tiap pengguna.</p>
					</div>
				</div>
			</div>
			<div class="row col-sm-10 col-md-10 col-lg-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2" style="margin-bottom: 75px;">
				<div class="col-xs-12 col-sm-5 col-md-4 col-lg-5 pull-right">
					<div class="div-image">
						<span class="span-image">
							<img src="{{asset('assets/images/winner/fav1.jpeg')}}" class="image-right" style="width:100%">
						</span>
					</div>
				</div>
				<div class="col-xs-12 col-sm-7 col-md-8 col-lg-7 pull-left">
					<div class="work-image">
						<h4 style="text-align: right; color: #fe7e41; margin-top: 20px;" class="p-header text-uppercase">JUARA FAVORIT JURI 1</h4>
						<h4 style="text-align: right;" class="p-header text-uppercase">BANDA</h4>
						<p style="font-family: 'Poppins Regular';text-align: justify;">
							Chatbot yang dapat dijadikan asisten informasi event dan ukm diwilayah tertentu. 
						</p>
					</div>
				</div>
			</div>
			<div class="row col-sm-10 col-md-10 col-lg-8 col-sm-offset-1 col-md-offset-1 col-lg-offset-2" style="margin-bottom: 75px;">
				<div class="col-xs-12 col-sm-5 col-md-4 col-lg-5">
					<div class="div-image">
						<span class="span-image">
							<img src="{{asset('assets/images/winner/fav2.jpeg')}}" class="image-left" style="width:100%">
						</span>
					</div>
				</div>
				<div class="col-xs-12 col-sm-7 col-md-8 col-lg-7">
					<div class="work-image">
						<h4 style="text-align: left; color: #fe7e41; margin-top: 20px;" class="p-header text-uppercase">JUARA FAVORIT JURI 2</h4>
						<h4 style="text-align: left;" class="p-header text-uppercase">XYNERGY</h4>
						<p style="font-family: 'Poppins Regular';text-align: justify;">Aplikasi yang dibuat untuk membantu user dan agen travel untuk merencanakan perjalanan wisata dengan rekomendasi dan kategori wisata tertentu.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<hr class="dt">
	</div>

	<!-- LIST PESERTA -->
	<div class="container-fluid full">
		<header class="clearfix">
			<h1>Peserta</h1>
		</header>
		<div class="main">
			<ul id="og-grid" class="og-grid">
				<li>
					<a href="" data-title="MANDALA" data-appname="KAMANA" data-teamlist='["Erdy Suryadarma","Dimas Dwi Adiguna","Krisna Fathurahman","Fadhli Dzil Ikram","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - MANDALA.mp4')}}" data-description="Aplikasi mobile end-to-end micro-event
					 management dan direktori event untuk industri kreatif di kota Bandung">
						<img src="{{asset('assets/images/winner1.png')}}" alt="" class="winner winner-badge">
						<div class="grid-title">
							<h4>MANDALA</h4>
							<h5>Kamana</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="SELLUTION PROJECT" data-appname="SELLUTION" data-teamlist='["Muhammad Naufal","Dion Arya Pamungkas","Lukmannudin","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - sellution project.mp4')}}" data-description="One stop sollution marketing apps
					 for SME dengan fitur smart create post and marketing performance control">
						<img src="{{asset('assets/images/winner2.png')}}" alt="" class="winner winner-badge">
						<div class="grid-title">
							<h4>Sellution Project</h4>
							<h5>Sellution</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="SADIKIN" data-appname="SATYA" data-teamlist='["Antonius Yonanda Chrisma Nugraha","HIBATUL GHAZI ZULHASMI","Aditya Laksana Suwandi","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - sadikin.mp4')}}" data-description="New way to spend your time. Perencanaan
					 wisata dengan personalisasi tiap pengguna">
						<img src="{{asset('assets/images/winner3.png')}}" alt="" class="winner winner-badge">
						<div class="grid-title">
							<h4>Sadikin</h4>
							<h5>Satya</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="BANDA" data-appname="BANDA" data-teamlist='["Ega Arie","Irfan Afif","","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - banda.mp4')}}" data-description="Chatbot yang dapat dijadikan asisten
					 informasi event dan ukm diwilayah tertentu">
						<img src="{{asset('assets/images/fav1.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Banda</h4>
							<h5>Banda</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="XYNERGY" data-appname="TRAV" data-teamlist='["Muhammad Nashrulloh Mukti","Prifani Hafidzah Fadillah","Wahyu Setiawan","Heru Julyanto Eka Supriyadi","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - xynergy.mp4')}}" data-description="Aplikasi yang dibuat untuk membantu user
					 dan agen travel untuk merencanakan perjalanan wisata dengan rekomendasi dan kategori wisata tertentu">
						<img src="{{asset('assets/images/fav2.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Xynergy</h4>
							<h5>Trav</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="ANT-ON" data-appname="SEBARULANG" data-teamlist='["Fakhri Abdullah","Beni Handoko","","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - ant-on.mp4')}}" data-description="Market Place Umkm terkhususkan bidang jasa
					 kota Bandung dengan management project terkontrol">
						<img src="{{asset('assets/images/finalis.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Ant-On</h4>
							<h5>SebarUlang</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="MINIM BUDGET" data-appname="UMKM MAJU" data-teamlist='["Timotius Muliawan Sugiarto","Fakhrul Zakaria","Anzalla Dzikri Dhamara","Muhammad Salman Abid","Windiana Dinda Sekaryus","Faresa Prasetyo N"]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - minim budget.mp4')}}" data-description="Memberikan sarana untuk mendapatkan
					 SDM yang berkualitas dan pemasaran yang meluas">
						<img src="{{asset('assets/images/finalis.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Minim Budget</h4>
							<h5>UMKM Maju</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="INVITEX" data-appname="TRIPERIA" data-teamlist='["Riksa Suviana Rochman","Sandi Setiawan","","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - invitex.mp4')}}" data-description="Aplikasi Trip Advisor bagi wisatawan asing
					 dengan fitur pencariran wisatawan dari negara yang sama">
						<img src="{{asset('assets/images/finalis.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Invitex</h4>
							<h5>Triperia</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="EMCORP STUDIO" data-appname="WARTA BANDUNG" data-teamlist='["Misbakhul Munir","","","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - emcorp studio.mp4')}}" data-description="Aplikasi berbasis mobile yang
					 menampilkan informasi wisata dan even kota bandung dengan fitur request join dan visit sebuah event">
						<img src="{{asset('assets/images/finalis.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Emcorp Studio</h4>
							<h5>Warta Bandung</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="INDIE DEVELOPER" data-appname="DASHBOARD" data-teamlist='["Try Fathur Rachman","","","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - indie developer.mp4')}}" data-description="Analitical dashboard for
					 Patrakomala">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Indie Developer</h4>
							<h5>dashboard</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="SDI ARRUHAMA" data-appname="BISMART" data-teamlist='["Yasir nahari Ari","","","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - sdi arruhama.mp4')}}" data-description="Bandung information of marketing.
					 Aplikasi yang memberikan informasi lengkap sebuah ukm">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>SDI ARRUHAMA</h4>
							<h5>Bismart</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				
				
				<li>
					<a href="" data-title="BANDUNG KEABADIAN" data-appname="TOUR BANDUNG" data-teamlist='["Azkal Fikri","Mohamad Ramdan","","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - bandung keabadian.mp4')}}" data-description="Aplikasi mobile tentang informasi
					 wisata kota bandung, rekomendasi wisata berdasarkan rute yang dilalui">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Bandung Keabadian</h4>
							<h5>Tour Bandung</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				
				<li>
					<a href="" data-title="DINNOVATION" data-appname="TANYAIN" data-teamlist='["Muhammad Iqbal Faturrahman","Muhammad Jalaludin","M Rifky Setiadji","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - dinnovation.mp4')}}" data-description="Web aplikasi forum yang dapat membantu
					 para pelaku bisnis dalam berbagi informasi dan tips usaha">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Dinnovation</h4>
							<h5>tanyain</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				
				<li>
					<a href="" data-title="KOMANDAN TOHA" data-appname="CAPON" data-teamlist='["I Putu Aldy Cahyakusuma","Ariq Heritsa Maalik","M Virgiawan Dwi Rangga","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - komandantoha.mp4')}}" data-description="Cari potensi umkm didaerah anda.
					 Dengan tujuan membantu para pelaku usaha dalam proses pengembangan usaha yang dimiliki">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>KOMANDAN TOHA</h4>
							<h5>Capon</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				
				<li>
					<a href="" data-title="THE RUSH" data-appname="INEKRAF" data-teamlist='["Azis Naufal","Irvan Lutfi Gunawan","Muhamad Panji Wiramanik","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - the rush.mp4')}}" data-description="Menjembatani investor dengan pemilik umkm.
					 Dengan tambahan vitur blog agar memilik umkm dapat membuat konten yang dapat menaikkan user engagement">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>The Rush</h4>
							<h5>Inekraf</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="ULINKUY" data-appname="ULIN.KUY" data-teamlist='["Mirza Febrian","Ahmad Ginanjar","Livinda Christy Monica","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - ulinkuy.mp4')}}" data-description="Tour planner lengkap dengan pemilihan tour
					 guide sesuai keinginan">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Ulinkuy</h4>
							<h5>Ulin.kuy</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				
				<li>
					<a href="" data-title="TALEUS" data-appname="PROMOTO" data-teamlist='["Ahmad Farhan Ghifari","Febi Agil Ifdillah","Harry Alvin Waidan Kefas","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - taleus.mp4')}}" data-description="Aplikasi guna membantu seniman pertunjukan
					 musik dalam menggalang dana dan mempromosikan event">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Taleus</h4>
							<h5>Promoto</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="RADYA PLAYGROUND" data-appname="BANDUNG POINT" data-teamlist='["Ade Rifaldi","Rudi Hartono","Puja Pramudya","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - radya playground.mp4')}}" data-description="Platform yang dapat memudahkan
					 umkm, disbudpar dalam memasarkan produk dan mempublikasikan kegiatan yang diadakan">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Radya Playground</h4>
							<h5>Bandung Point</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="LAMBDA" data-appname="ZPORT" data-teamlist='["Firyal Huwaida Fauzi","Tiara Ainal Salsabilla","Rahmah Najiyah Imtihan","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - lambda.mp4')}}" data-description="Aplikasi mobile untuk memudahkan pengelolaan
					 usaha">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Lambda</h4>
							<h5>Zport</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="TEACHING FACTORY" data-appname="TEFA" data-teamlist='["Fauzi ramadani","irsan firmansyah","Luthfi nur rohman","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - teaching factory.mp4')}}" data-description="Market place berbasis web
					 app untuk memajukan industri kreatif kota bandung">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Teaching Factory</h4>
							<h5>Tefa</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="ABTEAM" data-appname="BANDUNG KULINER" data-teamlist='["Muhammad Alif Faishol","Arief Rahman Yusniardi","Susep Supriatna","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - abteam.mp4')}}" data-description="Web aplikasi informasi kuliner yang juga
					 dipadukan dengan info even serta proposal penawaran kerjasama">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>ABTeam</h4>
							<h5>Bandung Kuliner</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="BUKAN KALENG KALENG" data-appname="BANDUNG GUIDE" data-teamlist='["Alif Jafar Fatkhurrohman","Muhammad Ilham","Ghuniyu Fattah Rozaq","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - bukankalengkaleng.mp4')}}" data-description="Aplikasi berbasis web yang
					 dapat menentukan kegiatan wisata, dengan suggestion event yang deiselenggarakan saat ini berdasarkan kondisi cuaca
					 sehingga dapat dengan tanpa khawatir menjalankan aktivitas wisata">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Bukan Kaleng Kaleng</h4>
							<h5>Bandung Guide</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="35UTECH" data-appname="TUKANG DAGANG" data-teamlist='["Try Setyo Utomo","Triana Giantara","Rijal Azani","Wildan Egi Ardiawan","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - 35utech.mp4')}}" data-description="Aplikasi berbasis mobile app yang
					 menghubungkan antara pedagang keliling dan pembeli dimana pembeli dapat memesan secara online dan realtime">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>35utech</h4>
							<h5>Tukang dagang</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="BINARY NUSANTARA" data-appname="ALVENT" data-teamlist='["Hadian Rahmat","Dede Rusliandi","Muhammad Angga","Alwan Alfian Setiawan","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - binarynusantara.mp4')}}" data-description="Aplikasi berbasis Web Aplikasi
					 yang dapat mempermudah pembuatan / perencanaan sebuah event atau acara">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Binary Nusantara</h4>
							<h5>Alvent</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="EGOS" data-appname="FORECAST" data-teamlist='["Fakhreza Taysaar Fajari","Aditya Permana Putra","Angga Hermawan","Dwi Fitria Al Husaeni","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - egos.mp4')}}" data-description="Aplikasi management bahan kuliner untuk
					 membantu para wirausahawan yang masih ragu akan bahan yang dibutuhkan dalam pembuatan produk">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>EGOS</h4>
							<h5>Forecast</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="HMIF ITENAS BANDUNG" data-appname="D : UPLOAD, COLAB, CREATE ARTS" data-teamlist='["Kevin Eza Rizky","M Alif Abhiesa Al-Kautsar","Reza Fadilah D","Mochamad Fahrizky Rohmana Putra","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - hmifitenas.mp4')}}" data-description="Aplikasi kolaborasi pecinta
					 musik ">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>HMIF Itenas Bandung</h4>
							<h5>d : upload, colab, create arts</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="KOPIPASTEU" data-appname="NYOBA.IN" data-teamlist='["Agit Naeta","Aep Saepudin","Gina Dwitasari","YUFUP FIRDAUS","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - kopipasteu.mp4')}}" data-description="Aplikasi tour planner berbasis mobile
					 apps">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>KopiPasteu</h4>
							<h5>nyoba.in</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="STM IOT TECH" data-appname="BIMA" data-teamlist='["Ahmad Herdiansyah","Alvin Ardiansyah Maulana","Suparlan","Rizqi Nino Firmansyah","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - stmiottech.mp4')}}" data-description="Bisnis masyarakat. Aplikasi yang
					 digunakan untuk menjawab keluhan-keluhan para pengusaha kreatif di kota Bandung. Terutama pengusaha yang baru dan
					 akan memasuki dunia digital">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>STM IoT Tech</h4>
							<h5>Bima</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="TIM OFATAN" data-appname="SHOPATU" data-teamlist='["Iqbal M Fauzan","Dika Ramadhan","Azhari M Marzan","Dilla Fajar Sukma Dilaga","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - timofatan.mp4')}}" data-description="Market place sepatu khusus produk lokal
					 dengan fitur jual beli, titip jual, dan rawat sepatu">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Tim Ofatan</h4>
							<h5>Shopatu</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				
				<li>
					<a href="" data-title="MAKNA" data-appname="SEJAHTERA" data-teamlist='["Fiko Gunawan","Trisna Risnandar","Ragil nurhawanti","Herlina Andriani","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - makna.mp4')}}" data-description="Sistem pinjaman modal usaha terpadu
					 rakyat">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4> Makna</h4>
							<h5>Sejahtera</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="QWERTY" data-appname="ASONG" data-teamlist='["Rahmat Aziz Al Hakam","Angga Arya Saputra","Diffa Dwi Desyawan","Rahmatullah Furqan","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - qwerty.mp4')}}" data-description="Aplikasi berbasis mobile app yang
					 menghubungkan antara pedagang asongan dan para pembelinya">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>QWERTY</h4>
							<h5>Asong</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="MA`SOEM SUPER TIM" data-appname="BANDUNG VM" data-teamlist='["Risca Nurzantika,Addieni Rifda Fadhilla","Moh Wahyu Septiansyah","Riki Ramadan","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - masoem super tim.mp4')}}" data-description="Aplikasi mobile yang akan
					 mempermudah pembelian produk industri rumahan, tanpa distributor sehingga harga beli yang ditawarkan akan semakin
					 murah">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>MA'SOEM SUPER TIM</h4>
							<h5>Bandung VM</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="SUARSOCIAL" data-appname="EVE" data-teamlist='["Restu Arif Priyono","Faris Sundara Putra","","","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - suarsocial.mp4')}}" data-description="Berjualan semudah update socmed, mecari
					 -cari tinggal swipe">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Suarsocial</h4>
							<h5>Eve</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				
				<li>
					<a href="" data-title="SMK PELITA BANDUNG" data-appname="PARIWISATA KOTA BANDUNG" data-teamlist='["KIKI FIRMANSYAH","Ardi radiansyah","Nendy Ruspendi","Imam Haryadi","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - smkpelita.mp4')}}" data-description="Aplikasi yang menampilkan
					 informasi lengkap tempat -tempat wisata kota bandung">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>SMK PELITA BANDUNG</h4>
							<h5>Pariwisata kota bandung</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="GJS" data-appname="YUK MAIN YUK" data-teamlist='["Raden Yudha Pratama Rahaui","Maulana Malik Ibrahim","Muhammad Khotibul Umam","Muhammad Iqbal Nurohman","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - gjs.mp4')}}" data-description="Web aplikasi untuk membantu pencarian info dan
					 rute angkutan umum">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>GJS</h4>
							<h5>Yuk main yuk</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="ASHABUL KAHFI" data-appname="FOOD JASERA" data-teamlist='["Qoriah Indah Susilowati","Rifqi Ardian Nugraha","Nikko Eka Saputra","Nurjandi Abdillah bronkhitis","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - ashabul kahfi.mp4')}}" data-description="Aplikasi Pembelian makanan pujasera
					 serta info pujasera terdekat">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Ashabul Kahfi</h4>
							<h5>Food Jasera</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				
				<li>
					<a href="" data-title="TIMHORE" data-appname="NGERANGKUL" data-teamlist='["Rayhan Rafiud Darojat","Didi Kurniawan","Dhiwani Maharani Aulia Nur Esya","Ramadhani Samudra Gawang indiyanto","Agfid Danu Prasetyo","Marzandi Zahran Affandi Leta"]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - timhore.mp4')}}" data-description="Aplikasi informasi referensi usaha dan
					 investasi serta statistik pengembangan usaha">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>TimHore</h4>
							<h5>Ngerangkul</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="TUDANG SIPULUNG" data-appname="DANA LANGIT" data-teamlist='["Muhammad Fikri Makmur","Rifqi Rosidin","Wiguna Ramadhan","Erza Putra Albasori","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - tudangsipulung.mp4')}}" data-description="Merupakan Aplikasi berbasis web dan
					 line bot yang menghubungkan antara UMKM dengan perusahaan. Bertujuan untuk memaksimalkan dan CSR dari perusahaan agar
					 dapat tepat guna dalam pengalokasiannya">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Tudang Sipulung</h4>
							<h5>Dana Langit </h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="MERAH MUDA DEV" data-appname="BANDUNG DESTINATION" data-teamlist='["Ujang Wahyu","Ferdhika Yudira","Dimas Aji Wardhana","Muhammad Ridwan Fathin","Herdhiantoko Fathani Sandra","Renaldy Rizki Maulana"]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - merah muda dev.mp4')}}" data-description="Platform yang bertujuan untuk
					 memberikan informasi tenant-tenant dengan lengkap guna menarik minat pengunjung serta dapat memberikan ruang kepada
					 pengguna untuk memberikan feedback kepada para tenant">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>Merah Muda Dev</h4>
							<h5>Bandung Destination </h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="OMICRON" data-appname="BECONNECT" data-teamlist='["Ziady Mubaraq","Rahman Abdul Razak","Achmad Abdul Rofiq","Rizki Nugraha","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - omicron.mp4')}}" data-description="Aplikasi mobile berbasis gamifikasi yang
					 dapat meng-generate itenary wisata dan industri kreatif berdasarkan hasil review dari pengguna">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>OMICRON</h4>
							<h5>Beconnect</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="SEMANGATKULIAH" data-appname="TOPIQ" data-teamlist='["Rizky Rahmat Hakiki","Dendy Armandiaz Aziz","Rizqi Prima Hariadhy","Michael Addryan Liwe","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - semangatkuliah.mp4')}}" data-description="Ceritakan masalah mu dan tunggu para
					 pelaku bisnis mengatasinya untukmu">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>SemangatKuliah</h4>
							<h5>Topiq</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="SMK SETIA BHAKTI" data-appname="TRAVEL & TOUR" data-teamlist='["Aldik","Faris Yusuf Chandra Nawijaya","Irsyad Nashirul Haq","Ferdy Arief Maulana","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - smk setia bhakti.mp4')}}" data-description="Web aplikasi yang menyajikan
					 informasi wisata serta estimasi harga kunjungan dan rekomendasi sesuai budget yang ditetapkan">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>SMK SETIA BHAKTI</h4>
							<h5>Travel & Tour</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
				<li>
					<a href="" data-title="SMK SETIA BHAKTI" data-appname="TRAVEL & TOUR" data-teamlist='["Aldik","Faris Yusuf Chandra Nawijaya","Irsyad Nashirul Haq","Ferdy Arief Maulana","",""]'
					 data-video="{{asset('assets/videos/HackathonBandungLautanAPI - smk setia bhakti.mp4')}}" data-description="Web aplikasi yang menyajikan
					 informasi wisata serta estimasi harga kunjungan dan rekomendasi sesuai budget yang ditetapkan">
						<img src="{{asset('assets/images/peserta.png')}}" alt="" class="winner-badge">
						<div class="grid-title">
							<h4>SMK SETIA BHAKTI</h4>
							<h5>Travel & Tour</h5>
						</div>
						<img src="{{asset('assets/images/news/hackathon.png')}}" alt="">
					</a>
				</li>
			</ul>
		</div>
	</div>
		
	<!-- <div class="container-fluid full detail-hackathon1 detail-hackathon" style="background:url('{{asset('assets/images/hackathon/BG.png')}}');background-size:100% auto;background-repeat: no-repeat;background-position:center center;">
		<div class="col-xs-12">
			<div class="desc-cool">
				<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
					<div class="col-xs-12 col-sm-10 col-sm-offset-1">
						<h4 class="text-uppercase">@lang('general.desc.hackathon.4')</h4>
						<p>@lang('general.desc.hackathon.4a')</p>
						<p>@lang('general.desc.hackathon.4b')</p>	
						<p>@lang('general.desc.hackathon.4c')</p>	
					</div>
				</div>
			</div>
		</div>
	</div> -->
	<br><br>
	<div class="container-fluid">
		<hr class="dt">
	</div>
	<!-- <hr class="dt"> -->
	<!-- <div class="container detail-hackathon">
		<div class="col-xs-12 row">
			<div class="page-detail2 blue">
				<p class="page-title text-uppercase">@lang('general.desc.hackathon.11')</p>
			</div>
		</div>
		<div class="col-xs-12 row" >
			<div class="desc-cool">
				<div class="col-xs-12 col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2 desc-cool-list">
					<div class="col-xs-12 col-sm-12">
						<div class="col-xs-5 center">10 September - 3 Oktober, 2018 </div><div class="col-xs-7"> : Pendaftaran Peserta</div>
					</div>
					<div class="col-xs-12 col-sm-12">
						<div class="col-xs-5 center">4 Oktober 2018 </div><div class="col-xs-7"> : Technical Meeting</div>
					</div>
					<div class="col-xs-12 col-sm-12">
						<div class="col-xs-5 center">6 - 7 Oktober 2018 </div><div class="col-xs-7"> : Hackathon Days</div>
					</div>
					<div class="col-xs-12 col-sm-12">
						<div class="col-xs-5 center">November 2018 </div><div class="col-xs-7"> : Pengumuman Pemenang</div>
					</div>
				</div>
			</div>
		</div>
	</div>-->

	<!-- SLIDER -->
	<div class="container-fluid full slider-container">
		<header class="clearfix">
			<h1>Galeri</h1>	
		</header>
		<div id="amazingslider-wrapper-1" style="display:block;position:relative;max-width:900px;margin:0px auto 108px;">
			<div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
				<ul class="amazingslider-slides" style="display:none;">
					<li><img src="{{asset('assets/images/winner/winner1.jpeg')}}" alt=""  title="Penyerahan hadiah secara simbolik Juara 1 Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/winner2.jpeg')}}" alt=""  title="Penyerahan hadiah secara simbolik Juara 2 Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/winner3.jpeg')}}" alt=""  title="Penyerahan hadiah secara simbolik Juara 3 Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/fav1-1.jpeg')}}" alt=""  title="Penyerahan hadiah secara simbolik Juara Favorit Juri 1 Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/fav22.jpeg')}}" alt=""  title="Penyerahan hadiah secara simbolik Juara Favorit Juri 2 Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/img-1.jpeg')}}" alt=""  title="Para pemenang Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/img-2.jpeg')}}" alt=""  title="Para peserta Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/img-3.jpeg')}}" alt=""  title="Pengalungan medali Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/img-4.jpeg')}}" alt=""  title="Pengalungan medali Hackathon Bandung Lautan API 2018" />
					</li>
					
					<!-- <li><img src="{{asset('assets/images/slider/peta.PNG')}}" alt="Sample WebM, Ogg, and MP4 Video Files for HTML5 _ TechSlides" title="Sample WebM, Ogg, and MP4 Video Files for HTML5 _ TechSlides" />
						<video preload="none" src="{{asset('assets/images/slider/Sample.mp4')}}"></video>
					</li> -->
				</ul>
				<ul class="amazingslider-thumbnails" style="display:none;">
					<li><img src="{{asset('assets/images/winner/winner1.jpeg')}}" alt=""  title="Penyerahan hadiah secara simbolik Juara 1 Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/winner2.jpeg')}}" alt=""  title="Penyerahan hadiah secara simbolik Juara 2 Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/winner3.jpeg')}}" alt=""  title="Penyerahan hadiah secara simbolik Juara 3 Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/fav1.jpeg')}}" alt=""  title="Penyerahan hadiah secara simbolik Juara Favorit Juri 1 Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/fav2.jpeg')}}" alt=""  title="Penyerahan hadiah secara simbolik Juara Favorit Juri 2 Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/img-1.jpeg')}}" alt=""  title="Para pemenang Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/img-2.jpeg')}}" alt=""  title="Para peserta Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/img-3.jpeg')}}" alt=""  title="Pengalungan medali Hackathon Bandung Lautan API 2018" />
					</li>
					<li><img src="{{asset('assets/images/winner/img-4.jpeg')}}" alt=""  title="Pengalungan medali Hackathon Bandung Lautan API 2018" />
					</li>
					<!-- <li><img src="{{asset('assets/images/slider/peta-tn.PNG')}}" alt="Sample WebM, Ogg, and MP4 Video Files for HTML5 _ TechSlides" title="Sample WebM, Ogg, and MP4 Video Files for HTML5 _ TechSlides" /></li> -->
				</ul>
			</div>
		</div>
	</div>

	<br><br>
	<div class="container-fluid">
		<hr class="dt">
	</div>
	<br><br>

	<!-- FAQ -->
	<div class="container-fluid detail-hackathon">
		<div class="col-xs-12">
			<div class="box">
				<!-- <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
					<div class="box-list" style="width:100%;">
						<h4>@lang('general.desc.hackathon.6')</h4>
						<p>@lang('general.desc.hackathon.7')</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-5">
					<div class="box-list" style="width:100%;">
						<h4>@lang('general.desc.hackathon.8')</h4>
						<ul>
							<li>@lang('general.desc.hackathon.8a')</li>
							<li>@lang('general.desc.hackathon.8b')</li>
							<li>@lang('general.desc.hackathon.8c')</li>
							<li>@lang('general.desc.hackathon.8d')</li>
						</ul>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
					<div class="box-list" style="width:100%;">
						<h4>@lang('general.desc.hackathon.9')</h4>
						<ul>
							<li>@lang('general.desc.hackathon.9a')</li>
							<li>@lang('general.desc.hackathon.9b')</li>
							<li>@lang('general.desc.hackathon.9c')</li>
						</ul>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-5" style="text-align:center;">
					<div class="reg">
						<p><a href="#" id="btn-stay-register" class="btn btn-square btn-orange">PENDAFTARAN DITUTUP</a></p>
						<img src="{{asset('assets/images/hackathon/Icon_5.png')}}">
						<p>@lang('general.desc.hackathon.2')</p>
					</div>
				</div> -->
				<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
					<div class="col-xs-12 box-list">
						<h4>FAQS</h4>
						<p><strong>@lang('general.desc.faq1.8')</strong></p>
						<p>@lang('general.desc.faq2.8')</p><br/>
						<p><strong>@lang('general.desc.faq1.9')</strong></p>
						<p>@lang('general.desc.faq2.9')</p><br/>
						<p><strong>@lang('general.desc.faq1.10')</strong></p>
						<p>@lang('general.desc.faq2.10')</p><br/>
						<p><strong>@lang('general.desc.faq1.11')</strong></p>
						<p>@lang('general.desc.faq2.11')</p><br/>
						<p><strong>@lang('general.desc.faq1.12')</strong></p>
						<p>@lang('general.desc.faq2.12')</p><br/>
						<p><strong>@lang('general.desc.faq1.13')</strong></p>
						<p>@lang('general.desc.faq2.13')</p>
					</div>
				</div>
				
			</div>
		</div>
	</div>

	<!-- SPONSOR -->
	<div class="container detail-hackathon partner-hackathon" style="text-align:center;">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<p>Diselenggarakan Oleh:</p>
					<div class="col-xs-12 col-md-4">
						<img src="{{asset('assets/images/hackathon/logo-disbudpar.png')}}" class="logo logo1">
					</div>
					<div class="col-xs-12 col-md-4">
						<img src="{{asset('assets/images/hackathon/logo-stunning.png')}}" class="logo logo2">
					</div>
					<div class="col-xs-12 col-md-4">
						<img src="{{asset('assets/images/hackathon/logo-patrakomala-square.png')}}" class="logo logo3">
					</div>
			</div>
<!-- 			<div class="col-xs-12 col-sm-12 col-md-12">
				<p>Partners:</p>
				<img src="{{asset('assets/images/hackathon/all_Partners.png')}}" class='partner'>
			</div> -->
		</div>
	</div>

@stop

@section('custom_scripts')
	<!-- <script src="{{asset('vendor/jquery/jquery-1.9.1.min.js')}}"></script> -->
	<script src="{{asset('vendor/sliderengine/amazingslider.js')}}"></script>
	<script src="{{asset('vendor/sliderengine/initslider-1.js')}}"></script>
	<script src="{{asset('vendor/grid/grid.js')}}"></script>
	<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
	<script src="{{asset('vendor/lightbox/script.js')}}"></script>
	<script type="text/javascript">
		$('.select2').select2();
		// $(window).scroll(function(){
		// 	if(isScrolledIntoView('#btn-stay-register')){
		// 		$('#btn-scroll-register').css('opacity','.5');
		// 	}
		// 	else{
		// 		$('#btn-scroll-register').css('opacity','1');	
		// 	}
		// });
		// $('#btn-scroll-register').on('mouseover',function(){
		// 	$(this).css('opacity','1');
		// });
		// $('#btn-scroll-register').on('mouseout',function(){
		// 	if(isScrolledIntoView('#btn-stay-register')){
		// 		$('#btn-scroll-register').css('opacity','.5');
		// 	}
		// 	else{
		// 		$('#btn-scroll-register').css('opacity','1');	
		// 	}		
		// });
		$(function() {
			Grid.init();
		});
		
	</script>
	
@stop