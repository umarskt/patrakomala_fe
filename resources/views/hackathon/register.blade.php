@extends('layouts.hackathon')

@section('title')
    @lang('general.title.hackathon')
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_map1.css')}}">
	<link type="text/css" rel="stylesheet" href="{{asset('vendor/lightbox/style.css')}}" />
	<style>
		.lSSlideOuter {
			width: 100%;
		}
    	ul{
			list-style: none outside none;
		    padding-left: 0;
            margin: 0;
		}
		.content-slider li{
		    /*background-color: #ed3020;*/
		    text-align: center;
		}
		.content-slider img{
			width: 100%;
			height: auto;
		}
		.lSPager.lSpg{
			display: none;
		}
		.divider{
			color: #fe7e41;
		}
		@media(max-width: 450px) AND (max-height: 850px){
			.countdown td{
				padding: 10px !important;
			}
			.col-xs-12{
				padding-right: 0px !important;
			}
			/*#logo-bandung{
				width: auto !important;
				height: 200px !important;
			}*/
		}
    </style>
@stop

@section('head_scripts')
	<!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCEKkG9jlRbavkuE6unDPFGjZ6Ur5cYjHM&libraries=geometry"></script> -->
	<!-- @include('tennants.scripts._detail_tenant_maps') -->
	<script type="text/javascript">
		$(document).ready(function() {
		    // $("#content-slider").lightSlider({
      //           loop:true,
      //           keyPress:true
      //       });
	    });
	</script>
@stop

@section('content')
	<div class="container-fluid full">
		<!-- <div class="jumbotron jumbotron-tenant-detail" style="background:url('{{asset('assets/images/tenant/BANNER.png')}}');background-size:auto 100%;background-repeat: no-repeat;background-position:center;"> -->
		<div class="jumbotron jumbotron-hackathon center" style="background:url('{{asset('assets/images/hackathon/BG_header.png')}}');background-size:100% 100%;background-repeat: no-repeat;background-position:top center;">
			<img src="{{asset('assets/images/hackathon/Logo-Bandung-Lautan-Api.png')}}" alt="Logo Bandung Lautan Api" style="width:300px;height:auto;">
			<h3 class="text-uppercase">
				Hackathon 2018
			</h3>
			<h4>
				@lang('general.desc.hackathon.14')<br/>@lang('general.desc.hackathon.15')
			</h4>
			<table class="countdown">
				<tr>
					<td>
						<span class="pink" id="day"></span>
						<br/>@lang('general.desc.hackathon.16')
					</td>
					<td>
						<span class="pink" id="hour"></span>
						<br/>@lang('general.desc.hackathon.17')
					</td>
					<td>
						<span class="pink" id="minute"></span>
						<br/>@lang('general.desc.hackathon.18')
					</td>
					<td>
						<span class="pink" id="second"></span>
						<br/>@lang('general.desc.hackathon.19')
					</td>
				</tr>
			</table>
			<h4>
				<u>Pendaftaran telah ditutup</u>
			</h4>
		</div>
	</div>
	<div class="container detail-hackathon" id="dh">

		<div class="col-xs-12 row">
			<div class="register-hackathon">
				<div class="col-xs-12 col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2">
					<div class="col-xs-12 col-sm-12">
						<h4 class="text-uppercase">@lang('general.desc.hackathon.0')</h4>
						<p class="center">@lang('general.desc.hackathon.1')</p><br/>
						<form method="post" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="form-group">
								<label>Nama Tim</label>
								<input type="text" id="team_name" name="team_name" class="form-control" value="{{\Input::old('team_name')}}">
								<span class="help-block">{{@$errors->first('team_name')}}</span>
							</div>
<!-- 							<div class="form-group">
								<label>Ide</label>
								<textarea name="description" class="form-control">{{Input::old('description')}}</textarea>
								<span class="help-block">{{@$errors->first('description')}}</span>
							</div>
							<div class="form-group">
								<label>File Proposal</label>
								<input type="file" name="file" class="form-control">
								<span class="help-block">{{@$errors->first('file')}}</span>
							</div> -->
							<label>Kontak</label><hr style="border-bottom:1px solid #fe7e41;" />
							<p style="font-size: small;">*email kontak yang menerima notifikasi</p>
							<div class="form-group form-group2">
								<label>No HP</label>
								<input type="text" name="contact_phone" class="form-control" value="{{Input::old('contact_phone')}}">
								<span class="help-block">{{@$errors->first('contact_phone')}}</span>
							</div>
							<div class="form-group form-group2">
								<label>Alamat Email</label>
								<input type="email" name="contact_email" class="form-control" value="{{Input::old('contact_email')}}">
								<span class="help-block">{{@$errors->first('contact_email')}}</span>
							</div>
							<label>Anggota</label><hr style="border-bottom:1px solid #fe7e41;" />
							<p style="font-size: small;">*isikan data seluruh anggota</p>
							<div class="render-member">
								<div class="divider">
								    <hr class="pull-left"/>Member 1<hr class="pull-right" />
								</div>
								<div class="the-member">
									
									<div class="form-group form-group2 col-sm-6">
										<label>Nama Lengkap </label>
										<input type="text" name="name[]" class="form-control names" value="{{Input::old('name')[0]}}">
										<!-- <p class="select3-m1"></p> -->
										<!-- <select class="select3 form-control" name="team_name[]" multiple="multiple">
										</select> -->
										<span class="help-block">{{@$errors->first('name.0')}}</span>
									</div>
									<div class="form-group form-group2 col-sm-6">
										<label>No HP </label>
										<input type="text" name="phone[]" class="form-control" value="{{Input::old('phone')[0]}}">
										<span class="help-block">{{@$errors->first("phone.0")}}</span>
									</div>
									<div class="form-group form-group2 col-sm-6">
										<label>Alamat Email </label>
										<input type="email" name="email[]" class="form-control" value="{{Input::old('email')[0]}}">
										<span class="help-block">{{@$errors->first('email.0')}}</span>
									</div>
									<div class="form-group form-group2 col-sm-6">
										<label>Riwayat Penyakit </label>
										<input type="text" name="ill[]" class="form-control" value="{{Input::old('ill')[0]}}">
										<span class="help-block">{{@$errors->first('ill.0')}}</span>
									</div>
									<div class="form-group form-group2 col-sm-12">
										<label>Alamat </label>
										<textarea class="form-control" name="address[]">{{Input::old('address')[0]}}</textarea>
										<span class="help-block">{{@$errors->first('address.0')}}</span>
									</div>
									<div class="form-group form-group2 col-sm-6">
										<label>Kontak Darurat (Nama) </label>
										<input type="text" name="emergency_name[]" class="form-control" value="{{Input::old('emergency_name')[0]}}">
										<span class="help-block">{{@$errors->first("emergency_name.0")}}</span>
									</div>
									<div class="form-group form-group2 col-sm-6">
										<label>Kontak Darurat (Telepon) </label>
										<input type="text" name="emergency_phone[]" class="form-control" value="{{Input::old('emergency_phone')[0]}}">
										<span class="help-block">{{@$errors->first('emergency_phone.0')}}</span>
									</div>
									<div class="form-group form-group2 col-sm-12">
										<label>Kontak Darurat (Alamat) </label>
										<textarea class="form-control" name="emergency_address[]">{{Input::old('emergency_address')[0]}}</textarea>
										<span class="help-block">{{@$errors->first('emergency_address.0')}}</span>
									</div>
								</div>
								@for($i=1;$i<=10;$i++)
									@if(@Input::old('name')[$i] || @Input::old('phone')[$i] || @Input::old('email')[$i] || @Input::old('ill')[$i] || @Input::old('address')[$i] || @Input::old('emergency_name')[$i] || @Input::old('emergency_phone')[$i] || @Input::old('emergency_address')[$i])
										<div class="divider" style="margin-top:60px;">
											<hr class="pull-left"/>Member {{($i+1)}}<hr class="pull-right" />
										</div>
										<div class="the-member">
											<div class="form-group form-group2 col-sm-6">
												<label>Nama Lengkap </label>
												<input type="text" name="name[]" class="form-control names" value="{{Input::old('name')[$i]}}">
												<!-- <p class="select3-m1"></p> -->
												<!-- <select class="select3 form-control" name="team_name[]" multiple="multiple">
												</select> -->
												<span class="help-block">{{@$errors->first('name.'.$i)}}</span>
											</div>
											<div class="form-group form-group2 col-sm-6">
												<label>No HP </label>
												<input type="text" name="phone[]" class="form-control" value="{{Input::old('phone')[$i]}}">
												<span class="help-block">{{@$errors->first("phone.".$i)}}</span>
											</div>
											<div class="form-group form-group2 col-sm-6">
												<label>Alamat Email </label>
												<input type="email" name="email[]" class="form-control" value="{{Input::old('email')[$i]}}">
												<span class="help-block">{{@$errors->first('email.'.$i)}}</span>
											</div>
											<div class="form-group form-group2 col-sm-6">
												<label>Riwayat Penyakit </label>
												<input type="text" name="ill[]" class="form-control" value="{{Input::old('ill')[$i]}}">
												<span class="help-block">{{@$errors->first('ill.'.$i)}}</span>
											</div>
											<div class="form-group form-group2 col-sm-12">
												<label>Alamat </label>
												<textarea class="form-control" name="address[]">{{Input::old('address')[$i]}}</textarea>
												<span class="help-block">{{@$errors->first('address.'.$i)}}</span>
											</div>
											<div class="form-group form-group2 col-sm-6">
												<label>Kontak Darurat (Nama) </label>
												<input type="text" name="emergency_name[]" class="form-control" value="{{Input::old('emergency_name')[$i]}}">
												<span class="help-block">{{@$errors->first("emergency_name.".$i)}}</span>
											</div>
											<div class="form-group form-group2 col-sm-6">
												<label>Kontak Darurat (Phone) </label>
												<input type="text" name="emergency_phone[]" class="form-control" value="{{Input::old('emergency_phone')[$i]}}">
												<span class="help-block">{{@$errors->first('emergency_phone.'.$i)}}</span>
											</div>
											<div class="form-group form-group2 col-sm-12">
												<label>Kontak Darurat (Alamat) </label>
												<textarea class="form-control" name="emergency_address[]">{{Input::old('emergency_address')[$i]}}</textarea>
												<span class="help-block">{{@$errors->first('emergency_address.'.$i)}}</span>
											</div>
										</div>
									@endif
								@endfor
							</div>
							<div class="form-group2 " style="clear:both;">
								<hr style="border-top:1px solid gray;margin-top:30px;">
							</div>
							<div class="form-group form-group2 right col-sm-12">
								<a href="javascript:void(0);" class="btn btn-default add-more" style="color:#999;"><i class="fa fa-plus"></i> Member</a>
							</div>
							<div class="form-group right col-sm-12">
								<button type="submit" class="btn btn-orange">SUBMIT</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-xs-12 row">
				<a href="{{route('hackathon.index')}}" id="btn-stay-register" class="btn pull-left" style="color: #000;text-decoration: none;"><i class="fa fa-caret-left"></i> Kembali ke halaman detail</a>				
			</div>
		</div>
	</div>

@stop

@section('custom_scripts')
	<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
	<script src="{{asset('vendor/lightbox/script.js')}}"></script>
	<script type="text/javascript">
		$('input[name=file]').bind('change', function() {
			if((this.files[0].size/1024/1024) > 8){
				$(this).parent().find('.help-block').text('File upload maximal 8MB');
				$('button[type=submit]').attr('disabled','disabled');
			}
			else{
				$(this).parent().find('.help-block').text('');
				$('button[type=submit]').removeAttr('disabled');
			}
        });
		@if(@$errors->first('team_name'))
			let inp = document.getElementById('dh');
			inp.scrollIntoView();
		@endif
		$('.select2').select2();
		$('.select3').select2({
			tags: true,
    		tokenSeparators: [','],
			createTag: function (params) {
				var term = $.trim(params.term);

				if (term === '') {
				  return null;
				}

				return {
				  id: term,
				  text: term,
				  newTag: true // add additional parameters
				}
			},
			language: {
			    noResults: function (params) {
			      return "";
			    }
			}
		});

		$('.add-more').click(function(){
			let member_length = $('.names').length;
			let html = '<div class="divider" style="margin-top:60px;"><hr class="pull-left"/>Member '+(member_length+1)+'<hr class="pull-right" /></div><div class="the-member"><div class="form-group form-group2 col-sm-6"><label>Nama Lengkap </label><input type="text" name="name[]" class="form-control names"><span class="help-block"></span></div><div class="form-group form-group2 col-sm-6"><label>No HP </label><input type="text" name="phone[]" class="form-control"><span class="help-block"></span></div><div class="form-group form-group2 col-sm-6"><label>Alamat Email </label><input type="email" name="email[]" class="form-control"><span class="help-block"></span></div><div class="form-group form-group2 col-sm-6"><label>Riwayat Penyakit </label><input type="text" name="ill[]" class="form-control"><span class="help-block"></span></div><div class="form-group form-group2 col-sm-12"><label>Alamat </label><textarea class="form-control" name="address[]"></textarea><span class="help-block"></span></div><div class="form-group form-group2 col-sm-6"><label>Kontak Darurat (Nama) </label><input type="text" name="emergency_name[]" class="form-control" value=""><span class="help-block"></span></div><div class="form-group form-group2 col-sm-6"><label>Kontak Darurat (Phone) </label><input type="text" name="emergency_phone[]" class="form-control" value=""><span class="help-block"></span></div><div class="form-group form-group2 col-sm-12"><label>Kontak Darurat (Alamat) </label><textarea class="form-control" name="emergency_address[]"></textarea><span class="help-block"></span></div><div class="form-group form-group2 col-sm-12"><a href="javascript:void(0);" class="btn btn-default remove-member" style="color:#999;" onclick="removeThis(this);"><i class="fa fa-minus"></i> Member</a></div></div>';
			$('.render-member').append(html);
		});

		function removeThis(e){
			// console.log();
			console.log($(e).parents('.the-member').next('.divider'));
			if($(e).parents('.the-member').next('.divider').length >0){
				$(e).parents('.the-member').next('.divider').remove();
			}
			else{
				$(e).parents('.the-member').prev('.divider').remove();
			}
			$(e).parent().parent().html('');
		}
	</script>
	
@stop