@extends('layouts.application')

@section('title')
    @lang('general.title.safarihki')
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_map1.css')}}">
	<link type="text/css" rel="stylesheet" href="{{asset('vendor/lightbox/style.css')}}" />
	<style>
		#btn-scroll-register{
			color: #000;
			/*height: 100px;*/
			/*width: 100px;*/
			/*font-size: 0;*/
		}
		#btn-scroll-register p{
			font-family: 'Poppins Semibold';
			font-size: small;
		}
		#btn-scroll-register img{
			/*height: 100%;*/
			/*width: 100%;*/
			/*font-size: 0;*/
		}
		@media(max-width: 450px) AND (max-height: 850px){
			#btn-scroll-register{
				/*background-image: url("/assets/images/hackathon/icon-registration.png");*/
				background-color: transparent;
				top: 480px !important;
				right: 10px !important;
				/*height: 90px;*/
				/*width: 90px;*/
				/*font-size: 0;*/
			}
			#btn-scroll-register:after{
				content: "";
			}
			.countdown td{
				padding: 0px 10px !important;
			}
			/*#logo-bandung{
				width: auto !important;
				height: 200px !important;
			}*/
		}
		@media(max-width: 350px) AND (max-height: 600px){
			#btn-scroll-register{
				/*background-image: url("/assets/images/hackathon/icon-registration.png");*/
				background-color: transparent;
				top: 420px !important;
				right: 10px !important;
				/*height: 80px;*/
				/*width: 80px;*/
				/*font-size: 0;*/
			}
		}
    </style>
@stop

@section('head_scripts')
	<!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCEKkG9jlRbavkuE6unDPFGjZ6Ur5cYjHM&libraries=geometry"></script> -->
	<!-- @include('tennants.scripts._detail_tenant_maps') -->
	<script type="text/javascript">
		$(document).ready(function() {
		    // $("#content-slider").lightSlider({
      //           loop:true,
      //           keyPress:true
      //       });
	    });
	</script>
@stop

@section('content')
	<div class="container-fluid full">
		<!-- <div class="jumbotron jumbotron-tenant-detail" style="background:url('{{asset('assets/images/tenant/BANNER.png')}}');background-size:auto 100%;background-repeat: no-repeat;background-position:center;"> -->
		<div class="jumbotron jumbotron-hackathon center" style="background:url('{{asset('assets/images/hackathon/BG_header.png')}}');background-size:100% 100%;background-repeat: no-repeat;background-position:top center;height: auto;padding-bottom: 70px;">
			<!-- <img src="{{asset('assets/images/hackathon/Logo-Bandung-Lautan-Api.png')}}" alt="Logo Bandung Lautan Api" id="logo-bandung" style="width:300px;height:auto;"> -->
			<h3 class="text-uppercase safarihki">
				Safari HKI
			</h3>
			<h4>
				@lang('general.desc.safarihki.1')<br/>@lang('general.desc.safarihki.1a')
			</h4>
			<h4 style="margin: 25px 0;">
				<a href="https://goo.gl/forms/omJzDWkdY2sIJU1j1" target="_blank" style="color: #000;"><u>Daftar Sekarang</u></a>
			</h4>
		</div>
	</div>
<!-- 	<div class="container-fluid detail-hackathon">
		<div class="reg">
			<a href="{{route('hackathon.register')}}" id="btn-scroll-register" class="btn " style="position: fixed;right:20px;top:500px;z-index: 999;">
				<img src="{{asset('assets/images/hackathon/icon-registration.png')}}">
				<p>DAFTARKAN <br/>TIMMU</p>
			</a>
		</div>
	</div> -->
	<div class="container-fluid full detail-hackathon1 detail-hackathon" style="background:url('{{asset('assets/images/hackathon/BG.png')}}');background-size:100% auto;background-repeat: no-repeat;background-position:center center;">
		<div class="col-xs-12">
			<div class="desc-cool">
				<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
					<div class="col-xs-12 col-sm-10 col-sm-offset-1">
						<h4 class="text-uppercase">@lang('general.desc.safarihki.2')</h4>
						<p>@lang('general.desc.safarihki.2a')</p>
						<p>@lang('general.desc.safarihki.2b')</p>	
						<!-- <p>@lang('general.desc.hackathon.4c')</p>	 -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<hr class="dt">
	</div>
	<br><br>
	<div class="container-fluid detail-hackathon detail-safarihki">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="box">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="box-list" style="width:100%;">
								<h4>@lang('general.desc.safarihki.3')</h4>
								<p>@lang('general.desc.safarihki.3a')</p>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="box-list" style="width:100%;">
								<h4>@lang('general.desc.safarihki.6')</h4>
								<p>@lang('general.desc.safarihki.6a')</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="box-list long-content" style="width:100%;">
								<h4>@lang('general.desc.safarihki.5')</h4>
								<ul>
									<li>@lang('general.desc.safarihki.5a')</li>
									<li>@lang('general.desc.safarihki.5b')</li>
									<li>@lang('general.desc.safarihki.5c')</li>
									<li>@lang('general.desc.safarihki.5d')</li>
									<li>@lang('general.desc.safarihki.5e')</li>
									<li>@lang('general.desc.safarihki.5f')</li>
								</ul>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="box-list long-content" style="width:100%;">
								<h4>@lang('general.desc.safarihki.4')</h4>
								<p>@lang('general.desc.safarihki.4a')</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="box-list" style="width:100%;">
								<h4>@lang('general.desc.safarihki.7')</h4>
								<p>@lang('general.desc.safarihki.7a')</p>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6" style="text-align:center;">
							<div class="reg">
								<p><a href="https://goo.gl/forms/omJzDWkdY2sIJU1j1" target="_blank" id="btn-stay-register" class="btn btn-square btn-orange">DAFTAR SEKARANG</a></p>
								<img src="{{asset('assets/images/hackathon/Icon_5.png')}}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<div class="col-xs-12 box-list">
								<h4>FAQS</h4>
								<p><strong>@lang('general.desc.faq1.14')</strong></p>
								<p>@lang('general.desc.faq2.14')</p><br/>
								<p><strong>@lang('general.desc.faq1.15')</strong></p>
								<p>@lang('general.desc.faq2.15')</p><br/>
								<p><strong>@lang('general.desc.faq1.16')</strong></p>
								<p>@lang('general.desc.faq2.16')</p><br/>
								<p><strong>@lang('general.desc.faq1.17')</strong></p>
								<p>@lang('general.desc.faq2.17')</p><br/>
								<p><strong>@lang('general.desc.faq1.18')</strong></p>
								<p>@lang('general.desc.faq2.18')</p><br/>
								<p><strong>@lang('general.desc.faq1.19')</strong></p>
								<p>@lang('general.desc.faq2.19')</p><br/>
								<p><strong>@lang('general.desc.faq1.20')</strong></p>
								<p>@lang('general.desc.faq2.20')</p><br/>
								<p><strong>@lang('general.desc.faq1.21')</strong></p>
								<p>@lang('general.desc.faq2.21')</p><br/>
								<p><strong>@lang('general.desc.faq1.22')</strong></p>
								<ol class="pl-1-sm">
									<li><p>@lang('general.desc.faq2.22a')</p></li>
									<li><p>@lang('general.desc.faq2.22b')</p>
										<ul class="pl-1-sm">
											<li><p>@lang('general.desc.faq2.22ba')</p></li>
											<li><p>@lang('general.desc.faq2.22bb')</p></li>
											<li><p>@lang('general.desc.faq2.22bc')</p></li>
											<li><p>@lang('general.desc.faq2.22bd')</p></li>
											<li><p>@lang('general.desc.faq2.22be')</p></li>
											<li><p>@lang('general.desc.faq2.22bf')</p></li>
										</ul>
									</li>
								</ol>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<hr>
	<div class="container detail-hackathon partner-hackathon" style="text-align:center;">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<p>Diselenggarakan Oleh:</p>
					<div class="col-xs-12 col-md-4">
						<img src="{{asset('assets/images/hackathon/logo-disbudpar.png')}}" class="logo logo1">
					</div>
					<div class="col-xs-12 col-md-4">
						<img src="{{asset('assets/images/hackathon/logo-stunning.png')}}" class="logo logo2">
					</div>
					<div class="col-xs-12 col-md-4">
						<img src="{{asset('assets/images/hackathon/logo-patrakomala-square.png')}}" class="logo logo3">
					</div>
			</div>
<!-- 			<div class="col-xs-12 col-sm-12 col-md-12">
				<p>Partners:</p>
				<img src="{{asset('assets/images/hackathon/all_Partners.png')}}" class='partner'>
			</div> -->
		</div>
	</div>

@stop

