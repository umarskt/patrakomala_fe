@extends('layouts.application')

@section('title')
    Not Found
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
    <div class="container-fluid" style="min-height: 800px;padding-top: 200px;" >
        <div class="col-xs-12 col-sm-12">
                <p class="center">The page you are looking for, does not exist</p>
        </div>
    </div>
@stop