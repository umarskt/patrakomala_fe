@extends('layouts.application2')

@section('title')
    @lang('general.title.event.detail')
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container event-detail">
		<div class="col-xs-12 col-sm-5 header-image">
			<!-- <img src="{{@$event['image']}}" alt="{{@$event['title']}}" class="thi"> -->
			@if(is_array(@$event['images']))
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators" style="display: none;">
					@foreach($event['images'] as $key => $image)
					    <li data-target="#myCarousel" data-slide-to="{{$key}}" class="{{($key==0)?'active':''}}"></li>
				    @endforeach
				  </ol>
				  <div class="carousel-inner">
					@foreach($event['images'] as $key => $image)
					    <div class="item {{($key==0)?'active':''}}">
					      <img src="{{@$image}}" alt="Event gallery">
					    </div>
				    @endforeach
				  </div>
				  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#myCarousel" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			@endif
		</div>
		<div class="col-xs-12 col-sm-7">
			<h3 class="text-capitalize">{{@$event['title']}}</h3>
		</div>
	</div>
	<div class="container event-detail">
		<div class="col-xs-12 col-sm-8">
			<div class="event-description">
				{!!@$event['description']!!}
			</div>
			@if(is_array(@$event['urls']))
				@if(count(@$event['urls']) > 0)
					<div class="event-description">
						<h4>Link-link terkait:</h4>
						<ul >
						@foreach($event['urls'] as $key=>$url)
							<li><a href="{{$url}}" target=_blank>{{$url}}</a></li>
						@endforeach
						</ul>
					</div>
				@endif
			@endif
		</div>
		<div class="col-xs-12 col-sm-4">
			<div class="event-opsi">
				<!-- <a href="{{route('users.sign_up')}}" class="btn btn-orange">REGISTER</a> -->
				<p>
					<img src="{{asset('assets/images/event/Icon_01.png')}}"><br/>
					<span class="span1">{{(@$event['start_date'])?date_format(date_create($event['start_date']),'d, M Y'):''}} - {{(@$event['end_date']) ? date_format(date_create($event['end_date']),'d, M Y') : 'END'}}</span>
				</p>
				<p>
					<img src="{{asset('assets/images/event/Icon_02.png')}}"><br/>
					<span class="span2 text-uppercase">{{(@$event['start_time'])?date_format(date_create($event['start_time']),'H:i'):''}} - {{(@$event['end_time']) ? date_format(date_create($event['end_time']),'H:i') : 'END'}}</span>
				</p>
				<p>
					<img src="{{asset('assets/images/event/Icon_03.png')}}"><br/>
					<span class="span1">{{@$event['take_place']}}</span><br/>
					<span class="span2">{{@$event['address']}}</span>
				</p>
			</div>
		</div>
	</div>
	<div class="container event-detail event-tags">
		<h4>TAGS</h4>
		@if(@$event['tags'])
			<p>
			@foreach($event['tags'] as $key => $value)
				<span class="badge badge-tags">{{@$value['name']}}</span>
			@endforeach
			</p>
		@endif
	</div>
@stop

@section('custom_scripts')

@stop