@extends('layouts.application')

@section('title')
    @lang('general.title.event.listing')
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container-fluid full">
		<div class="jumbotron jumbotron-event">
		</div>
	</div>
	<div class="container">
		<div class="page-detail blue">
			<h3 class="page-title">EKRAF EVENTS IN BANDUNG</h3>
			<p>Proin in tellus sit amet nibh dignissim sagittis. Donec iaculis gravida nulla. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Pellentesque pretium lectus id turpis. Mauris metus.</p>
		</div>
	</div>
	<div class="container-fluid event-list">
		<div class="row load-more-render">
		  	<div class="col-xs-6 col-md-4">
		  		<div class="thumbnail">
			      	<img src="{{asset('assets/images/Image_01-event.png')}}" alt="...">
			      	<div class="thumbnail-detail">
				      	<p>Sabtu, 27 Oktober 2018 12:00</p>
				      	<hr>
				      	<p class="diff">Lorem Ipsum Dolor</p>
				      	<hr>
				      	<p>Gedung A bin, Jl. Soekarno Hatta</p>
				    </div>
			      	<a href="#" class="btn btn-blue-full">see details</a>
			    </div>
		  	</div>
		  	<div class="col-xs-6 col-md-4">
			    <div class="thumbnail">
			      	<img src="{{asset('assets/images/Image_02-event.png')}}" alt="...">
			      	<div class="thumbnail-detail">
				      	<p>Sabtu, 27 Oktober 2018 12:00</p>
				      	<hr>
				      	<p class="diff">Lorem Ipsum Dolor</p>
				      	<hr>
				      	<p>Gedung A bin, Jl. Soekarno Hatta</p>
				    </div>
			      	<a href="#" class="btn btn-blue-full">see details</a>
			    </div>
		  	</div>
		  	<div class="col-xs-6 col-md-4">
			    <div class="thumbnail">
			      	<img src="{{asset('assets/images/Image_03-event.png')}}" alt="...">
			      	<div class="thumbnail-detail">
				      	<p>Sabtu, 27 Oktober 2018 12:00</p>
				      	<hr>
				      	<p class="diff">Lorem Ipsum Dolor</p>
				      	<hr>
				      	<p>Gedung A bin, Jl. Soekarno Hatta</p>
				    </div>
			      	<a href="#" class="btn btn-blue-full">see details</a>
			    </div>
		  	</div>
		</div>
		<a href="javascript:void(0);" class="btn btn-blue-full load-more" style="margin-top:50px;">Load more ...</a>
	</div>

@stop

@section('custom_scripts')
	<script type="text/javascript">
		$('.load-more').on('click',function(){
					// $('.load-more-render').load("{{route('events.index')}}");

			$.ajax({
				url: "{{route('events.index')}}",
				type: 'get',
				data: {},
				success: function(data){
					$('.load-more-render').append(data);
				},
				dataType: 'html'
			});
		});
	</script>
@stop