@if(@$content['data'])
	@foreach($content['data'] as $key => $value)
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
			<a href="{{route((($value['type']=='news')?'news':'events').'.detail',$value['id'])}}" title="{!!$value['title']!!}">
		  		<div class="thumbnail">
			      	<div class="thumbnail-detail">
				      	<p>{!!substr($value['title'],0,30)!!}</p>
				      	<p class="pull-right category">read more</p>
				    </div>
			      	<div class="div-image">
			      		<span class="span-image">
			      			<img src="{!!$value['image']!!}" alt="...">
			      		</span>
			      	</div>
			    </div>
			</a>
	  	</div>
	@endforeach
	<div class="row col-xs-12 pagination-box">
		<ul class="pagination pagination-lg">
		<!-- page = 7 current = 3 -->
		@if(@$content['data'])
			@if((@$content['meta']['current_page'] - 2) > 1)
		    	<li><a href="javascript:void(0);" class="pag-left-pull" data-page="{{@$content['meta']['current_page'] - 3}}" onclick="nextPage(this);"><i class="fa fa-angle-double-left"></i></a></li>
	    	@endif
			@if(@$content['meta']['prev_page_url'])
		    	<li><a href="javascript:void(0);" class="pag-left" data-page="{{@$content['meta']['current_page'] - 1}}" onclick="nextPage(this);"><i class="fa fa-chevron-left"></i></a></li>
		    @endif
		    @for($i=@$content['meta']['current_page']-2;$i<=@$content['meta']['current_page']+2;$i++)
		    	@if(($i >= 1) and ($i <= @$content['meta']['last_page']))
			    	<li class="{{(@$content['meta']['current_page'] == $i) ? 'active' : ''}}"><a href="javascript:void(0);" data-page="{{$i}}" onclick="nextPage(this);">{!!$i!!}</a></li>
			    @endif
		    @endfor
		    @if((@$content['meta']['last_page']) > @$content['meta']['current_page'])
		    	<li><a href="javascript:void(0);" class="pag-right" data-page="{{@$content['meta']['current_page'] + 1}}" onclick="nextPage(this);"><i class="fa fa-chevron-right"></i></a></li>
	    	@endif
	    	@if((@$content['meta']['last_page'] - 2) > @$content['meta']['current_page'])
		    	<li><a href="javascript:void(0);" class="pag-right-pull" data-page="{{@$content['meta']['current_page'] + 3}}" onclick="nextPage(this);"><i class="fa fa-angle-double-right"></i></a></li>
	    	@endif
		@endif
		</ul>
	</div>
@endif
<script type="text/javascript">
	var data = {!!json_encode(@$content['data'])!!};;
	console.log(data);
	if(Array.isArray(data)){
		if(data.length >= 0){
			$('.tenant-found').text('found: '+data.length);
		}
		else{
			$('.tenant-found').text('found: 0');
		}
	}
	else{
		$('.tenant-found').text('found: 0');
	}
</script>