@extends('layouts.application')

@section('title')
    @lang('general.title.home')
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container-fluid tenant-list" id="body">
		<div class="pull-left col-xs-12 col-sm-8 global left-content">
			<div class="bg-layer">
				<div class="blur-layer">
					<a href="javascript:void(0);" class="btn btn-default pull-right global menu-collapser"><i class="fa fa-angle-double-right fa-2x"></i></a>
					<div class="page-detail row">
						<h3 class="page-title text-capitalize">
						@if(@\Request::get('type') == 'news')
							{{@\Request::get('type')}}
						@else
							@lang('general.label.news-event')
						@endif
						</h3>
						<hr>
						<p class="page-desc">@lang('general.desc.home')</p>
					</div>
					<div class="filter-render row">
					@if(@$content['data'])
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
							<a href="{{route('hackathon.index')}}" title="Hackathon 2018">
						  		<div class="thumbnail" style="background-color: #fff;">
							      	<div class="thumbnail-detail">
								      	<p><span class="thumbnail-title">Hackathon 2018</span></p>
								      	<p class="pull-right category">read more</p>
								    </div>
								    <div class="div-image">
							      		<span class="span-image">
							      			<img src="{{asset('assets/images/hackathon/Logo-Bandung-Lautan-Api.png')}}" alt="Hackathon 2018">
							      		</span>
							      	</div>
							    </div>
							</a>
					  	</div>
						@foreach($content['data'] as $key => $value)
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
								<a href="{{route((($value['type']=='news')?'news':'events').'.detail',$value['id'])}}" title="{!!$value['title']!!}">
							  		<div class="thumbnail">
								      	<div class="thumbnail-detail">
									      	<p><span class="thumbnail-title">{!!substr($value['title'],0,30)!!}</span></p>
									      	<p class="pull-right category">read more</p>
									    </div>
									    <div class="div-image">
								      		<span class="span-image">
								      			<img src="{!!$value['image']!!}" alt="...">
								      		</span>
								      	</div>
								    </div>
								</a>
						  	</div>
						@endforeach
			    	@endif
						<div class="row col-xs-12 pagination-box">
							<ul class="pagination pagination-lg">
							<!-- page = 7 current = 3 -->
							@if(@$content['data'])
								@if((@$content['meta']['current_page'] - 2) > 1)
							    	<li><a href="javascript:void(0);" class="pag-left-pull" data-page="{{@$content['meta']['current_page'] - 3}}" onclick="nextPage(this);"><i class="fa fa-angle-double-left"></i></a></li>
						    	@endif
								@if(@$content['meta']['prev_page_url'])
							    	<li><a href="javascript:void(0);" class="pag-left" data-page="{{@$content['meta']['current_page'] - 1}}" onclick="nextPage(this);"><i class="fa fa-chevron-left"></i></a></li>
							    @endif
							    @for($i=@$content['meta']['current_page']-2;$i<=@$content['meta']['current_page']+2;$i++)
							    	@if(($i >= 1) and ($i <= @$content['meta']['last_page']))
								    	<li class="{{(@$content['meta']['current_page'] == $i) ? 'active' : ''}}"><a href="javascript:void(0);" data-page="{{$i}}" onclick="nextPage(this);">{!!$i!!}</a></li>
								    @endif
							    @endfor
							    @if((@$content['meta']['last_page']) > @$content['meta']['current_page'])
							    	<li><a href="javascript:void(0);" class="pag-right" data-page="{{@$content['meta']['current_page'] + 1}}" onclick="nextPage(this);"><i class="fa fa-chevron-right"></i></a></li>
						    	@endif
						    	@if((@$content['meta']['last_page'] - 2) > @$content['meta']['current_page'])
							    	<li><a href="javascript:void(0);" class="pag-right-pull" data-page="{{@$content['meta']['current_page'] + 3}}" onclick="nextPage(this);"><i class="fa fa-angle-double-right"></i></a></li>
						    	@endif
							@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="pull-right col-xs-5 col-sm-4 global right-option">
			<p class="col-xs-12">
				<a href="{{route('home')}}" class="btn btn-sm pull-left" style="color:#fff;text-decoration:none;border-radius:5px;background: #a7a7a7;"><i class="fa fa-circle-o"></i> clear filter</a>
				<span class="tenant-found pull-right" style="color: #000;">{!!(@$content)? '' : 'found: 0'!!}</span>
			</p>
			<div class="search-box">
				<div class="input-group">
				  <input type="text" class="form-control" name="search">
				  <span class="input-group-btn">
					<button class="btn btn-default search-btn" type="button"><span class="sm">@lang('general.label.search')</span><span class="xs"><i class="fa fa-search"></i></span></button>
				  </span>
				</div>
			</div>
			<ul class="parent">
				<li class="radio">
						<label>@lang('general.label.news')<input type="radio" name="type" id="type-news" data-title="news" class="pull-right" value="news"><span class="checkmark"></span></label>
				</li>
			</ul>
			<ul class="parent">
				<a href="javascript:void(0);" class="event-list"><li class="parent-title">@lang('general.label.event.self') <i class="fa fa-chevron-left pull-right"></i> <span id="event-info-filter" class="info-filter"></span></li></a>
				<ul class="child">
					<li class="radio">
						<label>@lang('general.label.event.ongoing')<input type="radio" name="type" data-title="on-going list" class="pull-right" value="ongoing"><span class="checkmark"></span></label>
					</li>
					<li class="radio">
						<label>@lang('general.label.event.upcoming')<input type="radio" name="type" data-title="upcoming list" class="pull-right" value="upcoming"><span class="checkmark"></span></label>
					</li>
					<li class="radio">
						<label>@lang('general.label.event.past')<input type="radio" name="type" data-title="past list" class="pull-right" value="past"><span class="checkmark"></span></label>
					</li>
				</ul>
			</ul>
		</div>
	</div>
@stop

@section('custom_scripts')
	<script type="text/javascript">
	// params using slug , so it is string
		var filter_params = {
			'type': '',
			'page': 1,
			'keyword': ''
		};
		var current_page = 1;

		$(document).ready(function(){
			$('.event-list').click();
			@if(@\Request::get('type') == 'news')
				$('#type-news').attr('checked',true);
			@endif
		});

		$('input[name=type]').click(function(){
			// $('.page-detail p').css('display','none');
			if($(this).val() == 'news'){
				$('.page-title').text('News');
				$('.page-desc').text('{{trans("general.desc.news-event")}}');
			}
			else{
				$('.page-title').text('Events');
				$('.page-desc').text('{{trans("general.desc.news-event")}}');
			}
			filter_params['type'] = $(this).val();
			getHomeList();
			filteredBy(this);
		});

		$('.search-btn').on('click',function(){
			var keyword = $('input[name=search]').val();
			if(!((keyword == null) || (keyword == undefined) || (keyword == ''))){
				current_page = 1;
				filter_params['type'] = '';
				filter_params['page'] = 1;
				filter_params['keyword'] = keyword;
				$('.page-title').text('News & Events');
				$('.page-desc').text('{{trans("general.desc.home")}}');
				getHomeList();
			}
		});

		function nextPage(e){
			var next = $(e).data('page');
			if(current_page != next){
				filter_params['page'] = next;
				getHomeList();
				current_page = next;
			}
		};

		function getHomeList(params=null){
			// if !params {params = {};}
			$.ajax({
				url: "{{route('home')}}",
				type: 'get',
				data: filter_params,
				success: function(data){
					console.log('sukses');
					// console.log(data);
					$('.filter-render').html(data).hide();
					$('.filter-render').fadeIn('5000');
				},
				error: function(e){
					console.log(e);
				},
				dataType: 'html'
			});
		}

		
	</script>
@stop