@extends('layouts.application2')

@section('title')
    @lang('general.title.term-condition')
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container faq">
		<div class="page-detail blue">
			<h3 class="page-title">TERMS AND CONDITIONS @lang('general.label.term')</h3>
			<p>&nbsp;</p>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>1. Introduction</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>
					The Terms and Conditions written on this webpage shall manage your use of Patrakomala
					accessible at patrakomala.disbudpar.bandung.go.id.
					These Terms will be applied fully and affect to your use of Patrakomala. By using Patrakomala,
					you agreed to accept all terms and conditions written in here. You must not use Patrakomala if
					you disagree with any of these Terms and Conditions.
				</p>
			</div>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>2. Intellectual Property Rights</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>
					Other than the content you own, under these Terms, Patrakomala and/or its licensors own all the
					intellectual property rights and materials contained in Patrakomala.
					You are granted limited license only for purposes of viewing the material contained on
					Patrakomala.
				</p>
			</div>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>3. Restrictions</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>
					You are specifically restricted from all of the following:
					<ul>
						<li>publishing any Patrakomala material in any other media;</li>
						<li>selling, sublicensing and/or otherwise commercializing any Patrakomala material;</li>
						<li>publicly performing and/or showing any Patrakomala material;</li>
						<li>using Patrakomala in any way that is or may be damaging to Patrakomala;</li>
						<li>using Patrakomala in any way that impacts user access to Patrakomala;</li>
						<li>using Patrakomala contrary to applicable laws and regulations, or in any way may cause harm to the Website, or to any person or business entity;</li>
						<li>engaging in any data mining, data harvesting, data extracting or any other similar activity in relation to Patrakomala;</li>
						<li>using Patrakomala to engage in any advertising or marketing. Certain areas of Patrakomala are restricted from being access by you and Patrakomala may further restrict access by you to any areas of Patrakomala, at any time, in absolute discretion. Any user ID and password you may have for Patrakomala are confidential and you must maintain confidentiality as well.</li>
					</ul>
				</p>
			</div>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>4. Your Content</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>
					In these Website Standard Terms and Conditions, “Your Content” shall mean any audio, video text,
					images or other material you choose to display on Patrakomala. By displaying Your Content, you
					grant Patrakomala a non-exclusive, worldwide irrevocable, sub licensable license to use,
					reproduce, adapt, publish, translate and distribute it in any and all media.
					Your Content must be your own and must not be invading any third-party’s rights. Patrakomala
					reserves the right to remove any of Your Content from Patrakomala at any time without notice.
				</p>
			</div>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>5. No warranties</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>
					Patrakomala is provided “as is,” with all faults, and Patrakomala express no representations or
					warranties, of any kind related to Patrakomala or the materials contained on Patrakomala. Also,
					nothing contained on Patrakomala shall be interpreted as advising you.

				</p>
			</div>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>6. Limitation of liability</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>
					In no event shall Patrakomala, nor any of its officers, directors and employees, shall be held liable
					for anything arising out of or in any way connected with your use of Patrakomala whether such
					liability is under contract. Patrakomala, including its officers, directors and employees shall not be
					held liable for any indirect, consequential or special liability arising out of or in any way related to
					your use of Patrakomala.
				</p>
			</div>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>7. Indemnification</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>
					You hereby indemnify to the fullest extent Patrakomala from and against any and/or all liabilities,
					costs, demands, causes of action, damages and expenses arising in any way related to your
					breach of any of the provisions of these Terms.

				</p>
			</div>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>8. Severability</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>
					If any provision of these Terms is found to be invalid under any applicable law, such provisions
					shall be deleted without affecting the remaining provisions herein.
				</p>
			</div>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>9. Variation of Terms</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>
					Patrakomala is permitted to revise these Terms at any time as it sees fit, and by using
					Patrakomala you are expected to review these Terms on a regular basis.

				</p>
			</div>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>10. Assignment</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>
					Patrakomala is allowed to assign, transfer, and subcontract its rights and/or obligations under
					these Terms without any notification. However, you are not allowed to assign, transfer, or
					subcontract any of your rights and/or obligations under these Terms.
				</p>
			</div>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>11. Entire Agreement</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>
					These Terms constitute the entire agreement between Patrakomala and you in relation to your use
					of Patrakomala, and supersede all prior agreements and understandings.

				</p>
			</div>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>12. Governing Law & Jurisdiction</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>
					These Terms will be governed by and interpreted in accordance with the laws of the Republic of
					Indonesia, and you submit to the non-exclusive jurisdiction of the state located in the Republic of
					Indonesia for the resolution of any disputes.
				</p>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')
	
@stop