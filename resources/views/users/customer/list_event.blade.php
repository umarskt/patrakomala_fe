@extends('layouts.application2')

@section('title')
    List Event
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container-fluid full">
		<div class="jumbotron jumbotron-profile">
		</div>
	</div>
	<div class="container-fluid my-account customer col-xs-10 col-sm-10 col-md-8 col-md-offset-2 col-sm-offset-1 col-xs-offset-1">
		<div class="btn-setting">
			<a href="{{route('users.account.new-event')}}" title="Create a new Event" class="btn-key">
				<i class="fa fa-plus"></i> New Event
			</a>
		</div>
		<div style="clear:both;"></div>
		<hr><br/>
		@if(is_array(@$events))
			@if(count($events) < 1)
				<h3 class="center">Currently, you have no Events</h3>
			@else		
				<h2 class="left my-req-title">Events #{{count($events)}}</h2>
				<div class="requirement" style="margin-top: 30px;">
					<ul>
						@foreach($events as $key => $value)
							<li class="left">
								<p class="list-header job-item">
									<a href="javascript:void(0);" >
										{{@$value['title']}}<span class="pull-right">status : {{@$value['status']}}</span>
									</a>
								</p>
								<div class="req-detail" style="display: none;">
									<h5 class="left text-capitalize">#{{@$value['type']}}
										<span class="pull-right">Subsektor: list subsektor disini</span>
									</h5>
									<hr>
									<p>
										<label>Tanggal : </label>
										tanggal disini
									</p>
									<p>
										<label>Detail &nbsp;&nbsp;&nbsp;: </label>
										{!!@$value['description']!!}
									</p>
									<p>
										<label>Alamat &nbsp;&nbsp;&nbsp;: </label>
										{!!@$value['take_place']!!}
									</p>
									<p>
										<label>Tags &nbsp;&nbsp;&nbsp;: </label>
										@if(count(@$value['tags']) != 0)
											@php $t = []; @endphp
											@foreach($value['tags'] as $k => $v)
												@php $t[$k] = @$v['name']; @endphp
											@endforeach
											{!!implode(', ',$t)!!}
										@endif
									</p>
									<p>
										<label>Link Terkait &nbsp;&nbsp;&nbsp;: </label>
										{!!(@$value['urls']) ? implode(', ',$value['urls']) : '-'!!}
									</p>
									<p>
										@if(@$value['images'])
										<label>Gambar terkait &nbsp;&nbsp;&nbsp;: </label>
											<div class="col-xs-12" style="height:200px;">
												@foreach($value['images'] as $k => $v)
												<div class="col-sm-3">
													<img src="{{@$v}}" style="width:100%;height:auto;max-height: 180px;">
												</div>
												@endforeach
											</div>
										@endif
									</p>
									<p></p>
									<p>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<hr>
										<label>Waktu Mulai: </label> <strong>{{(@$value['start_date'])?dateTimeIDN($value['start_date']):''}} - {{@$value['start_time']}}</strong> 
									</p>
									<p>
										<label>Waktu Berakhir: </label> <strong>{{(@$value['end_date'])?dateTimeIDN($value['end_date']):''}} - {{@$value['end_time']}}</strong>
									</p>
									<p style="text-align:right;">
										<a href="{{route('users.account.edit-event',$value['id'])}}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
									</p>
								</div>
							</li>
						@endforeach
					</ul>
				</div>
			@endif
				
		@endif
		<br/>
		<hr>
	</div>
@stop

@section('custom_scripts')
	<script type="text/javascript">
		$('.job-item').click(function(){
			
			if($(this).parent().find('.req-detail').is(':visible')){
				$(this).parent().find('.req-detail').slideUp('fast');
			}
			else{
				$(this).parent().find('.req-detail').slideDown('fast');
			}
		});
	</script>
@stop