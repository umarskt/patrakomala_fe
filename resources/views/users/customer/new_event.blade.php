@extends('layouts.application2')

@section('title')
    New Event
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
	<!-- <script src="https://cdn.ckeditor.com/ckeditor5/11.1.1/classic/ckeditor.js"></script> -->
@stop

@section('custom_styles_after')
	<style type="text/css">
		.my-account.customer.new-job .radio label{
			padding: 4px 20px;
		}
		.ck-editor__editable {
		    min-height: 400px;
		}
		#datetimepicker1 .btn{
			color: #337ab7 !important;
		}
		#datetimepicker2 .btn{
			color: #337ab7 !important;
		}
	</style>
@stop

@section('head_scripts')
	<script type="text/javascript" src="{{asset('vendor/moment/moment.js')}}"></script>
	<!-- <script type="text/javascript" src="{{asset('vendor/bootstrap3/js/collapse.js')}}"></script> -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
@stop

@section('content')
	<div class="container-fluid full">
		<div class="jumbotron jumbotron-profile">
		</div>
	</div>
	<div class="container-fluid my-account customer new-job col-xs-10 col-sm-10 col-md-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
		<div class="" style="margin-bottom: 40px;">
			<a href="{{route('users.account.dashboard')}}" class="btn btn-back"></a>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6">
			<h2 class="new-job-title text-uppercase">Create a new event</h2>
			<p></p>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 ">
			<form method="post" action="{{route('users.account.post-event')}}" enctype="multipart/form-data">
				{{csrf_field()}}
					<input type="hidden" name="email" value="{{(@session()->get('user')['email'])?@session()->get('user')['email']:'email_hint'}}">
				<div class="form-group">
					<label for='detail'>Judul Event</label> <span class="required">*</span>
					<input type="text" name="title" class="form-control" placeholder="Judul Event" value="{{Input::old('title')}}">
					<span class="help-block">{{$errors->first('title')}}</span>
				</div>
				<div class="form-group">
					<label for='description'>Deskripsi</label> <span class="required">*</span>
					<textarea class="form-control" id=text placeholder="Deskripsi" name="description" style="margin-top:10px;">{{Input::old('description')}}</textarea>
					<span class="help-block">{{$errors->first('description')}}</span>
				</div>
				
				<div class="form-group">
					<label for='detail'>Tempat Acara</label> <span class="required">*</span>
					<input type="text" name="building" class="form-control" placeholder="Gedung" value="{{Input::old('building')}}">
					<span class="help-block">{{$errors->first('building')}}</span>
					<input type="text" name="address" class="form-control" placeholder="Alamat" value="{{Input::old('address')}}">
					<span class="help-block">{{$errors->first('address')}}</span>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<label for='detail'>Waktu</label> <span class="required">*</span>
					</div>
					<div class="col-xs-6">
		                    <input type='text' class="form-control" id='datetimepicker1' name="start_date"  placeholder="Mulai" value="{{Input::old('start_date')}}" />
						<span class="help-block">{{$errors->first('start_date')}}</span>
					</div>
					<div class="col-xs-6">
		                    <input type='text' class="form-control" id='datetimepicker2' name="end_date"  placeholder="Berakhir" value="{{Input::old('end_date')}}" />
						<span class="help-block">{{$errors->first('end_date')}}</span>
					</div>
				</div>
				<div class="form-group">
					<label for='subsector'>Gambar</label> <span class="required">*</span>
					<div class="image-boxes">	
						<input type="file" name="images[]" class="form-control">
					</div>
					<span class="help-block">{{$errors->first('images')}}</span>
					<button type="button" class="btn btn-default btn-flat btn-add"><i class="fa fa-plus"></i> Tambah</button>
				</div>
				<div class="form-group">
					<label for='subsector'>Tags</label> <span class="required">*</span>
					<p class="select3-m1"></p>
					<select class="select3 form-control" name="tags[]" multiple="multiple">
					</select>
					<span class="help-block">{{$errors->first('tags')}}</span>
				</div>
				<div>
					<label for='subsector'>Link terkait</label> <span class="required">*</span>
					<p class="select3-m1"></p>
					<select class="select3 form-control" name="links[]" multiple="multiple">
					</select>
					<span class="help-block">{{$errors->first('links')}}</span>
				</div>
				<div class="form-group">
					<a href="{{route('users.account.dashboard')}}" class="btn btn2 btn-grey pull-left">Cancel</a>
					<input type="submit" name="new-job-sbm" value="Create Event" class="btn btn2 btn-orange pull-right">
				</div>
			</form>
		</div>
	</div>
@stop

@section('custom_scripts')
	<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
	<script src="//cdn.ckeditor.com/4.10.1/standard/ckeditor.js"></script>
	
	<script type="text/javascript">
		$('.select2').select2();
		$('.select3').select2({
			tags: true,
    		tokenSeparators: [','],
			createTag: function (params) {
				var term = $.trim(params.term);

				if (term === '') {
				  return null;
				}

				return {
				  id: term,
				  text: term,
				  newTag: true // add additional parameters
				}
			},
			language: {
			    noResults: function (params) {
			      return "";
			    }
			}
		});
    	CKEDITOR.replace( 'text', {
    		// extraPlugins: 'uploadimage',
    		// filebrowserUploadMethod: 'form',
    		// filebrowserBrowseUrl : '{{route("users.account.get-picture")}}?type=Images',
        	// filebrowserUploadUrl : '{{route("users.account.upload-picture")}}?command=QuickUpload&_token={{csrf_token()}}&type=Files&responseType=json'
    	} );
    	$(function () {
    		var todayDate = new Date().getDate();
            $('#datetimepicker1').datetimepicker({
				// format:'m/d/Y',
				minDate: new Date(),
				// maxDate: new Date(new Date().setDate(todayDate + 30))
			});
            $('#datetimepicker2').datetimepicker({
				// format:'m/d/Y',
				minDate: new Date(),
				// maxDate: new Date(new Date().setDate(todayDate + 30))
			});
        });

        $('.btn-add').on('click',function(){
        	$('.image-boxes').append('<input type="file" name="images[]" class="form-control" style="margin-top:14px;">');
        });
	</script>
@stop