@extends('layouts.application2')

@section('title')
    New Job
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />
	
@stop

@section('custom_styles_after')
	<style type="text/css">
		.my-account.customer.new-job .radio label{
			padding: 4px 20px;
		}
	</style>
@stop

@section('content')
	<div class="container-fluid full">
		<div class="jumbotron jumbotron-profile">
		</div>
	</div>
	<div class="container-fluid my-account customer new-job col-xs-10 col-sm-10 col-md-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
		<div class="" style="margin-bottom: 40px;">
			<a href="{{route('users.account.dashboard')}}" class="btn btn-back"></a>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6">
			<h2 class="new-job-title text-uppercase">Create a new job</h2>
			<p><!-- Lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem --></p>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 ">
			<form method="post" action="{{route('users.account.post-job')}}">
				{{csrf_field()}}
					<input type="hidden" name="email" value="{{(@session()->get('user')['email'])?@session()->get('user')['email']:'email_hint'}}">
				<div class="form-group">
					<label for='detail'>What is i need</label> <span class="required">*</span>
					<input type="text" name="title" class="form-control" placeholder="Subject" value="{{Input::old('title')}}">
					<span class="help-block">{{$errors->first('title')}}</span>
					<textarea class="form-control" placeholder="Detail" name="description" style="margin-top:10px;">{{Input::old('description')}}</textarea>
					<span class="help-block">{{$errors->first('description')}}</span>
				</div>
				<div class="form-group">
					<label for='product_type'>I am enquiring about (select an option)</label> <span class="required">*</span>
					<div class="col-xs-6 radio">
						<label class="radio-inline">
							PRODUCT <input type="radio" name="type" value='product' {{(Input::old('type')=='product')?'checked':''}}>
							<span class="checkmark"></span>
						</label>
					</div>
					<div class="col-xs-6 radio">
						<label class="radio-inline">
							SERVICE <input type="radio" name="type" value='service' {{(Input::old('type')=='service')?'checked':''}}>
							<span class="checkmark"></span>
						</label>
					</div>
					<span class="help-block">{{$errors->first('type')}}</span>
				</div>
				<div class="form-group">
					<label for='subsector'>Subsektor</label> <span class="required">*</span>
					<select name="sub_sectors[]" class="form-control select2" multiple="true">
						@if(@$subsectors)
							@foreach($subsectors as $key => $value)
								<option value="{{@$value['id']}}" {{(Input::old('product_type')==@$value['id'])?'selected':''}}>{{@$value['sub_sector_name']}}</option>
							@endforeach
						@endif
					</select>
					<span class="help-block">{{$errors->first('subsector')}}</span>
				</div>
				<div class="form-group">
					<label for='estimated_cost'>Estimated cost required</label> <span class="required">*</span>
					<input type="hidden" name="estimated_cost" value="{{Input::old('estimated_cost')}}">
					<div class="input-group">
						<span class="input-group-addon">Rp</span>
						<input type="text" name="estimated_costT" class="form-control number-dec" value="{{Input::old('estimated_costT')}}">
					</div>
					<span class="help-block">{{$errors->first('estimated_cost')}}</span>
				</div>
				<div class="form-group">
					<a href="{{route('users.account.dashboard')}}" class="btn btn2 btn-grey pull-left">Cancel</a>
					<input type="submit" name="new-job-sbm" value="Create Job" class="btn btn2 btn-orange pull-right">
				</div>
			</form>
		</div>
	</div>
@stop

@section('custom_scripts')
	<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
	<script type="text/javascript">
		$('.select2').select2();

		$('input[name=estimated_costT]').keyup(function(){
			$('input[name=estimated_cost]').val(fromMoneyIDR($(this)));
			$(this).val(toMoneyIDR($(this),$('input[name=estimated_cost]')));
		});
	</script>
@stop