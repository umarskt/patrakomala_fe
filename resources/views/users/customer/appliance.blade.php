@extends('layouts.application2')

@section('title')
    Applicant
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />

@stop

@section('content')
	<div class="container-fluid full">
		<div class="jumbotron jumbotron-profile">
		</div>
	</div>
	<div class="container-fluid my-account customer appliance col-xs-10 col-sm-10 col-md-10 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
		<h2>THE COMPANY WHO SUITS FOR YOU</h2>
		<div class="col-xs-12 row tenant-list">
			<h4>* select to see your company profile</h4>
			<div class="col-xs-6 col-sm-4 tenant">
				<a href="#">
					<img src="{{asset('assets/images/tenant/P.png')}}">
				</a>
			</div>
			<div class="col-xs-6 col-sm-4 tenant">
				<a href="#">
					<img src="{{asset('assets/images/tenant/ADIDAS.png')}}">
				</a>
			</div>
			<div class="col-xs-6 col-sm-4 tenant">
				<a href="#">
					<img src="{{asset('assets/images/tenant/GOJEK.png')}}">
				</a>
			</div>
		</div>
		<div class="col-xs-12 center">
			<a href="#" class="btn btn-large btn-orange">CHOOSE AND CONTACT THE COMPANY</a>
		</div>
		<div class="col-xs-12">
			<h4 class="center">OR</h4>
		</div>
		<div class="col-xs-12 center">
			<a href="#" class="btn btn-large btn-orange">WAIT FOR COMPANY TO CONTACT YOU</a>
		</div>
	</div>
@stop

@section('custom_scripts')
	<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
	<script type="text/javascript">
		$('.select2').select2();
	</script>
@stop