@extends('layouts.application2')

@section('title')
    Dashboard
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container-fluid full">
		<div class="jumbotron jumbotron-profile">
		</div>
	</div>
	<div class="container-fluid my-account customer col-xs-10 col-sm-10 col-md-8 col-md-offset-2 col-sm-offset-1 col-xs-offset-1">
		<div class="btn-setting">
			<a data-toggle="modal" data-target="#generate-key" title="List Api Key" class="btn-key">
				<i class="fa fa-key"></i>
			</a>
			<a href="{{route('users.account.list-event')}}" title="List Event" class="btn-key">
				<i class="fa fa-calendar"></i>
			</a>
			<a href="{{route('users.account.new-job')}}" title="Create new job" class="btn-key">
				<i class="fa fa-plus"></i>
			</a>
			<a href="{{route('users.account.setting')}}" title="Update your profile data" class="btn-key">
				<i class="fa fa-gear"></i>
			</a>
			<a href="{{route('users.sign_out')}}" title="Logout here" class="btn-key">
				<i class="fa fa-power-off"></i>
			</a>
		</div>
		<div style="clear:both;"></div>
		<div class="welcome-user">
			<p class="left">
				Welcome, <strong>{{@session()->get('user')['first_name']}} {{@session()->get('user')['last_name']}}</strong><br><br>
				<small>This is the list job you have created<br>
				Please change status to 'closed' if you already get the company for your requirements</small>
			</p>
		</div>
		<div style="clear:both;"></div>
		<hr><br/>
		@if(is_array(@$jobs))
			@if(count($jobs) < 1)
				<h3 class="center">Currently, you have no requirements</h3>
			@else		
				<h2 class="left my-req-title">MY REQUIREMENTS #{{count($jobs)}}</h2>
				<div class="requirement" style="margin-top: 30px;">
					<ul>
						@foreach($jobs as $key => $value)
							<li class="left">
								<p class="list-header job-item">
									<a href="javascript:void(0);" >
										{{@$value['title']}}<span class="pull-right">status : {{@$value['status']}}</span>
									</a>
								</p>
								<div class="req-detail" style="display: none;">
									<h5 class="left text-capitalize">#{{@$value['type']}}
										<span class="pull-right">Subsektor: list subsektor disini</span>
									</h5>
									<hr>
									<p>
										<label>Tanggal : </label>
										tanggal disini
									</p>
									<p>
										<label>Detail &nbsp;&nbsp;&nbsp;: </label>
										{{@$value['description']}}
									</p>
									<hr>
									<p>
										<label>Estimated cost required: </label> <strong>{{(@$value['estimated_cost'])?currency($value['estimated_cost']):'0'}}</strong> Rupiah
									</p>
								</div>
							</li>
						@endforeach
					</ul>
				</div>
				<div style="clear:both;"></div><br/><br/>
				<div class="notes left">
					<small>* The closed requirements will be lost</small><br/>
					<small>* Your requirements will be sent to relevant company</small><br/>
					<small>* You can back to this thread if follow</small>
				</div>
			@endif
				
		@endif
		<br/>
		<hr>
	</div>
	@if(@session()->get('user')['user_type']=='client')
		<div id="generate-key" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
						<h4 class="modal-title">List of Generated Key</h4>
					</div>
					<div class="modal-body">
						<form action="{{route('users.generate-key.post')}}" method='post'>
							{{csrf_field()}}
							<button type="submit" class="btn btn-info"><i class="fa fa-refresh"></i> Generate Key</button>
						</form>
						<br/>
						@if(session()->has('generated-key'))
							<p style="font-size: 20px;font-family: 'Poppins Light';color:#000;font-weight: bold;">Generated Key : <span style="font-weight: normal;font-style: italic;">{{session()->get('generated-key')}}</span></p>
						@endif
					</div>
				</div>
			</div>
		</div>
	@endif
	<div id="modal-claim" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
					<h4 class="modal-title">Claim Business</h4>
				</div>
				<div class="modal-body">
					<p>Claim this business ({{@$claim['tenant_name']}})?</p>
				</div>
				<div class="modal-footer">
					<div class="form-group">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
						<form method="post" action="{{route('users.claim.post')}}"> 
							{{csrf_field()}}
							<input type="hidden" name="tenant_id" value="{{@$claim['tenant_id']}}">
							<input type="submit" name="claim-sbm" value="Yes" class="btn btn-orange pull-right">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')
	<script type="text/javascript">
		@if(@$claim)
			$('#modal-claim').modal('show');
		@endif
		$('.job-item').click(function(){
			
			if($(this).parent().find('.req-detail').is(':visible')){
				$(this).parent().find('.req-detail').slideUp('fast');
			}
			else{
				$(this).parent().find('.req-detail').slideDown('fast');
			}
		});
		@if(session()->has('generated-key'))
			// $('#generate-key').modal('show');
		@endif
	</script>
@stop