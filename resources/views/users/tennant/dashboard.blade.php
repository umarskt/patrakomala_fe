@extends('layouts.application2')

@section('title')
    Dashboard
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_map1.css')}}">
	<style type="text/css">
		#clear{
			display:none;
		}
		output.result div .thumbnail{
			max-height: 125px;
			width:auto;
			float:left;
			margin: 10px;
		}
		.result {
			/*border: 3px dotted #cccccc;*/
			/*display: none;*/
			float: right;
			margin:0 auto;
			width: 100%;
			min-height: 170px;
		}
		#map{
			height: 300px !important;
		}
		output.result {
		    overflow: auto;
		    white-space: nowrap;
		}

		output.result div {
		    display: inline-block;
		}
		output.result .clear {
		    margin-left: -18px;
		    color: #333;
		}
		output.result .clear .fa-remove{
		    color: #333;
		    font-size: 18px;
		    padding-top: 1.5px;
		}
		.pac-container { z-index: 100000 !important; }
	</style>
@stop

@section('head_scripts')
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCEKkG9jlRbavkuE6unDPFGjZ6Ur5cYjHM&libraries=geometry,places"></script>
	@include('users.tennant.scripts._location')
@endsection

@section('content')
	<div class="container-fluid full">
		<div class="jumbotron jumbotron-profile">
		</div>
	</div>
	<div class="container-fluid prev-log col-xs-10 col-sm-10 col-md-8 col-md-offset-2 col-sm-offset-1 col-xs-offset-1">
		<div class="pull-right">
			<a href="{{route('users.account.preview-page')}}" class="btn btn-preview btn-orange" target=
			'_blank'>Preview your page</a>
			<a href="{{route('users.sign_out')}}" class="btn btn-logout btn-orange">Logout</a>
		</div>
	</div>
	<div class="container-fluid my-account col-xs-10 col-sm-10 col-md-8 col-md-offset-2 col-sm-offset-1 col-xs-offset-1">
		<div class="btn-setting">
			<a href="{{route('users.account.jobs')}}">
				<img src="{{asset('assets/images/profile/Button_new_mail.png')}}">
			</a>
			<a href="#" data-toggle='modal' data-target="#share-social" >
				<img src="{{asset('assets/images/profile/Button_share_page.png')}}">
			</a>
			<a href="#" data-toggle='modal' data-target="#update-location" class="update-location">
				<img src="{{asset('assets/images/profile/Button_location.png')}}">
			</a>
			<a href="{{route('users.account.setting')}}">
				<img src="{{asset('assets/images/profile/Button_setting.png')}}">
			</a>
		</div>
		<br/>
		<br/>
		<br/>
		<div class="header-profile">
			<div class="col-sm-4 pp">
				<a href="#" data-toggle="modal" data-target="#update-logo">
					@if(@$profile['tenant_logo'])
						<img src="{{$profile['tenant_logo']}}" alt="tenant logo" title="update tenant logo">
					@else
						<img src="{{asset('assets/images/camera.png')}}" alt="update tenant logo" title="update tenant logo">
					@endif
				</a>
			</div>
			<div class="col-sm-8 detail">
				<p class="greet">Welcome,</p>
				<p class="pro-name text-uppercase">{{@session()->get('user')['tenant_name']}}</p>
				<p class="contact">{{@session()->get('user')['tenant_email']}}<br/>{{@session()->get('user')['tenant_phone']}}</p>
				<p>
				@if(is_array(@$profile['social_sites']))
					@foreach($profile['social_sites'] as $key => $value)
						@if(strtolower(@$value['social_site_type']) == 'twitter')
						<a href="{{@$value['social_site_url']}}" target=_blank>
							<img src="{{asset('assets/images/profile/sosmed_04.png')}}">
						</a>
						@endif
						@if(strtolower(@$value['social_site_type']) == 'facebook')
						<a href="{{@$value['social_site_url']}}" target=_blank>
							<img src="{{asset('assets/images/profile/sosmed_02.png')}}">
						</a>
						@endif
						@if(strtolower(@$value['social_site_type']) == 'behance')
						<a href="{{@$value['social_site_url']}}" target=_blank>
							<img src="{{asset('assets/images/profile/sosmed_03.png')}}">
						</a>
						@endif
						@if(strtolower(@$value['social_site_type']) == 'whatsapp')
						<a href="{{@$value['social_site_url']}}" target=_blank>
							<img src="{{asset('assets/images/profile/sosmed_01.png')}}">
						</a>
						@endif
						@if(strtolower(@$value['social_site_type']) == 'google')
						<a href="{{@$value['social_site_url']}}" target=_blank>
							<img src="{{asset('assets/images/profile/sosmed_05.png')}}">
						</a>
						@endif
						@if(strtolower(@$value['social_site_type']) == 'instagram')
						<a href="{{@$value['social_site_url']}}" target=_blank>
							<img src="{{asset('assets/images/profile/sosmed_06.png')}}">
						</a>
						@endif
						
					@endforeach
				@endif
					<a href="#" class="link-plus" data-toggle="modal" data-target="#update-social">
						<img src="{{asset('assets/images/profile/Button_plus_sosmed.png')}}">
					</a>
				</p>
			</div>
		</div>
		<div style="clear:both;"></div>
		<div class="update-form">
			<form class="form-horizontal" method="post" action="{{route('users.account.update-tenant')}}">
				{{csrf_field()}}
				<h4 class="pull-left">Tell about your company background</h4>
				<input type="submit" value="Update" class="btn btn-orange btn-update pull-right">
				<textarea class="form-control" name="tenant_profile">{!!@$profile['tenant_profile']!!}</textarea>
			</form>
		</div>
		<div style="clear:both;"></div>
		<div class="update-form">
			<form class="form-horizontal" method="post" action="{{route('users.account.update-tenant')}}">
				{{csrf_field()}}
				<h4 class="pull-left">Why people must work with you</h4>
				<input type="submit" value="Update" class="btn btn-orange btn-update pull-right">
				<textarea class="form-control" name="tenant_intro">{!!@$profile['tenant_intro']!!}</textarea>
			</form>
		</div>
		<div style="clear:both;"></div>
		<div class="update-form">
			<form class="form-horizontal" method="post" action="{{route('users.account.update-tenant')}}">
				{{csrf_field()}}
				<h4 class="pull-left">Tell about your company mission</h4>
				<input type="submit" value="Update" class="btn btn-orange btn-update pull-right">
				<textarea class="form-control" name="tenant_mission">{!!@$profile['tenant_mission']!!}</textarea>
			</form>
		</div>
		<div style="clear:both;"></div>
		<div class="update-form">
			<form class="form-horizontal" method="post" action="{{route('users.account.update-tenant')}}">
				{{csrf_field()}}
				<h4 class="pull-left">What your work</h4>
				<input type="submit" value="Update" class="btn btn-orange btn-update pull-right">
				<select class="form-control select3" multiple="true" name="tenant_service[]" id="tenant_service">
					@if(is_array(@$profile['tenant_services']))
						@foreach(@$profile['tenant_services'] as $key => $value)
							<option value="{{@$value['title']}}" selected>{{@$value['title']}}</option>
						@endforeach
					@endif
				</select>
				<p class="select3-m1"></p>
			</form>
		</div>
		<div style="clear:both;"></div>
		<hr style="border-bottom: 1.3px solid #666;margin-top: 50px;" />
		<div class="update-form">
			<form class="" method="post" action="{{route('users.account.update-tenant')}}" enctype="multipart/form-data">
				{{csrf_field()}}
				<h4 class="pull-left">Upload your clients' logo</h4>
				<input type="submit" value="Update" class="btn btn-orange btn-update pull-right">
				<div style="clear:both;"></div>
				<div class="box-upload-logo box-logo col-xs-12">
					@if(is_array(@$profile['clients']))
						@foreach($profile['clients'] as $key=>$value)
							<div class="row">
								<div class="form-group col-sm-6">
									<label for='client_name'>Nama Perusahaan</label>
									<input type="text" name="client_name[]" class="form-control" value="{{@$value['name_client']}}" multiple="true">
								</div>
								<div class="form-group col-sm-6">
									<img src="{{@$value['img_icon']}}" alt="{{@$value['name_client']}}" title="{{@$value['name_client']}}" style="height:120px;width:auto;">
									<input type="hidden" id="reclient_logo_{{@$value['id']}}" name="reclient_img_logo[]"  multiple="true" value="{{@$value['img_icon']}}"  />
									<input type="file" id="client_logo_{{@$value['id']}}" name="client_img_logo[]" onchange="removeReclient(this);"  multiple="true"/>
								</div>
							</div>
						@endforeach
					@endif
				</div>
				<div class="add-more pull-right">
					<span>add/remove client </span>
					<a href="javascript:void(0)" class="remove-client"><i class="fa fa-minus-circle fa-2x"></i></a>
					<a href="javascript:void(0);" class="add-client"><i class="fa fa-plus-circle fa-2x"></i></a>
				</div>
			</form>
		</div>
		<div style="clear:both;"></div>
		<hr style="border-bottom: 1.3px solid #666;margin-top: 50px;" />
		<div class="update-form" style="margin-top:100px;">
			<form class="" method="post" action="{{route('users.account.update-tenant')}}" enctype="multipart/form-data">
				{{csrf_field()}}
				<h4 class="pull-left">Upload Company Portfolio</h4>
				<input type="submit" value="Update" class="btn btn-orange btn-update pull-right">
				<div style="clear:both;"></div>
				<div class="">
				@if(is_array(@$profile['portofolios']))
					<div class="col-xs-12" style='margin-top:10px;margin-bottom:10px;'>
						@foreach($profile['portofolios'] as $key => $value)
							<div class="col-xs-4 col-sm-3 col-md-2" style="margin-right:10px;">
							@if(@$value['catalog'])
								<a href='javascript:void(0);' onclick='remPort(this);' data-key='{{$key}}'><i class='fa fa-remove pull-right'></i></a>
								<a href="{{$value['catalog']}}" target=_blank title="{{@$value['name']}}">
									<img src="{{asset('assets/images/tenant/PDF DOCUMENT.png')}}">
									<input type="hidden" id="company_portfolio" name="reportofolios[]" value="{{$value['catalog']}}" multiple="true" />
								</a>
							@endif
							</div>
						@endforeach
					</div>
				@endif
					<div class="box-upload-logo box-portfolio col-xs-12">
						<div class="row">
							<div class="form-group col-sm-6">
								<label for='client_logo'>Portfolio Name</label>
								<input type="text" id="portfolio_name" name="portfolio_name[]" multiple="true" />
							</div>
							<div class="form-group col-sm-6">
								<label for='client_logo'>Portfolio File</label>
								<input type="file" id="company_portfolio" name="portofolios[]" multiple="true" />
							</div>
						</div>
					</div>
				</div>
				<div class="add-more pull-right">
					<span>add/remove portfolio </span>
					<!-- <a href="javascript:void(0)" class="remove-portfolio"><i class="fa fa-minus-circle fa-2x"></i></a> -->
					<a href="javascript:void(0);" class="add-portfolio"><i class="fa fa-plus-circle fa-2x"></i></a>
				</div>
			</form>
		</div>
		<div style="clear:both;"></div>
		<hr style="border-bottom: 1.3px solid #666;margin-top: 50px;" />
		<div class="update-form edit-gallery">
			<h4 class="pull-left">Edit your galleries</h4>
			<div class="gallery-box">
			@if(@$profile['albums'])
				@foreach($profile['albums'] as $key => $data)
					<a href="#" class='btn btn-edit-gallery' data-target='#gallery{{$data["id"]}}' data-toggle='modal'>{{@$data['album_name']}}</a>
					@include('users.tennant.modals._gallery',['data'=>$data])
				@endforeach
			@endif
			</div>
			<div class="add-more pull-right">
				<span>add more gallery </span>
				<!-- <a href="javascript:void(0)" class="remove-gallery"><i class="fa fa-minus-circle fa-2x"></i></a> -->
				<a href="javascript:void(0);" class="add-gallery"><i class="fa fa-plus-circle fa-2x"></i></a>
				@include('users.tennant.modals._gallery',['data'=>null])
			</div>
		</div>
	</div>
	@include('users.tennant.modals._location')
	@include('users.tennant.modals._logo_tenant')
	@include('users.tennant.modals._social')
	@include('users.tennant.modals._work')
	@include('users.tennant.modals._share')
@stop

@section('custom_scripts')
	<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
	@include('users.tennant.scripts._general_script')
	<script type="text/javascript">
		var selected_services = [];

		$('.select2').select2();
		$('.select3').select2({
			tags: true,
    		tokenSeparators: [','],
			createTag: function (params) {
				var term = $.trim(params.term);

				if (term === '') {
				  return null;
				}

				return {
				  id: term,
				  text: term,
				  newTag: true // add additional parameters
				}
			},
			language: {
			    noResults: function (params) {
			      return "";
			    }
			}
		});
		var logo_files = [];
		window.onload = function(){

			if(window.File && window.FileList && window.FileReader){
				// $('.galleries').on("change", function(event) {
					// console.log('$(this)');
					// console.log($(this));
					// prevImage(this,event,'result');
					// $('.thegalist').append('<input type="file" name="gallery_image[]" multiple="true" />');
				// });
				
			}
			else{
				console.log("Your browser does not support File API");
			}
		}

		$('#files').on("click", function() {
			// $('.thumbnail').parent().remove();
			// $('result').hide();
			$(this).val("");
		});

		$('.add-gallery').click(function(){
			console.log($('.gallery-box').children('.btn-edit-gallery').length);
			if($('.gallery-box').children('.btn-edit-gallery').length < 3){
				$('#add-gallery').modal('show');
			}
		});

		function onChangeGalleries(e,event){
			console.log('$(e)');
			console.log($(e));
			prevImage(e,event,'result');
		}

		function removeReclient(e){
			$(e).parent().find('input[type=hidden]').val('');
		}
		
	</script>
@stop