<script type="text/javascript">
	function readURL(input) {
		console.log(input);
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				console.log(e);
			  $('.logo_prev').html('<img src="'+e.target.result+'" style="height: 100px;width:auto;top:0;bottom:0;left:0;right:0;margin:auto;position:absolute;" alt="preview logo" />');
			}
			reader.readAsDataURL(input.files[0]);
		}
		$('.logo_prev').show();
	}

	$("#logo_file").change(function() {
		readURL(this);
	});

	function uploadButton2(id){
		$('#'+id).click();

	}
	function uploadButton(e,gal){
		let counter = parseInt($(e).data('counter'));
		let len = parseInt($(gal+counter).data('length'));
		let id = parseInt($(gal+counter).data('id'));
		console.log($(gal+counter));
		$(gal+counter).click();
		$(e).data('counter',(counter+1));
		$('.thegalist'+id).append('<input type="file" id="galleries'+(counter+1)+'" onchange="onChangeGalleries(this,event);" class="galleries" name="gallery_image[]" data-id="'+id+'" multiple="true"  data-length="'+len+'" />');
	}

	function changeBackgroundLogo(color){
		$('#bg_color_input').val(color);
		$('.logo_prev').css('background',color);
	}

	function clearThis(e) {
		$('.thumbnail'+$(e).data('key')).parent().empty();
		// $('#result').hide();
		$('#files').val("");
		$(e).hide();
	}

	$('.add-client').click(function(){
		addBox('.box-logo','client');
	});
	$('.remove-client').click(function(){
		removeBox('.box-logo');
	});

	$('.add-social').click(function(){
		addBox('.social-box','social');
	});
	$('.remove-social').click(function(){
		removeBox('.social-box');
	});

	$('.add-portfolio').click(function(){
		addBox('.box-portfolio','portfolio');
	});
	$('.remove-portfolio').click(function(){
		removeBox('.box-portfolio');
	});

	function addBox(tbox,tpartial){
		$.ajax({
			url: "{{route('users.account.get-partial')}}",
			method: 'get',
			data: {
				partial: tpartial
			},
			dataType: 'html',
			success: function(html){
				$(tbox).append(html);
			},
			error: function(){
				console.log('add box error');
			}
		});
	}

	function removeBox(tbox){
		if($(tbox).children().length > 1){
			$(tbox).children().last().detach();
		}
	}

	function prevImage(e,event,target){
		var logo_files = [];
		var files = event.target.files; //FileList object
		var output = document.getElementById(target+$(e).data('id'));
		for(var i = 0; i< files.length; i++){
			var file = files[i];
			if(file.type.match('image.*')){
				if(e.files[0].size < 2097152){
					var picReader = new FileReader();
					picReader.addEventListener("load",function(event){
						var picFile = event.target;
						logo_files.push(picFile);
						var div = document.createElement("div");
						div.innerHTML = "<a href='javascript:void(0);' onclick='clearThis(this);' data-key='"+(logo_files.length-1)+"'><i class='fa fa-remove'></i></a><img class='thumbnail thumbnail"+(logo_files.length-1)+"' src='" + picFile.result + "'" +"title='preview image'/>";
						output.insertBefore(div,null);
					});
				$('#clear, #'+target+$(e).data('id')).show();
				picReader.readAsDataURL(file);
				}
				else{
					alert("Image Size is too big. Minimum size is 2MB.");
					$(e).val("");
				}
			}else{
				alert("You can only upload image file.");
				$(e).val("");
			}
		}
	}

	function remPort(e) {
		$(e).parent().remove();
		// $('#result').hide();
		// $('#files').val("");
		// $(e).hide();
	}
</script>