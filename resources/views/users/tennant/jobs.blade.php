@extends('layouts.application2')

@section('title')
    Job
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />
	<style type="text/css">
		#clear{
			display:none;
		}
		.thumbnail{
			max-height: 250px;
			width:auto;
			float:left;
			margin: 10px;
		}
		#result {
			/*border: 3px dotted #cccccc;*/
			display: none;
			float: right;
			margin:0 auto;
			width: 100%;
			min-height: 200px;
		}
		.left{
			text-align: left !important;
		}
	</style>
@stop

@section('content')
	<div class="container-fluid full">
		<div class="jumbotron jumbotron-profile">
		</div>
	</div>
	<div class="container-fluid my-account col-xs-10 col-sm-10 col-md-8 col-md-offset-2 col-sm-offset-1 col-xs-offset-1">
		<div class="pull-left">
			<a href="{{route('users.account.dashboard')}}" class="btn btn-back"></a>
		</div>
		<div class="pull-right btn-setting">
			<a href="" title="refresh list">
				<img src="{{asset('assets/images/profile/button_refresh.png')}}">
			</a>
			<!-- <a href="#">
				<img src="{{asset('assets/images/profile/button_search.png')}}">
			</a>
			<a href="#">
				<img src="{{asset('assets/images/profile/button_trash.png')}}">
			</a> -->
		</div>
		<div style="clear:both;"></div>
		@if(is_array(@$jobs))
			@if(count($jobs) < 1)
				<h3 class="center">No job found</h3>
			@endif			
			<h2 class="left my-req-title" style="margin-top: 50px;">MY REQUIREMENTS #{{count($jobs)}}</h2>
			<div class="requirement" style="margin-top: 30px;">
				<ul>
					@foreach($jobs as $key => $value)
						<li class="left">
							<p class="list-header job-item">
								<a href="javascript:void(0);" >
									{{@$value['title']}}<span class="pull-right">status : {{@$value['status']}}</span>
								</a>
							</p>
							<div class="req-detail" style="display: none;">
								<h5 class="left text-capitalize">#{{@$value['type']}}
									<span class="pull-right">Subsektor: list subsektor disini</span>
								</h5>
								<hr>
								<p>
									<label>Tanggal : </label>
									tanggal disini
								</p>
								<p>
									<label>Detail &nbsp;&nbsp;&nbsp;: </label>
									{{@$value['description']}}
								</p>
								<hr>
								<p>
									<label>Estimated cost required: </label> <strong>{{(@$value['estimated_cost'])?currency($value['estimated_cost']):'0'}}</strong> Rupiah
								</p>
							</div>
						</li>
					@endforeach
				</ul>
			</div>
				
			<div style="clear:both;"></div><br/><br/>
			<div class="notes left">
				<small>* The closed requirements will be lost</small><br/>
				<small>* Your requirements will be sent to relevant company</small><br/>
				<small>* You can back to this thread if follow</small>
			</div>
		@endif
	</div>
@stop

@section('custom_scripts')
	<script type="text/javascript">
		$('.job-item').click(function(){
			
			if($(this).parent().find('.req-detail').is(':visible')){
				$(this).parent().find('.req-detail').slideUp('fast');
			}
			else{
				$(this).parent().find('.req-detail').slideDown('fast');
			}
		});
	</script>
@stop