<div id="update-location" class="modal fade my-account tenant" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title">Edit Company Location</h4>
			</div>
			<form method="post" action="{{route('users.account.update-tenant')}}">
				{{csrf_field()}}
				<div class="modal-body">
					<div class="form-group">
						<input type="text" name="location" id='address_street' class="form-control" placeholder="Search your location">
					</div>
					<div id="map" class="map"></div>
					<div class="col-sm-12">
						<span id="current"></span>
					</div>
					<div class="col-sm-6">
						<input type='text' name='address_latitude' class="form-control" id="address_latitude" readonly="true" placeholder="Latitude" style="color:#000;" value="{{(@$profile['latitude'])?$profile['latitude']:''}}"/>
						<span class="help-block">{{@$errors->first('address_latitude')}}</span>
					</div>
					<div class="col-sm-6">
						<input type='text' name='address_longitude' class="form-control" id="address_longitude" readonly="true" placeholder="Longitude" style="color:#000;" value="{{(@$profile['longitude'])?$profile['longitude']:''}}"/>
						<span class="help-block">{{@$errors->first('address_longitude')}}</span>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group right">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<input type="submit" name="location-sbm" value="Save" class="btn btn-orange">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('input[name=location-sbm]').click(function(evt){
		if(!confirm('Yakin akan mengubah lokasi?')){
			evt.preventDefault();
		}
	});
</script>