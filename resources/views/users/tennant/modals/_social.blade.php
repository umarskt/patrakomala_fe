<div id="update-social" class="modal fade my-account tenant" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title">Update Social</h4>
			</div>
			<form method="post" action="{{route('users.account.update-tenant')}}">
				{{csrf_field()}}
				<div class="modal-body">
					<div class="form-group center" style="margin:30px 0;">
						@if(is_array(@$profile['social_sites']))
							@foreach($profile['social_sites'] as $key => $value)
								@if(strtolower(@$value['social_site_type']) == 'twitter')
								<a href="{{@$value['social_site_url']}}" target=_blank>
									<img src="{{asset('assets/images/profile/sosmed_04.png')}}">
								</a>
								@endif
								@if(strtolower(@$value['social_site_type']) == 'facebook')
								<a href="{{@$value['social_site_url']}}" target=_blank>
									<img src="{{asset('assets/images/profile/sosmed_02.png')}}">
								</a>
								@endif
								@if(strtolower(@$value['social_site_type']) == 'behance')
								<a href="{{@$value['social_site_url']}}" target=_blank>
									<img src="{{asset('assets/images/profile/sosmed_03.png')}}">
								</a>
								@endif
								@if(strtolower(@$value['social_site_type']) == 'whatsapp')
								<a href="{{@$value['social_site_url']}}" target=_blank>
									<img src="{{asset('assets/images/profile/sosmed_01.png')}}">
								</a>
								@endif
								@if(strtolower(@$value['social_site_type']) == 'google')
								<a href="{{@$value['social_site_url']}}" target=_blank>
									<img src="{{asset('assets/images/profile/sosmed_05.png')}}">
								</a>
								@endif
								@if(strtolower(@$value['social_site_type']) == 'instagram')
								<a href="{{@$value['social_site_url']}}" target=_blank>
									<img src="{{asset('assets/images/profile/sosmed_06.png')}}">
								</a>
								@endif
							@endforeach
						@endif
					</div>
					<div class="row">
						<div class="form-group col-sm-12">
							<label for='social' style="color:#000;">Social Media URL</label>
						</div>
					</div>
					<div class="social-box">
						@if(is_array(@$profile['social_sites']))
							@foreach($profile['social_sites'] as $key => $value)
								<div class="row">
									<div class="form-group col-sm-6">
										<select name="social_site_type[]" class="form-control">
											<option value="" disabled="" selected="">--pilih--</option>
											<option value="whatsapp" {{(strtolower(@$value['social_site_type']) == 'whatsapp') ? 'selected' : ''}}>Whatsapp</option>
											<option value="facebook" {{(strtolower(@$value['social_site_type']) == 'facebook') ? 'selected' : ''}}>Facebook</option>
											<option value="behance" {{(strtolower(@$value['social_site_type']) == 'behance') ? 'selected' : ''}}>behance</option>
											<option value="twitter" {{(strtolower(@$value['social_site_type']) == 'twitter') ? 'selected' : ''}}>Twitter</option>
											<option value="google" {{(strtolower(@$value['social_site_type']) == 'google') ? 'selected' : ''}}>Google+</option>
											<option value="instagram" {{(strtolower(@$value['social_site_type']) == 'instagram') ? 'selected' : ''}}>Instagram</option>
										</select>
									</div>
									<div class="form-group col-sm-6">
										<input type="text" name="social_site_url[]" multiple="true" class="form-control" value="{{@$value['social_site_url']}}" placeholder="Social Media URL/Account">							
									</div>
								</div>
							@endforeach
						@endif
					</div>
					<div class="row" style="margin-left:15px;margin-right:15px;">
						<div class="add-more pull-right">
							<span>add/remove social url </span>
							<a href="javascript:void(0)" class="remove-social"><i class="fa fa-minus-circle fa-2x"></i></a>
							<a href="javascript:void(0);" class="add-social"><i class="fa fa-plus-circle fa-2x"></i></a>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group right">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<input type="submit" name="new-job-sbm" value="Save" class="btn btn-orange">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>