<div id="update-logo" class="modal fade my-account tenant" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title">Update Logo</h4>
			</div>
			<form method="post" action="{{route('users.account.update-tenant')}}" enctype="multipart/form-data" name="logo">
				{{csrf_field()}}
				<div class="modal-body">
					<div class="form-group">
						<div style="height:0px;overflow:hidden">
						   <input type="file" id="logo_file" name="logo_file" />
						</div>
						<a href="javascript:void(0);" class="btn btn-grey" onclick="uploadButton2('logo_file');">Upload</a>
						<div style="height: 200px;width: 100%;border: 1px solid #444;text-align:center;vertical-align:middle;position:relative;background-color: {{(@$profile['tenant_color_background']) ? $profile['tenant_color_background'] : '#013759'}};" class="logo_prev">
							@if(@$profile["tenant_logo"])
								<img src="{{$profile['tenant_logo']}}"  style="height: 100px;width:auto;top:0;bottom:0;left:0;right:0;margin:auto;position:absolute;" alt="preview logo" >
							@endif
						</div>
					</div>
					<div class="form-group center" style="margin:30px 0;">
						<label style="color:#000;">Choose your background logo</label>
						<input type="hidden" name="bg_color" id="bg_color_input" value="{{(@$profile['tenant_color_background']) ? $profile['tenant_color_background'] : '#013759'}}" />
						<div style=" overflow-x: scroll;">
							<table>
								<tr>
									<td>
										<a href="javascript:void(0);" onclick="changeBackgroundLogo('#ffffff')">
											<div style="height:43px;width:43px;background:#ffffff;"></div>
										</a>
									</td>
									<td>
										<a href="javascript:void(0);" onclick="changeBackgroundLogo('#c8c8c8')">
											<div style="height:43px;width:43px;background:#c8c8c8;"></div>
										</a>
									</td>
									<td>
										<a href="javascript:void(0);" onclick="changeBackgroundLogo('#959595')">
											<div style="height:43px;width:43px;background:#959595;"></div>
										</a>
									</td>
									<td>
										<a href="javascript:void(0);" onclick="changeBackgroundLogo('#4d4d4d')">
											<div style="height:43px;width:43px;background:#4d4d4d;"></div>
										</a>
									</td>
									<td>
										<a href="javascript:void(0);" onclick="changeBackgroundLogo('#333333')">
											<div style="height:43px;width:43px;background:#333333;"></div>
										</a>
									</td>
									<td>
										<a href="javascript:void(0);" onclick="changeBackgroundLogo('#000000')">
											<div style="height:43px;width:43px;background:#000000;"></div>
										</a>
									</td>
									<td>
										<a href="javascript:void(0);" onclick="changeBackgroundLogo('#534741')">
											<div style="height:43px;width:43px;background:#534741;"></div>
										</a>
									</td>
									<td>
										<a href="javascript:void(0);" onclick="changeBackgroundLogo('#736357')">
											<div style="height:43px;width:43px;background:#736357;"></div>
										</a>
									</td>
									<td>
										<a href="javascript:void(0);" onclick="changeBackgroundLogo('#754c24')">
											<div style="height:43px;width:43px;background:#754c24;"></div>
										</a>
									</td>
									<td>
										<a href="javascript:void(0);" onclick="changeBackgroundLogo('#f6c87f')">
											<div style="height:43px;width:43px;background:#f6c87f;"></div>
										</a>
									</td>
									<td>
										<a href="javascript:void(0);" onclick="changeBackgroundLogo('#185f65')">
											<div style="height:43px;width:43px;background:#185f65;"></div>
										</a>
									</td>
									<td>
										<a href="javascript:void(0);" onclick="changeBackgroundLogo('#013759')">
											<div style="height:43px;width:43px;background:#013759;"></div>
										</a>
									</td>
									<td>
										<a href="javascript:void(0);" onclick="changeBackgroundLogo('#560204')">
											<div style="height:43px;width:43px;background:#560204;"></div>
										</a>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group right">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<input type="submit" name="new-job-sbm" value="Save" class="btn btn-orange">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>