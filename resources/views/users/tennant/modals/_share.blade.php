<div id="share-social" class="modal fade my-account tenant" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title">Share Your Page</h4>
			</div>
			<form method="post" action="{{route('users.account.update-tenant')}}">
				{{csrf_field()}}
				<div class="modal-body">
					<div class="form-group center" style="margin:30px 0;">
						<!-- <a href="#" title="Share to Whatsapp"> -->
							<!-- <img src="{{asset('assets/images/profile/sosmed_01.png')}}"> -->
						<!-- </a> -->
						<a href="{{@$shares['facebook']}}" title="Share to Facebook" target=_blank>
							<img src="{{asset('assets/images/profile/sosmed_02.png')}}">
						</a>
						<!-- <a href="#">
							<img src="{{asset('assets/images/profile/sosmed_03.png')}}">
						</a> -->
						<a href="{{@$shares['twitter']}}" title="Share to Twitter" target=_blank>
							<img src="{{asset('assets/images/profile/sosmed_04.png')}}">
						</a>
						<a href="{{@$shares['gplus']}}" title="Share to Google+" target=_blank>
							<img src="{{asset('assets/images/profile/sosmed_05.png')}}">
						</a>
						<!-- <a href="#" title="Share to Instagram"> -->
							<!-- <img src="{{asset('assets/images/profile/sosmed_06.png')}}"> -->
						<!-- </a> -->
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group right">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>