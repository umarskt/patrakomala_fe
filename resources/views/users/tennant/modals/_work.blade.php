<div id="update-specialization" class="modal fade my-account tenant" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title">Update Company Specialization</h4>
			</div>
			<form method="post" action="{{route('users.account.update-tenant')}}">
				{{csrf_field()}}
				<div class="modal-body">
					<div class="form-group">
						<label class="checkbox-inline">
							<span class="checkboxmark"></span>
							<input type="checkbox" name="specialization"/>
							Javascript
						</label>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group right">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<input type="submit" name="new-job-sbm" value="Save" class="btn btn-orange">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>