<div id="{{(@$data)?'':'add-'}}gallery{{@$data['id']}}" class="modal fade my-account tenant" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title">{{(@$data)?'Manage '.@$data['album_name']:'New Gallery'}}</h4>
			</div>
			<form method="post" action="{{route('users.account.update-tenant')}}" enctype="multipart/form-data">
				{{csrf_field()}}
				<div class="modal-body">
					<div class="form-group">
						@if(@$data['id'])
							<input type="hidden" name="id_album" value="{{$data['id']}}">
						@endif
						<label for='title'>Title</label>
						<input type="text" name="gallery_title" class="form-control" value="{{@$data['album_name']}}">
					</div>
					<div class="form-group">
						<label for='description'>Description</label>
						<textarea class="form-control" name="gallery_description">{{@$data['album_description']}}</textarea>
					</div>
					<div style="height:0px;overflow:hidden" class="thegalist thegalist{{(@$data['id'])?$data['id']:0}}">
						<!-- gallery_image -->
						<!-- <input type="file" id="gallery_image{{@$data['id']}}" name="galleries[]" data-id="{{@$data['id']}}" multiple="true" /> -->
						<input type="file" id="galleries0" class="galleries galleries{{(@$data['id'])?$data['id']:0}}" onchange="onChangeGalleries(this,event);" name="gallery_image[]" data-length="{{(is_array(@$data['galleries']))?count(@$data['galleries']):0}}" data-id="{{(@$data['id'])?$data['id']:0}}" multiple="true" />

					</div>
					<a href="javascript:void(0);" class="btn btn-grey" data-counter=0 onclick="uploadButton(this,'.thegalist{{(@$data['id'])?$data['id']:0}} #galleries');">Upload</a>
					<div style="clear:both;"></div>
					<div class="box-upload-logo">
						<output id="result{{(@$data['id'])?$data['id']:0}}" class='result'>
						@if(is_array(@$data['galleries']))
							@foreach($data['galleries'] as $key => $value)
								<div>
									<input type="hidden" name="regallery_image[]" value="{{$value['img_gallery']}}">
									<a href='javascript:void(0);' onclick='clearThis(this);' data-key='{{$key}}'><i class='fa fa-remove'></i></a><img class='thumbnail thumbnail{{$key}}' src='{{$value["img_gallery"]}}' title='preview image'/>
								</div>
							@endforeach
						@endif
						</output>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group right">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<input type="submit" name="new-job-sbm" value="Save" class="btn btn-orange">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>