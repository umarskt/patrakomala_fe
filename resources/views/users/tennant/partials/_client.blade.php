<div class="row">
	<div class="form-group col-sm-6">
		<label for='client_name'>Nama Perusahaan</label>
		<input type="text" name="client_name[]" class="form-control"  multiple="true">
	</div>
	<div class="form-group col-sm-6">
		<label for='client_logo'>Logo Perusahaan</label>
		<input type="file" id="client_logo" name="client_img_logo[]" multiple="true" />
	</div>
</div>