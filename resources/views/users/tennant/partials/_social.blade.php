<div class="row">
	<div class="form-group col-sm-6">
		<select name="social_site_type[]" class="form-control">
			<option value="" disabled="" selected="">--pilih--</option>
			<option value="whatsapp">Whatsapp</option>
			<option value="facebook">Facebook</option>
			<option value="be">be</option>
			<option value="twitter">Twitter</option>
			<option value="google+">Google+</option>
			<option value="instagram">Instagram</option>
		</select>
	</div>
	<div class="form-group col-sm-6">
		<input type="text" name="social_site_url[]" multiple="true" class="form-control" placeholder="Social Media URL">							
	</div>
</div>