@extends('layouts.application2')

@section('title')
    Setting
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container-fluid full">
		<div class="jumbotron jumbotron-profile">
		</div>
	</div>
	<div class="container-fluid my-account col-xs-10 col-sm-10 col-md-8 col-md-offset-2 col-sm-offset-1 col-xs-offset-1">
		<div class="">
			<a href="{{route('users.account.dashboard')}}" class="btn btn-back"></a>
		</div>
		<div class="setting">
			<h2>Account Setting</h2>
			<ul>
				<li>
					<p>
						<a href="javascript:void(0);">Tenant Name</a>
					</p>
					<div class="setting-form">
						<form class="form-horizontal" method="post" action="{{route('users.account.setting.post')}}">
							{{csrf_field()}}
							<div class="form-group">
								<div class="col-sm-4 right-label">
									<label for="username">Tenant Name</label>
								</div>
								<div class="col-sm-6">
									<input type="text" name="tenant_name" class="form-control" value="{{@$profile['tenant_name']}}">
								</div>
							</div>
							<div class="form-group right form-btn">
								<div class="col-sm-10">
									<a href="#" class="btn btn-cancel">Cancel</a>
									<input type="submit" class="btn btn-save" value="Save">
								</div>
							</div>
						</form>
					</div>
				</li>
				<li>
					<p>
						<a href="javascript:void(0);">Change Email Address</a>
					</p>
					<div class="setting-form">
						<form class="form-horizontal" method="post" action="{{route('users.account.setting.post')}}">
							{{csrf_field()}}
							<div class="form-group">
								<div class="col-sm-4 right-label">
									<label for="current">Current</label>
								</div>
								<div class="col-sm-6">
									<input type="email" name="current" class="form-control" value="{{@$profile['tenant_email']}}">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4 right-label">
									<label for="new">New</label>
								</div>
								<div class="col-sm-6">
									<input type="email" name="new" class="form-control">
								</div>
							</div>
							<div class="form-group right form-btn">
								<div class="col-sm-10">
									<a href="#" class="btn btn-cancel">Cancel</a>
									<input type="submit" class="btn btn-save" value="Save">
								</div>
							</div>
						</form>
					</div>
				</li>
				<li>
					<p>
						<a href="javascript:void(0);">Change Password</a>
					</p>
					<div class="setting-form">
						<form class="form-horizontal" method="post" action="{{route('users.account.setting.post')}}">
							{{csrf_field()}}
							<div class="form-group">
								<div class="col-sm-4 right-label">
									<label for="current">Current</label>
								</div>
								<div class="col-sm-6">
									<input type="password" name="current" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4 right-label">
									<label for="new">New</label>
								</div>
								<div class="col-sm-6">
									<input type="password" name="new" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4 right-label">
									<label for="retype">Re-Type</label>
								</div>
								<div class="col-sm-6">
									<input type="password" name="retype" class="form-control">
								</div>
							</div>
							<div class="form-group right form-btn">
								<div class="col-sm-10">
									<a href="#" class="btn btn-cancel">Cancel</a>
									<input type="submit" class="btn btn-save" value="Save">
								</div>
							</div>
						</form>
					</div>
				</li>
				<li>
					<p>
						<a href="javascript:void(0);">Add More Contact</a>
					</p>
					<div class="setting-form row contact">
						<div class="col-md-5 contact-exist">
							<ol>
<!-- 								<li>
									<label>Whatsapp</label>
									<input type="text" name="contact" value="082316680947">
									<a href="javascript:void(0);" class="add-rem pull-right"><i class="fa fa-times-circle"></i></a>
								</li> -->
								<li>
									<label>Mobile</label>
									<input type="text" name="contact" value="{{@$profile['tenant_phone']}}">
									<a href="javascript:void(0);" class="add-rem pull-right"><i class="fa fa-times-circle"></i></a>
								</li>
							</ol>
						</div>
						<div class="col-md-7 contact-form">
							<form class="form-horizontal" method="post" action="{{route('users.account.setting.post')}}">
							{{csrf_field()}}
							<div class="form-group">
								<div class="col-sm-4 right-label">
									<label for="contact_type">Contact Type</label>
								</div>
								<div class="col-sm-6">
									<select class="form-control" name="contact_type">
										<option value="phone" {{(@$profile['tenant_phone'])?'selected':''}}>Mobile Phone</option>
										<option value="wa">WhatsApp</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4 right-label">
									<label for="contact_detail">Contact Detail</label>
								</div>
								<div class="col-sm-6">
									<input type="text" name="contact_detail" class="form-control" value="{{@$profile['tenant_phone']}}">
								</div>
							</div>
							<div class="">
								<div class="col-xs-6">
									<h5>*this contact will appear in tenant dashboard who accept your requirements.</h5>
								</div>
								<div class="col-xs-4 pull-right">
									<a href="javascript:void(0);" class="rem-cont add-rem"><i class="fa fa-minus-circle fa-2x"></i></a>
									<a href="javascript:void(0);" class="add-cont add-rem"><i class="fa fa-plus-circle fa-2x"></i></a>
								</div>
							</div>
							<div class="form-group right form-btn">
								<div class="col-sm-10">
									<a href="#" class="btn btn-cancel">Cancel</a>
									<input type="submit" class="btn btn-save" value="Save">
								</div>
							</div>
						</form>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
@stop

@section('custom_scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			$('.setting-form').hide();
		});
		$('.setting>ul>li>p').on('click',function(){
			$('.setting-form').slideUp('fast');
			if($(this).next('.setting-form').is(':hidden')){
				$(this).next('.setting-form').slideDown('slow');
			}
			else{
				$(this).next('.setting-form').slideUp('slow');
			}
		});
	</script>
@stop