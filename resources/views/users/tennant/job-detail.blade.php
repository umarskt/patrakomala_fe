@extends('layouts.application2')

@section('title')
    Job Detail
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container-fluid full">
		<div class="jumbotron jumbotron-profile">
		</div>
	</div>
	<div class="container-fluid prev-log">
		<div class="pull-right">
			<a href="#" class="btn btn-logout btn-orange">Logout</a>
		</div>
	</div>
	<div class="container-fluid my-account">
		<div class="pull-right btn-setting">
			<a href="#">
				<img src="{{asset('assets/images/profile/button_refresh.png')}}">
			</a>
			<a href="#">
				<img src="{{asset('assets/images/profile/button_search.png')}}">
			</a>
			<a href="#">
				<img src="{{asset('assets/images/profile/button_trash.png')}}">
			</a>
		</div>
		<div style="clear:both;"></div>
		<div class="job-list">
			<h2>work that suits your company</h2>
			<hr class="hr-jobs">
			<div class="">
				<a href="#" class="btn btn-back"></a>
			</div>
			<div class="left">
				<div class="req-detail">
					<h5 class="left">#Product
						<span class="pull-right">Subsektor: Kuliner</span>
					</h5>
					<p>
						<label>Nama: </label>
						Rudy
					</p>
					<p>
						<label>Email: </label>
						aj.biner@gmail.com
					</p>
					<hr>
					<h5 class="left">
						<label>Subject: </label> Lorem Ipsum
						<span class="pull-right">30-06-2018</span>
					</h5>
					<p>
						<label>Detail: </label>
						Lorem ipsum tulang sumsum lorem ipsum tulang sumsum lorem ipsum tulang sumsum lorem ipsum tulang sumsum
					</p>
					<hr>
					<p>
						<label>Estimated cost required: </label> 10.000.000 - 45.000.000 Rupiah
					</p>
				</div>
			</div>
		</div>
		<div class="notes left">
			<!-- <p>* The closed requirements will be lost if user change status to closed</p> -->
		</div>
	</div>
@stop

@section('custom_scripts')

@stop