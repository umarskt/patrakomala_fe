@extends('layouts.application2')

@section('title')
    FAQ - Subsektor
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container faq">
		<div class="page-detail blue">
			<h3 class="page-title">F.A.Q (ABOUT SUBSEKTOR)</h3>
			<p>faq </p><p>subsektor</p>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>1. Apa itu Subsektor?</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>Ini detail faq 1</p>
			</div>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>2. Bagaimana cara bergabung?</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>Ini detail faq 2</p>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')

@stop