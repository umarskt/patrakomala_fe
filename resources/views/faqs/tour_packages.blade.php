@extends('layouts.application2')

@section('title')
    FAQ - Tour Packages
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container faq">
		<div class="page-detail blue">
			<h3 class="page-title">F.A.Q (TOUR PACKAGES)</h3>
			<p>faq </p><p>tour packages</p>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>1. Cara memilih destinasi?</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>Ini detail faq 1</p>
			</div>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>2. Cara mencari destinasi?</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>Ini detail faq 2</p>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')

@stop