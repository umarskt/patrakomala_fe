@extends('layouts.application2')

@section('title')
    FAQ - Job
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container faq">
		<div class="page-detail blue">
			<h3 class="page-title">F.A.Q (JOB)</h3>
			<p>faq </p><p>job</p>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>1. Bagaimana cara posting job?</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>Ini detail faq 1</p>
			</div>
		</div>
		<div class="col-xs-12 drop-faq">
			<h4>2. Bagaimana cara transaksi?</h4>
			<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
			<div class="detail-faq">
				<p>Ini detail faq 2</p>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			$('div.detail-faq').css('display','none');
		});
	</script>
	<script type="text/javascript" src="{{asset('assets/js/custom_scripts.js')}}"></script>
@stop