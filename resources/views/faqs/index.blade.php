@extends('layouts.application2')

@section('title')
    @lang('general.title.faq')
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container faq">
		<div class="page-detail blue">
			<h3 class="page-title">F.A.Q</h3>
			<p>&nbsp;</p>
		</div>
		@for($i=1;$i<=13;$i++)
			<div class="col-xs-12 drop-faq">
				<h4>{{$i}}. @lang('general.desc.faq1.'.$i)</h4>
				<span><a href="javascript:void(0)" class="btn-drop-faq"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-left hide"></i></a></span>
				<div class="detail-faq">
					<p>
						@lang('general.desc.faq2.'.$i)
					</p>
				</div>
			</div>
		@endfor
	</div>
@stop

@section('custom_scripts')

@stop