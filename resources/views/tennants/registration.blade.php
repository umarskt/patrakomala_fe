@extends('layouts.application')

@section('title')
		Sign Up Tennant
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/bootzard/css/form-elements.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/bootzard/css/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_map1.css')}}">
@stop

@section('head_scripts')
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCEKkG9jlRbavkuE6unDPFGjZ6Ur5cYjHM&libraries=geometry"></script>
	<script type="text/javascript" src="{{asset('assets/js/custom_maps1.js')}}"></script>
@stop

@section('content')
	<div class="container-fluid full">
		<div class="jumbotron-signup">
			
		</div>
	</div>
	<div class="container register-tenant col-xs-12 col-sm-12 col-md-12 regis-form">
		{!! Form::open(['route'=>'tennants.sign_up.post','method'=>'post','class'=>'f1','files'=>true]) !!}
			<div class="page-detail blue">
				<h3 class="page-title">SIGN UP YOUR COMPANY</h3>
				<p><!-- This is supposed to be the detail ot the page --></p>
			</div>
			<div class="f1-steps">
				<div class="f1-progress">
						<div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
				</div>
				<div class="f1-step active">
					<div class="f1-step-icon"><i class="fa fa-user"></i></div>
					<p>About</p>
				</div>
				<div class="f1-step">
					<div class="f1-step-icon"><i class="fa fa-key"></i></div>
					<p>Address & Contact</p>
				</div>
					<div class="f1-step">
					<div class="f1-step-icon"><i class="fa fa-twitter"></i></div>
					<p>Social & Product</p>
				</div>
			</div>
			<label></label>
			<fieldset>
				<div class="regis-form-box">
					<legend class="text-uppercase">*identitas pelaku usaha</legend>
					<div class="row regis-form-box2">
						<div class="form-group">
							{!! Form::label('company_name', 'Company Name') !!}
							{!! Form::text('company_name',null,['class'=>'form-control','autofocus'=>true]) !!}
						</div>
						<div class="form-group">
							{!! Form::label('ceo_name', 'CEO Name') !!}
							{!! Form::text('ceo_name',null,['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('founding_year', 'Founding Year') !!}
							{!! Form::text('founding_year',null,['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('company_background', 'Company Background') !!}
							{!! Form::textarea('company_background',null,['class'=>'form-control']) !!}
						</div>
						<div class="form-group col-xs-12 sel2">
							{!! Form::label('company_status', 'Status') !!}
							{!! Form::select('company_status',[0=>'Default',1=>'Else'],null,['class'=>'form-control select2']) !!}
						</div>
						<div class="form-group col-xs-12 sel2">
							{!! Form::label('subsektor', 'Subsektor') !!}
							{!! Form::select('subsektor[]',[0=>'Default',1=>'Else'],null,['class'=>'form-control select2','multiple'=>true]) !!}
						</div>
						<div class="f1-buttons">
							<button type="button" class="btn btn-next">NEXT</button>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>COMPANY ADDRESS 1</legend>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('company_address', 'Street') !!}
							{!! Form::textarea('company_address[]',null,['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('company_country', 'Country') !!}
							{!! Form::select('company_country[]',[0=>'Default',1=>'Else'],null,['class'=>'form-control select2']) !!}
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('company_building_number', 'Building Number') !!}
							{!! Form::text('company_building_number[]',null,['class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('company_postal_code', 'Postal Code') !!}
							{!! Form::text('company_postal_code[]',null,['class'=>'form-control']) !!}
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							{!! Form::label('company_lat_long', 'Address') !!}
							{!! Form::text('company_lat_long',null,['class'=>'form-control']) !!}
							<div class="row col-xs-12 col-sm-10 col-sm-offset-1 body-map">
								<div id="map" class="map"></div>
							</div>
						</div>
						<div class="form-group">
							<div class="add-more">
								<span>add more address </span>
								<a href="javascript:void(0)" class="add-address"><i class="fa fa-plus-circle fa-2x"></i></a>
								<a href="javascript:void(0)" class="remove-address"><i class="fa fa-minus-circle fa-2x"></i></a>
							</div>
						</div>
					</div>
				</div>
				<legend>CONTACT PERSON</legend>
				<div class="form-group">
					{!! Form::label('contact_name', 'Name') !!}
					{!! Form::text('contact_name[]',null,['class'=>'form-control']) !!}
				</div>
				<div class="row">
					<div class="col-sm-5 col-xs-5">
						<div class="form-group">
							{!! Form::label('contact_type', 'Contact Type') !!}
							{!! Form::select('contact_type[]',[0=>'Default',1=>'Else'],null,['class'=>'form-control select2']) !!}
						</div>
					</div>
					<div class="col-sm-7 col-xs-7">
						<div class="form-group">
							{!! Form::label('contact_detail', 'Contact Detail') !!}
							{!! Form::text('contact_detail[]',null,['class'=>'form-control']) !!}
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<div class="add-more">
								<span>add more contact </span>
								<a href="javascript:void(0)" class="add-contact"><i class="fa fa-plus-circle fa-2x"></i></a>
								<a href="javascript:void(0)" class="remove-contact"><i class="fa fa-minus-circle fa-2x"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="f1-buttons">
						<button type="button" class="btn btn-previous">PREVIOUS</button>
						<button type="button" class="btn btn-next">NEXT</button>
				</div>
			</fieldset>
			<fieldset>
				<div class="form-group">
					{!! Form::label('founder_name', 'Founder Name') !!}
					{!! Form::text('founder_name[]',null,['class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					<div class="add-more">
						<span>add more founder name </span>
						<a href="javascript:void(0)" class="add-founder"><i class="fa fa-plus-circle fa-2x"></i></a>
						<a href="javascript:void(0)" class="remove-founder"><i class="fa fa-minus-circle fa-2x"></i></a>
					</div>
				</div>
				<legend>UPLOAD A COMPANY FILE</legend>
				<div class="row">
					<div class="col-sm-5 col-xs-5">
						<div class="form-group">
							{!! Form::label('company_legality_type', 'Company Legality') !!}
							{!! Form::select('company_legality_type[]',[0=>'Document',1=>'Image'],null,['class'=>'form-control select2']) !!}
						</div>
					</div>
					<div class="col-sm-7 col-xs-7">
						<div class="form-group">
							{!! Form::label('company_legality', '') !!}
							{!! Form::file('company_legality[]',['class'=>'form-control']) !!}
						</div>
					</div>
					<div class="col-sm-5 col-xs-5">
						<div class="form-group">
							{!! Form::label('company_file_type', 'File Type') !!}
							{!! Form::select('company_file_type[]',[0=>'Document',1=>'Image'],null,['class'=>'form-control select2']) !!}
						</div>
					</div>
					<div class="col-sm-7 col-xs-7">
						<div class="form-group">
							{!! Form::label('company_file', '') !!}
							{!! Form::file('company_file[]',['class'=>'form-control']) !!}
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<div class="add-more">
								<span>add more file </span>
								<a href="javascript:void(0)" class="add-file"><i class="fa fa-plus-circle fa-2x"></i></a>
								<a href="javascript:void(0)" class="remove-file"><i class="fa fa-minus-circle fa-2x"></i></a>
							</div>
						</div>
					</div>
				</div>
				<legend>SOCIAL SITE</legend>
				<div class="row">
					<div class="col-sm-5 col-xs-5">
						<div class="form-group">
							{!! Form::label('social_type', 'Social Type') !!}
							{!! Form::select('social_type[]',[0=>'Twitter',1=>'Facebook'],null,['class'=>'form-control select2']) !!}
						</div>
					</div>
					<div class="col-sm-7 col-xs-7">
						<div class="form-group">
							{!! Form::label('social_url', 'URL') !!}
							{!! Form::text('social_url[]',null,['class'=>'form-control']) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5 col-xs-5">
						<div class="form-group">
							{!! Form::label('social_type', '') !!}
							{!! Form::select('social_type[]',[0=>'Twitter',1=>'Facebook'],null,['class'=>'form-control select2']) !!}
						</div>
					</div>
					<div class="col-sm-7 col-xs-7">
						<div class="form-group">
							{!! Form::label('social_url', '') !!}
							{!! Form::text('social_url[]',null,['class'=>'form-control']) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<div class="add-more">
								<span>add more type </span>
								<a href="javascript:void(0)" class="add-social"><i class="fa fa-plus-circle fa-2x"></i></a>
								<a href="javascript:void(0)" class="remove-social"><i class="fa fa-minus-circle fa-2x"></i></a>
							</div>
						</div>
					</div>
				</div>
				<legend>COMPANY PORTFOLIO OR PRODUCT</legend>
				<div class="row">
					<div class="col-sm-5 col-xs-5">
						<div class="form-group">
							{!! Form::label('portfolio_type', 'Type') !!}
							{!! Form::select('portfolio_type[]',[0=>'Portfolio',1=>'Product'],null,['class'=>'form-control select2']) !!}
						</div>
					</div>
					<div class="col-sm-7 col-xs-7">
						<div class="form-group">
							{!! Form::label('portfolio_url', 'URL') !!}
							{!! Form::text('portfolio_url[]',null,['class'=>'form-control']) !!}
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							{!! Form::label('portfolio_description', 'Description') !!}
							{!! Form::textarea('portfolio_description[]',null,['class'=>'form-control']) !!}
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<div class="add-more">
								<span>add more type </span>
								<a href="javascript:void(0)" class="add-portfolio"><i class="fa fa-plus-circle fa-2x"></i></a>
								<a href="javascript:void(0)" class="remove-portfolio"><i class="fa fa-minus-circle fa-2x"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="f1-buttons">
					<button type="button" class="btn btn-previous">PREVIOUS</button>
					<button type="submit" class="btn btn-submit">SUBMIT</button>
				</div>
			</fieldset>
		{!! Form::close() !!}
	</div>
@stop
@section('custom_scripts')
	<script type="text/javascript" src="{{asset('vendor/bootzard/js/jquery.backstretch.min.js')}}"></script>
	<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('vendor/bootzard/js/scripts.js')}}"></script>
	
	<script type="text/javascript">
		$('.select2').select2();
	</script>
@stop