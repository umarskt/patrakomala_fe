@extends('layouts.application')

@section('title')
    @lang('general.title.tenant.listing')
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />

@stop

@section('content')
	<div class="container-fluid tenant-list" id="body">
		<div class="pull-left col-xs-12 col-sm-8 global left-content">
			<div class="bg-layer">
				<div class="blur-layer">
					<a href="javascript:void(0);" class="btn btn-default pull-right global menu-collapser"><i class="fa fa-angle-double-right fa-2x"></i></a>
					<div class="page-detail row">
						<h3 class="page-title">@lang('general.title.market-directory')</h3>
						<hr>
						<p>@lang('general.desc.market-directory')</p>
					</div>
					<div class="filter-render row">
					@if(@$tenants['data'])
						@foreach($tenants['data'] as $key => $value)
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
								<a href="{{route('tennants.detail',$value['id'])}}" title="{!!$value['tenant_name']!!}">
							  		<div class="thumbnail">
								      	<div class="thumbnail-detail">
									      	<p><span class="thumbnail-title">{!!substr($value['tenant_name'],0,30)!!}</span></p>
									      	<p class="pull-right category">{!!$value['sub_sector_name']!!}</p>
									    </div>
								      	<div class="div-image">
								      		<span class="span-image">
								      			<img src="{!!$value['tenant_logo']!!}" alt="...">
								      		</span>
								      	</div>
								    </div>
								</a>
						  	</div>
						@endforeach
						<div class="row col-xs-12 pagination-box">
							<ul class="pagination pagination-lg">
							<!-- page = 7 current = 3 -->
								@if(($tenants['meta']['current_page'] - 2) > 1)
							    	<li><a href="javascript:void(0);" class="pag-left-pull" data-page="{{$tenants['meta']['current_page'] - 3}}" onclick="nextPage(this);"><i class="fa fa-angle-double-left"></i></a></li>
						    	@endif
								@if($tenants['meta']['prev_page_url'])
							    	<li><a href="javascript:void(0);" class="pag-left" data-page="{{$tenants['meta']['current_page'] - 1}}" onclick="nextPage(this);"><i class="fa fa-chevron-left"></i></a></li>
							    @endif
							    @for($i=$tenants['meta']['current_page']-2;$i<=$tenants['meta']['current_page']+2;$i++)
							    	@if(($i >= 1) and ($i <= $tenants['meta']['last_page']))
								    	<li class="{{($tenants['meta']['current_page'] == $i) ? 'active' : ''}}"><a href="javascript:void(0);" data-page="{{$i}}" onclick="nextPage(this);">{!!$i!!}</a></li>
								    @endif
							    @endfor
							    @if(($tenants['meta']['last_page']) > $tenants['meta']['current_page'])
							    	<li><a href="javascript:void(0);" class="pag-right" data-page="{{$tenants['meta']['current_page'] + 1}}" onclick="nextPage(this);"><i class="fa fa-chevron-right"></i></a></li>
						    	@endif
						    	@if(($tenants['meta']['last_page'] - 2) > $tenants['meta']['current_page'])
							    	<li><a href="javascript:void(0);" class="pag-right-pull" data-page="{{$tenants['meta']['current_page'] + 3}}" onclick="nextPage(this);"><i class="fa fa-angle-double-right"></i></a></li>
						    	@endif
							</ul>
						</div>
					@endif
					</div>
				</div>
			</div>
		</div>
		<div class="pull-right col-xs-5 col-sm-4 global right-option">
			<p class="right"><a class="btn btn-orange btn-new-job" data-target="#new-job" data-toggle="modal">@lang('general.desc.job.0')</a></p>
			<p class="col-xs-12">
				<a href="{{route('tennants.index')}}" class="btn btn-sm pull-left" style="color:#fff;text-decoration:none;border-radius:5px;background: #a7a7a7;"><i class="fa fa-circle-o"></i> clear filter</a>
				<span class="tenant-found pull-right" style="color:#000;">{!!(@$tenants)? '' : 'found: 0'!!}</span>
			</p>
			<div class="search-box">
				<div class="input-group">
				  <input type="text" class="form-control" name="search">
				  <span class="input-group-btn">
					<button class="btn btn-default search-btn" type="button"><span class="sm">@lang('general.label.search')</span><span class="xs"><i class="fa fa-search"></i></span></button>
				  </span>
				</div>
			</div>
			<ul class="parent subsector">
				<a href="javascript:void(0);"><li class="parent-title">@lang('general.label.tenant') <i class="fa fa-chevron-left pull-right"></i><span id="tenant-info-filter" class="info-filter"></span></li></a>
				<ul class="child">
				@if(@$subsectors)
					@foreach($subsectors as $key => $value)
						<li class="radio">
							<label>{!!$value['sub_sector_name']!!}<input type="radio" name="subsektor" class="pull-right" value="{!!$value['id']!!}" data-logo="{!!$value['sub_sector_img']!!}" data-slug="{!!$value['sub_sector_slug']!!}" data-title="{!!$value['sub_sector_name']!!}" data-desc="{!!$value['sub_sector_description']!!}" id="{{($value['sub_sector_slug'] == 'radio_and_tv') ? 'turun': ''}}"><span class="checkmark"></span></label>
						</li>
					@endforeach
				@endif
				</ul>
			</ul>
			<ul class="parent">
				<a href="javascript:void(0);"><li class="parent-title">@lang('general.label.subdistrict') <i class="fa fa-chevron-left pull-right"></i> <span id="subdistrict-info-filter" class="info-filter"></span></li></a>
				<ul class="child">
				@if(@$subdistricts)
					@foreach($subdistricts as $key => $value)
						<li class="checkbox">
							<label>{!!$value['kecamatan']!!}<input type="checkbox" name="subdistrict" class="pull-right" data-title="{!!$value['kecamatan']!!}" value="{!!$value['id']!!}"><span class="checkboxmark"></span></label>
						</li>
					@endforeach
				@endif
				</ul>
			</ul>
		</div>
	</div>
	@include('tennants.modals._new_job')
@stop

@section('custom_scripts')
	<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
	<script type="text/javascript">
		$('.select2').select2();
	</script>
	<script type="text/javascript">
	// params using slug , so it is string
		var filter_params = {
			'subsector':'',
			'subdistrict': [],
			'page': 1,
			'keyword': ''
		};
		var page_detail_param = {
			'title': '',
			'desc': '',
			'logo': '',
			'slug': ''
		};
		var current_page = 1;
		
		$(document).ready(function(){
			@if(@$subsectors)
				$('ul.parent.subsector a').click();
			@endif

			@if(@$errors->first('title') || @$errors->first('description') || @$errors->first('type') || @$errors->first('sub_sectors') || @$errors->first('estimated_cost'))
				$('#new-job').modal('show');
			@endif
		});		

		$('input[name=subsektor]').click(function(){
			if($(this).val() != filter_params['subsector']){
				filter_params['subsector'] = $(this).val();
				page_detail_param = {
					'title': $(this).data('title'),
					'desc': $(this).data('desc'),
					'logo': $(this).data('logo'),
					'slug': $(this).data('slug')
				};
				getTenantList();
				getSubsektorDetail(page_detail_param);
				checked_subsektor = $(this).val();
				filteredBy(this);
			}
		});

		$('input[name=subdistrict]').click(function(){
			filter_params['subdistrict'] = [];
			$('input[name=subdistrict]:checked').map(function(){
				filter_params['subdistrict'].push(parseInt(this.value));
			});
			getTenantList();
		});

		$('.search-btn').on('click',function(){
			var keyword = $('input[name=search]').val();
			if(!((keyword == null) || (keyword == undefined) || (keyword == ''))){
				filter_params['keyword'] = keyword;
				getTenantList();
			}
		});

		function nextPage(e){
			var next = $(e).data('page');
			if(current_page != next){
				filter_params['page'] = next;
				console.log('the nex page'+next);
				getTenantList();
				current_page = next;
			}
		};

		

		function getTenantList(params=filter_params){
			// if !params {params = {};}
			console.log(params);
			$.ajax({
				url: "{{route('tennants.index')}}",
				type: 'get',
				data: params,
				success: function(data){
					console.log('sukses');
					// console.log(data);
					$('.filter-render').html(data).hide();
					$('.filter-render').fadeIn('5000');
				},
				error: function(e){
					console.log(e);
				},
				dataType: 'html'
			});
		}

		function getSubsektorDetail(params=null){
			$.ajax({
				url: "{{route('subsector.detail')}}",
				type: 'get',
				data: params,
				success: function(data){
					$('.page-detail').html(data).hide();
					$('.page-detail').fadeIn('3000');
					$('.left-content').css('background','rgb(240, 240, 240)');
					$('.bg-layer').css('background-repeat','no-repeat');
					$('.bg-layer').css('background-size','100% auto');
					$('.bg-layer').css('background-position','right');
				},
				dataType: 'html'
			});	
		}

		$('input[name=estimated_costT]').keyup(function(){
			$('input[name=estimated_cost]').val(fromMoneyIDR($(this)));
			$(this).val(toMoneyIDR($(this),$('input[name=estimated_cost]')));
		});


	</script>
@stop