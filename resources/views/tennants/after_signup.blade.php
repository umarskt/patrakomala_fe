@extends('layouts.application2')

@section('title')
		Sign Up Tennant
@stop

@section('custom_meta')
	<meta name="robots" content="noindex"/>
@stop

@section('custom_styles')
@stop

@section('content')
	<div class="container-fluid full">
		<div class="jumbotron-signup">
			
		</div>
	</div>
	<div class="container center">
		<div class="page-detail blue">
			<h3 class="page-title" style="font-size: 32px;color:#fe7e41;font-family: 'Poppins Medium';">THANK YOU</h3>
			<br/>
			<p>We have received your information. Your ID and password will sent to </p><p>your email after verification process by admin.</p>
		</div>
		<p><br/></p>
		<p><a href="{{route('home')}}" class="btn btn-red-full">BACK TO HOME</a></p>
	</div>
@stop

@section('custom_scripts')
@stop