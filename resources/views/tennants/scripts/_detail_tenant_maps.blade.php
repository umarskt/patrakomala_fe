<script type="text/javascript">
var map;
  function initialize() {
    var bandungCenter = new google.maps.LatLng(parseFloat("{{@$tenant['latitude']}}"), parseFloat("{{@$tenant['longitude']}}"));

    var isMobile = (navigator.userAgent.toLowerCase().indexOf('android') > -1) ||
      (navigator.userAgent.match(/(iPod|iPhone|iPad|BlackBerry|Windows Phone|iemobile)/));
    if (isMobile) {
      var viewport = document.querySelector("meta[name=viewport]");
      viewport.setAttribute('content', 'initial-scale=1.0, user-scalable=no');
    }
    var mapDiv = document.getElementById('map');
    mapDiv.style.width = isMobile ? '100%' : '100%';
    mapDiv.style.height = isMobile ? '100%' : '500px';
    map = new google.maps.Map(mapDiv, {
      center: bandungCenter,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend-open'));
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend'));

    var icon = {
      url: '/assets/images/marker-icon.png',
      scaledSize: new google.maps.Size(35, 45),
      origin: new google.maps.Point(0,0),
      anchor: new google.maps.Point(0, 0)
  };

    var marker = new google.maps.Marker({
      position: bandungCenter,
      title: "{{@$tenant['tenant_name']}}",
      // draggable: true,
      animation: google.maps.Animation.DROP,
      icon: icon
    });

    marker.setMap(map);

    var contentString = 
          '<div class="content">'+
            '<div class="pull-left">'+
              '<img src="{{@$tenant["tenant_logo"]}}" alt="{{@$tenant["tenant_name"]}}" class="marker-logo"/>'+
            '</div>'+
            '<div class="pull-right">'+
              '<h5>{{@$tenant["tenant_name"]}}</h5>'+
            '</div>'+
          '</div>';

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    marker.addListener('click', toggleBounce);
    marker.addListener('click', function() {
          infowindow.open(map, marker);
        });

    function toggleBounce() {
      if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
      } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
      }
    }

    if (isMobile) {
      var legend = document.getElementById('googft-legend');
      var legendOpenButton = document.getElementById('googft-legend-open');
      var legendCloseButton = document.getElementById('googft-legend-close');
      // legend.style.display = 'none';
      // legendOpenButton.style.display = 'block';
      // legendCloseButton.style.display = 'block';
      // legendOpenButton.onclick = function() {
      //   legend.style.display = 'block';
      //   legendOpenButton.style.display = 'none';
      // }
      // legendCloseButton.onclick = function() {
      //   legend.style.display = 'none';
      //   legendOpenButton.style.display = 'block';
      // }
    }
  }

  google.maps.event.addDomListener(window, 'load', initialize);

</script>