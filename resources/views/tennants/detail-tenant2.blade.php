@extends('layouts.application2')

@section('title')
    @lang('general.title.tenant.detail')
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_map1.css')}}">
	<link type="text/css" rel="stylesheet" href="{{asset('vendor/lightbox/style.css')}}" />
	<link type="text/css" rel="stylesheet" href="{{asset('assets/css/detail-tenant.css')}}" />
	<style>
		.lSSlideOuter {
			width: 100%;
		}
    	ul{
			list-style: none outside none;
		    padding-left: 0;
            margin: 0;
		}
		.content-slider li{
		    /*background-color: #ed3020;*/
		    text-align: center;
		}
		.content-slider img{
			width: 100%;
			height: auto;
		}
		.lSPager.lSpg{
			display: none;
		}
		#modal-claim{
			font-size: 16px;
		}
		#modal-claim p{
			font-size: 16px;
		}
		#modal-claim .modal-title{
			font-weight: bold;
		}
		#modal-claim a, #modal-claim input[type=submit]{
			font-size: 16px;
			color: #fff;
		}
		.google-map #map {
			height: 300px !important;
		}
    </style>
@stop

@section('head_scripts')
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCEKkG9jlRbavkuE6unDPFGjZ6Ur5cYjHM&libraries=geometry"></script>
	@include('tennants.scripts._detail_tenant_maps')
	<script type="text/javascript">
		$(document).ready(function() {
		    // $("#content-slider").lightSlider({
      //           loop:true,
      //           keyPress:true
      //       });
	    });
	</script>
@stop

@section('content')
	<div class="container-fluid full">
		<!-- <div class="jumbotron jumbotron-tenant-detail" style="background:url('{{asset('assets/images/tenant/BANNER.png')}}');background-size:auto 100%;background-repeat: no-repeat;background-position:center;"> -->
		<div class="jumbotron jumbotron-tenant-detail center" style="background:{!!(@$tenant['tenant_color_background'])?$tenant['tenant_color_background']:'#013759'!!};border-bottom:#fe7e41;background-size:auto 100%;">
			<img src="{{@$tenant['tenant_logo']}}" alt="{{@$tenant['tenant_name']}}" class="tenant-detail-logo">
			@if(in_array(@$tenant['tenant_color_background'],['#ffffff','#c8c8c8','#959595','#f6c87f']))
				<h3 style="color: #000;" class="tenant-detail-name">{{@$tenant['tenant_name']}}</h3>
			@else
				<h3 style="color: #fff;" class="tenant-detail-name">{{@$tenant['tenant_name']}}</h3>
			@endif
		</div>
	</div>
<div class="container-fluid container-fluid2">
	<div class="row">
		<div class="col-lg-3 col-sm-6">
			@if(@$tenant['status'] == 'claimed')	
				<div class="page-box-member">
					<p><i class="fa fa-star" aria-hidden="true"></i> Member <!-- <span><a href="" target="_blank">what's this?</a></span> --></p>
				</div>
			@endif
			<div class="page-box">
				<div class="info">email</div>
				<div class="value"><a href="mailto:{{@$tenant['tenant_email']}}" target="_blank">{{@$tenant['tenant_email']}}</a></div>
				<div class="info">telephone</div>
				<div class="value"><a href="tel:{{@$tenant['tenant_phone']}}" target="_blank">{{@$tenant['tenant_phone']}}</a></div>
				<!-- <div class="info">website url</div>
				<div class="value"><a target="_blank" href="http://bazaar-group.uk/">http://bazaar-group.uk/</a></div> -->
				<div class="info">address</div>
				<div class="value">{{@$tenant['tenant_address']['street']}}<br>{{@$tenant['tenant_address']['rtandrw']}}<br>{{@$tenant['tenant_address']['postal_code']}}</div>
				@if(is_array(@$tenant['social_sites']))
				@if(count(@$tenant['social_sites']) > 0)
					@foreach($tenant['social_sites'] as $key => $value)
						@if(strtolower($value['social_site_type'])=='whatsapp')
							<a target="_blank" href="{{@$value['social_site_url']}}"><img class="social-icon" src="{{asset('assets/images/tenant/WA ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}" alt="Whatsapp"></a>
						@elseif(strtolower($value['social_site_type'])=='facebook')
							<a target="_blank" href="{{@$value['social_site_url']}}"><img class="social-icon" src="{{asset('assets/images/tenant/FB ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}" alt="Facebook"></a>
						@elseif(strtolower($value['social_site_type'])=='youtube')
							<a target="_blank" href="{{@$value['social_site_url']}}"><img class="social-icon" src="{{asset('assets/images/tenant/YOUTUBE ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}" alt="Youtube"></a>
						@elseif(strtolower($value['social_site_type'])=='behance')
							<a target="_blank" href="{{@$value['social_site_url']}}"><img class="social-icon" src="{{asset('assets/images/tenant/BEHANCE ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}" alt="Behance"></a>
						@elseif(strtolower($value['social_site_type'])=='twitter')
							<a target="_blank" href="{{@$value['social_site_url']}}"><img class="social-icon" src="{{asset('assets/images/tenant/TWITTER ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}" alt="Twitter"></a>
						@elseif(strtolower($value['social_site_type'])=='google')
							<a target="_blank" href="{{@$value['social_site_url']}}"><img class="social-icon" src="{{asset('assets/images/tenant/GOOGLE PLUS ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}" alt="Google+"></a>
						@elseif(strtolower($value['social_site_type'])=='instagram')
							<a target="_blank" href="{{@$value['social_site_url']}}"><img class="social-icon" src="{{asset('assets/images/tenant/INSTAGRAM ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}" alt="Instagram"></a>
						@elseif(strtolower($value['social_site_type'])=='pinterest')
							<a target="_blank" href="{{@$value['social_site_url']}}"><img class="social-icon" src="{{asset('assets/images/tenant/PINTEREST ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}" alt="Pinterest"></a>
						@endif
					@endforeach
				@endif
			@endif
			</div>
			<div class="google-map">
		<div id="map" class="map"></div>
				
			</div>
		</div>
		<div class="col-sm-6">

			<div class="profile-center-col" style="margin-bottom: 40px;">
				<div class="content justify">
					@if(@$tenant['tenant_profile'])
						<p><strong>WHO WE ARE</strong><br>{!!@$tenant['tenant_profile']!!}</p>
					@endif
					@if(@$tenant['tenant_intro'])
						<p><strong>WHY WORK WITH US</strong><br>{!!@$tenant['tenant_intro']!!}</p>
					@endif
					@if(@$tenant['tenant_mission'])
						<p><strong>OUR MISSION</strong><br>{!!@$tenant['tenant_mission']!!}</p>
					@endif
				</div>
				@if(is_array(@$tenant['portofolios']))
					@if(count(@$tenant['albums'])>0)
						<hr>
						<div class="content">
							<h2 style="color: #000;">OUR PORTFOLIO IN CATALOG</h2>
							<div class="row">
								@foreach($tenant['portofolios'] as $key => $value)
									<div class="col-xs-4 col-sm-4 col-md-3">
									@if(@$value['catalog'])
										<a href="{{$value['catalog']}}" target=_blank title="download this pdf">
											<img src="{{asset('assets/images/tenant/PDF DOCUMENT.png')}}" style="width:90%;height:auto;">
										</a>
									@endif
									</div>
								@endforeach
							</div>
						</div>
					@endif
				@endif
				@if(is_array(@$tenant['albums']))
					@if(count(@$tenant['albums'])>0)
						<hr>
						<div class="content">
							<h2 style="color: #000;">GALLERY</h2>
							<div class="row">
								@foreach($tenant['albums'] as $key => $value)
								  	<div class="column">
								    	<img src="{{$value['galleries'][0]['img_gallery']}}" style="width:90%;height:90%;margin-right:10px;" onclick="openModal('myModal{{$key}}');currentSlide(1,'mySlides{{$key}}','{{$key}}')" class="hover-shadow cursor">
								  	</div>
								@endforeach
							</div>
							@foreach($tenant['albums'] as $key => $value)
								<div id="myModal{{$key}}" class="modal detail-tenant-gallery">
								  	<span class="close cursor" onclick="closeModal('myModal{{$key}}')">&times;</span>
								  	<div class="modal-content col-xs-12">
										<div class="modal-content2 col-xs-10 col-sm-8 col-md-6 col-lg-6 col-xs-offset-1 col-sm-offset-2 col-md-offset-3 col-lg-offset-3">
											@if(is_array($value['galleries']))
												@foreach($value['galleries'] as $k => $v)
												    <div class="mySlides mySlides{{$key}}">
												      	<div class="numbertext">{{$k+1}} / {{count($value['galleries'])}}</div>
												      	<img src="{{$v['img_gallery']}}" style="width:100%;height:650px;">
												    </div>
												@endforeach
											    <a class="prev" onclick="plusSlides(-1,'mySlides{{$key}}','{{$key}}')">&#10094;</a>
											    <a class="next" onclick="plusSlides(1,'mySlides{{$key}}','{{$key}}')">&#10095;</a>
											    <div class="caption-container">
											      	<p id="caption{{$key}}">{{@$value['album_title']}}</p>
											    </div>
												@foreach($value['galleries'] as $k => $v)
												    <div class="column">
												      	<img class="demo demo{{$key}} cursor" src="{{$v['img_gallery']}}" style="width:180px;height:140px;" onclick="currentSlide('{{$k+1}}','mySlides{{$key}}','{{$key}}')" alt="">
												    </div>
												@endforeach
											@endif
										</div>
								    </div>
								</div>
							@endforeach
						</div>
					@endif
				@endif
			@if(@$tenant['status'] != 'claimed')
				<div class="page-box">
					<h2>@lang('general.desc.claim.0')</h2>
					<div class="content">
						<p>@lang('general.desc.claim.1')</p>
					</div>
					<div class="center">
						<a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#modal-claim">@lang('general.desc.claim.5')</a>
						<br/><br/>
						<p>@lang('general.desc.claim.2')</p>
					</div>
				</div>
			@endif
			</div>
		</div>
		<div class="col-lg-3 col-sm-12">
			
			@if(is_array(@$tenant['sub_sectors']))
				@if(count($tenant['sub_sectors']) > 0)
					<div class="page-box profile-cats">
						<h2>Subsectors</h2>
						@foreach($tenant['sub_sectors'] as $key=>$value)
							<div class="cat">{!!@$value['sub_sector_name']!!}</div>
						@endforeach
					</div>
				@endif
			@endif
			@if(is_array(@$tenant['clients']))
				@if(count($tenant['clients']) > 0)
					<div class="page-box profile-cats">
						<h2>Clients</h2>
						@foreach($tenant['clients'] as $key=>$value)
							<div class="cat">{!!@$value['name_client']!!}</div>
						@endforeach
					</div>
				@endif
			@endif
			@if(is_array(@$tenant['tenant_services']))
				@if(count($tenant['tenant_services']) > 0)
					<div class="page-box profile-cats">
						<h2>Our Services</h2>
						@foreach($tenant['tenant_services'] as $key=>$value)
							<div class="cat">{!!@$value['title']!!}</div>
						@endforeach
					</div>
				@endif
			@endif
		</div>
	</div>
</div>
@include('tennants.modals._claim')

@stop

@section('custom_scripts')
	<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
	@if(is_array(@$tenant['albums']))
		@if(count(@$tenant['albums'])>0)
			<script src="{{asset('vendor/lightbox/script.js')}}"></script>
		@endif
	@endif
	<script type="text/javascript">
		$('.select2').select2();
		// $('.detail-tenant.social img').click(function(){
		// 	console.log($(this).data('title'));
		// 	$('.show-social').text($(this).data('title'));
		// 	$('.show-social').parent().show();
		// });
	</script>
	
@stop