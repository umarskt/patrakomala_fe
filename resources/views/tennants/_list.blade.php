@if(@$tenants['data'])
	@foreach($tenants['data'] as $key => $value)
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
			<a href="{{route('tennants.detail',$value['id'])}}" title="{!!$value['tenant_name']!!}">
		  		<div class="thumbnail">
			      	<div class="thumbnail-detail">
				      	<p>{!!substr($value['tenant_name'],0,30)!!}</p>
				      	<p class="pull-right category">{!!$value['sub_sector_name']!!}</p>
				    </div>
			      	<div class="div-image">
			      		<span class="span-image">
			      			<img src="{!!$value['tenant_logo']!!}" alt="...">
			      		</span>
			      	</div>
			    </div>
			</a>
	  	</div>
	@endforeach
	<div class="row col-xs-12 pagination-box">
		<ul class="pagination pagination-lg">
		<!-- page = 7 current = 3 -->
		@if($tenants['data'])
			@if(($tenants['meta']['current_page'] - 2) > 1)
		    	<li><a href="javascript:void(0);" class="pag-left-pull" data-page="{{$tenants['meta']['current_page'] - 3}}" onclick="nextPage(this);"><i class="fa fa-angle-double-left"></i></a></li>
	    	@endif
			@if($tenants['meta']['prev_page_url'])
		    	<li><a href="javascript:void(0);" class="pag-left" data-page="{{$tenants['meta']['current_page'] - 1}}" onclick="nextPage(this);"><i class="fa fa-chevron-left"></i></a></li>
		    @endif
		    @for($i=$tenants['meta']['current_page']-2;$i<=$tenants['meta']['current_page']+2;$i++)
		    	@if(($i >= 1) and ($i <= $tenants['meta']['last_page']))
			    	<li class="{{($tenants['meta']['current_page'] == $i) ? 'active' : ''}}"><a href="javascript:void(0);" data-page="{{$i}}" onclick="nextPage(this);">{!!$i!!}</a></li>
			    @endif
		    @endfor
		    @if(($tenants['meta']['last_page']) > $tenants['meta']['current_page'])
		    	<li><a href="javascript:void(0);" class="pag-right" data-page="{{$tenants['meta']['current_page'] + 1}}" onclick="nextPage(this);"><i class="fa fa-chevron-right"></i></a></li>
	    	@endif
	    	@if(($tenants['meta']['last_page'] - 2) > $tenants['meta']['current_page'])
		    	<li><a href="javascript:void(0);" class="pag-right-pull" data-page="{{$tenants['meta']['current_page'] + 3}}" onclick="nextPage(this);"><i class="fa fa-angle-double-right"></i></a></li>
	    	@endif
		@endif
		</ul>
	</div>
	
@endif
<script type="text/javascript">
	var data = {!!json_encode(@$tenants['data'])!!};;
	console.log(data);
	if(Array.isArray(data)){
		if(data.length >= 0){
			$('.tenant-found').text('found: '+data.length);
		}
		else{
			$('.tenant-found').text('found: 0');
		}
	}
	else{
		$('.tenant-found').text('found: 0');
	}
</script>