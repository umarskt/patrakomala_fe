@extends('layouts.application')

@section('title')
    @lang('general.title.tenant.detail')
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_map1.css')}}">
@stop

@section('head_scripts')
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCEKkG9jlRbavkuE6unDPFGjZ6Ur5cYjHM&libraries=geometry"></script>
	<script type="text/javascript" src="{{asset('assets/js/custom_maps1.js')}}"></script>
@stop

@section('content')
	<div class="container detail">
		<div class="col-xs-12">
			<div class="image-detail col-xs-4">
				<img src="{{asset('assets/images/APps.png')}}" alt="">	
			</div>
			<div class="page-detail blue col-xs-8">
				<h3 class="page-title">Apps & Game Developer</h3>
				<hr>
				<p>Proin in tellus sit amet nibh dignissim sagittis. Donec iaculis gravida nulla. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Pellentesque pretium lectus id turpis. Mauris metus. Etiam commodo dui eget wisi. Duis pulvinar. Praesent in mauris eu tortor porttitor accumsan. Pellentesque ipsum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse nisl.</p>
			</div>
		</div>
	</div>
	<div class="container-fluid detail-bg" style="background:url('{{asset('assets/images/DEVELOPER ILLUSTRATION.png')}}');background-position:right;background-repeat:no-repeat;">
	</div>
	<div class="container detail the-map">
		<h3>App & Game Developer in Bandung</h3>
		<div id="map" class="map"></div>
		<div class="col-xs-12 col-sm-6 col-md-5 row">
			<select class="form-control select2" id="filter-loc">
				<option value="0" selected disabled>Pilih Kecamatan</option>
				<option value="1">Banjaran</option>
			</select>
		</div>
	</div>
@stop

@section('custom_scripts')
	<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
	
	<script type="text/javascript">
		$('.select2').select2();
		$('#filter-loc').on('change',function(){
			console.log($(this).select2('data')[0].id);
			newLocation(48.1293954,11.556663);
		});


	</script>
@stop