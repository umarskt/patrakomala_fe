@extends('layouts.application')

@section('title')
    Tennant List
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container listing subsektor-listing">
		<div class="page-detail">
			<h3 class="page-title">SUBSEKTOR</h3>
			<hr>
			<p>Proin in tellus sit amet nibh dignissim sagittis. Donec iaculis gravida nulla. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Pellentesque pretium lectus id turpis. Mauris metus. Etiam commodo dui eget wisi. Duis pulvinar. Praesent in mauris eu tortor porttitor accumsan. Pellentesque ipsum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse nisl. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Pellentesque ipsum. Nullam at arcu a est sollicitudin euismod. Mauris tincidunt sem sed arcu.</p>
		</div>
		<div class="row subsektor-icon-listing">
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/APPS _ GAME ICON.png')}}"></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/ARCHITECTURE ICON_2.png')}}"></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/GRAPHIC DESIGN ICON.png')}}"></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/INTERIOR DESIGN ICON.png')}}"></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/PRODUCT DESIGN ICON.png')}}"></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/FASHION ICON.png')}}"></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/FILM ICON.png')}}"></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/POTOGRAPHY ICON.png')}}"></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/KRIYA ICON.png')}}"></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/CULINARY ICON.png')}}"></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/MUSIC ICON.png')}}"></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/PUBLISHING ICON.png')}}"></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/ADVERTISING ICON.png')}}"></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/PERFORMING ARTS ICON.png')}}"></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/ARTS ICON .png')}}"></a>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 subsektor-icon">
				<a href="{{route('tennants.subsektor.detail',1)}}"><img src="{{asset('assets/images/RADIO AND TV ICON.png')}}"></a>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')

@stop