<div class="col-sm-12">
	<label>@lang('general.label.tenant-page.kelurahan') *</label>
</div>
<div class="col-sm-12">
	<select name="kelurahan" id="kelurahan" class="select2 form-control">
		<option value='' selected disabled></option>
		@if(@$villages)	
			@foreach($villages as $key=>$value)
				<option value="{{$value['id']}}" {{(@$selected_id == $value['id'])?'selected':''}}>{{$value['village_name']}}</option>
			@endforeach
		@endif
	</select>
	<span class="help-block">{{@$errors->first('kelurahan')}}</span>
</div>
	