@extends('layouts.application')

@section('title')
		Sign In Tennant
@stop

@section('custom_meta')
@stop

@section('custom_styles')
@stop

@section('content')
	<div class="container-fluid full">
		<div class="jumbotron-signup">
			
		</div>
	</div>
	<div class="container">
		
	{!! Form::open(['route'=>'tennants.sign_in.post','method'=>'post','class'=>'']) !!}
			<div class="page-detail blue">
				<h3 class="page-title">SIGN IN YOUR COMPANY</h3>
				<p>sign in</p><p>your company</p>
			</div>
			<div class="col-xs-12 col-sm-4 col-sm-offset-4 login-form green-bg">
				<h3>Already have account</h3>
				<div class="form-group">
					{!! Form::label('username', 'Username') !!}
					{!! Form::text('username',null,['class'=>'form-control','autofocus'=>true,'placeholder'=>'Username/Email']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('password', 'Password') !!}
					{!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
				</div>
				<div class="form-group auth-link-right">
					<a href="{{route('users.forgot_password')}}">forgot password?</a>
				</div>
				<button type="submit" class="btn btn-red-full">SIGN IN</button>
			</div>
			<div class="col-xs-12 col-sm-4 col-sm-offset-4 login-form green-bg">
				<h3>New Company</h3>
				<a href="{{route('tennants.sign_up')}}" class="btn btn-red-full">SIGN UP</a>
			</div>
		{!! Form::close() !!}
	</div>
@stop

@section('custom_scripts')
@stop