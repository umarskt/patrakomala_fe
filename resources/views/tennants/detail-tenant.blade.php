@extends('layouts.application2')

@section('title')
    @lang('general.title.tenant.detail')
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_map1.css')}}">
	<link type="text/css" rel="stylesheet" href="{{asset('vendor/lightbox/style.css')}}" />
	<style>
		.lSSlideOuter {
			width: 100%;
		}
    	ul{
			list-style: none outside none;
		    padding-left: 0;
            margin: 0;
		}
		.content-slider li{
		    /*background-color: #ed3020;*/
		    text-align: center;
		}
		.content-slider img{
			width: 100%;
			height: auto;
		}
		.lSPager.lSpg{
			display: none;
		}
    </style>
@stop

@section('head_scripts')
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCEKkG9jlRbavkuE6unDPFGjZ6Ur5cYjHM&libraries=geometry"></script>
	@include('tennants.scripts._detail_tenant_maps')
	<script type="text/javascript">
		$(document).ready(function() {
		    // $("#content-slider").lightSlider({
      //           loop:true,
      //           keyPress:true
      //       });
	    });
	</script>
@stop

@section('content')
	<div class="container-fluid full">
		<!-- <div class="jumbotron jumbotron-tenant-detail" style="background:url('{{asset('assets/images/tenant/BANNER.png')}}');background-size:auto 100%;background-repeat: no-repeat;background-position:center;"> -->
		<div class="jumbotron jumbotron-tenant-detail center" style="background:{!!(@$tenant['tenant_color_background'])?$tenant['tenant_color_background']:'#013759'!!};border-bottom:#fe7e41;background-size:auto 100%;">
			<img src="{{@$tenant['tenant_logo']}}" alt="{{@$tenant['tenant_name']}}" class="tenant-detail-logo">
			@if(in_array(@$tenant['tenant_color_background'],['#ffffff','#c8c8c8','#959595','#f6c87f']))
				<h3 style="color: #000;" class="tenant-detail-name">{{@$tenant['tenant_name']}}</h3>
			@else
				<h3 style="color: #fff;" class="tenant-detail-name">{{@$tenant['tenant_name']}}</h3>
			@endif
		</div>
	</div>
@if(@$tenant['tenant_profile'] || @$tenant['tenant_intro'] || @$tenant['tenant_mission'])
	<div class="container detail-tenant">
		<div class="col-xs-12 row">
			<div class="page-detail blue">
				<h4 class="page-title aboutus-title">ABOUT US</h4>
			</div>
			<div class="desc-cool">
			@if(@$tenant['tenant_profile'])
				<div class="col-xs-12 col-sm-10 col-md-9 col-sm-offset-1 col-md-offset-1">
					<div class="col-xs-12 col-sm-4 img1">
						<img src="{{asset('assets/images/tenant/WHO WE ARE.png')}}">
					</div>
					<div class="col-xs-12 col-sm-8 desc1">
						<h4>Who We Are</h4>
						<p>{!!@$tenant['tenant_profile']!!}</p>
						<hr>			
					</div>
				</div>
			@endif
			@if(@$tenant['tenant_intro'])
				<div class="col-xs-12 col-sm-10 col-md-9 col-sm-offset-1 col-md-offset-1">
					<div class="col-xs-12 col-sm-4 img2">
						<img src="{{asset('assets/images/tenant/WHY WORK WITH US.png')}}">
						
					</div>
					<div class="col-xs-12 col-sm-8 desc2">
						<h4>Why Work With Us</h4>
						<p>{!!@$tenant['tenant_intro']!!}</p>
						<hr>
					</div>
				</div>
			@endif
			@if(@$tenant['tenant_mission'])
				<div class="col-xs-12 col-sm-10 col-md-9 col-sm-offset-1 col-md-offset-1">
					<div class="col-xs-12 col-sm-4 img1">
						<img src="{{asset('assets/images/tenant/OUR MISSION.png')}}">
						
					</div>
					<div class="col-xs-12 col-sm-8 desc1">
						<h4>Our Mission</h4>
						<p>{!!@$tenant['tenant_mission']!!}</p>
					</div>
				</div>
			@endif
			</div>
		</div>
	</div>
	<hr class="dt">
@endif
@if(is_array(@$tenant['tenant_services']))
	@if(count($tenant['tenant_services']) > 0)
	<div class="container detail-tenant">
		<div class="col-xs-12 row">
			<div class="page-detail2 blue">
				<h4 class="page-title what-title">WHAT WE CAN DO</h4>
			</div>
		</div>
		<div class="row tenant-what-list">
		@foreach($tenant['tenant_services'] as $key=>$value)
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="tenant-what">
					<p>{!!@$value['title']!!}</p>
				</div>
			</div>
		@endforeach
		</div>
	</div>
	<hr class="dt">
	@endif
@endif
@if(is_array(@$tenant['clients']))
	@if(count($tenant['clients']) > 0)	
		<div class="container detail-tenant work">
			<div class="col-xs-12 row">
				<div class="page-detail2 blue">
					<h4 class="page-title ourwork-title">OUR WORKS AND CLIENTS</h4>
				</div>
			</div>
			<div class="row">
			@foreach($tenant['clients'] as $key => $value)
				<div class="col-xs-6 col-sm-3 col-md-2">
					<div class="work-image">
						<img src="{!!@$value['img_icon']!!}" alt="{{@$tenant['clients'][$key]['name_client']}}" title="{{@$tenant['clients'][$key]['name_client']}}">
					</div>
				</div>
			@endforeach
			</div>
		</div>
	@endif
@endif
@if(is_array(@$tenant['portofolios']))
	<!-- <hr class="dt"> -->
	<div class="container detail-tenant catalog">
		<div class="col-xs-12 row">
			<div class="page-detail2 blue">
				<h4 class="page-title portfolio-title">OUR PORTFOLIO IN CATALOG</h4>
			</div>
		</div>
		<div class="row">
			@foreach($tenant['portofolios'] as $key => $value)
				<div class="col-xs-4 col-sm-3 col-md-2">
				@if(@$value['catalog'])
					<a href="{{$value['catalog']}}" target=_blank title="download this pdf">
						<img src="{{asset('assets/images/tenant/PDF DOCUMENT.png')}}">
					</a>
				@endif
				</div>
			@endforeach
		</div>
	</div>
	<hr class="dt">
@endif
@if(is_array(@$tenant['albums']))
	@if(count(@$tenant['albums'])>0)
		<div class="container detail-tenant gallery">
			<div class="col-xs-12 row">
				<div class="page-detail2 blue">
					<h4 class="page-title gallery-title">GALLERY</h4>
				</div>
			</div>
			<div class="row">
				@foreach($tenant['albums'] as $key => $value)
				  	<div class="column">
				    	<img src="{{$value['galleries'][0]['img_gallery']}}" style="width:auto;height:300px;margin-right:10px;" onclick="openModal('myModal{{$key}}');currentSlide(1,'mySlides{{$key}}','{{$key}}')" class="hover-shadow cursor">
				  	</div>
				@endforeach
			</div>
			@foreach($tenant['albums'] as $key => $value)
				<div id="myModal{{$key}}" class="modal detail-tenant-gallery">
				  	<span class="close cursor" onclick="closeModal('myModal{{$key}}')">&times;</span>
				  	<div class="modal-content col-xs-12">
						<div class="modal-content2 col-xs-10 col-sm-8 col-md-6 col-lg-6 col-xs-offset-1 col-sm-offset-2 col-md-offset-3 col-lg-offset-3">
							@if(is_array($value['galleries']))
								@foreach($value['galleries'] as $k => $v)
								    <div class="mySlides mySlides{{$key}}">
								      	<div class="numbertext">{{$k+1}} / {{count($value['galleries'])}}</div>
								      	<img src="{{$v['img_gallery']}}" style="width:100%;height:650px;">
								    </div>
								@endforeach
							    <a class="prev" onclick="plusSlides(-1,'mySlides{{$key}}','{{$key}}')">&#10094;</a>
							    <a class="next" onclick="plusSlides(1,'mySlides{{$key}}','{{$key}}')">&#10095;</a>
							    <div class="caption-container">
							      	<p id="caption{{$key}}">{{@$value['album_title']}}</p>
							    </div>
								@foreach($value['galleries'] as $k => $v)
								    <div class="column">
								      	<img class="demo demo{{$key}} cursor" src="{{$v['img_gallery']}}" style="width:180px;height:140px;" onclick="currentSlide('{{$k+1}}','mySlides{{$key}}','{{$key}}')" alt="">
								    </div>
								@endforeach
							@endif
						</div>
				    </div>
				</div>
			@endforeach
		</div>
	@endif
@endif
@if(@$tenant['latitude'] && @$tenant['longitude'])
	<div class="container detail-tenant the-map">
		<div class="col-xs-12 row">
			<div class="page-detail2 blue">
				<h4 class="page-title findus-title">FIND US</h4>
			</div>
		</div>
		<div id="map" class="map"></div>
	</div>
@endif
@if(is_array(@$tenant['social_sites']))
	@if(count(@$tenant['social_sites']) > 0)
		<div class="container detail-tenant social">
			<div class="col-xs-12 row">
				<div class="page-detail2 blue">
					<h4 class="page-title followus-title">FOLLOW US</h4>
				</div>
				<div class="row">
				@foreach($tenant['social_sites'] as $key => $value)
					@if(strtolower($value['social_site_type'])=='whatsapp')
					<a href="{{@$value['social_site_url']}}" target=_blank>
						<span>
							<img src="{{asset('assets/images/tenant/WA ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}">
						</span>
					</a>
					@elseif(strtolower($value['social_site_type'])=='facebook')
					<a href="{{@$value['social_site_url']}}" target=_blank>
						<span>
							<img src="{{asset('assets/images/tenant/FB ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}">
						</span>
					</a>
					@elseif(strtolower($value['social_site_type'])=='youtube')
					<a href="{{@$value['social_site_url']}}" target=_blank>
						<span>
							<img src="{{asset('assets/images/tenant/YOUTUBE ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}">
						</span>
					</a>
					@elseif(strtolower($value['social_site_type'])=='behance')
					<a href="{{@$value['social_site_url']}}" target=_blank>
						<span>
							<img src="{{asset('assets/images/tenant/BEHANCE ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}">
						</span>
					</a>
					@elseif(strtolower($value['social_site_type'])=='twitter')
					<a href="{{@$value['social_site_url']}}" target=_blank>
						<span>
							<img src="{{asset('assets/images/tenant/TWITTER ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}">
						</span>
					</a>
					@elseif(strtolower($value['social_site_type'])=='google')
					<a href="{{@$value['social_site_url']}}" target=_blank>
						<span>
							<img src="{{asset('assets/images/tenant/GOOGLE PLUS ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}">
						</span>
					</a>
					@elseif(strtolower($value['social_site_type'])=='instagram')
					<a href="{{@$value['social_site_url']}}" target=_blank>
						<span>
							<img src="{{asset('assets/images/tenant/INSTAGRAM ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}">
						</span>
					</a>
					@elseif(strtolower($value['social_site_type'])=='pinterest')
					<a href="{{@$value['social_site_url']}}" target=_blank>
						<span>
							<img src="{{asset('assets/images/tenant/PINTEREST ICON.png')}}" data-title="{{@$value['social_site_url']}}" title="{{@$value['social_site_url']}}">
						</span>
					</a>
					@endif
				@endforeach
				</div>
				<div class="row center" style="margin-top:30px;border:1px solid #555;border-radius:6px;display: none;">
					<span class="show-social"></span>
				</div>
			</div>
		</div>
	@endif
@endif
@stop

@section('custom_scripts')
	<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
	@if(is_array(@$tenant['albums']))
		@if(count(@$tenant['albums'])>0)
			<script src="{{asset('vendor/lightbox/script.js')}}"></script>
		@endif
	@endif
	<script type="text/javascript">
		$('.select2').select2();
		// $('.detail-tenant.social img').click(function(){
		// 	console.log($(this).data('title'));
		// 	$('.show-social').text($(this).data('title'));
		// 	$('.show-social').parent().show();
		// });
	</script>
	
@stop