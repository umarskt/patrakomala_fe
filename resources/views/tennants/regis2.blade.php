@extends('layouts.application2')

@section('title')
    @lang('general.title.registertenant')
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/bootzard/css/form-elements.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/bootzard/css/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_map1.css')}}">
	<style type="text/css">
		.select2-results{
			font-size: 13px !important;
		}
		@media (min-width: 1440px){
			.select2-results{
				font-size: 18px !important;
			}
		}
		input[type=file]{
			color:#000;
		}
	</style>
@stop

@section('head_scripts')
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCEKkG9jlRbavkuE6unDPFGjZ6Ur5cYjHM&libraries=geometry,places"></script>
	<script type="text/javascript" src="{{asset('assets/js/custom_maps_regis.js')}}"></script>
@stop

@section('content')
	<!-- <div class="container-fluid full">
		<div class="jumbotron-signup">
			
		</div>
	</div> -->
	<div class="container-fluid register-tenant">
		<div class="page-detail blue">
			<h3 class="page-title center">@lang('general.title.tenant.registration')</h3>
			<p class="center">
				@lang('general.desc.regis-tenant')
			</p>
		</div>
		{!! Form::open(['route'=>'tennants.sign_up.post','method'=>'post','class'=>'register-tenant-form','id'=>"register-tenant-form",'files'=>true]) !!}
			<p class="required">* @lang('general.title.required')</p>
			<fieldset class="regis-form-box">
				<a href="javascript:void(0);" class="drop-item"><h4 class="text-uppercase">* @lang('general.title.tenant.step1')</h4></a>
				<div class="row regis-form-box2 active" id="step1">
					<div class="col-sm-12">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.company_name') *</label>
							</div>
							<div class="col-sm-12">
								<input type="text" name="company_name" id="tenant_name" class="form-control" value="{{Input::old('company_name')}}">
								<span class="help-block">{{@$errors->first('company_name')}}</span>
							</div>
						</div>
					</div>
					<div class="row address-box" id="address-box1" style="padding-left:30px;padding-right:30px;">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.address_street') *</label>
									</div>
									<div class="col-sm-12">
										<p class="right" style="padding-right: 0px;">@lang('general.label.tenant-page.address_street_message')</p>
										<input type="text" name="address_street" id="address_street" placeholder="Enter your location" class="form-control" value="{{Input::old('address_street')}}">
										<span class="help-block">{{@$errors->first('address_street')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.address_subdistrict_id') *</label>
									</div>
									<div class="col-sm-12">
										<select name="address_subdistrict_id" id="kecamatan" onchange="getVillage(this);" class="select2 form-control">
											<option value='' selected disabled></option>
											@if(@$subdistricts)	
												@foreach($subdistricts as $key=>$value)
													<option value="{{$value['id']}}" {{(Input::old('address_subdistrict_id') == $value['id'])?'selected':''}}>{{$value['kecamatan']}}</option>
												@endforeach
											@endif
										</select>
										<span class="help-block">{{@$errors->first('address_subdistrict_id')}}</span>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.rtrw') *</label>
									</div>
									<div class="col-sm-12">
										<input type="text" name="rtrw" id="rtrw" class="form-control" placeholder="001/002" value="{{Input::old('rtrw')}}">
										<span class="help-block">{{@$errors->first('rtrw')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group row kelurahan" style="display: none;">
								</div>
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.address_postal_code') *</label>
									</div>
									<div class="col-sm-12">
										<input type="text" name="address_postal_code" id="postal_code" class="form-control number-only" value="{{Input::old('address_postal_code')}}">
										<span class="help-block">{{@$errors->first('address_postal_code')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<p class="right" style="padding-right: 0px;">@lang('general.label.tenant-page.map_message')</p>
										<div id="map" class="map"></div>
									</div>
									<div class="col-sm-12">
										<span id="current" style="display:none;"></span>
									</div>
									<div class="col-sm-6">
										<input type='text' name='address_latitude' class="form-control" id="address_latitude" readonly="true" placeholder="Latitude" style="color:#000;" value="{{Input::old('address_latitude')}}"/>
										<span class="help-block">{{@$errors->first('address_latitude')}}</span>
									</div>
									<div class="col-sm-6">
										<input type='text' name='address_longitude' class="form-control" id="address_longitude" readonly="true" placeholder="Longitude" style="color:#000;" value="{{Input::old('address_longitude')}}"/>
										<span class="help-block">{{@$errors->first('address_longitude')}}</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- <div class="col-sm-12">
						<div class="form-group">
							<div class="add-more right">
								<span>@lang('general.label.tenant-page.manage_address') </span>
								<a href="javascript:void(0)" class="remove-address"><i class="fa fa-minus-circle fa-2x"></i></a>
								<a href="javascript:void(0)" class="add-address"><i class="fa fa-plus-circle fa-2x"></i></a>
							</div>
						</div>
					</div> -->
					<div class="col-sm-12">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.company_year')</label>
							</div>
							<div class="col-sm-6">
								<select name="company_year" id="tenant_founding_year" class="select2 form-control">
									<option value='' selected disabled>@lang('general.label.year')</option>
									@for($i=(int)date('Y');$i>=(int)date('Y')-50;$i--)
										<option value="{{$i}}" {{(Input::old('company_year') == $i)?'selected':''}}>{{$i}}</option>
									@endfor
								</select>
								<span class="help-block">{{@$errors->first('company_year')}}</span>
							</div>
							<div class="col-sm-6">
								<select name="company_month" id="tenant_founding_month" class="select2 form-control">
									<option value='' selected disabled>@lang('general.label.month')</option>
									<option value="1" {{(Input::old('company_month') == '1')?'selected':''}}>@lang('general.label.month-name.1')</option>
									<option value="2" {{(Input::old('company_month') == '2')?'selected':''}}>@lang('general.label.month-name.2')</option>
									<option value="3" {{(Input::old('company_month') == '3')?'selected':''}}>@lang('general.label.month-name.3')</option>
									<option value="4" {{(Input::old('company_month') == '4')?'selected':''}}>@lang('general.label.month-name.4')</option>
									<option value="5" {{(Input::old('company_month') == '5')?'selected':''}}>@lang('general.label.month-name.5')</option>
									<option value="6" {{(Input::old('company_month') == '6')?'selected':''}}>@lang('general.label.month-name.6')</option>
									<option value="7" {{(Input::old('company_month') == '7')?'selected':''}}>@lang('general.label.month-name.7')</option>
									<option value="8" {{(Input::old('company_month') == '8')?'selected':''}}>@lang('general.label.month-name.8')</option>
									<option value="9" {{(Input::old('company_month') == '9')?'selected':''}}>@lang('general.label.month-name.9')</option>
									<option value="10" {{(Input::old('company_month') == '10')?'selected':''}}>@lang('general.label.month-name.10')</option>
									<option value="11" {{(Input::old('company_month') == '11')?'selected':''}}>@lang('general.label.month-name.11')</option>
									<option value="12" {{(Input::old('company_month') == '12')?'selected':''}}>@lang('general.label.month-name.12')</option>
								</select>
								<span class="help-block">{{@$errors->first('company_month')}}</span>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.tenant_phone') *</label>
							</div>
							<div class="col-sm-12">
								<input type="text" name="tenant_phone" id="phone" class="form-control number-only" value="{{Input::old('tenant_phone')}}">
								<span class="help-block">{{@$errors->first('tenant_phone')}}</span>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.tenant_email') *</label>
							</div>
							<div class="col-sm-12">
								<input type="email" name="tenant_email" id="email" class="form-control" value="{{Input::old('tenant_email')}}">
								<span class="help-block">{{@$errors->first('tenant_email')}}</span>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="regis-form-box">
				<a href="javascript:void(0);" class="drop-item"><h4 class="text-uppercase">@lang('general.title.tenant.step2')</h4></a>
				<div class="row regis-form-box2" id="step11">
					<div class="col-sm-12">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.founder_name')</label>
							</div>
							<div class="col-sm-12">
								<input type="text" name="founder_name" id="owner_name" class="form-control" value="{{Input::old('founder_name')}}">
								<span class="help-block">{{@$errors->first('founder_name')}}</span>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.founder_email')</label>
							</div>
							<div class="col-sm-12">
								<input type="email" name="founder_email" id="email" class="form-control" value="{{Input::old('founder_email')}}">
								<span class="help-block">{{@$errors->first('tenant_email')}}</span>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.hp')</label>
							</div>
							<div class="col-sm-12">
								<input type="text" name="hp" id="hp" class="form-control number-only" value="{{Input::old('hp')}}">
								<span class="help-block">{{@$errors->first('hp')}}</span>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.founder_last_education')</label>
							</div>
							<div class="col-sm-6">
								<select name="founder_last_education" id="grade" class="select2 form-control" >
									<option value="" selected disabled></option>
									<!-- <option value="sd">SD</option> -->
									<option value="smp" {{(Input::old('founder_last_education') == 'smp')?'selected':''}}>SMP / Junior High</option>
									<option value="sma" {{(Input::old('founder_last_education') == 'sma')?'selected':''}}>SMA/SMK / Senior High</option>
									<option value="d1" {{(Input::old('founder_last_education') == 'd1')?'selected':''}}>D1 / Associate Degree</option>
									<option value="d3" {{(Input::old('founder_last_education') == 'd3')?'selected':''}}>D3 / Associate Degree</option>
									<option value="s1" {{(Input::old('founder_last_education') == 's1')?'selected':''}}>D4/S1 / Bachelor's Degree</option>
									<option value="s2" {{(Input::old('founder_last_education') == 's2')?'selected':''}}>S2 / Master's Degree</option>
									<option value="s3" {{(Input::old('founder_last_education') == 's3')?'selected':''}}>S3 / Doctoral Degree</option>
								</select>
								<span class="help-block">{{@$errors->first('founder_last_education')}}</span>
							</div>
							<div class="col-sm-6">
								<input type="text" class="form-control" value="{{Input::old('founder_last_education_department')}}" name="founder_last_education_department" placeholder="Jurusan/Department" />
								<span class="help-block">{{@$errors->first('founder_last_education_department')}}</span>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.gender')</label>
							</div>
							<div class="col-sm-12 radio">
								<label class="radio-inline"><input type="radio" name="gender" id="genderp" value="f" {{(Input::old('gender') == 'f')?'checked':''}}><span class="checkmark"></span> @lang('general.label.tenant-page.gender1')</label>
								<label class="radio-inline"><input type="radio" name="gender" id="genderl" value="m" {{(Input::old('gender') == 'm')?'checked':''}}><span class="checkmark"></span> @lang('general.label.tenant-page.gender2')</label>
							</div>
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.npwp_type')</label>
							</div>
							<div class="col-sm-12 radio">
								<label class="radio-inline"><input type="radio" name="npwp_type" id="npwp_type0" value="0" checked><span class="checkmark"></span> @lang('general.label.tenant-page.npwp_type1')</label>
								<label class="radio-inline pull-right"><input type="radio" name="npwp_type" id="npwp_type1" value="1" {{(Input::old('npwp_type') == '1')?'checked':''}}><span class="checkmark"></span> @lang('general.label.tenant-page.npwp_type2')</label>
								<label class="radio-inline pull-right"><input type="radio" name="npwp_type" id="npwp_type2" value="2" {{(Input::old('npwp_type') == '2')?'checked':''}}><span class="checkmark"></span> @lang('general.label.tenant-page.npwp_type3')</label>
								<span class="help-block">{{@$errors->first('npwp_type')}}</span>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="regis-form-box">
				<a href="javascript:void(0);" class="drop-item"><h4 class="text-uppercase">@lang('general.title.tenant.step3')</h4></a>
				<div class="regis-form-box2 row" id="step2">
					<div class="col-sm-6">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.business_type')</label>
							</div>
							<div class="col-sm-12">
								<select name="business_type" id="business_type" class="select2 form-control" >
									<option value="" selected disabled></option>
									<option value="pt" {{(Input::old('business_type') == 'pt')?'selected':''}}>PT</option>
									<option value="cv" {{(Input::old('business_type') == 'cv')?'selected':''}}>CV</option>
									<option value="pd" {{(Input::old('business_type') == 'pd')?'selected':''}}>PD</option>
									<option value="fa" {{(Input::old('business_type') == 'fa')?'selected':''}}>FA</option>
									<option value="perum" {{(Input::old('business_type') == 'perum')?'selected':''}}>PERUM</option>
									<option value="koperasi" {{(Input::old('business_type') == 'koperasi')?'selected':''}}>KOPERASI</option>
									<option value="yayasan" {{(Input::old('business_type') == 'yayasan')?'selected':''}}>YAYASAN</option>
								</select>
								<span class="help-block">{{@$errors->first('business_type')}}</span>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.hki')</label>
							</div>
							<div class="col-sm-12">
								<select name="hki" id="hki" class="select2 form-control">
									<option value="" selected disabled></option>
									<option value="hak cipta" {{(Input::old('hki') == 'hak cipta')?'selected':''}}>Hak Cipta</option>
									<option value="hak paten" {{(Input::old('hki') == 'hak paten')?'selected':''}}>Hak Paten</option>
									<option value="merek dagang" {{(Input::old('hki') == 'merek dagang')?'selected':''}}>Merek Dagang</option>
									<option value="rahasia dagang" {{(Input::old('hki') == 'rahasia dagang')?'selected':''}}>Rahasia Dagang</option>
								</select>
								<span class="help-block">{{@$errors->first('hki')}}</span>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.legality_doc_name')</label>
							</div>
							<div class="col-sm-12">
								<select name="legality_doc_name[]" id="legal_document" class="select2 form-control" multiple>
									<option value="sp" {{in_array('sp',(Input::old('legality_doc_name'))?Input::old('legality_doc_name'):[])?'selected':''}}>Sertifikat Pendirian</option>
									<option value="sku" {{in_array('sku',(Input::old('legality_doc_name'))?Input::old('legality_doc_name'):[])?'selected':''}}>Surat Keterangan Usaha (SKU)</option>
									<option value="npwp" {{in_array('npwp',(Input::old('legality_doc_name'))?Input::old('legality_doc_name'):[])?'selected':''}}>NPWP & SIUPP</option>
								</select>
								<span class="help-block">{{@$errors->first('legality_doc_name')}}</span>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.legality_doc_file')</label>
							</div>
							<div class="col-sm-12 render-legality">
								
							</div>
							@if(Input::old('legality_doc_name'))
								<span class="help-block">@lang('general.label.tenant-page.legality_doc_file_reinput')</span>	
							@endif
							<span class="help-block">{{@$errors->first('tenant_legality_images')}}</span>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="regis-form-box">
				<a href="javascript:void(0);" class="drop-item"><h4 class="text-uppercase">* @lang('general.title.tenant.step4')</h4></a>
				<div class="regis-form-box2 row" id="step3">
					<div class="col-sm-12">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.tenant_category') *</label>
							</div>
							<div class="col-sm-12">
								<select name="tenant_category[]" id="tenant_category" class="select2 form-control" multiple="true">
									@if(@$subsectors)	
										@foreach($subsectors as $key=>$value)
											<option value="{{$value['id']}}" {{in_array($value['id'],(Input::old('tenant_category'))?Input::old('tenant_category'):[])?'selected':''}}>{{$value['sub_sector_name']}}</option>
										@endforeach
									@endif
								</select>
								<span class="help-block">{{@$errors->first('tenant_category')}}</span>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group row">
							<div class="col-sm-12">
								<label>@lang('general.label.tenant-page.jenis_usaha_kreatif')</label>
							</div>
							<div class="col-sm-12">
								<input type="text" class="form-control" name="jenis_usaha_kreatif" value="{{Input::old('jenis_usaha_kreatif')}}" />
								<span class="help-block">{{@$errors->first('jenis_usaha_kreatif')}}</span>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="regis-form-box">
				<a href="javascript:void(0);" class="drop-item"><h4 class="text-uppercase">@lang('general.title.tenant.step5')</h4></a>
				<div class="regis-form-box2 row" id="step4">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.bentuk_usaha')</label>
									</div>
									<div class="col-sm-12 radio">
										<label class="radio-inline"><input type="radio" name="bentuk_usaha" id="bentuk_usaha1" value="produk" {{(Input::old('bentuk_usaha') == 'produk')?'checked':''}}><span class="checkmark"></span> @lang('general.label.tenant-page.bentuk_usaha1')</label>
										<label class="radio-inline"><input type="radio" name="bentuk_usaha" id="bentuk_usaha2" value="jasa" {{(Input::old('bentuk_usaha') == 'jasa')?'checked':''}}><span class="checkmark"></span> @lang('general.label.tenant-page.bentuk_usaha2')</label>
										<span class="help-block">{{@$errors->first('bentuk_usaha')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.jenis_produk')</label>
									</div>
									<div class="col-sm-12 radio">
										<label class="radio-inline"><input type="radio" name="jenis_produk" id="jenis_produk1" value="continues" {{(Input::old('jenis_produk') == 'continues')?'checked':''}}><span class="checkmark"></span> @lang('general.label.tenant-page.jenis_produk1')</label>
										<label class="radio-inline"><input type="radio" name="jenis_produk" id="jenis_produk2" value="intermitten" {{(Input::old('jenis_produk') == 'intermitten')?'checked':''}}><span class="checkmark"></span> @lang('general.label.tenant-page.jenis_produk2')</label>
										<label class="radio-inline"><input type="radio" name="jenis_produk" id="jenis_produk3" value="projek" {{(Input::old('jenis_produk') == 'projek')?'checked':''}}><span class="checkmark"></span> @lang('general.label.tenant-page.jenis_produk3')</label>
										<span class="help-block">{{@$errors->first('jenis_produk')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.karakter_produk')</label>
									</div>
									<div class="col-sm-12 radio">
										<label class="radio-inline"><input type="radio" name="karakter_produk" id="karakter_produk1" value="inovasi" {{(Input::old('karakter_produk') == 'inovasi')?'checked':''}}><span class="checkmark"></span> @lang('general.label.tenant-page.karakter_produk1')</label>
										<label class="radio-inline"><input type="radio" name="karakter_produk" id="karakter_produk2" value="imitasi" {{(Input::old('karakter_produk') == 'imitasi')?'checked':''}}><span class="checkmark"></span> @lang('general.label.tenant-page.karakter_produk2')</label>
										<span class="help-block">{{@$errors->first('karakter_produk')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.quantity')</label>
									</div>
									<div class="col-sm-6">
										<input type="hidden" name="quantity" value="{{input::old('quantity')}}">
										<input type="text" name="quantityT" id="quantity" class="form-control number-only currency-this" value="{{input::old('quantityT')}}" data-real-source="quantity">
										<span class="help-block">{{@$errors->first('jumlah_produk_kg')}}</span>
									</div>
									<div class="col-sm-6">
										<select class="select2 form-control" name="unit">
											<option value='' selected disabled></option>
											<option value="ton" {{(Input::old('unit') == 'ton')?'selected':''}}>Ton</option>
											<option value="kg" {{(Input::old('unit') == 'kg')?'selected':''}}>KG</option>
											<option value="buah" {{(Input::old('unit') == 'buah')?'selected':''}}>@lang('general.label.Buah')</option>
											<option value="set" {{(Input::old('unit') == 'set')?'selected':''}}>Set</option>
										</select>
										<span class="help-block">{{@$errors->first('jumlah_produk_set')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.raw_material_name')</label>
									</div>
									<div class="col-sm-12">
										<input type="text" name="raw_material_name" id="raw_material_name" class="form-control" value="{{input::old('raw_material_name')}}" >
										<span class="help-block">{{@$errors->first('raw_material_name')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.provider')</label>
									</div>
									<div class="col-sm-12">
										<input type="text" name="provider" id="provider" class="form-control" value="{{input::old('provider')}}" >
										<span class="help-block">{{@$errors->first('nama_pemasok')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.bahan_baku_th')</label>
									</div>
									<div class="col-sm-12">
										<div class="input-group">
											<input type="hidden" name="bahan_baku_th" value="{{input::old('bahan_baku_th')}}">
											<input type="text" name="bahan_baku_thT" id="bahan_baku_th" class="form-control number-dec currency-this" data-real-source="bahan_baku_th" value="{{input::old('bahan_baku_thT')}}" ><span class="input-group-addon">ton</span>
										</div>
										<span class="help-block">{{@$errors->first('bahan_baku_th')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.bahan_baku_rp')</label>
									</div>
									<div class="col-sm-12">
										<div class="input-group">
											<input type="hidden" name="bahan_baku_rp" value="{{input::old('bahan_baku_rp')}}">
											<input type="text" name="bahan_baku_rpT" id="bahan_baku_rp" class="form-control number-dec currency-this" data-real-source="bahan_baku_rp" value="{{input::old('bahan_baku_rpT')}}" ><span class="input-group-addon">rupiah</span>
										</div>
										<span class="help-block">{{@$errors->first('bahan_baku_rp')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.equipment_name')</label>
									</div>
									<div class="col-sm-12">
										<input type="text" name="equipment_name" id="equipment_name" class="form-control" value="{{input::old('equipment_name')}}" >
										<span class="help-block">{{@$errors->first('equipment_name')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.harga_mesin')</label>
									</div>
									<div class="col-sm-12">
										<input type="hidden" name="harga_mesin" value="{{input::old('harga_mesin')}}">
										<input type="text" name="harga_mesinT" id="harga_mesin" class="form-control number-dec currency-this" data-real-source="harga_mesin" value="{{input::old('harga_mesinT')}}">
										<span class="help-block">{{@$errors->first('harga_mesin')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.jumlah_mesin')</label>
									</div>
									<div class="col-sm-6">
										<div class="input-group">
											<input type="hidden" name="jumlah_mesin" value="{{input::old('jumlah_mesin')}}">
											<input type="text" name="jumlah_mesinT" id="jumlah_mesin" class="form-control number-dec currency-this" data-real-source="jumlah_mesin" value="{{input::old('jumlah_mesinT')}}"><span class="input-group-addon">@lang('general.label.buah')</span>
										</div>
										<span class="help-block">{{@$errors->first('jumlah_mesin')}}</span>
									</div>
									<div class="col-sm-6">
										<div class="input-group">
											<input type="hidden" name="kapasitas_mesin" value="{{input::old('kapasitas_mesin')}}">
											<input type="text" name="kapasitas_mesinT" id="kapasitas_mesin" class="form-control number-dec currency-this" data-real-source="kapasitas_mesin" value="{{input::old('kapasitas_mesinT')}}"><span class="input-group-addon">ton/kg</span>
										</div>
										<span class="help-block">{{@$errors->first('kapasitas_mesin')}}</span>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.nama_brand')</label>
									</div>
									<div class="col-sm-12">
										<input type="text" name="nama_brand" id="nama_brand" class="form-control" value="{{input::old('nama_brand')}}">
										<span class="help-block">{{@$errors->first('nama_brand')}}</span>
									</div>
								</div>
							</div>
<!-- 							<div class="col-sm-6">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>HKI</label>
									</div>
									<div class="col-sm-12 radio">
										<label class="radio-inline"><input type="radio" name="hki" id="hki_0" value="0"><span class="checkmark"></span> Tidak Ada</label>
										<label class="radio-inline"><input type="radio" name="hki" id="hki_1" value="1"><span class="checkmark"></span> Ada</label>
									</div>
								</div>
							</div> -->
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="regis-form-box">
				<a href="javascript:void(0);" class="drop-item"><h4 class="text-uppercase">@lang('general.title.tenant.step6')</h4></a>
				<div class="regis-form-box2 row" id="step5">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.marketing_type')</label>
									</div>
									<div class="col-sm-12 radio">
										<label class="radio-inline"><input type="radio" name="marketing_type" id="marketing_type_0" value="0" {{(Input::old('marketing_type') == '0')?'checked':''}}><span class="checkmark"></span> Offline</label>
										<label class="radio-inline"><input type="radio" name="marketing_type" id="marketing_type_1" value="1" {{(Input::old('marketing_type') == '1')?'checked':''}}><span class="checkmark"></span> Online</label>
									</div>
								</div>
							</div>
							<div class="col-sm-12 online-set">
								<div class="form-group row online-box">
									@if(Input::old('online_market_type'))
										@foreach(Input::old('online_market_type') as $key => $value)
										<div class="row">
											<div class="col-sm-12">
												<label>@lang('general.label.tenant-page.online_market')</label>
											</div>
											<div class="col-xs-12 col-sm-4">
												<select name="online_market_type[]" class="select2 form-control">
													<option value='' selected disabled></option>
													<option value="web" {{($value=='web')?'selected':''}}> Web Mandiri</option>
													<option value="whatsapp" {{($value=='whatsapp')?'selected':''}}> WA</option>
													<option value="bbm" {{($value=='bbm')?'selected':''}}> BBM</option>
													<option value="facebook" {{($value=='facebook')?'selected':''}}> FB</option>
													<option value="e-commerce" {{($value=='e-commerce')?'selected':''}}> Situs E-commerce</option>
													<option value="instagram" {{($value=='instagram')?'selected':''}}> IG</option>
													<option value="twitter" {{($value=='twitter')?'selected':''}}> TW</option>
													<option value="other" {{($value=='other')?'selected':''}}> Lainnya</option>
												</select>
											</div>
											<div class="col-xs-12 col-sm-8">
												<div class="wa">
													<input type="text" name="online_market_url[]" value="{{Input::old('online_market_url.'.$key)}}" class="form-control">
												</div>
											</div>
										</div>
										@endforeach
									@else
										<div class="row">
											<div class="col-sm-12">
												<label>@lang('general.label.tenant-page.online_market')</label>
											</div>
											<div class="col-xs-12 col-sm-4">
												<select name="online_market_type[]" class="select2 form-control">
													<option value='' selected disabled></option>
													<option value="web"> Web Mandiri</option>
													<option value="whatsapp"> WA</option>
													<option value="bbm"> BBM</option>
													<option value="facebook"> FB</option>
													<option value="e-commerce"> Situs E-commerce</option>
													<option value="instagram"> IG</option>
													<option value="twitter"> TW</option>
													<option value="other"> Lainnya</option>
												</select>
											</div>
											<div class="col-xs-12 col-sm-8">
												<div class="wa">
													<input type="text" name="online_market_url[]" value="" class="form-control">
												</div>
											</div>
										</div>
									@endif
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<div class="add-more right">
											<span>@lang('general.label.tenant-page.manage_online_market') </span>
											<a href="javascript:void(0)" class="remove-online"><i class="fa fa-minus-circle fa-2x"></i></a>
											<a href="javascript:void(0)" class="add-online"><i class="fa fa-plus-circle fa-2x"></i></a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.jam_buka')</label>
									</div>
									<div class="col-sm-12">
										<input type="time" name="jam_buka" id="jam_buka" class="form-control" value="{{Input::old('jam_buka')}}">
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.jam_tutup')</label>
									</div>
									<div class="col-sm-12">
										<input type="time" name="jam_tutup" id="jam_tutup" class="form-control" value="{{Input::old('jam_tutup')}}">
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.fasilitas_toko')</label>
									</div>
									<div class="col-sm-12 checkbox">
										<label class="checkbox-inline"><input type="checkbox" name="fasilitas_toko[0]" id="fasilitas_toko_1" value="parkir" {{(Input::old('fasilitas_toko.0') == 'parkir')?'checked':''}}><span class="checkboxmark"></span> Parkir</label>
										<label class="checkbox-inline"><input type="checkbox" name="fasilitas_toko[1]" id="fasilitas_toko_2" value="toilet" {{(Input::old('fasilitas_toko.1') == 'toilet')?'checked':''}}><span class="checkboxmark"></span> Toilet</label>
										<label class="checkbox-inline"><input type="checkbox" name="fasilitas_toko[2]" id="fasilitas_toko_3" value="mushola" {{(Input::old('fasilitas_toko.2') == 'mushola')?'checked':''}}><span class="checkboxmark"></span> Mushola</label>
										<label class="checkbox-inline"><input type="checkbox" name="fasilitas_toko[3]" id="fasilitas_toko_4" value="showroom" {{(Input::old('fasilitas_toko.3') == 'showroom')?'checked':''}}><span class="checkboxmark"></span> Showroom</label>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.luas_area')</label>
									</div>
									<div class="col-sm-12">
										<input type="hidden" name="luas_area" value="{{Input::old('luas_area')}}">
										<input type="text" name="luas_areaT" id="luas_area" class="form-control number-dec currency-this" data-real-source="luas_area" value="{{Input::old('luas_areaT')}}">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="regis-form-box">
				<a href="javascript:void(0);" class="drop-item"><h4 class="text-uppercase">@lang('general.title.tenant.step7')</h4></a>
				<div class="regis-form-box2 row" id="step7">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.jumlah_karyawan')</label>
									</div>
									<div class="col-sm-4">
										<div class="input-group">
											<span class="input-group-addon">@lang('general.label.tenant-page.jumlah_karyawan2')</span>
											<input type="text" name="jumlah_karyawan_pria" id="jumlah_karyawan_pria" class="form-control number-only" value="{{Input::old('jumlah_karyawan_pria')}}">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="input-group">
											<span class="input-group-addon">@lang('general.label.tenant-page.jumlah_karyawan3')</span>
											<input type="text" name="jumlah_karyawan_wanita" id="jumlah_karyawan_wanita" class="form-control number-only" value="{{Input::old('jumlah_karyawan_wanita')}}">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="input-group">
											<span class="input-group-addon">@lang('general.label.tenant-page.jumlah_karyawan1')</span>
											<input type="text" name="jumlah_karyawan_total" id="jumlah_karyawan_total" class="form-control number-only" readonly="true" value="{{Input::old('jumlah_karyawan_total')}}">
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.minimum_salary')</label>
									</div>
									<div class="col-sm-12 radio">
										<select name="minimum_salary" id="minimum_salary" class="select2 form-control">
											<option value='' selected disabled></option>
											<option value="<= 1jt" {{(Input::old('minimum_salary') == '<= 1jt')?'selected':''}}><= 1 juta</option>
											<option value="1.1jt - 1.5jt" {{(Input::old('minimum_salary') == '1.1jt - 1.5jt')?'selected':''}}> 1.1 juta - 1.5 juta</option>
											<option value="1.6jt - 2jt" {{(Input::old('minimum_salary') == '1.6jt - 2jt')?'selected':''}}> 1.6 juta - 2 juta</option>
											<option value="2.1jt - 2.5jt" {{(Input::old('minimum_salary') == '2.1jt - 2.5jt')?'selected':''}}> 2.1 juta - 2.5 juta</option>
											<option value="2.6jt - 3jt" {{(Input::old('minimum_salary') == '2.6jt - 3jt')?'selected':''}}> 2.6 juta - 3 juta</option>
											<option value="> 3jt" {{(Input::old('minimum_salary') == '> 3jt')?'selected':''}}> > 3 juta</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group row edu-regis-tenant">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.edu_major')</label>
									</div>
									<div class="col-sm-3">
										<div class="input-group" title="SD / Elementary School">
											<span class="input-group-addon">SD/ES</span>
											<input type="text" name="edu_major[sd]" id="edu_major_sd" class="form-control number-only" value="{{Input::old('edu_major.sd')}}">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="input-group" title="SMP / Junior High">
											<span class="input-group-addon">SMP/JH</span>
											<input type="text" name="edu_major[smp]" id="edu_major_smp" class="form-control number-only" value="{{Input::old('edu_major.smp')}}">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="input-group" title="SMA / Senior High">
											<span class="input-group-addon">SMA/SH</span>
											<input type="text" name="edu_major[sma]" id="edu_major_sma" class="form-control number-only" value="{{Input::old('edu_major.sma')}}">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="input-group" title="D1 / Associate Degree">
											<span class="input-group-addon">D1/AD</span>
											<input type="text" name="edu_major[d1]" id="edu_major_d1" class="form-control number-only" value="{{Input::old('edu_major.d1')}}">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="input-group" title="D3 / Associate Degree">
											<span class="input-group-addon">D3/AD</span>
											<input type="text" name="edu_major[d3]" id="edu_major_d3" class="form-control number-only" value="{{Input::old('edu_major.d3')}}">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="input-group" title="S1 /  Bachelor's Degree">
											<span class="input-group-addon">S1/BD</span>
											<input type="text" name="edu_major[s1]" id="edu_major_s1" class="form-control number-only" value="{{Input::old('edu_major.s1')}}">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="input-group" title="S2 / Master's Degree">
											<span class="input-group-addon">S2/MD</span>
											<input type="text" name="edu_major[s2]" id="edu_major_s2" class="form-control number-only" value="{{Input::old('edu_major.s2')}}">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="input-group" title="S3 / Doctoral Degree">
											<span class="input-group-addon">S3/DD</span>
											<input type="text" name="edu_major[s3]" id="edu_major_s3" class="form-control number-only" value="{{Input::old('edu_major.s3')}}">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="regis-form-box">
				<a href="javascript:void(0);" class="drop-item"><h4 class="text-uppercase">@lang('general.title.tenant.step8')</h4></a>
				<div class="regis-form-box2 row" id="step8">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.asset')</label>
									</div>
									<div class="col-sm-12">
										<div class="input-group">
											<span class="input-group-addon">@lang('general.label.rp')</span>
											<input type="hidden" name="asset" value="{{Input::old('asset')}}">
											<input type="text" name="assetT" id="asset" class="form-control number-dec currency-this" data-real-source="asset" value="{{Input::old('assetT')}}">
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.omset')</label>
									</div>
									<div class="col-sm-4">
										<div class="input-group">
											<input type="hidden" name="omset[hari]" value="{{Input::old('omset.hari')}}">
											<input type="text" name="omsetT[hari]" id="omset_hari" class="form-control number-dec currency-this" data-real-source="omset[hari]" value="{{Input::old('omsetT.hari')}}">
											<span class="input-group-addon">/ @lang('general.label.day')</span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="input-group">
											<input type="hidden" name="omset[bulan]" value="{{Input::old('omset.bulan')}}">
											<input type="text" name="omsetT[bulan]" id="omset_bulan" class="form-control number-dec currency-this" data-real-source="omset[bulan]" value="{{Input::old('omsetT.bulan')}}">
											<span class="input-group-addon">/ @lang('general.label.month')</span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="input-group">
											<input type="hidden" name="omset[tahun]" value="{{Input::old('omset.tahun')}}">
											<input type="text" name="omsetT[tahun]" id="omset_tahun" class="form-control number-dec currency-this" data-real-source="omset[tahun]" value="{{Input::old('omsetT.tahun')}}">
											<span class="input-group-addon">/ @lang('general.label.year')</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.netto')</label>
									</div>
									<div class="col-sm-4">
										<div class="input-group">
											<input type="hidden" name="netto[hari]" value="{{Input::old('netto.hari')}}">
											<input type="text" name="nettoT[hari]" id="netto_hari" class="form-control number-dec currency-this" data-real-source="netto[hari]" value="{{Input::old('nettoT.hari')}}">
											<span class="input-group-addon">/ @lang('general.label.day')</span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="input-group">
											<input type="hidden" name="netto[bulan]" value="{{Input::old('netto.bulan')}}">
											<input type="text" name="nettoT[bulan]" id="netto_bulan" class="form-control number-dec currency-this" data-real-source="netto[bulan]" value="{{Input::old('nettoT.bulan')}}">
											<span class="input-group-addon">/ @lang('general.label.month')</span>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="input-group">
											<input type="hidden" name="netto[tahun]" value="{{Input::old('netto.tahun')}}">
											<input type="text" name="nettoT[tahun]" id="netto_tahun" class="form-control number-dec currency-this" data-real-source="netto[tahun]" value="{{Input::old('nettoT.tahun')}}">
											<span class="input-group-addon">/ @lang('general.label.year')</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group row">
									<div class="col-sm-12">
										<label>@lang('general.label.tenant-page.financing_access')</label>
									</div>
									<div class="col-sm-12 radio">
										<label class="radio-inline"><input type="radio" name="financing_access" id="financing_access_1" value="mudah" {{(Input::old('financing_access') == 'mudah')?'checked':''}}><span class="checkmark"></span> @lang('general.label.tenant-page.financing_access1')</label>
										<label class="radio-inline"><input type="radio" name="financing_access" id="financing_access_2" value="sedang" {{(Input::old('financing_access') == 'sedang')?'checked':''}}><span class="checkmark"></span> @lang('general.label.tenant-page.financing_access2')</label>
										<label class="radio-inline"><input type="radio" name="financing_access" id="financing_access_3" value="sukar" {{(Input::old('financing_access') == 'sukar')?'checked':''}}><span class="checkmark"></span> @lang('general.label.tenant-page.financing_access3')</label>
									</div>
								</div>
							</div>
							<div class="col-sm-12 investment-box">
								@if(Input::old('sumber_pembiayaan'))
									@foreach(Input::old('sumber_pembiayaan') as $key => $value)
										<div class="form-group row">
											<div class="col-sm-12">
												<label>@lang('general.label.tenant-page.sumber_pembiayaan')</label>
											</div>
											<div class="col-sm-12">
												<input type="text" name="sumber_pembiayaan[]" multiple="true" class="form-control" value="{{$value}}">
											</div>
										</div>
									@endforeach
								@else
									<div class="form-group row">
										<div class="col-sm-12">
											<label>@lang('general.label.tenant-page.sumber_pembiayaan')</label>
										</div>
										<div class="col-sm-12">
											<input type="text" name="sumber_pembiayaan[]" id="sumber_pembiayaan" multiple="true" class="form-control">
										</div>
									</div>
								@endif
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<div class="add-more right">
										<span>@lang('general.label.tenant-page.manage_investment') </span>
										<a href="javascript:void(0)" class="remove-investment"><i class="fa fa-minus-circle fa-2x"></i></a>
										<a href="javascript:void(0)" class="add-investment"><i class="fa fa-plus-circle fa-2x"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="regis-form-box">
				<a href="javascript:void(0);" class="drop-item"><h4 class="text-uppercase">@lang('general.title.tenant.step9')</h4></a>
				<div class="regis-form-box2 row" id="step9">
					<div class="col-sm-12 coaching-box">
						@if(Input::old('coach_name'))
							@foreach(Input::old('coach_name') as $key => $value)
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group row">
											<div class="col-sm-12">
												<label>@lang('general.label.tenant-page.coach_name')</label>
											</div>
											<div class="col-sm-12">
												<input type="text" name="coach_name[]" class="form-control" value="{{Input::old('coach_name.'.$key)}}">
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group row">
											<div class="col-sm-12">
												<label>@lang('general.label.tenant-page.coaching_type')</label>
											</div>
											<div class="col-sm-12">
												<input type="text" name="coaching_type[]" class="form-control" value="{{Input::old('coaching_type.'.$key)}}">
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group row">
											<div class="col-sm-12">
												<label>@lang('general.label.tenant-page.coaching_year')</label>
											</div>
											<div class="col-sm-12">
												<select name="coaching_year[]" class="select2 form-control">
													<option value='' selected disabled>@lang('general.label.year')</option>
													@for($i=(int)date('Y');$i>=(int)date('Y')-50;$i--)
														<option value="{{$i}}" {{(Input::old('coaching_year.'.$key) == $i)?'selected':''}}>{{$i}}</option>
													@endfor
												</select>
											</div>
										</div>
									</div>
							@endforeach
						@else
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group row">
										<div class="col-sm-12">
											<label>@lang('general.label.tenant-page.coach_name')</label>
										</div>
										<div class="col-sm-12">
											<input type="text" name="coach_name[]" class="form-control">
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group row">
										<div class="col-sm-12">
											<label>@lang('general.label.tenant-page.coaching_type')</label>
										</div>
										<div class="col-sm-12">
											<input type="text" name="coaching_type[]" class="form-control">
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group row">
										<div class="col-sm-12">
											<label>@lang('general.label.tenant-page.coaching_year')</label>
										</div>
										<div class="col-sm-12">
											<select name="coaching_year[]" class="select2 form-control">
												<option value='' selected disabled>@lang('general.label.year')</option>
												@for($i=(int)date('Y');$i>=(int)date('Y')-50;$i--)
													<option value="{{$i}}">{{$i}}</option>
												@endfor
											</select>
										</div>
									</div>
								</div>
							</div>
						@endif
					</div>
					<div class="col-xs-12">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<div class="add-more right">
										<span>@lang('general.label.tenant-page.manage_coaching') </span>
										<a href="javascript:void(0)" class="remove-coaching"><i class="fa fa-minus-circle fa-2x"></i></a>
										<a href="javascript:void(0)" class="add-coaching"><i class="fa fa-plus-circle fa-2x"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="regis-form-box">
				<a href="javascript:void(0);" class="drop-item"><h4 class="text-uppercase">@lang('general.title.tenant.step10')</h4></a>
				<div class="regis-form-box2 row" id="step10">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-12 community-box">
								@if(Input::old('community_name'))
									@foreach(Input::old('community_name') as $key => $value)
										<div class="form-group row">
											<div class="col-sm-12">
												<label>@lang('general.label.tenant-page.community_name')</label>
											</div>
											<div class="col-sm-12">
												<input type="text" name="community_name[]" class="form-control" value="{{$value}}">
											</div>
										</div>
									@endforeach
								@else
									<div class="form-group row">
										<div class="col-sm-12">
											<label>@lang('general.label.tenant-page.community_name')</label>
										</div>
										<div class="col-sm-12">
											<input type="text" name="community_name[]" class="form-control">
										</div>
									</div>
								@endif
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<div class="add-more right">
										<span>@lang('general.label.tenant-page.manage_community') </span>
										<a href="javascript:void(0)" class="remove-community"><i class="fa fa-minus-circle fa-2x"></i></a>
										<a href="javascript:void(0)" class="add-community"><i class="fa fa-plus-circle fa-2x"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="regis-form-box">
				<a href="javascript:void(0);" class="drop-item"><h4 class="text-uppercase">@lang('general.title.tenant.step11')</h4></a>
				<div class="regis-form-box2 row" id="step12">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-12 strength-box">
								@if(Input::old('business_strength'))
									@foreach(Input::old('business_strength') as $key => $value)
										<div class="form-group row">
											<div class="col-sm-12">
												<label>@lang('general.label.tenant-page.business_strength')</label>
											</div>
											<div class="col-sm-12">
												<textarea name="business_strength[]" class="form-control" placeholder="">{{$value}}</textarea>
											</div>
										</div>
									@endforeach
								@else
									<div class="form-group row">
										<div class="col-sm-12">
											<label>@lang('general.label.tenant-page.business_strength')</label>
										</div>
										<div class="col-sm-12">
											<textarea name="business_strength[]" id="business_strength" class="form-control" placeholder=""></textarea>
										</div>
									</div>
								@endif
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<div class="add-more right">
										<span>@lang('general.label.tenant-page.manage_strength') </span>
										<a href="javascript:void(0)" class="remove-strength"><i class="fa fa-minus-circle fa-2x"></i></a>
										<a href="javascript:void(0)" class="add-strength"><i class="fa fa-plus-circle fa-2x"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="regis-form-box">
				<a href="javascript:void(0);" class="drop-item"><h4 class="text-uppercase">@lang('general.title.tenant.step12')</h4></a>
				<div class="regis-form-box2 row" id="step6">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-12 opportunity-box">
								@if(Input::old('business_opportunity'))
									@foreach(Input::old('business_opportunity') as $key => $value)
										<div class="form-group row">
											<div class="col-sm-12">
												<label>@lang('general.label.tenant-page.business_opportunity')</label>
											</div>
											<div class="col-sm-12">
												<textarea name="business_opportunity[]" class="form-control" placeholder="">{{$value}}</textarea>
											</div>
										</div>
									@endforeach
								@else
									<div class="form-group row">
										<div class="col-sm-12">
											<label>@lang('general.label.tenant-page.business_opportunity')</label>
										</div>
										<div class="col-sm-12">
											<textarea name="business_opportunity[]" id="business_opportunity" class="form-control" placeholder=""></textarea>
										</div>
									</div>
								@endif
								
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<div class="add-more right">
										<span>@lang('general.label.tenant-page.manage_opportunity') </span>
										<a href="javascript:void(0)" class="remove-opportunity"><i class="fa fa-minus-circle fa-2x"></i></a>
										<a href="javascript:void(0)" class="add-opportunity"><i class="fa fa-plus-circle fa-2x"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<div class="col-sm-12 register-tenant-sbm">
				<div class="form-group row">
					<div class="col-sm-12 right">
						<label class="checkbox-inline" style="border: none;">
							<input type="checkbox" name="term" value="1">
							<span class="checkboxmark"></span>
							<a href="#" data-toggle="modal" data-target="#modal-term" style="text-decoration: none;">I accept Terms and Conditions</a>
						</label>
						<!-- <input type="submit" name="register-tenant-sbm" value="FINISH" class="btn btn-orange"> -->
						<button type="button" name="register-tenant-sbm" class="btn btn-orange" disabled="true">@lang('general.label.tenant-page.register-tenant-sbm')</button>
					</div>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
	@include('auth.modals._terms')
@stop
@section('custom_scripts')
	<script type="text/javascript" src="{{asset('vendor/bootzard/js/jquery.backstretch.min.js')}}"></script>
	<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('vendor/bootzard/js/scripts.js')}}"></script>
	{{-- JsValidator::formRequest('App\Http\Requests\TenantRegisterRequest') --}}
	<script type="text/javascript">
		var inc_address;
		$('input[type=checkbox][name=term]').click(function(){
			if($(this).is(':checked')){
				$('button[name=register-tenant-sbm]').removeAttr('disabled');
			}
			else{
				$('button[name=register-tenant-sbm]').attr('disabled',true);	
			}
		});
		$('button[name=register-tenant-sbm]').click(function(){
			document.getElementById('register-tenant-form').submit();
		});
		var selected_document = [];
		$('.select2').select2();
		$('.drop-item').click(function(){
			$('.select2').css('width','100%');
			if($(this).next('.regis-form-box2').is(':visible')){
				$(this).next('.regis-form-box2').slideUp('fast');
			}
			else{
				$(this).next('.regis-form-box2').slideDown('slow');
			}
		});

		$('#legal_document').change(function(){
			var html = '';
			var selected_vals = $(this).val();
			selected_document.filter( function(n) { 
				if(!this.has(n)){
					$('#legal_'+n).remove();
				}
			}, new Set(selected_vals) );
			var sle = {};
			$(this).select2('data').map(function(data,key){
				sle[data.id] = data.text;
			});
			selected_vals.filter( function(n) { 
				if(!this.has(n)){
					html += '<div id="legal_'+n+'"><label style="font-weight:normal;font-size:small;">'+sle[n]+'</label><input type="file" name="tenant_legality_images['+n+']" id="legal_document_file" class="form-control"></div>';
				}
			}, new Set(selected_document) );
			// $(this).select2('data').forEach(function(data,key){
			// 	if(selected_document[data] == undefined || selected_document[data] == null || selected_document[data] == ''){
			// 		html += '<div class="legal_'+data.id+'"><label style="font-weight:normal;font-size:small;">'+data.text+'</label><input type="file" name="tenant_legality_images['+data.id+']" id="legal_document_file" class="form-control"></div>';
			// 	}
			// });
			selected_document = selected_vals;
			$('.render-legality').append(html);
		});
		$(document).ready(function(){
			$('.select2').css('width','100%');
			@if(@$errors->first('company_name') ||
				@$errors->first('company_year') ||
				@$errors->first('company_month') ||
				@$errors->first('address_street') ||
				@$errors->first('address_subdistrict_id') ||
				@$errors->first('rtrw') ||
				@$errors->first('kelurahan') ||
				@$errors->first('address_postal_code') ||
				@$errors->first('address_latitude') ||
				@$errors->first('address_longitude') ||
				@$errors->first('tenant_email') ||
				@$errors->first('tenant_phone') ||
				Input::old('company_name') ||
				Input::old('company_year') ||
				Input::old('company_month') ||
				Input::old('address_street') ||
				Input::old('address_subdistrict_id') ||
				Input::old('rtrw') ||
				Input::old('kelurahan') ||
				Input::old('address_postal_code') ||
				Input::old('address_latitude') ||
				Input::old('address_longitude') ||
				Input::old('tenant_email') ||
				Input::old('tenant_phone'))
				$('#step1').show();
			@endif
			@if(@$errors->first('founder_name') ||
				@$errors->first('founder_email') ||
				@$errors->first('founder_last_education') ||
				@$errors->first('founder_last_education_department') ||
				@$errors->first('hp') ||
				@$errors->first('gender') ||
				@$errors->first('npwp_type') ||
				Input::old('founder_name') ||
				Input::old('founder_email') ||
				Input::old('founder_last_education') ||
				Input::old('founder_last_education_department') ||
				Input::old('hp') ||
				Input::old('gender') ||
				Input::old('npwp_type'))
				$('#step11').show();
			@endif
			@if(@$errors->first('tenant_category') || 
				@$errors->first('business_type') ||
				@$errors->first('legality_doc_name') || 
				@$errors->first('tenant_legality_images') || 
				@$errors->first('hki') || 
				Input::old('business_type') || 
				Input::old('legality_doc_name') || 
				Input::old('tenant_legality_images') ||
				Input::old('hki') || 
				Input::old('tenant_category'))
				$('#step2').show();
			@endif
			@if(@$errors->first('tenant_category') || 
				@$errors->first('jenis_usaha_kreatif') ||
				Input::old('tenant_category') || 
				Input::old('jenis_usaha_kreatif'))
				$('#step3').show();
			@endif
			@if(@$errors->first('bentuk_usaha') ||
				@$errors->first('jenis_produk') ||
				@$errors->first('karakter_produk') ||
				@$errors->first('quantity') ||
				@$errors->first('unit') ||
				@$errors->first('raw_material_name') ||
				@$errors->first('provider') ||
				@$errors->first('jumlah_produk_kg') ||
				@$errors->first('jumlah_produk_set') ||
				@$errors->first('bahan_baku_th') ||
				@$errors->first('bahan_baku_rp') ||
				@$errors->first('equipment_name') ||
				@$errors->first('nama_pemasok') ||
				@$errors->first('jumlah_mesin') ||
				@$errors->first('kapasitas_mesin') ||
				@$errors->first('harga_mesin') ||
				@$errors->first('nama_brand') ||
				@$errors->first('hak_cipta') ||
				Input::old('bentuk_usaha') ||
				Input::old('jenis_produk') ||
				Input::old('karakter_produk') ||
				Input::old('quantity') ||
				Input::old('unit') ||
				Input::old('raw_material_name') ||
				Input::old('provider') ||
				Input::old('jumlah_produk_kg') ||
				Input::old('jumlah_produk_set') ||
				Input::old('bahan_baku_th') ||
				Input::old('bahan_baku_rp') ||
				Input::old('equipment_name') ||
				Input::old('nama_pemasok') ||
				Input::old('jumlah_mesin') ||
				Input::old('kapasitas_mesin') ||
				Input::old('harga_mesin') ||
				Input::old('nama_brand') ||
				Input::old('hak_cipta'))
				$('#step4').show();
			@endif
			@if(@$errors->first('marketing_type') || 
				@$errors->first('online_market_type') ||
				@$errors->first('online_market_url') ||
				@$errors->first('jam_buka') ||
				@$errors->first('jam_tutup') ||
				@$errors->first('fasilitas_toko') ||
				@$errors->first('luas_area') ||
				Input::old('marketing_type') || 
				Input::old('online_market_type') ||
				Input::old('online_market_url') ||
				Input::old('jam_tutup') ||
				Input::old('fasilitas_toko') ||
				Input::old('luas_area') ||
				Input::old('jam_buka'))
				$('#step5').show();
			@endif
			@if(@$errors->first('jumlah_karyawan_pria') || 
				@$errors->first('jumlah_karyawan_wanita') ||
				@$errors->first('jumlah_karyawan_total') ||
				@$errors->first('minimum_salary') ||
				@$errors->first('edu_major') ||
				Input::old('jumlah_karyawan_pria') || 
				Input::old('jumlah_karyawan_wanita') ||
				Input::old('jumlah_karyawan_total') ||
				Input::old('minimum_salary') ||
				Input::old('edu_major'))
				$('#step7').show();
			@endif
			@if(@$errors->first('asset') || 
				@$errors->first('omset') ||
				@$errors->first('financing_access') ||
				@$errors->first('sumber_pembiayaan') ||
				Input::old('asset') || 
				Input::old('omset') ||
				Input::old('financing_access') ||
				Input::old('sumber_pembiayaan'))
				$('#step8').show();
			@endif
			@if(@$errors->first('coach_name') || 
				@$errors->first('coaching_type') ||
				@$errors->first('coaching_year') ||
				Input::old('coach_name') || 
				Input::old('coaching_type') ||
				Input::old('coaching_year'))
				$('#step9').show();
			@endif
			@if(@$errors->first('community_name') || 
				Input::old('community_name'))
				$('#step10').show();
			@endif
			@if(@$errors->first('business_strength') || 
				Input::old('business_strength'))
				$('#step12').show();
			@endif
			@if(@$errors->first('business_opportunity') || 
				Input::old('business_opportunity'))
				$('#step6').show();
			@endif
			if($('#legal_document').val()){
				var html = '';
				var selected_vals = $('#legal_document').val();
				var sle = {};
				$('#legal_document').select2('data').map(function(data,key){
					sle[data.id] = data.text;
				});
				selected_vals.filter( function(n) { 
						html += '<div id="legal_'+n+'"><label style="font-weight:normal;font-size:small;">'+sle[n]+'</label><input type="file" name="tenant_legality_images['+n+']" id="legal_document_file" class="form-control"></div>';
				}, new Set(selected_document) );
				selected_document = selected_vals;
				$('.render-legality').append(html);
			}
		});
		$('.add-address').click(function(){
			
		});

		$('.add-opportunity').click(function(){
			addBox('.opportunity-box','opportunity');
		});
		$('.remove-opportunity').click(function(){
			removeBox('.opportunity-box');
		});

		$('.add-strength').click(function(){
			addBox('.strength-box','strength');
		});
		$('.remove-strength').click(function(){
			removeBox('.strength-box');
		});

		$('.add-coaching').click(function(){
			addBox('.coaching-box','coaching');
		});
		$('.remove-coaching').click(function(){
			removeBox('.coaching-box');
		});

		$('.add-investment').click(function(){
			addBox('.investment-box','investment');
		});
		$('.remove-investment').click(function(){
			removeBox('.investment-box');
		});

		$('.add-online').click(function(){
			addBox('.online-box','online');
		});
		$('.remove-online').click(function(){
			removeBox('.online-box');
		});

		$('.add-address').click(function(){
			addBox('.address-box','address',$('.address-box').children().length);

		});
		$('.remove-address').click(function(){
			removeBox('.address-box');
		});

		$('.add-community').click(function(){
			addBox('.community-box','community');
		});
		$('.remove-community').click(function(){
			removeBox('.community-box');
		});

		function addBox(tbox,tpartial,boxChildren=null){
			$.ajax({
				url: "{{route('tennants.sign_up.partial')}}",
				method: 'get',
				data: {
					partial: tpartial,
					box_children: boxChildren
				},
				dataType: 'html',
				success: function(html){
					$(tbox).append(html);
					if(tpartial == 'address'){
						
					}
					$('.select2').select2();
				},
				error: function(){
					console.log('add box error');
				}
			});
		}

		function removeBox(tbox){
			if($(tbox).children().length > 1){
				$(tbox).children().last().detach();
			}
		}

		$('input[name=marketing_type').click(function(){
			if($(this).val() == 1 || $(this).val() == '1'){
				$('.online-set').show();
			}
			else{
				$('.online-set').hide();	
			}
		});

		$('input[name=bahan_baku_rpT]').keyup(function(){
			$('input[name=bahan_baku_rp]').val(fromMoneyIDR($(this)));
			$(this).val(toMoneyIDR($(this),$('input[name=bahan_baku_rp]')));
		});

		$('input[name=bahan_baku_rpT]').change(function(){
			$('input[name=bahan_baku_rp]').val(fromMoneyIDR($(this)));
			$(this).val(toMoneyIDR($(this),$('input[name=bahan_baku_rp]')));
		});

		$('input[name=harga_mesinT]').keyup(function(){
			$('input[name=harga_mesin]').val(fromMoneyIDR($(this)));
			$(this).val(toMoneyIDR($(this),$('input[name=harga_mesin]')));
		});

		$('input[name=harga_mesinT]').change(function(){
			$('input[name=harga_mesin]').val(fromMoneyIDR($(this)));
			$(this).val(toMoneyIDR($(this),$('input[name=harga_mesin]')));
		});

		$('input[name=assetT]').keyup(function(){
			$('input[name=asset]').val(fromMoneyIDR($(this)));
			$(this).val(toMoneyIDR($(this),$('input[name=asset]')));
		});

		$('input[name=assetT]').change(function(){
			$('input[name=asset]').val(fromMoneyIDR($(this)));
			$(this).val(toMoneyIDR($(this),$('input[name=asset]')));
		});

		$('input[name=omsetT]').keyup(function(){
			$('input[name=omset]').val(fromMoneyIDR($(this)));
			$(this).val(toMoneyIDR($(this),$('input[name=omset]')));
		});

		$('input[name=omsetT]').change(function(){
			$('input[name=omset]').val(fromMoneyIDR($(this)));
			$(this).val(toMoneyIDR($(this),$('input[name=omset]')));
		});

		$('input[name=nettoT]').keyup(function(){
			$('input[name=netto]').val(fromMoneyIDR($(this)));
			$(this).val(toMoneyIDR($(this),$('input[name=netto]')));
		});

		$('input[name=nettoT]').change(function(){
			$('input[name=netto]').val(fromMoneyIDR($(this)));
			$(this).val(toMoneyIDR($(this),$('input[name=netto]')));
		});

		$('input[name=jumlah_karyawan_pria]').change(function(){
			var pria = parseInt($(this).val());
			var wanita = 0;
			if($('input[name=jumlah_karyawan_wanita]').val()){
				wanita = parseInt($('input[name=jumlah_karyawan_wanita]').val());
			}
			$('input[name=jumlah_karyawan_total]').val(pria+wanita);
		});

		$('input[name=jumlah_karyawan_wanita]').change(function(){
			var pria = 0;
			var wanita = parseInt($(this).val());
			if($('input[name=jumlah_karyawan_pria]').val()){
				pria = parseInt($('input[name=jumlah_karyawan_pria]').val());
			}
			$('input[name=jumlah_karyawan_total]').val(pria+wanita);
		});

		$(document).ready(function(){
			@if(Input::old('address_subdistrict_id') && Input::old('kelurahan'))
				getVillage("{{Input::old('address_subdistrict_id')}}","{{Input::old('kelurahan')}}");
			@endif
		});

		function getVillage(e,selected_id=null){
			let id = e;
			if(typeof e != 'string'){
				id = $(e).val();
			}
			$('.kelurahan').hide();
			$.ajax({
				url: "{{route('tennants.sign_up')}}",
				method: 'get',
				data: {
					subdistrict_id: id,
					selected_id: selected_id
				},
				dataType: 'html',
				success: function(html){
					$('.kelurahan').html(html);
					$('.kelurahan').show();
					$('.select2').select2();
				},
				error: function(){
					console.log('add box error');
				}
			});
		};
		$('.oke').click(function(){ //term condition modal
			$('input[name=term]').trigger('click');
		});
	</script>
@stop