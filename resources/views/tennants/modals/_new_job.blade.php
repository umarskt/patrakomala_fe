<div id="new-job" class="modal fade modal-new-job my-account customer new-job" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title">@lang('general.desc.job.0')</h4>
			</div>
			<form method="post" action="{{route('tennants.post-job')}}">
				{{csrf_field()}}
				<div class="modal-body">
					<p>@lang('general.desc.job.1')</p>
					<div class="form-group">
						<label for='detail'>Email</label> <span class="required">*</span>
						<input type="email" name="email" class="form-control" placeholder="Email" value="{{Input::old('email')}}">
						<span class="help-block">{{$errors->first('email')}}</span>
						<span style="font-size:14px;">@lang('general.desc.job.2')</span>
					</div>
					<div class="form-group">
						<label for='detail'>@lang('general.desc.job.3')</label> <span class="required">*</span>
						<input type="text" name="title" class="form-control" placeholder="@lang('general.desc.job.4')" value="{{Input::old('title')}}">
						<span class="help-block">{{$errors->first('title')}}</span>
						<textarea class="form-control" placeholder="@lang('general.desc.job.5')" name="description" style="margin-top:10px;">{{Input::old('description')}}</textarea>
						<span class="help-block">{{$errors->first('description')}}</span>
					</div>
					<div class="form-group">
						<label for='product_type'>@lang('general.desc.job.6')</label> <span class="required">*</span>
						<div class="col-xs-6 radio">
							<label class="radio-inline">
								@lang('general.desc.job.7') <input type="radio" name="type" value='product' {{(Input::old('type')=='product')?'checked':''}}>
								<span class="checkmark"></span>
							</label>
						</div>
						<div class="col-xs-6 radio">
							<label class="radio-inline">
								@lang('general.desc.job.8') <input type="radio" name="type" value='service' {{(Input::old('type')=='service')?'checked':''}}>
								<span class="checkmark"></span>
							</label>
						</div>
						<span class="help-block">{{$errors->first('type')}}</span>
					</div>
					<div class="form-group">
						<label for='subsector'>@lang('general.desc.job.9')</label> <span class="required">*</span>
						<select name="sub_sectors[]" class="form-control select2" multiple="true">
							@if(@$subsectors)
								@foreach($subsectors as $key => $value)
									<option value="{{@$value['id']}}" {{(Input::old('product_type')==@$value['id'])?'selected':''}}>{{@$value['sub_sector_name']}}</option>
								@endforeach
							@endif
						</select>
						<span class="help-block">{{$errors->first('subsector')}}</span>
					</div>
					<div class="form-group">
						<label for='estimated_cost'>@lang('general.desc.job.10')</label> <span class="required">*</span>
						<input type="hidden" name="estimated_cost" value="{{Input::old('estimated_cost')}}">
						<div class="input-group">
							<span class="input-group-addon">Rp</span>
							<input type="text" name="estimated_costT" class="form-control number-dec" value="{{Input::old('estimated_costT')}}">
						</div>
						<span class="help-block">{{$errors->first('estimated_cost')}}</span>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group">
						<button type="button" class="btn btn2 btn-orange pull-left" data-dismiss="modal">Cancel</button>
						<input type="submit" name="new-job-sbm" value="Create Job" class="btn btn2 btn-orange pull-right">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>