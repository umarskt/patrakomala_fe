<div id="modal-claim" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
				<h4 class="modal-title">Claim Business</h4>
			</div>
			<div class="modal-body">
				<p>Claim this business ({{@$tenant['tenant_name']}})?</p>
			</div>
			<div class="modal-footer">
				<div class="form-group">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
					@if(@session()->get('user_token'))
						<form method="post" action="{{route('users.claim.post')}}">
							{{csrf_field()}}
							<input type="hidden" name="tenant_id" value="{{@$tenant['id']}}">
							<input type="submit" name="claim-sbm" value="Yes" class="btn btn-orange pull-right">
						</form>
					@else
						<a href="{{route('users.sign_in')}}?claim=true&tenant_id={{@$tenant['id']}}&tenant_name={{@$tenant['tenant_name']}}" class="btn btn-orange pull-right">Yes</a>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>