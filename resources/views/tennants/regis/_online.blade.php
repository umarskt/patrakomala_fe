<div class="row">
	<div class="col-sm-12">
		<label>@lang('general.label.tenant-page.online_market')</label>
	</div>
	<div class="col-xs-12 col-sm-4">
		<select name="online_market_type[]" class="select2 form-control">
			<option> Web Mandiri</option>
			<option> WA</option>
			<option> BBM</option>
			<option> FB</option>
			<option> Situs E-commerce</option>
			<option> IG</option>
			<option> TW</option>
			<option> Lainnya</option>
		</select>
	</div>
	<div class="col-xs-12 col-sm-8">
		<div class="wa">
			<input type="text" name="online_market_url[]" class="form-control">
		</div>
	</div>
</div>