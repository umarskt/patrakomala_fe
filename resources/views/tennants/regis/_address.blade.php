<div class="row">
	<div class="col-sm-12">
		<div class="form-group row">
			<div class="col-sm-12">
				<label>@lang('general.label.tenant-page.address_street') *</label>
			</div>
			<div class="col-sm-12">
				<p class="right" style="padding-right: 0px;">@lang('general.label.tenant-page.address_street_message')</p>
				<input type="text" name="address_street" id="address_street{{(int)@$n}}" placeholder="Enter your location" class="form-control">
				<span class="help-block">{{@$errors->first('address_street')}}</span>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group row">
			<div class="col-sm-12">
				<label>@lang('general.label.tenant-page.address_subdistrict_id') *</label>
			</div>
			<div class="col-sm-12">
				<select name="address_subdistrict_id" id="kecamatan" class="select2 form-control">
					<option value='' selected disabled></option>
					@if(@$subdistricts)	
						@foreach($subdistricts as $key=>$value)
							<option value="{{$value['id']}}">{{$value['kecamatan']}}</option>
						@endforeach
					@endif
				</select>
				<span class="help-block">{{@$errors->first('address_subdistrict_id')}}</span>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-12">
				<label>@lang('general.label.tenant-page.rtrw') *</label>
			</div>
			<div class="col-sm-12">
				<input type="text" name="rtrw" id="rtrw" class="form-control" placeholder="001/002">
				<span class="help-block">{{@$errors->first('rtrw')}}</span>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group row">
			<div class="col-sm-12">
				<label>@lang('general.label.tenant-page.kelurahan') *</label>
			</div>
			<div class="col-sm-12">
				<select name="kelurahan" id="kelurahan" class="select2 form-control">
					<option value='' selected disabled></option>
					<option value="batununggal">Batununggal</option>
				</select>
				<span class="help-block">{{@$errors->first('kelurahan')}}</span>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-12">
				<label>@lang('general.label.tenant-page.address_postal_code') *</label>
			</div>
			<div class="col-sm-12">
				<input type="text" name="address_postal_code" id="postal_code" class="form-control number-only">
				<span class="help-block">{{@$errors->first('address_postal_code')}}</span>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="form-group row">
			<div class="col-sm-12">
				<p class="right" style="padding-right: 0px;">@lang('general.label.tenant-page.map_message')</p>
				<div id="map{{(int)@$n}}" class="map"></div>
			</div>
			<div class="col-sm-12">
				<span id="current{{(int)@$n}}" style="display:none;"></span>
			</div>
			<div class="col-sm-6">
				<input type='text' name='address_latitude' class="form-control" id="address_latitude{{(int)@$n}}" readonly="true" placeholder="Latitude" style="color:#000;"/>
				<span class="help-block">{{@$errors->first('address_latitude')}}</span>
			</div>
			<div class="col-sm-6">
				<input type='text' name='address_longitude' class="form-control" id="address_longitude{{(int)@$n}}" readonly="true" placeholder="Longitude" style="color:#000;"/>
				<span class="help-block">{{@$errors->first('address_longitude')}}</span>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			setTimeout(function(){
				initMultiMaps($('.address-box').children().length-1);
			},100);
		});
	</script>
</div>