<div class="form-group row">
	<div class="col-sm-12">
		<label>@lang('general.label.tenant-page.business_strength')</label>
	</div>
	<div class="col-sm-12">
		<textarea name="business_strength[]" class="form-control" placeholder=""></textarea>
	</div>
</div>