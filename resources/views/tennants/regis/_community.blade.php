<div class="form-group row">
	<div class="col-sm-12">
		<label>@lang('general.label.tenant-page.community_name')</label>
	</div>
	<div class="col-sm-12">
		<input type="text" name="community_name[]" class="form-control">
	</div>
</div>