<div class="row">
	<div class="col-sm-12">
		<div class="form-group row">
			<div class="col-sm-12">
				<label>@lang('general.label.tenant-page.coach_name')</label>
			</div>
			<div class="col-sm-12">
				<input type="text" name="coach_name[]" class="form-control">
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group row">
			<div class="col-sm-12">
				<label>@lang('general.label.tenant-page.coaching_type')</label>
			</div>
			<div class="col-sm-12">
				<input type="text" name="coaching_type[]" class="form-control">
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group row">
			<div class="col-sm-12">
				<label>@lang('general.label.tenant-page.coaching_year')</label>
			</div>
			<div class="col-sm-12">
				<select name="coaching_year[]" class="select2 form-control">
					<option value='' selected disabled>@lang('general.label.year')</option>
					@for($i=(int)date('Y');$i>=(int)date('Y')-50;$i--)
						<option value="{{$i}}">{{$i}}</option>
					@endfor
				</select>
			</div>
		</div>
	</div>
</div>