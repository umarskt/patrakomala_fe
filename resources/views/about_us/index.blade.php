@extends('layouts.application')

@section('title')
    @lang('general.title.aboutus')
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container-fluid full">
		<div class="jumbotron jumbotron-about-us">
		</div>
	</div>
	<div class="container about-us">
		<h3>Welcome to this site</h3>
		<p>Proin in tellus sit amet nibh dignissim sagittis. Donec iaculis gravida nulla. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet. Praesent in mauris eu tortor porttitor accumsan. Pellentesque ipsum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse nisl. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet. Pellentesque ipsum. Nullam at arcu a est sollicitudin euismod. Mauris tincidunt sem sed arcu.</p>
		<h3>PatraKomala</h3>
		<p>Proin in tellus sit amet nibh dignissim sagittis. Donec iaculis gravida nulla. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet. Praesent in mauris eu tortor porttitor accumsan. Pellentesque ipsum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse nisl. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet. Pellentesque ipsum. Nullam at arcu a est sollicitudin euismod. Mauris tincidunt sem sed arcu.</p>
		<div class="row our-team-odd">
			<div class="col-xs-12 col-sm-5 our-team-pict">
				<img src="{{asset('assets/images/Pict-1.png')}}">
			</div>
			<div class="col-xs-12 col-sm-7 our-team-detail">
				<h3>Our Team</h3>
				<p>Proin in tellus sit amet nibh dignissim sagittis. Donec iaculis gravida nulla. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet. Pellentesque pretium lectus id turpis. Mauris metus.</p>
			</div>
		</div>
		<div class="row our-team-even">
			<div class="col-xs-12 col-sm-5 our-team-pict">
				<img src="{{asset('assets/images/Pict-1.png')}}">
			</div>
			<div class="col-xs-12 col-sm-7 our-team-detail">
				<h3>Our Team</h3>
				<p>Proin in tellus sit amet nibh dignissim sagittis. Donec iaculis gravida nulla. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet. Pellentesque pretium lectus id turpis.</p>
			</div>
		</div>
		<h3>What PatraKomala Do?</h3>
		<div class="row about-what">
			<div class="col-xs-12 col-sm-4">
				<img src="{{asset('assets/images/Icon-image_01.png')}}">
				<h4>Lorem</h4>
				<p>Proin in tellus sit amet nibh dignissim sagittis. Donec iaculis gravida nulla. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.</p>
			</div>
			<div class="col-xs-12 col-sm-4">
				<img src="{{asset('assets/images/Icon-image_02.png')}}">
				<h4>Ipsum</h4>
				<p>Proin in tellus sit amet nibh dignissim sagittis. Donec iaculis gravida nulla. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.</p>
			</div>
			<div class="col-xs-12 col-sm-4">
				<img src="{{asset('assets/images/Icon-image_03.png')}}">
				<h4>Dolor</h4>
				<p>Proin in tellus sit amet nibh dignissim sagittis. Donec iaculis gravida nulla. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.</p>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')

@stop