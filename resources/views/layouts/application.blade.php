<!DOCTYPE html>
<html>
<head>
	<title>@yield('title') | PatraKomala</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="keywords" content="">
	<meta name="description" content="">
	@yield('custom_meta')
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap3/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}">
	@yield('custom_styles')
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_styles.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/css_new.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_flash.css')}}">
	@yield('custom_styles_after')
	<script type="text/javascript" src="{{asset('vendor/jquery/jquery-1.12.4.min.js')}}"></script>
	@yield('head_scripts')
</head>
<!--<body style="{{--asset_bg()--}}" class="ntp">-->
<body class="ntp">
	@include('flash::message')
	@include('layouts.partials._header')
	<div class="content">
		@yield('content')
	</div>
	@include('layouts.partials._footer')
	<script type="text/javascript" src="{{asset('vendor/bootstrap3/js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/custom_scripts.js')}}"></script>
	<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
	<script type="text/javascript">
		$('#flash-overlay-modal').modal();
		$(document).ready(function(){
			$('.select3-m1').text('gunakan koma (,) sebagai pemisah');
			setTimeout(function(){
				$('.alert').animate({right: '-800'}).fadeOut('fast');
			},5000);
			// for faq
			$('div.detail-faq').css('display','none');
			if($('.right-option').is(':hidden')){
				$(this).find('.fa-angle-double-right').addClass('fa-angle-double-left');
				$(this).find('.fa-angle-double-left').removeClass('fa-angle-double-right');
			}
		});
		$('.global.menu-collapser').click(function(){
			$(this).css('display','none');
			if($('.right-option').is(':visible')){
				$(this).find('.fa-angle-double-right').addClass('fa-angle-double-left');
				$(this).find('.fa-angle-double-left').removeClass('fa-angle-double-right');
				$('.left-content').removeClass('col-xs-7').addClass('col-xs-12');
				$('.left-content').removeClass('col-sm-8').addClass('col-sm-12');
				$('.filter-render .col-md-6').removeClass('col-md-6').addClass('col-md-4');
				$('.filter-render .col-lg-4').removeClass('col-lg-4').addClass('col-lg-3');
				$('.right-option').animate({right: '-450'}).fadeOut('fast').removeClass('col-sm-4');
			}
			else{
				$(this).find('.fa-angle-double-left').addClass('fa-angle-double-right');
				$(this).find('.fa-angle-double-right').removeClass('fa-angle-double-left');
				$('.left-content').removeClass('col-sm-12').addClass('col-sm-8');
				$('.left-content').removeClass('col-xs-12').addClass('col-xs-12');
				$('.filter-render .col-md-4').removeClass('col-md-4').addClass('col-md-6');
				$('.filter-render .col-lg-3').removeClass('col-lg-3').addClass('col-lg-4');
				$('.right-option').animate({right: '0'}).fadeIn('slow').addClass('col-sm-4');	
			}
			$(this).fadeIn('slow');
		});

		$('.tour-coll.menu-collapser').click(function(){
			$(this).css('display','none');
			if($('.right-option').is(':visible')){
				$(this).find('.fa-angle-double-right').addClass('fa-angle-double-left');
				$(this).find('.fa-angle-double-left').removeClass('fa-angle-double-right');
				// $('.left-content').removeClass('col-xs-7').addClass('col-xs-12');
				// $('.left-content').removeClass('col-sm-8').addClass('col-sm-12');
				$('.right-option').animate({right: '-700'}).fadeOut(1).removeClass('col-sm-4');
			}
			else{
				$(this).find('.fa-angle-double-left').addClass('fa-angle-double-right');
				$(this).find('.fa-angle-double-right').removeClass('fa-angle-double-left');
				// $('.left-content').removeClass('col-sm-12').addClass('col-sm-8');
				$('.right-option').animate({right: '0'}).fadeIn('slow').addClass('col-sm-4');	
			}
			$(this).fadeIn('slow');
		});

		function loadingShow(){
			$('.loading').show();
		}
		function loadingHide(){
			$('.loading').hide();
		}

		function toMoneyUSD(money){
		 	if (money.which != 110 ){
			 	var mone = number_format(source.val(),0,'.',',');
			 	return mone.toLocaleString();
			}
		 }
		 function toMoneyIDR(money,source){
		 	if (money.which != 110 ){
			 	var mone = number_format(source.val(),0,',','.');
			 	return mone.toLocaleString();
			}
		 }
		 function fromMoneyIDR(money){
		 	var mone = money.val().replace(/\./g,'');
		 	return mone.toLocaleString();
		 }
		 function fromMoneyUSD(money){
		 	var mone = money.val().replace(/\,/g,'');
		 	return mone.toLocaleString();
		 }

		 $('.number-only').keypress(function(evt){
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /^[0-9]+$/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		});
		$('.number-dec').keypress(function(evt){
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			let key2 = String.fromCharCode( key );
			var regex = /^[0-9.]+$/;
			if(key > 31 && (key < 48 || key > 57)){
				if( !regex.test(key2) ) {
					theEvent.returnValue = false;
					if(theEvent.preventDefault) theEvent.preventDefault();
				}
			}
		});

		function number_format (number, decimals, dec_point, thousands_sep) {
			// Strip all characters but numerical ones.
			number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
			var n = !isFinite(+number) ? 0 : +number,
			  prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
			  sep = (typeof thousands_sep === 'undefined') ? '.' : thousands_sep,
			  dec = (typeof dec_point === 'undefined') ? ',' : dec_point,
			  s = '',
			  toFixedFix = function (n, prec) {
			      var k = Math.pow(10, prec);
			      return '' + Math.round(n * k) / k;
			  };
			// Fix for IE parseFloat(0.55).toFixed(0) = 0;
			s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
			if (s[0].length > 3) {
			  s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
			}
			if ((s[1] || '').length < prec) {
			  s[1] = s[1] || '';
			  s[1] += new Array(prec - s[1].length + 1).join('0');
			}
			return s.join(dec);
		}
	</script>
	@yield('custom_scripts')
</body>
</html>