<footer>
	<div class="">
		<div class="">
			<div class="col-xs-12 footer">
				<ul class="">
					<li class="logo-footer col-xs-12 col-sm-1" style="display:none;"><a href="#"><img src="{{asset('assets/images/1920/PATRA KOMALA FOOTER.png')}}"></a></li>
					<li class="menu menu-seperated col-xs-5 col-sm-3"><a href="{{route('how-project-work.index')}}">@lang('general.label.work')</a></li>
					<li class="menu menu-seperated col-xs-4 col-sm-3"><a href="{{route(
					'terms_and_conditions.index')}}">@lang('general.label.term')</a></li>
					<li class="menu col-xs-3 col-sm-2"><a href="{{route('faq.index')}}">F A Q</a></li>
					<li class="copyright col-xs-12 col-sm-3">
						<!-- <a href="{{route('change.locale').'?language='.((@session()->get('locale') == 'en')?'id':'en')}}">
							<img src="{{asset('assets/images/'.((@session()->get("locale") == 'en')?'id':'en').'.png')}}" style="width:30px;height:20px;margin-right:10px;" title="{{(@session()->get('locale') == 'en') ? 'Ubah ke Bahasa Indonesia' : 'Change to English' }}">
						</a> -->
						Copyright 2018 @lang('general.label.copy')</li>
				</ul>
			</div>
		</div>
	</div>
</footer>