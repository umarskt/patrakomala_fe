<nav class="navbar navbar-default header-menu navbar-fixed-top sticky" role='navigation'>
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{{route('home')}}">
				<img src="{{asset('assets/images/1920/LOGO HEADER.png')}}">
			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right text-uppercase">
				<li><span class="logo-separator"></span></li>
				<li class="{{(Request::is(locale().'') || Request::is(locale().'/news/*') || Request::is(locale().'/events/*')) ? 'active' : ''}}"><a href="{{route('home')}}">@lang('general.header.menu.home') <span class="sr-only">(current)</span></a></li>
				<li class="{{(Request::is(locale().'/tenants*') && !Request::is(locale().'/tenants/sign-up*')) ? 'active' : ''}}"><a href="{{route('tennants.index')}}">@lang('general.header.menu.tenant')</a></li>
				<li class="{{(Request::is(locale().'/safari-hki*') && !Request::is(locale().'/tenants/sign-up*')) ? 'active' : ''}}"><a href="{{route('safari_hki.index')}}">@lang('general.header.menu.safarihki')</a></li>
				<li class="{{Request::is(locale().'/tour-packages*') ? 'active' : ''}}"><a href="{{route('tour_packages.index')}}">@lang('general.header.menu.tour')</a></li>
				@if(current_user_token())
<!-- 					<li class="{{Request::is(locale().'/my-account*') ? 'active' : ''}}"><a href="{{route('users.account.dashboard')}}" class="dashboard">@lang('general.header.menu.dashboard')</a></li> -->
					<li class="dropdown">
				        <a class="dropdown-toggle" data-toggle="dropdown" href="#">You
				        <span class="caret"></span></a>
				        <ul class="dropdown-menu">
					          <li><a href="{{route('users.account.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
					          <li><a href="{{route('users.account.setting')}}"><i class="fa fa-gear"></i> Setting</a></li>
					          <li><a href="{{route('users.sign_out')}}"><i class="fa fa-sign-out"></i> Logout</a></li>
				        </ul>
			      </li>
				@else
					<li class="{{Request::is(locale().'/users/sign-in*') ? 'active' : ''}}"><a href="{{route('users.sign_in')}}" class="login">@lang('general.header.menu.login')</a></li>
					<!-- <li class="{{(Request::is(locale().'/users/sign-up*') || Request::is(locale().'/tenants/sign-up*')) ? 'active' : ''}}"><a href="{{route('users.sign_up')}}" class="signup">@lang('general.header.menu.register')</a></li> -->
				@endif
				<!-- <li class="">
					<select class="select-locale">
						<option value="" disabled="true"></option>
						<option value="en" {{(@session()->get('locale') == 'en') ? 'selected' : ''}}>en</option>
						<option value="id" {{(@session()->get('locale') == 'id') ? 'selected' : ''}}>id</option>
					</select>
				</li> -->
				<li>
					<p><a href="{{(@session()->get('locale') == 'id')?'#':route('change.locale').'?language=id'}}" class="btn {{(@session()->get('locale') == 'id')?'active':'inactive'}}">
						ID	
					</a>
					<small>/</small>
					<a href="{{(@session()->get('locale') == 'en')?'#':route('change.locale').'?language=en'}}" class="btn {{(@session()->get('locale') == 'en')?'active':'inactive'}}">
						EN
					</a></p>
				</li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>

@include('layouts.scripts._locale')
