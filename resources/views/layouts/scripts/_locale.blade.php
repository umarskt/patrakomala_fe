<script type="text/javascript">
$(document).ready(function(){
	// $('.select-locale').val("{{@Session::get('locale')}}");
});

$('.select-locale').on('change',function(){
	var base_url = window.location.origin;
	var arr = window.location.href.toString().split(window.location.host);
	var selected_locale  = $(this).val();
	if(arr[1] != null || arr[1] != undefined){
		arr=arr[1].split('/');
		arr[1] = selected_locale;
		arr = arr.join('/');
		$.ajax({
			url: "{{route('change.locale')}}",
			type: 'get',
			data: {
				language: selected_locale
			},
			success: function(data){
				if(data == selected_locale){
					location.replace(base_url+arr);
				}
			},
			dataType: 'json'
		});
	}
});
</script>