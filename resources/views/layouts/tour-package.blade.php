<!DOCTYPE html>
<html>
<head>
	<title>@yield('title') | PatraKomala</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="keywords" content="">
	<meta name="description" content="">
	@yield('custom_meta')
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap3/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}">
	@yield('custom_styles')
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_styles.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/css_new.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_flash.css')}}">
	@yield('custom_styles_after')
	<script type="text/javascript" src="{{asset('vendor/jquery/jquery-1.12.4.min.js')}}"></script>
	@yield('head_scripts')
</head>
<body style="{{asset_bg()}}" class="tp">
	<div class="loading"><img src="{{asset('assets/images/loading.gif')}}"></div>
	@include('flash::message')
	@include('layouts.partials._header')
	<div class="content">
		@yield('content')
	</div>
	@include('layouts.partials._footer')
	<script type="text/javascript" src="{{asset('vendor/bootstrap3/js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/custom_scripts.js')}}"></script>
	<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
	<script type="text/javascript">
		$('#flash-overlay-modal').modal();
		$(document).ready(function(){
			setTimeout(function(){
				$('.alert').animate({right: '-800'}).fadeOut('fast');
			},5000);
			// for faq
			$('div.detail-faq').css('display','none');
			if($('.right-option').is(':hidden')){
				$(this).find('.fa-angle-double-right').addClass('fa-angle-double-left');
				$(this).find('.fa-angle-double-left').removeClass('fa-angle-double-right');
			}
		});
		$('.global.menu-collapser').click(function(){
			$(this).css('display','none');
			if($('.right-option').is(':visible')){
				$(this).find('.fa-angle-double-right').addClass('fa-angle-double-left');
				$(this).find('.fa-angle-double-left').removeClass('fa-angle-double-right');
				$('.left-content').removeClass('col-xs-7').addClass('col-xs-12');
				$('.left-content').removeClass('col-sm-8').addClass('col-sm-12');
				$('.right-option').animate({right: '-450'}).fadeOut('fast').removeClass('col-sm-4');
			}
			else{
				$(this).find('.fa-angle-double-left').addClass('fa-angle-double-right');
				$(this).find('.fa-angle-double-right').removeClass('fa-angle-double-left');
				$('.left-content').removeClass('col-sm-12').addClass('col-sm-8');
				$('.left-content').removeClass('col-xs-12').addClass('col-xs-7');
				$('.right-option').animate({right: '0'}).fadeIn('slow').addClass('col-sm-4');	
			}
			$(this).fadeIn('slow');
		});

		$('.tour-coll.menu-collapser').click(function(){
			$(this).css('display','none');
			if($('.right-option').is(':visible')){
				$(this).find('.fa-angle-double-right').addClass('fa-angle-double-left');
				$(this).find('.fa-angle-double-left').removeClass('fa-angle-double-right');
				// $('.left-content').removeClass('col-xs-7').addClass('col-xs-12');
				// $('.left-content').removeClass('col-sm-8').addClass('col-sm-12');
				$('.right-option').animate({right: '-700'}).fadeOut(1).removeClass('col-sm-4');
			}
			else{
				$(this).find('.fa-angle-double-left').addClass('fa-angle-double-right');
				$(this).find('.fa-angle-double-right').removeClass('fa-angle-double-left');
				// $('.left-content').removeClass('col-sm-12').addClass('col-sm-8');
				$('.right-option').animate({right: '0'}).fadeIn('slow').addClass('col-sm-4');	
			}
			$(this).fadeIn('slow');
		});

		function loadingShow(){
			$('.loading').show();
		}
		function loadingHide(){
			$('.loading').hide();
		}
	</script>
	@yield('custom_scripts')
</body>
</html>