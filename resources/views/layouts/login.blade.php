<!DOCTYPE html>
<html>
<head>
	<title>@yield('title') | PatraKomala</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="keywords" content="">
	<meta name="description" content="">
	@yield('custom_meta')
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap3/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_styles.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_flash.css')}}">
	@yield('custom_styles')
</head>
<body>
	@include('flash::message')
	@include('layouts.partials._header')
	@yield('content')
	@include('layouts.partials._footer')
	<script type="text/javascript" src="{{asset('vendor/jquery/jquery-1.12.4.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('vendor/bootstrap3/js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
	@yield('custom_scripts')
</body>
</html>