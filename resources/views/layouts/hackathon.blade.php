<!DOCTYPE html>
<html>
<head>
	<title>@yield('title') | PatraKomala</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="keywords" content="">
	<meta name="description" content="">
	@yield('custom_meta')
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap3/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}">
	@yield('custom_styles')
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_styles.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/css_new.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_flash.css')}}">
	@yield('custom_styles_after')
	<style type="text/css">
		.lSSlideOuter {
			width: 100%;
		}
    	ul{
			/*list-style: none outside none;*/
		    padding-left: 0;
            margin: 0;
		}
		.content-slider li{
		    /*background-color: #ed3020;*/
		    text-align: center;
		}
		.content-slider img{
			width: 100%;
			height: auto;
		}
		.lSPager.lSpg{
			display: none;
		}
		.countdown{
			font-family: 'Poppins Medium';
			font-size: 17px;
			margin: 30px auto;
		}
		.countdown td{
			padding: 0px 20px;
		}
		.pink{
			color: #F55267;
			font-size: 35px;
		}
	</style>
	<script type="text/javascript" src="{{asset('vendor/jquery/jquery-1.12.4.min.js')}}"></script>
	@yield('head_scripts')
</head>
<!--<body style="{{--asset_bg()--}}" class="ntp">-->
<body class="ntp2">
	@include('flash::message')
	@include('layouts.partials._header')
	<div class="content">
		@yield('content')
	</div>
	@include('layouts.partials._footer')
	<script type="text/javascript" src="{{asset('vendor/bootstrap3/js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/custom_scripts.js')}}"></script>
	<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
	<script type="text/javascript">
		$('#flash-overlay-modal').modal();
		$(document).ready(function(){
			$('.select3-m1').text('gunakan koma (,) sebagai pemisah');
			setTimeout(function(){
				$('.alert').animate({right: '-800'}).fadeOut('fast');
			},5000);
			// for faq
			$('div.detail-faq').css('display','none');
			if($('.right-option').is(':hidden')){
				$(this).find('.fa-angle-double-right').addClass('fa-angle-double-left');
				$(this).find('.fa-angle-double-left').removeClass('fa-angle-double-right');
			}
		});
		$('.global.menu-collapser').click(function(){
			$(this).css('display','none');
			if($('.right-option').is(':visible')){
				$(this).find('.fa-angle-double-right').addClass('fa-angle-double-left');
				$(this).find('.fa-angle-double-left').removeClass('fa-angle-double-right');
				$('.left-content').removeClass('col-xs-7').addClass('col-xs-12');
				$('.left-content').removeClass('col-sm-8').addClass('col-sm-12');
				$('.filter-render .col-md-6').removeClass('col-md-6').addClass('col-md-4');
				$('.filter-render .col-lg-4').removeClass('col-lg-4').addClass('col-lg-3');
				$('.right-option').animate({right: '-450'}).fadeOut('fast').removeClass('col-sm-4');
			}
			else{
				$(this).find('.fa-angle-double-left').addClass('fa-angle-double-right');
				$(this).find('.fa-angle-double-right').removeClass('fa-angle-double-left');
				$('.left-content').removeClass('col-sm-12').addClass('col-sm-8');
				$('.left-content').removeClass('col-xs-12').addClass('col-xs-12');
				$('.filter-render .col-md-4').removeClass('col-md-4').addClass('col-md-6');
				$('.filter-render .col-lg-3').removeClass('col-lg-3').addClass('col-lg-4');
				$('.right-option').animate({right: '0'}).fadeIn('slow').addClass('col-sm-4');	
			}
			$(this).fadeIn('slow');
		});

		$('.tour-coll.menu-collapser').click(function(){
			$(this).css('display','none');
			if($('.right-option').is(':visible')){
				$(this).find('.fa-angle-double-right').addClass('fa-angle-double-left');
				$(this).find('.fa-angle-double-left').removeClass('fa-angle-double-right');
				// $('.left-content').removeClass('col-xs-7').addClass('col-xs-12');
				// $('.left-content').removeClass('col-sm-8').addClass('col-sm-12');
				$('.right-option').animate({right: '-700'}).fadeOut(1).removeClass('col-sm-4');
			}
			else{
				$(this).find('.fa-angle-double-left').addClass('fa-angle-double-right');
				$(this).find('.fa-angle-double-right').removeClass('fa-angle-double-left');
				// $('.left-content').removeClass('col-sm-12').addClass('col-sm-8');
				$('.right-option').animate({right: '0'}).fadeIn('slow').addClass('col-sm-4');	
			}
			$(this).fadeIn('slow');
		});

		function loadingShow(){
			$('.loading').show();
		}
		function loadingHide(){
			$('.loading').hide();
		}

		// Set the date we're counting down to
		// var countDownDate = new Date("Oct 6, 2018 09:00:00").getTime();

		// // Update the count down every 1 second
		// var x = setInterval(function() {

		//     // Get todays date and time
		//     var now = new Date().getTime();
		    
		//     // Find the distance between now and the count down date
		//     var distance = countDownDate - now;
		    
		//     // Time calculations for days, hours, minutes and seconds
		//     var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		//     var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		//     var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		//     var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		    
		//     // Output the result in an element with id="demo"
		//     document.getElementById("day").innerHTML = days;
		//     document.getElementById("hour").innerHTML = hours;
		//     document.getElementById("minute").innerHTML = minutes;
		//     document.getElementById("second").innerHTML = seconds;
		    
		//     // If the count down is over, write some text 
		//     if (distance < 0) {
		//         clearInterval(x);
		//         document.getElementById("day").innerHTML = 0;
		// 	    document.getElementById("hour").innerHTML = 0;
		// 	    document.getElementById("minute").innerHTML = 0;
		// 	    document.getElementById("second").innerHTML = 0;
		//     }
		// }, 1000);
		// function isScrolledIntoView(elem){
		//     var docViewTop = $(window).scrollTop();
		//     var docViewBottom = docViewTop + $(window).height();

		//     var elemTop = $(elem).offset().top;
		//     var elemBottom = elemTop + $(elem).height();

		//     return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
		// }
	</script>
	@yield('custom_scripts')
</body>
</html>