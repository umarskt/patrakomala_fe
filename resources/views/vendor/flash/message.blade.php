@foreach (session('flash_notification', collect()) as $message)
    @if (@$message['overlay'])
        @include('flash::modal', [
            'modalClass' => 'flash-modal',
            'title'      => @$message['title'],
            'body'       => @$message['message']
        ])
    @else
        <div class="alert alert-{{ $message['level'] }} {{ $message['important'] ? 'alert-important' : '' }}" role="alert">
            @if ($message['important'])
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            @endif
            <i class="fa fa-{{($message['level'] =='success')?'check-circle' : 'exclamation-circle'}}"></i>
            <strong>{{($message['level'] =='success')?'SUCCESS :' : 'WARNING : '}}</strong>
            {!! $message['message'] !!}!

        </div>
    @endif
@endforeach
@if(session()->has('flash_notification2'))
        <div class="alert alert-{{ (@session('flash_notification2')['level'] == 'success')?'success':'danger' }} {{ @session('flash_notification2')['important'] ? 'alert-important' : '' }}" role="alert">
            @if (@session('flash_notification2')['important'])
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            @endif
            <i class="fa fa-{{(@session('flash_notification2')['level'] =='success')?'check-circle' : 'exclamation-circle'}}"></i>
            <strong>{{(@session('flash_notification2')['level'] =='success')?'SUCCESS :' : 'WARNING : '}}</strong>
            {!! @session('flash_notification2')['message'] !!}!

        </div>
@endif
{{ session()->forget('flash_notification') }}
{{ session()->forget('flash_notification2') }}
