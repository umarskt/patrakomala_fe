<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Waypoints in directions</title>
    <style>
      #right-panel {
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }

      #right-panel select, #right-panel input {
        font-size: 15px;
      }

      #right-panel select {
        width: 100%;
      }

      #right-panel i {
        font-size: 12px;
      }
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
        float: left;
        width: 70%;
        height: 100%;
      }
      #right-panel {
        margin: 20px;
        border-width: 2px;
        width: 20%;
        height: 400px;
        float: left;
        text-align: left;
        padding-top: 0;
      }
      #directions-panel {
        margin-top: 10px;
        background-color: #FFEE77;
        padding: 10px;
        overflow: scroll;
        height: 174px;
      }
      .label-tour{
        font-weight: bold;
        color: red !important;
      }
    </style>
  </head>
  <body>
    <div id="map" class="map"></div>
    <div id="right-panel">
    <div>
    <b>Start:</b>
    <select id="start">
      <!-- <option value="Antapani, ID30">Halifax, NS</option> -->
      <option value="-6.30303030,107.40404040">Boston, MA</option>
      
    </select>
    <br>
    <b>Waypoints:</b> <br>
    <i>(Ctrl+Click or Cmd+Click for multiple selection)</i> <br>
    <select multiple id="waypoints">
      <option value="-6.40404030,107.50505040">New York, NY</option>
      <option value="-6.60607030,107.10405040">Miami, FL</option>
    </select>
    <br>
    <b>End:</b>
    <select id="end">
      <option value="-6.60708090,107.70506090">New York, NY</option>
    </select>
    <br>
      <input type="submit" id="submit">
    </div>
    <div id="directions-panel"></div>
    </div>
    <script>
        var directionsService;
        var directionsDisplay;
        var map;
        var icon;
      function initMap() {
        directionsService = new google.maps.DirectionsService;
        directionsDisplay = new google.maps.DirectionsRenderer({
          polylineOptions: {
            strokeColor: "#fe7e41",
            strokeWeight: 5
          }
        });
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: {lat: -6.858585, lng: 107.656565}
        });
        icon = {
            url: '/assets/images/marker-icon.png',
            scaledSize: new google.maps.Size(30, 45),
            labelOrigin: new google.maps.Point(15,15)
        };
        directionsDisplay.setMap(map);

        document.getElementById('submit').addEventListener('click', function() {
          calculateAndDisplayRoute(directionsService, directionsDisplay);
        });
      }

      function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var waypts = [];
        var checkboxArray = document.getElementById('waypoints');
        var latlong;
        for (var i = 0; i < checkboxArray.length; i++) {
          if (checkboxArray.options[i].selected) {
            latlong = checkboxArray[i].value.split(',');
            latlong[0] = parseFloat(latlong[0]);
            latlong[1] = parseFloat(latlong[1]);
            // addMarker({lat:latlong[0],lng:latlong[1]}, (i+1));
            waypts.push({
              location: checkboxArray[i].value,
              stopover: true
            });
          }
        }

        directionsService.route({
          origin: document.getElementById('start').value,
          destination: document.getElementById('end').value,
          waypoints: waypts,
          optimizeWaypoints: true,
          travelMode: 'DRIVING'
        }, function(response, status) {
          if (status === 'OK') {
            
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            var summaryPanel = document.getElementById('directions-panel');
            summaryPanel.innerHTML = '';
            // addMarker(route.legs[0].start_location, 0);
            // addMarker(route.legs[route.legs.length-1].end_location, route.legs.length);
            console.log(route.legs);
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
              // addMarker(route.legs[i].end_location, i);
              var routeSegment = i + 1;
              summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
                  '</b><br>';
              summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
              summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
              summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
            }
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });


      }
      function addMarker(position, i) {
          return new google.maps.Marker({
            // @see http://stackoverflow.com/questions/2436484/how-can-i-create-numbered-map-markers-in-google-maps-v3 for numbered icons
            icon: icon,
            position: position,
            map: map,
            label: {
              text: ""+(i+1),
              color: 'white',
              fontWeight: 'bold',
              fontSize: '13px'
            },
            labelClass: "label-tour",
            labelColor: '#fff',
            // labelAnchor: new google.maps.Point(0, 10),
            optimized: false,
            zIndex: 9999999
          })
        }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEKkG9jlRbavkuE6unDPFGjZ6Ur5cYjHM&callback=initMap&language=id&region=ID">
    </script>
  </body>
</html>