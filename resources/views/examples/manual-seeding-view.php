<?php
$limit = 9;
        $total = 12;
        $news_no = 0;
        $event_no = 0;
        for($i=($page*$limit-9);$i<=((($limit*$page) > $total)?$total-1:($limit*$page-1));$i++){
            $title = 'News';
        	if($i%2==0){
        		$asset = asset('assets/images/news/bandung.png');
                $title = 'Events';
                $event_no = ++$event_no; 
        	}
        	else if($i%3==1){
        		$asset = asset('assets/images/news/hackathon.png');
                $news_no = ++$news_no; 
        	}
        	else if($i%4==1){
        		$asset = asset('assets/images/news/colonial.png');
                $news_no = ++$news_no; 
        	}
        	else{
        		$asset = asset('assets/images/news/city.png');
                $news_no = ++$news_no; 
        	}
        	$data['news']['data'][$i] = [
        		'id'=> $i+1,
        		'title'=> $title.' yang ke-'.((($title == 'News') ? $news_no : $event_no)),
        		'image'=> $asset,
                'type'=> strtolower($title)
        	];
        	
        }
        	$data['news']['meta'] = [
        		'current_page'=> (@$request->get('page')) ? @$request->get('page') : 1,
        		'last_page'=> 2,
        		'prev_page_url'=> '#'

        	];