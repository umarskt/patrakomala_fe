<script type="text/javascript">
var map;
var marker_initial;
var marker = new Array();
var markerContent = new Array();
var active_marker;
var last_active_marker;
var icon;
var activeIcon;
var bounds_initial;
var locations = {!!json_encode(@$tenants)!!};
var directionsService;
var directionsDisplay;
var mapCenter;
var infowindow;
// locations = JSON.parse(locations);
function initialize() {
  marker_initial = google.maps.Marker;
  bounds_initial = google.maps.LatLngBounds;
  infowindow = new google.maps.InfoWindow();
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer({
    polylineOptions: {
      strokeColor: "#fe7e41",
      strokeWeight: 5
    },
    suppressMarkers: true
  });
  var bounds = new google.maps.LatLngBounds();
  var isMobile = (navigator.userAgent.toLowerCase().indexOf('android') > -1) ||
    (navigator.userAgent.match(/(iPod|iPhone|iPad|BlackBerry|Windows Phone|iemobile)/));
  if (isMobile) {
    var viewport = document.querySelector("meta[name=viewport]");
    viewport.setAttribute('content', 'initial-scale=1.0, user-scalable=no');
  }
  var mapDiv = document.getElementById('map');
  mapDiv.style.width = isMobile ? '100%' : '100%';
  mapDiv.style.height = isMobile ? '100%' : '500px';
  map = new google.maps.Map(mapDiv, {
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend-open'));
  map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend'));

  icon = {
      url: '/assets/images/marker_1.png',
      scaledSize: new google.maps.Size(30, 45),
      labelOrigin: new google.maps.Point(15,15)
  };
  active_icon = {
      url: '/assets/images/marker_2.png',
      scaledSize: new google.maps.Size(30, 45),
      labelOrigin: new google.maps.Point(15,15)
  };
  if(locations){
    map.setCenter(new google.maps.LatLng(parseFloat(locations[0].latitude), parseFloat(locations[0].longitude)));
    map.setZoom(12);
    for(var i=0;i<locations.length;i++){
      // if(i > 0){
        marker[i] = new google.maps.Marker({
          position: new google.maps.LatLng(parseFloat(locations[i].latitude), parseFloat(locations[i].longitude)),
          // title: 'KOTA BANDUNG',
          data: locations[i],
          // draggable: true,
          animation: google.maps.Animation.DROP,
          icon: icon
        });

        marker[i].setMap(map);
        bounds.extend(marker[i].position);
        marker[i].addListener('click', function(){
          active_marker = this;
          toggleBounce(this);
          showMarkerModal(this);
        });
        markerContent[locations[i].id] = 
          '<div class="content">'+
            '<div class="pull-left">'+
              '<img src="'+locations[i].tenant_logo+'" alt="'+locations[i].tenant_name+'" class="marker-logo"/>'+
            '</div>'+
            '<div class="pull-right">'+
              '<h5>'+locations[i].tenant_name+'</h5>'+
            '</div>'+
          '</div>';

        marker[i].addListener('mouseover', function(){
          console.log(this.data.id);
          infowindow.setContent(markerContent[this.data.id]);
          infowindow.open(map, this);
        });
        marker[i].addListener('mouseout', function(){
          infowindow.close();
        });
      // }
    }
    mapCenter = bounds;
    map.fitBounds(bounds);
    recenterMap();
  }
  else{
    locations = [{latitude: -6.9032739,longitude: 107.5729448}];
    map.setCenter(new google.maps.LatLng(parseFloat(locations[0].latitude), parseFloat(locations[0].longitude)));
    map.setZoom(12);
  }

  if (isMobile) {
    var legend = document.getElementById('googft-legend');
    var legendOpenButton = document.getElementById('googft-legend-open');
    var legendCloseButton = document.getElementById('googft-legend-close');
  }
}

google.maps.event.addDomListener(window, 'load', initialize);

function calculateAndDisplayRoute(directionsService, directionsDisplay, data=[]) {
  directionsDisplay.setMap(map);
  locations = data;
  if(locations != null){
    var waypts = [];
    var bounds = new bounds_initial();
    for(var i=1;i<(locations.length-1);i++){
      marker[i] = addMarker(locations[i], i);
      marker[i].addListener('click', function(){
        active_marker = this;
        toggleBounce(this);
        showMarkerModal(this);
      });
      markerContent[locations[i].id] = 
          '<div class="content">'+
            '<div class="pull-left">'+
              '<img src="'+locations[i].tenant_logo+'" alt="'+locations[i].tenant_name+'" class="marker-logo"/>'+
            '</div>'+
            '<div class="pull-right">'+
              '<h5>'+locations[i].tenant_name+'</h5>'+
            '</div>'+
          '</div>';

        marker[i].addListener('mouseover', function(){
          infowindow.setContent(markerContent[this.data.id]);
          infowindow.open(map, this);
        });
      marker[i].addListener('mouseout', function(){
        infowindow.close();
      });
      bounds.extend(marker[i].position);
      waypts.push({
        location: ""+locations[i].latitude+","+locations[i].longitude,
        stopover: true
      });
    }
    marker[0] = addMarker(locations[0], 0);
    marker[(locations.length-1)] = addMarker(locations[(locations.length-1)], (locations.length-1));
    bounds.extend(marker[0].position);
    bounds.extend(marker[(locations.length-1)].position);
    marker[0].addListener('click', function(){
      active_marker = this;
      toggleBounce(this);
      showMarkerModal(this);
    });
    markerContent[locations[i].id] = 
      '<div class="content">'+
        '<div class="pull-left">'+
          '<img src="'+locations[i].tenant_logo+'" alt="'+locations[i].tenant_name+'" class="marker-logo"/>'+
        '</div>'+
        '<div class="pull-right">'+
          '<h5>'+locations[i].tenant_name+'</h5>'+
        '</div>'+
      '</div>';

    marker[0].addListener('mouseover', function(){
      infowindow.setContent(markerContent[this.data.id]);
      infowindow.open(map, this);
    });
    marker[0].addListener('mouseout', function(){
      infowindow.close();
    });
    marker[(locations.length-1)].addListener('click', function(){
      active_marker = this;
      toggleBounce(this);
      showMarkerModal(this);
    });
    markerContent[locations[i].id] = 
      '<div class="content">'+
        '<div class="pull-left">'+
          '<img src="'+locations[i].tenant_logo+'" alt="'+locations[i].tenant_name+'" class="marker-logo"/>'+
        '</div>'+
        '<div class="pull-right">'+
          '<h5>'+locations[i].tenant_name+'</h5>'+
        '</div>'+
      '</div>';

    marker[(locations.length-1)].addListener('mouseover', function(){
      infowindow.setContent(markerContent[this.data.id]);
      infowindow.open(map, this);
    });
    marker[(locations.length-1)].addListener('mouseout', function(){
      infowindow.close();
    });
    

    directionsService.route({
      origin: ""+locations[0].latitude+","+locations[0].longitude,
      destination: ""+locations[(locations.length-1)].latitude+","+locations[(locations.length-1)].longitude,
      waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: 'DRIVING'
    }, function(response, status) {
      if (status === 'OK') {
        console.log(response);
        directionsDisplay.setDirections(response);
        var route = response.routes[0];
      } else {
        window.alert('There something wrong with the request  ' + status);
      }
    });
    mapCenter = bounds;
    map.fitBounds(bounds);
  }
  else{
    directionsDisplay.setMap(null);
    recenterMap();
  }
}

function packageLines(data){
  removeMarker();
  calculateAndDisplayRoute(directionsService, directionsDisplay, data);
  recenterMap();

}

function removeMarker(){
  marker.forEach(function(value){
    value.setMap(null);
    active_label = value.getLabel();
    if(active_label){
      active_label.text = '';
      value.setLabel(active_label);
    }
  });
}

function addMarker(datum, i) {
  console.log('addMarker');
  console.log(datum);
  return new marker_initial({
    // @see http://stackoverflow.com/questions/2436484/how-can-i-create-numbered-map-markers-in-google-maps-v3 for numbered icons
    icon: icon,
    data: datum,
    position: {lat:parseFloat(datum.latitude),lng:parseFloat(datum.longitude)},
    map: map,
    // animation: google.maps.Animation.DROP,
    label: {
      text: ""+(i+1),
      color: 'white',
      fontWeight: 'bold',
      fontSize: '13px'
    },
    labelClass: "label-tour",
    labelColor: '#fff',
    // labelAnchor: new google.maps.Point(0, 10),
    optimized: false,
    zIndex: 999+i
  })
}

function toggleBounce(marker) {
  var active_label;
  if(last_active_marker === undefined){
    last_active_marker = marker;
  }
  else{
    last_active_marker.setIcon(icon);
    active_label = last_active_marker.getLabel();
    if(active_label){
      active_label.color = 'white';
      last_active_marker.setLabel(active_label);
    }
  }
    marker.setIcon(active_icon);
    active_label = marker.getLabel();
    if(active_label){
      active_label.color = 'black';
      marker.setLabel(active_label);
    }
}

function newLocation(newLat,newLng)
{
  map.setCenter({
    lat : newLat,
    lng : newLng
  });
  map.setZoom(15);
  recenterMap();
  // map.setMapTypeId(google.maps.MapTypeId.SATELLITE);
}

function showMarkerModal(e){
  $("html").animate({ scrollTop: 0 }, "slow");
  loadingShow();
  if(active_marker){
    $.ajax({
      url: "{{route('tour_packages.detail')}}",
      type: 'get',
      data: {
        id: e.data.id
      },
      success: function(data){
        console.log(data);
        if(data){
          if(data.package_name){
            $('.modal-marker .modal-title').show();
            $('.package_name').text(data.package_name || "Detail Tenant");            
          }
          else{
            $('.modal-marker .modal-title').hide();
          }
          $('.tenant_name').text(data.tenant_name || "-");
          var detail_text = "";
          detail_text += "<tr><td colspan='3'><span>"+(data.address || "-")+"</span></td></tr>";
          detail_text += "<tr><td>Jam operasional</td><td> : </td><td> <span>"+((data.operasional) ? (data.operasional.store_open_at || "")+" - "+(data.operasional.store_close_at || "") : "-")+"</span></td></tr>";
          detail_text += "<tr><td>Access</td><td> : </td><td> <span>"+(data.access || "-")+"</span></td></tr>";
          detail_text += "<tr><td>Facility</td><td> : </td><td> <span>"+((data.facilities || []).join(", ") || "-")+"</span></td></tr>";
          detail_text += "<tr><td>Capacity</td><td> : </td><td> <span>"+(data.capacity || "-")+"</span></td></tr>";
          detail_text += "<tr><td>Contact</td><td> : </td><td> <span>"+(data.contact || "-")+"</span></td></tr>";
          if(data.social_sites){
            data.social_sites.forEach(function(site,x){
              if(site.social_site_type!=null && site.social_site_url!=null){
                detail_text += "<tr><td>"+site.social_site_type+"</td><td> : </td><td> <span>"+(site.social_site_url || "-")+"</span></td></tr>";
              }
            });
          }
          if(data.provider){
            detail_text += "<tr><td>Provider</td><td> : </td><td> <span>"+(data.provider || "-")+"</span></td></tr>";
          }
          detail_text += "<tr><td colspan='3'><span class='marker-image-box'>";
          if(data.galleries != null || data.galleries != undefined){
            data.galleries.forEach(function(gallery,x){ 
              if(x<2){
                detail_text += "<img class='marker-detail-image' src='"+(gallery.img_gallery || "")+"' />";
              }
            });
          }
          detail_text != "</span></td></tr>";

          
          $('tbody#detail-tenant').html(detail_text);
          $('#modal-marker').modal('show');
          $('.modal-backdrop').removeClass('col-xs-12 col-sm-8');
          $('.modal-backdrop').addClass('col-xs-12 col-sm-8');
          setTimeout(function(){
            loadingHide();
          },300);
        }
        else{
          console.log('tenant tidak ada');
          loadingHide();
        }
      },
      error: function(e){
        console.log(e);
      },
      dataType: 'json'
    });
    
  }
}

$('#modal-marker .close').on('click',function(){
  active_marker.setAnimation(null);
  active_marker.setIcon(icon);
  active_label = active_marker.getLabel();
  if(active_label){
    active_label.color = 'white';
    active_marker.setLabel(active_label);
  }
});

function setNewMarkers(data){
  removeMarker();
  directionsDisplay.setMap(null);
  if(data != null){
    if(data.length !== undefined){
      var bounds = new bounds_initial();
      data.forEach(function(datum,x){
        marker[x] = new marker_initial({
          position: {lat:parseFloat(datum.latitude),lng:parseFloat(datum.longitude)},
          data: datum,
          icon: icon,
          map: map
        });
        marker[x].addListener('click', function(){
          active_marker = this;
          toggleBounce(this);
          showMarkerModal(this);
        });
        markerContent[datum.id] = 
      '<div class="content">'+
        '<div class="pull-left">'+
          '<img src="'+datum.tenant_logo+'" alt="'+datum.tenant_name+'" class="marker-logo"/>'+
        '</div>'+
        '<div class="pull-right">'+
          '<h5>'+datum.tenant_name+'</h5>'+
        '</div>'+
      '</div>';

    marker[x].addListener('mouseover', function(){
      infowindow.setContent(markerContent[this.data.id]);
      infowindow.open(map, this);
    });
        marker[x].addListener('mouseout', function(){
          infowindow.close();
        });
        bounds.extend(marker[x].position);
      });
      mapCenter = bounds;
      map.fitBounds(bounds);
      recenterMap();

    }
    else{
      recenterMap();
    }
  }
  else{
    recenterMap();
  }
}

function recenterMap(){
  console.log(mapCenter);
  if(mapCenter){
    if(marker == []){
      var bounds = new bounds_initial();
      var marker = addMarker({latitude: -6.9032739,longitude: 107.5729448}, 0);
      bounds.extend(marker.position);
      map.fitBounds(bounds);
      // map.panBy(200, 0);
    }
    else{
      map.fitBounds(mapCenter);
      // map.panBy(200, 0);
    }
  }
}
</script>