@if(@$packages['data'])
	@foreach($packages['data'] as $key => $value)
		<li class="radio">
			<label>{!!$value['package_name']!!}<input type="radio" name="package" class="pull-right package" value="{!!$value['id']!!}" onclick="filterPackage(this);" {{(@$selected_package == $value['id'])? 'checked' : ''}}><span class="checkmark"></span></label>
		</li>
	@endforeach
@endif