@extends('layouts.application')

@section('title')
    Tour Packages - List Tour
@stop

@section('custom_meta')

@stop

@section('custom_styles')
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_map1.css')}}">
	<style type="text/css">
		#map{
			width: 100% !important;
			height: 1000px !important;
		}
	</style>
@stop

@section('head_scripts')
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCEKkG9jlRbavkuE6unDPFGjZ6Ur5cYjHM&libraries=geometry"></script>
	<script type="text/javascript" src="{{asset('assets/js/custom_maps1.js')}}"></script>
@stop

@section('content')
	<div class="container-fluid full-tour">
		<div class="jumbotron jumbotron-tour"></div>
	</div>
	<div class="container-fluid tour-package">
		<div class="col-sm-4 col-md-3 col-lg-3 tour-side tour-side2">
			<h3 class="tp-1">BANDUNG</h3>
			<h3 class="tp-2">CREATIVE</h3>
			<h3 class="tp-3">TOURISM</h3>
			<hr>
			<h4 class="tp-menu-header">TOUR PACKAGES</h4>
			<hr>
			<h4><img src="{{asset('assets/images/tourpackages/Icon_01.png')}}"> CULLINARY</h4>
			<h4><img src="{{asset('assets/images/tourpackages/Icon_02.png')}}"> FASHION</h4>
			<h4><img src="{{asset('assets/images/tourpackages/Icon_03.png')}}"> CRAFT</h4>
			<h4><img src="{{asset('assets/images/tourpackages/Icon_04.png')}}"> CREATIVE GALLERY</h4>
			<h4><img src="{{asset('assets/images/tourpackages/Icon_05.png')}}"> HOTEL</h4>
		</div>
		<div class="tour-map">
			<div id="map" class="map"></div>
		</div>
	</div>
@stop

@section('custom_scripts')

@stop