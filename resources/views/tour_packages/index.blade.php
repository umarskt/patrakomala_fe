@extends('layouts.tour-package')

@section('title')
    @lang('general.title.tour-package')
@stop

@section('custom_meta')
	<meta name="description" content="{{trans('general.desc.beat')}}">
@stop

@section('custom_styles')
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_map1.css')}}">
@stop

@section('head_scripts')
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEKkG9jlRbavkuE6unDPFGjZ6Ur5cYjHM&libraries=geometry&language=id&region=ID"></script> -->
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEKkG9jlRbavkuE6unDPFGjZ6Ur5cYjHM&language=id&region=ID"></script>
	<!-- <script type="text/javascript" src="{{asset('assets/js/tour-map.js')}}"></script> -->
@stop

@section('content')
	<div class="container-fluid tour-package full">
		<div class="pull-right col-xs-5 col-sm-4 right-option">
			<div class="header-tour">
				<h3>
					<a href="{{route('tour_packages.index')}}">
						<span class="h31">BANDUNG</span>
						<span class="h32">CREATIVE</span>
						<span class="h33">BELT</span>
					</a>
				</h3>
				<hr>
			</div>
			<p class="col-xs-12">
				<a href="javascript:void(0);" class="btn btn-sm pull-left recenter" style="color:#fff;text-decoration:none;border-radius:5px;background: #a7a7a7;"><i class="fa fa-dot-circle-o"></i> Re-center</a>
				<span class="tenant-found pull-right">{!!(@$tenants)? '' : 'found: 0'!!}</span>
			</p>
			<div class="search-box">
				<div class="input-group">
				  <input type="text" class="form-control" name="search">
				  <span class="input-group-btn">
					<button class="btn btn-default search-btn" type="button"><span class="sm">@lang('general.label.search')</span><span class="xs"><i class="fa fa-search"></i></span></button>
				  </span>
				</div>
			</div>
			
			<ul class="parent">
				<a href="javascript:void(0);"><li class="parent-title">@lang('general.label.belt') <i class="fa fa-chevron-left pull-right"></i><span id="belt-info-filter" class="info-filter"></span></li></a>
				<ul class="child">
				@if(@$belts)
					@foreach($belts as $key => $value)
						<li class="checkbox">
							<label>{!!$value['belt_title']!!}<input type="checkbox" name="belt[]" class="pull-right belt" value="{!!$value['id']!!}"><span class="checkboxmark"></span></label>
						</li>
					@endforeach
				@endif
				</ul>
			</ul>
			<ul class="parent">
				<a href="javascript:void(0);"><li class="parent-title">@lang('general.label.subsector') <i class="fa fa-chevron-left pull-right"></i> <span id="subsektor-info-filter" class="info-filter"></span></li></a>
				<ul class="child">
				@if(@$subsectors)
					@foreach($subsectors as $key => $value)
						<li class="checkbox">
							<label>{!!$value['sub_sector_name']!!}<input type="checkbox" name="subsektor[]" class="pull-right subsektor" value="{!!$value['id']!!}" data-title="{!!$value['sub_sector_name']!!}" id="{{($value['sub_sector_slug'] == 'radio_and_tv') ? 'turun': ''}}"><span class="checkboxmark"></span></label>
						</li>
					@endforeach
				@endif
				</ul>
			</ul>
			<ul class="parent list-package">
				<a href="javascript:void(0);"><li class="parent-title">@lang('general.label.tour-package') <i class="fa fa-chevron-left pull-right"></i> <span id="package-info-filter" class="info-filter"></span></li></a>
				<ul class="child">
				@if(@$packages)
					@foreach($packages as $key => $value)
						<li class="radio">
							<label>{!!$value['package_name']!!}<input type="radio" name="package" class="pull-right package" value="{!!$value['id']!!}" onclick="filterPackage(this);"><span class="checkmark"></span></label>
						</li>
					@endforeach
				@endif
				</ul>
			</ul>
		</div>
		<div class="pull-left col-xs-12 col-sm-12 left-content">
			<a href="javascript:void(0);" class="btn btn-default pull-right tour-coll menu-collapser"><i class="fa fa-angle-double-right fa-2x"></i></a>
			<div id="map" class="map"></div>
			<div id="modal-marker" class="modal fade modal-marker col-xs-12 col-sm-8" role="dialog" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle"></i></button>
							<h4 class="modal-title"> <span class="package_name">Paket 1</span></h4>
						</div>
						<div class="modal-body">
							<h4><img src="{{asset('assets/images/marker-icon.png')}}" alt="">  <span class="tenant_name">THE PARLOR</span></h4>
							<hr>
							<table>
								<tbody id="detail-tenant">
									<tr> <td colspan='3'><span class="address">Lokasi Jl Raya Ranca Kendal Luhur no 9, Dago, Coblong Bandung, Ciburial, Cimenyan, Kota Bandung Jawa Barat, 40191</span></td></tr>
									<tr><td>Waktu Operasional</td><td> : </td><td><span class="operational_time">11.00 - 22.00</span></td></tr>
									<tr><td>Akses</td><td> : </td><td> <span class="access">Melalui jalan bukit dago</span></td></tr>
									<tr><td>Kapasitas</td><td> : </td><td> <span class="capacity">2000m2</span></td></tr>
									<tr><td>Fasilitas</td><td> : </td><td> <span class="facility">Cafe, Gallery Store</span></td></tr>
									<tr class='social'></tr>
									<tr><td>Kontak</td><td> : </td><td> <span class="contact">0823-1668-0947</span></td></tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')
	@include('tour_packages._maps_js')
	<script type="text/javascript">
		var selected_subsektor = [];
		var search;
		var selected_belt = [];
		var selected_package = [];
		var checked_package;

		$(document).ready(function(){
			@if(@$packages)
				$('ul.parent.list-package a').click();
			@endif
		});

		$('.recenter').click(function(){
			recenterMap();
		});
		$('.subsektor').click(function(){
			selected_subsektor = [];
			$('input[name=package]:checked').prop('checked',false);
			selected_package = [];
			checked_package = null;
			$('.subsektor:checked').map(function(){
				selected_subsektor.push(parseInt(this.value));
			});
			filter_map();
		});

		$('.belt').click(function(){
			selected_belt = [];
			$('.belt:checked').map(function(){
				selected_belt.push(parseInt(this.value));
			});
			// selected_package = ($('input[name=package]:checked')[0]) ? [$('input[name=package]:checked')[0].value] : [];
			$('input[name=package]:checked').prop('checked',false);
			selected_package = [];
			checked_package = null;
			filter_map();
			$.ajax({
				url: "{{route('tour_packages.list-package')}}",
				type: 'get',
				data: {
					belt_id: selected_belt,
					selected_package: (selected_package) ? selected_package[0] : null
				},
				success: function(data){
					console.log('suksesbelt');
					$('.list-package .child').html(data);
				},
				error: function(e){
					console.log(e);
				},
				dataType: 'html'
			});
		});

		$('.search-btn').click(function(){
			search = $('input[name=search]').val();
			selected_package = [];
			selected_belt = [];
			selected_subsektor = [];
			filter_map();

		});

		function filterPackage(e){
			$(e).map(function(){
				if(e.value == checked_package){
					$(e).prop('checked',false);
					selected_package = [];
					filter_map();
					checked_package = null;
				}
				else{
					checked_package = e.value;
					selected_package = [parseInt(e.value)];
					filter_map2();
				}
			});

		};

		function filter_map2(){
          	$('#modal-marker').modal('hide');
			loadingShow();
			var params = {
				'subsector': selected_subsektor,
				'keyword': search,
				'belt': selected_belt,
				'package': selected_package
			};
			$.ajax({
				url: "{{route('tour_packages.index')}}",
				type: 'get',
				data: params,
				success: function(data){
					if(data.error !== 1){
						console.log('sukses');
						console.log(data);
						if(data.data != null){
							if(data.data.length >= 0){
								$('.tenant-found').text('found: '+data.data.length);
							}
							else{
								$('.tenant-found').text('found: 0');
							}
						}
						else{
							$('.tenant-found').text('found: 0');
						}
						packageLines(data.data);
					}
					loadingHide();
				},
				error: function(e){
					console.log(e);
					loadingHide();
				},
				dataType: 'json'
			});
		}

		function filter_map(){
          	$('#modal-marker').modal('hide');
			loadingShow();
			var params = {
				'subsector': selected_subsektor,
				'keyword': search,
				'belt': selected_belt,
				'package': selected_package
			};
			$.ajax({
				url: "{{route('tour_packages.index')}}",
				type: 'get',
				data: params,
				success: function(data){
					if(data.error !== 1){
						console.log('sukses');
						console.log(data);
						if(data.data != null){
							if(data.data.length >= 0){
								$('.tenant-found').text('found: '+data.data.length);
							}
							else{
								$('.tenant-found').text('found: 0');
							}
						}
						else{
							$('.tenant-found').text('found: 0');
						}
						setNewMarkers(data.data);
					}
					loadingHide();
				},
				error: function(e){
					console.log('e');
					console.log(e);
					loadingHide();
				},
				dataType: 'json'
			});
		}
	</script>
@stop