@extends('layouts.application2')

@section('title')
    @lang('general.title.project-work')
@stop

@section('custom_meta')

@stop

@section('custom_styles')

@stop

@section('content')
	<div class="container-fluid project-work">
		<h3 class="text-uppercase center page-title">@lang('general.label.work')</h3>
		<div class="row col-xs-12">
			<div class="col-md-1 home-red-list-empty">
				
			</div>
			<div class="col-md-2 col-sm-4 home-red-list">
				<h4>Search</h4>
				<div class="option-icon">
					<img src="{{asset('assets/images/project-works/01.png')}}">
				</div>
				<p>
					@lang('general.desc.work.1')
				</p>
			</div>
			<div class="col-md-2 col-sm-4 home-red-list">
				<h4>Post</h4>
				<div class="option-icon">
					<img src="{{asset('assets/images/project-works/02.png')}}">
				</div>
				<p>
					@lang('general.desc.work.2')
				</p>
			</div>
			<div class="col-md-2 col-sm-4 home-red-list">
				<h4>Spread and Inform</h4>
				<div class="option-icon">
					<img src="{{asset('assets/images/project-works/03.png')}}">
				</div>
				<p>
					@lang('general.desc.work.3')
				</p>				
			</div>
			<div class="col-md-2 col-sm-4 home-red-list">
				<h4>Contact and Consult</h4>
				<div class="option-icon">
					<img src="{{asset('assets/images/project-works/04.png')}}">
				</div>
				<p>
					@lang('general.desc.work.4')
				</p>
			</div>
			<div class="col-md-2 col-sm-4 home-red-list">
				<h4>Make a deal</h4>
				<div class="option-icon">
					<img src="{{asset('assets/images/project-works/05.png')}}">
				</div>
				<p>
					@lang('general.desc.work.5')
				</p>
			</div>
			<div class="col-md-1 home-red-list-empty">
				
			</div>
		</div>
	</div>
@stop

@section('custom_scripts')
@stop