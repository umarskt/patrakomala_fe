// for FAQ
$('.btn-drop-faq').on('click',function(){
	if($(this).children('i.fa-chevron-left').hasClass('hide')){
		$(this).closest('.drop-faq').children('div.detail-faq').first().slideDown('slow');
		$(this).children('i.fa-chevron-down').addClass('hide');
		$(this).children('i.fa-chevron-left').removeClass('hide');
	}
	else{
		$(this).closest('.drop-faq').children('div.detail-faq').first().slideUp('slow');	
		$(this).children('i.fa-chevron-left').addClass('hide');
		$(this).children('i.fa-chevron-down').removeClass('hide');
	}
});

$(document).ready(function(){	
	// for right-option
	$('ul.child').css('display','none');
});

$('ul.parent a').on('click',function(){
	rightMenuBehavior(this);
});

function rightMenuBehavior(e){
	if($(e).next('ul.child').is(':hidden')){
		$(e).next('ul.child').slideDown('slow');
		$(e).find('li>i.fa').removeClass('fa-chevron-left');
		$(e).find('li>i.fa').addClass('fa-chevron-down');
		$(e).parent().find('li.parent-title').css('background','#fe7e41');
		// for other menu
		$(e).parent().siblings().find('ul.child').slideUp();
		$(e).parent().siblings().find('li>i.fa').removeClass('fa-chevron-down');
		$(e).parent().siblings().find('li>i.fa').addClass('fa-chevron-left');
		$(e).parent().siblings().find('li.parent-title').css('background','rgb(240, 240, 240)');
	}
	else if($(e).next('ul.child').is(':visible')){
		$(e).next('ul.child').slideUp('fast');
		$(e).find('li>i.fa').removeClass('fa-chevron-down');
		$(e).find('li>i.fa').addClass('fa-chevron-left');
		$(e).parent().find('li.parent-title').css('background','rgb(240, 240, 240)');
	}
}

function filteredBy(e){
	$(e).parents('ul.parent').find('span.info-filter').html('<b>Filtered by: '+$(e).data('title')+'</b>');
}