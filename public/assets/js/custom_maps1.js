var map;
  function initialize() {
    var bandungCenter = new google.maps.LatLng(-6.8984945, 107.6303244);
    var exampleMarker1 = new google.maps.LatLng(-6.914959, 107.7067274);
    var exampleMarker2 = new google.maps.LatLng(-6.9178365, 107.6517312);
    var isMobile = (navigator.userAgent.toLowerCase().indexOf('android') > -1) ||
      (navigator.userAgent.match(/(iPod|iPhone|iPad|BlackBerry|Windows Phone|iemobile)/));
    if (isMobile) {
      var viewport = document.querySelector("meta[name=viewport]");
      viewport.setAttribute('content', 'initial-scale=1.0, user-scalable=no');
    }
    var mapDiv = document.getElementById('map');
    mapDiv.style.width = isMobile ? '100%' : '100%';
    mapDiv.style.height = isMobile ? '100%' : '500px';
    map = new google.maps.Map(mapDiv, {
      center: bandungCenter,
      zoom: 12.7,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend-open'));
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend'));

    layer = new google.maps.FusionTablesLayer({
      heatmap: { enabled: false },
      query: {
        select: "col6",
        from: "1zUR6IlFME_r4eSqlhbHtwJ4qA3MFM7tQRV2F271L",
        where: ""
      },
      options: {
        styleId: 2,
        templateId: 2
      }
    });

    layer.setMap(map);

    var marker = new google.maps.Marker({
      position: bandungCenter,
      title: 'KOTA BANDUNG',
      // draggable: true,
      animation: google.maps.Animation.DROP
    });

    marker.setMap(map);

    var marker2 = new google.maps.Marker({
      position: exampleMarker1,
      title: 'CIBIRU',
      // draggable: true,
      animation: google.maps.Animation.DROP
    });

    marker2.setMap(map);

    var marker3 = new google.maps.Marker({
      position: exampleMarker2,
      title: 'ANTAPANI',
      // draggable: true,
      animation: google.maps.Animation.DROP
    });

    marker3.setMap(map);

    var contentString = 
      '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Kota Bandung</h1>'+
        '<div id="bodyContent">'+
          '<p>Kota metropolitan.</p>'+
          '<p><a href="/tenants/1/detail">Detail</a></p>'+
        '</div>'+
      '</div>';

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    marker.addListener('click', toggleBounce);
    marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
    // marker.setMap(null);
    var contentString2 = 
      '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Kota Bandung</h1>'+
        '<div id="bodyContent">'+
          '<p>Kota metropolitan.</p>'+
          '<p><a href="#">Detail</a></p>'+
        '</div>'+
      '</div>';

    var infowindow2 = new google.maps.InfoWindow({
      content: contentString2
    });

    marker2.addListener('click', function() {
          infowindow2.open(map, marker2);
        });

    var line = new google.maps.Polyline({
      path: [bandungCenter, exampleMarker1,exampleMarker2],
      strokeColor: "#FF0000",
      strokeOpacity: 1.0,
      strokeWeight: 10,
      geodesic: true
    });

    line.setMap(map);

    function toggleBounce() {
      if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
      } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
      }
    }

    if (isMobile) {
      var legend = document.getElementById('googft-legend');
      var legendOpenButton = document.getElementById('googft-legend-open');
      var legendCloseButton = document.getElementById('googft-legend-close');
      legend.style.display = 'none';
      legendOpenButton.style.display = 'block';
      legendCloseButton.style.display = 'block';
      legendOpenButton.onclick = function() {
        legend.style.display = 'block';
        legendOpenButton.style.display = 'none';
      }
      legendCloseButton.onclick = function() {
        legend.style.display = 'none';
        legendOpenButton.style.display = 'block';
      }
    }
  }

  google.maps.event.addDomListener(window, 'load', initialize);

  function newLocation(newLat,newLng)
  {
    map.setCenter({
      lat : newLat,
      lng : newLng
    });
    map.setZoom(15);
    // map.setMapTypeId(google.maps.MapTypeId.SATELLITE);
  }