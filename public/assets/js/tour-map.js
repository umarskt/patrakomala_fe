var map;
  function initialize() {
    var bandungCenter = new google.maps.LatLng(-6.8984945, 107.6303244);
    var isMobile = (navigator.userAgent.toLowerCase().indexOf('android') > -1) ||
      (navigator.userAgent.match(/(iPod|iPhone|iPad|BlackBerry|Windows Phone|iemobile)/));
    if (isMobile) {
      var viewport = document.querySelector("meta[name=viewport]");
      viewport.setAttribute('content', 'initial-scale=1.0, user-scalable=no');
    }
    var mapDiv = document.getElementById('map');
    mapDiv.style.width = isMobile ? '100%' : '100%';
    mapDiv.style.height = isMobile ? '100%' : '500px';
    map = new google.maps.Map(mapDiv, {
      center: bandungCenter,
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend-open'));
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend'));

    var icon = {
        url: '/assets/images/marker-icon.png',
        scaledSize: new google.maps.Size(50, 50),
        origin: new google.maps.Point(0,0),
        anchor: new google.maps.Point(0, 0)
    };

    var marker = new google.maps.Marker({
      position: bandungCenter,
      title: 'KOTA BANDUNG',
      // draggable: true,
      animation: google.maps.Animation.DROP,
      icon: icon
    });

    marker.setMap(map);

    var contentString = 
      '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Kota Bandung</h1>'+
        '<div id="bodyContent">'+
          '<p>Kota metropolitan.</p>'+
          '<p class="link-detail"><a href="/tenants/1/detail" class="btn btn-detault btn-sm btn-flat btn-map">Detail</a></p>'+
        '</div>'+
      '</div>';

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    marker.addListener('click', toggleBounce);
    marker.addListener('click', function() {
          // infowindow.open(map, marker);
          showMarkerModal();
        });

    function toggleBounce() {
      if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
      } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
      }
    }

    if (isMobile) {
      var legend = document.getElementById('googft-legend');
      var legendOpenButton = document.getElementById('googft-legend-open');
      var legendCloseButton = document.getElementById('googft-legend-close');
      // legend.style.display = 'none';
      // legendOpenButton.style.display = 'block';
      // legendCloseButton.style.display = 'block';
      // legendOpenButton.onclick = function() {
      //   legend.style.display = 'block';
      //   legendOpenButton.style.display = 'none';
      // }
      // legendCloseButton.onclick = function() {
      //   legend.style.display = 'none';
      //   legendOpenButton.style.display = 'block';
      // }
    }
  }

  google.maps.event.addDomListener(window, 'load', initialize);

  function newLocation(newLat,newLng)
  {
    map.setCenter({
      lat : newLat,
      lng : newLng
    });
    map.setZoom(15);
    // map.setMapTypeId(google.maps.MapTypeId.SATELLITE);
  }

  function showMarkerModal(){
    $('#modal-marker').modal('show');
    $('.modal-backdrop').addClass('col-xs-12 col-sm-8');
  }