var map;
  function initialize() {
    var bandungCenter = new google.maps.LatLng(-6.8984945, 107.6303244);
    var isMobile = (navigator.userAgent.toLowerCase().indexOf('android') > -1) ||
      (navigator.userAgent.match(/(iPod|iPhone|iPad|BlackBerry|Windows Phone|iemobile)/));
    if (isMobile) {
      var viewport = document.querySelector("meta[name=viewport]");
      viewport.setAttribute('content', 'initial-scale=1.0, user-scalable=no');
    }
    var mapDiv = document.getElementById('map');
    mapDiv.style.width = isMobile ? '100%' : '100%';
    mapDiv.style.height = isMobile ? '100%' : '300px';
    map = new google.maps.Map(mapDiv, {
      center: bandungCenter,
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend-open'));
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend'));
    var input = document.getElementById('address_street');
    var searchBox = new google.maps.places.SearchBox(input);

    var icon = {
      url: '/assets/images/marker-icon.png',
      scaledSize: new google.maps.Size(37, 45),
      origin: new google.maps.Point(0,0),
      anchor: new google.maps.Point(0, 0)
  };

    var marker = new google.maps.Marker({
      position: bandungCenter,
      title: 'KOTA BANDUNG',
      draggable: true,
      animation: google.maps.Animation.DROP,
      icon: icon
    });

    marker.setMap(map);

    var contentString = 
      '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Kota Bandung</h1>'+
        '<div id="bodyContent">'+
          '<p>Kota metropolitan.</p>'+
          '<p class="link-detail"><a href="/tenants/1/detail" class="btn btn-detault btn-sm btn-flat btn-map">Detail</a></p>'+
        '</div>'+
      '</div>';

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    marker.addListener('click', toggleBounce);
    marker.addListener('click', function() {
          // infowindow.open(map, marker);
          // showMarkerModal();
        });

    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // Clear out the old markers.
      marker.setMap(null);

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }

        // Create a marker for each place.
        marker = new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location,
          draggable: true,
          animation: google.maps.Animation.DROP
        });

        google.maps.event.addListener(marker, 'dragend', function(evt){
          // document.getElementById('current').innerHTML = '<p>Lokasi terpilih: Lat: <b>' + evt.latLng.lat().toFixed(8) + '</b> & Lng: <b>' + evt.latLng.lng().toFixed(8) + '</b></p>';
          document.getElementById('current').innerHTML = '<p>Lokasi terpilih:</p>';
          document.getElementById('address_latitude').value = evt.latLng.lat().toFixed(8);
          document.getElementById('address_longitude').value = evt.latLng.lng().toFixed(8);
        });

        google.maps.event.addListener(marker, 'dragstart', function(evt){
          document.getElementById('current').innerHTML = '<p>Mencari lokasi ...</p>';
          document.getElementById('address_latitude').value = null;
          document.getElementById('address_longitude').value = null;
        });
        // document.getElementById('current').innerHTML = '<p>Lokasi terpilih: Lat: <b>' + place.geometry.location.lat() + '</b> & Lng: <b>' + place.geometry.location.lng() + '</b></p>';
        document.getElementById('current').innerHTML = '<p>Lokasi terpilih:</p>';
        document.getElementById('address_latitude').value = place.geometry.location.lat();
        document.getElementById('address_longitude').value = place.geometry.location.lng();
        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });

    function toggleBounce() {
      if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
      } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
      }
    }

    if (isMobile) {
      var legend = document.getElementById('googft-legend');
      var legendOpenButton = document.getElementById('googft-legend-open');
      var legendCloseButton = document.getElementById('googft-legend-close');
      // legend.style.display = 'none';
      // legendOpenButton.style.display = 'block';
      // legendCloseButton.style.display = 'block';
      // legendOpenButton.onclick = function() {
      //   legend.style.display = 'block';
      //   legendOpenButton.style.display = 'none';
      // }
      // legendCloseButton.onclick = function() {
      //   legend.style.display = 'none';
      //   legendOpenButton.style.display = 'block';
      // }
    }

    google.maps.event.addListener(marker, 'dragend', function(evt){
      // document.getElementById('current').innerHTML = '<p>Lokasi terpilih: Lat: <b>' + evt.latLng.lat().toFixed(8) + '</b> & Lng: <b>' + evt.latLng.lng().toFixed(8) + '</b></p>';
      document.getElementById('current').innerHTML = '<p>Lokasi terpilih:</p>';
      document.getElementById('address_latitude').value = evt.latLng.lat().toFixed(8);
      document.getElementById('address_longitude').value = evt.latLng.lng().toFixed(8);
    });

    google.maps.event.addListener(marker, 'dragstart', function(evt){
      document.getElementById('current').innerHTML = '<p>Mencari lokasi ...</p>';
      document.getElementById('address_latitude').value = null;
      document.getElementById('address_longitude').value = null;
    });
  }

  google.maps.event.addDomListener(window, 'load', initialize);

  function newLocation(newLat,newLng)
  {
    map.setCenter({
      lat : newLat,
      lng : newLng
    });
    map.setZoom(15);
    // map.setMapTypeId(google.maps.MapTypeId.SATELLITE);
  }

  function showMarkerModal(){
    $('#modal-marker').modal('show');
    $('.modal-backdrop').addClass('col-xs-12 col-sm-8');
  }

  function initMultiMaps(no) {
    var maps;
    var bandungCenter = new google.maps.LatLng(-6.8984945, 107.6303244);
    var isMobile = (navigator.userAgent.toLowerCase().indexOf('android') > -1) ||
      (navigator.userAgent.match(/(iPod|iPhone|iPad|BlackBerry|Windows Phone|iemobile)/));
    if (isMobile) {
      var viewport = document.querySelector("meta[name=viewport]");
      viewport.setAttribute('content', 'initial-scale=1.0, user-scalable=no');
    }
    var mapDivs = document.getElementById('map'+no);
    mapDivs.style.width = isMobile ? '100%' : '100%';
    mapDivs.style.height = isMobile ? '100%' : '300px';
    maps = new google.maps.Map(mapDivs, {
      center: bandungCenter,
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    maps.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend-open'));
    maps.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend'));
    var input = document.getElementById('address_street'+no);
    var searchBox = new google.maps.places.SearchBox(input);

    var icon = {
      url: '/assets/images/marker-icon.png',
      scaledSize: new google.maps.Size(37, 45),
      origin: new google.maps.Point(0,0),
      anchor: new google.maps.Point(0, 0)
    };

    var marker = new google.maps.Marker({
      position: bandungCenter,
      title: 'KOTA BANDUNG',
      draggable: true,
      animation: google.maps.Animation.DROP,
      icon: icon
    });

    marker.setMap(maps);
    // marker.addListener('click', toggleBounce);

    maps.addListener('bounds_changed', function() {
      searchBox.setBounds(maps.getBounds());
    });
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // Clear out the old markers.
      marker.setMap(null);

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }

        // Create a marker for each place.
        marker = new google.maps.Marker({
          map: maps,
          icon: icon,
          title: place.name,
          position: place.geometry.location,
          draggable: true,
          animation: google.maps.Animation.DROP
        });

        google.maps.event.addListener(marker, 'dragend', function(evt){
          // document.getElementById('current').innerHTML = '<p>Lokasi terpilih: Lat: <b>' + evt.latLng.lat().toFixed(8) + '</b> & Lng: <b>' + evt.latLng.lng().toFixed(8) + '</b></p>';
          document.getElementById('current'+no).innerHTML = '<p>Lokasi terpilih:</p>';
          console.log(evt.latLng.lat().toFixed(8));
          document.getElementById('address_latitude'+no).value = evt.latLng.lat().toFixed(8);
          document.getElementById('address_longitude'+no).value = evt.latLng.lng().toFixed(8);
        });

        google.maps.event.addListener(marker, 'dragstart', function(evt){
          document.getElementById('current'+no).innerHTML = '<p>Mencari lokasi ...</p>';
          document.getElementById('address_latitude'+no).value = null;
          document.getElementById('address_longitude'+no).value = null;
        });
        // document.getElementById('current').innerHTML = '<p>Lokasi terpilih: Lat: <b>' + place.geometry.location.lat() + '</b> & Lng: <b>' + place.geometry.location.lng() + '</b></p>';
        document.getElementById('current'+no).innerHTML = '<p>Lokasi terpilih:</p>';
        document.getElementById('address_latitude'+no).value = place.geometry.location.lat();
        document.getElementById('address_longitude'+no).value = place.geometry.location.lng();
        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      maps.fitBounds(bounds);
    });

    google.maps.event.addListener(marker, 'dragend', function(evt){
      // document.getElementById('current').innerHTML = '<p>Lokasi terpilih: Lat: <b>' + evt.latLng.lat().toFixed(8) + '</b> & Lng: <b>' + evt.latLng.lng().toFixed(8) + '</b></p>';
      document.getElementById('current'+no).innerHTML = '<p>Lokasi terpilih:</p>';
      document.getElementById('address_latitude'+no).value = evt.latLng.lat().toFixed(8);
      document.getElementById('address_longitude'+no).value = evt.latLng.lng().toFixed(8);
    });

    google.maps.event.addListener(marker, 'dragstart', function(evt){
      document.getElementById('current'+no).innerHTML = '<p>Mencari lokasi ...</p>';
      document.getElementById('address_latitude'+no).value = null;
      document.getElementById('address_longitude'+no).value = null;
    });
  }

