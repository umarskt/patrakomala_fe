<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class AboutUsController extends ApiController
{
    public function index(Request $request){
    	$data = [];
    	// $data['aboutus'] = $this->responseJSON($this->sendRequestToApi('aboutus', 'GET')->getBody())['data'];
    	return view('about_us.index',$data);

    }

    public function contactUs(Request $request){
    	$data = [];
    	// $data['aboutus'] = $this->responseJSON($this->sendRequestToApi('aboutus', 'GET')->getBody())['data'];
    	return view('contact_us.index',$data);
    }

    public function termsAndConditions(Request $request){
    	$data = [];
    	// $data['aboutus'] = $this->responseJSON($this->sendRequestToApi('aboutus', 'GET')->getBody())['data'];
    	return view('terms_and_conditions.index',$data);
    }
}
