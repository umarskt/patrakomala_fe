<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\RegisterHackathonRequest;

class HackathonController extends ApiController
{
    public function index(Request $request){
        $data = [];
        $result = \Browser::detect();
        // dd($result);
        $cred = [
            'page_slug' =>  'hackathon-page',
            'ip_address'    =>  $request->getClientIP(true),
            'os'    =>  $result->platformName(),
            'browser'   =>  $result->browserFamily(),
            'device'    =>  ($result->isDesktop()?'desktop':($result->isMobile()?'mobile':($result->isTablet()?'tablet':'unknown')))
        ];
        // dd(json_encode($cred));
        $res = $this->sendRequestToApi('/api/v1/analytic/counter', 'POST',$cred);
        // dd($res);
    	return view('hackathon.index',$data);
    }

    public function phpinfoDD(){
        dd(phpinfo());
        
    }

    public function register(Request $request){
        $data = [];
        // session()->set('flash_notification2',[
        //     'important'=>true,
        //     'level'=>'error',
        //     'message'=> "Mohon maaf. Pendaftaran telah ditutup."
        // ]);
        // return redirect()->route('hackathon.index');
        // $data['aboutus'] = $this->responseJSON($this->sendRequestToApi('aboutus', 'GET')->getBody())['data'];
    	return view('hackathon.register',$data);
    }

    public function postRegister(RegisterHackathonRequest $request){
        // session()->set('flash_notification2',[
        //     'important'=>true,
        //     'level'=>'error',
        //     'message'=> "Mohon maaf. Pendaftaran telah ditutup."
        // ]);
        // return redirect()->route('hackathon.index');

        // set_upload_max_filesize('20M');
        // set_post_max_size('21M');
        // set_time_limit(6000000);
        // dd($request->all());
        $reqs = [
            'team_name' =>  $request->get('team_name'),
            'description'   =>  (@$request->get('description'))?$request->get('description'):'-',
            'contact_email' =>  $request->get('contact_email'),
            'contact_phone' =>  $request->get('contact_phone')
        ];
        // dd(@$request->get('name'));
        if(@$request->has('name')){
            $members = [];
            for ($key=0;$key<count($request->get('name'));$key++) {
                $tmem[$key]['name'] = $request->get('name')[$key];
                if(@$request->get('email')[$key]){
                    $tmem[$key]['email'] = $request->get('email')[$key];
                }
                if(@$request->get('address')[$key]){
                    $tmem[$key]['address'] = $request->get('address')[$key];
                }
                if(@$request->get('phone')[$key]){
                    $tmem[$key]['phone'] = $request->get('phone')[$key];
                }
                if(@$request->get('ill')[$key]){
                    $tmem[$key]['ill'] = $request->get('ill')[$key];
                }
                if(@$request->get('emergency_name')[$key]){
                    $tmem[$key]['emergency_name'] = $request->get('emergency_name')[$key];
                }
                if(@$request->get('emergency_phone')[$key]){
                    $tmem[$key]['emergency_phone'] = $request->get('emergency_phone')[$key];
                }
                if(@$request->get('emergency_address')[$key]){
                    $tmem[$key]['emergency_address'] = $request->get('emergency_address')[$key];
                }
                $members = $tmem;
            }
            // dd($members);
            $reqs['members'] = $members;
        }
        // $d = json_encode($reqs);
        // $dd = json_decode($d);
        // dd($dd);
        $cred = [
            [
                'name' => 'data',
                'contents' => json_encode($reqs)
            ]
        ];
        if($request->has('file')){
            $cred = array_merge($cred,
                [
                    [
                        'name' => 'file',
                        'contents' => fopen( $request->file('file')->getPathname(), 'r' )
                    ]
                ]);
        }
        // dd($cred);

        $data = [];
        $res = $this->responseJSON2($this->sendRequestToApi('/api/v1/hackathon/register-hackathon', 'POSTForm',$cred));
        // dd($res);
        if(@$res){
    	   return redirect()->route('hackathon.thanks');
        }
        else{
            return redirect()->back()->withInput();
        }
    }

    public function thanks(Request $request){
        $data = [];
        // $data['aboutus'] = $this->responseJSON($this->sendRequestToApi('aboutus', 'GET')->getBody())['data'];
    	return view('hackathon.thanks',$data);
    }
}
