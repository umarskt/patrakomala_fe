<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class NewsController extends ApiController
{
    public function detail(Request $request,$id){
        $data = [];
        $params = [
            'filter' => [
                'news'  => 'latest'
            ]
        ];
        // $data['news_listing'] = $this->responseJSON($this->sendRequestToApi('/api/v1/content/filter-content', 'POST',$params)->getBody());
        $data['news'] = $this->responseJSON($this->sendRequestToApi('/api/v1/content/detail-content?content_id='.@$id, 'GET'));
        // dd($data);
        return view('news.detail',$data);
    }
}
