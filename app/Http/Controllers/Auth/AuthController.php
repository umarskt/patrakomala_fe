<?php

namespace App\Http\Controllers\Auth;

// use App\User;
// use Validator;
// use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\ThrottlesLogins;
// use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\ClientRegisterRequest;
use App\Http\Requests\ResendEmailRequest;

use Laravel\Socialite\Facades\Socialite;
use App;
use Cookie;
class AuthController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    // use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $lang = session()->get('locale');
        error_log($lang);
        if ($lang != null) App::setLocale($lang);
        error_log(App::getLocale());
        // $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    public function login(Request $request){
        if(@$request->has('claim')){
            $data = [
                'tenant_id' =>  @$request->get('tenant_id'),
                'tenant_name' =>  @$request->get('tenant_name')
            ];
            Cookie::queue('new-claim', $data, 2880);
            session()->set('flash_notification2',[
                'important'=>true,
                'level'=>'success',
                'message'=>'You need to login first to process your claim'
            ]);
        }
    	return view('auth.login');
    }

    public function register(){
        return view('auth.register');
    }

    public function postRegisterClient(ClientRegisterRequest $request){
        $data = $request->except('_token','term');
        // $data['user_type'] = 'client';
        // dd($data);
        $res = $this->responseJSON2($this->sendRequestToApi('/api/v1/user/client/register', 'POST', $data));
        // dd($res);
        if(@$res['status'] != 'failed'){
            // flash('Check your email for verification')->success()->important();
            session()->set('flash_notification2',[
                'important'=>true,
                'level'=>'success',
                'message'=>'Check your email for verification'
            ]);
            return redirect()->route('home');
        }
        else{
            $errors = new \Illuminate\Support\MessageBag();
            $errors->add('client_email', @$res['message']['app_email'][0]);
            $errors->add('client_password', @$res['message']['client_password'][0]);
            // dd($errors);
            // flash('Something went wrong')->error()->important();
            return redirect()->back()->withInput()->withErrors($errors);
        }
    }

    public function postLogin(LoginRequest $request){
        $data = $request->only('username','password');
        $res = $this->responseJSON2($this->sendRequestToApi('/api/v1/user/auth', 'POST', $data));
        // dd($res);
        if((@$res['errors'] == 0) && !(@$res['status'] == 'error') && !(@$res['status'] == 'failed')){
            if(@$res['message'] === 'userNotActive'){
                $errors = new \Illuminate\Support\MessageBag();
                $errors->add('username', 'Username tidak aktif. Verify terlebih dahulu');
                // dd($errors);
                // flash(@$res['message'])->error()->important();
                return redirect()->back()->withInput()->withErrors($errors);    
            }
            session()->put('user_token',@$res['data']['token']);
            session()->put('user',@$res['data']['user']);
            // flash('Welcome')->success()->important();
            session()->set('flash_notification2',[
                'important'=>true,
                'level'=>'success',
                'message'=>'Welcome'
            ]);
            // dd(session()->get('user'));
            if(session()->get('user')['user_type'] != 'tenant'){
                if(@Cookie::get('new-job')){ //job!
                    $cred = array_merge(Cookie::get('new-job'), ['token'=>session()->get('user_token')]);
                    if(@$cred['email'] == session()->get('user')['email']){
                        foreach ($cred['sub_sectors'] as $key => $value) {
                            $cred['sub_sectors'][$key] = (int)$value;
                        }
                        $res = $this->responseJSON2($this->sendRequestToApi('/api/v1/user/client/post-project', 'POST',$cred));
                        if(@$res['status'] == 'success'){
                            Cookie::queue(
                                Cookie::forget('new-job')
                            );
                            session()->set('flash_notification2',[
                                'important'=>true,
                                'level'=>'success',
                                'message'=> 'your unsaved job is already saved successfully'
                            ]);
                        }
                        else{
                            session()->set('flash_notification2',[
                                'important'=>true,
                                'level'=>'error',
                                'message'=> (@$res['message'])?$res['message']:'Internal server error'
                            ]);
                        }
                    }
                }
                
            }
            return redirect()->route('users.account.dashboard');
        }
        else{
            $errors = new \Illuminate\Support\MessageBag();
            $errors->add('username', (@$res['message'] == 'InvalidCredential') ? 'Email atau password salah' : 'Something wrong');
            // dd($errors);
            // flash(@$res['message'])->error()->important();
            return redirect()->back()->withInput()->withErrors($errors);
        }
    }

    public function logout(Request $request){
        session()->forget('user');
        session()->forget('user_token');
        // flash('Logout sucess')->success()->important();
        session()->set('flash_notification2',[
                'important'=>true,
                'level'=>'success',
                'message'=>'Logout sucess'
            ]);
        return redirect()->route('home');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
        // dd($user->email);
        if(!@$user->email){
            // flash('Please set your email first on your '.$provider.' account.')->error()->important();
            session()->set('flash_notification2',[
                'important'=>true,
                'level'=>'error',
                'message'=>'Please set your email first on your '.$provider.' account.'
            ]);
            return redirect()->route('users.sign_in');
        }
        $data = [
            'username'=>$user->email,
            'social_id'=>$user->id,
            'social'=>$provider,
            'name'=>$user->name,
            'image'=>$user->avatar_original,
            'user_type'=>'client'
        ];
        // dd($data);
        $res = $this->responseJSON2($this->sendRequestToApi('/api/v1/user/auth', 'POST', $data));
        // dd($res);
        if(((int)@$res['errors'] == 0) && ((int)@$res['error'] == 0)){
            session()->put('user_token',@$res['data']['token']);
            session()->put('user',@$res['data']['user']);
            // flash('Welcome')->success()->important();
            session()->set('flash_notification2',[
                'important'=>true,
                'level'=>'success',
                'message'=>'Welcome'
            ]);
            return redirect()->route('users.account.dashboard');
        }
        else{
         // flash('Something went wrong')->error()->important();
         session()->set('flash_notification2',[
                'important'=>true,
                'level'=>'error',
                'message'=>(@$res['message']) ? $res['message'] : 'Something went wrong'
            ]);
            return redirect()->route('users.sign_in');   
        }
    }

    public function confirmation(Request $request){
        $res = $this->responseJSON2($this->sendRequestToApi('/api/v1/user/verification', 'POST', @$request->only('code')));
        session()->set('flash_notification2',[
            'important'=>true,
            'level'=>'success',
            'message'=>'Verification succeed. You can login now.'
        ]);
        return redirect()->route('users.sign_in');
    }

    public function resendEmail(ResendEmailRequest $request){
        $res = $this->responseJSON2($this->sendRequestToApi('/api/v1/user/renewal-verification', 'POST', @$request->only('email')));
        // dd($res);
        error_log(@$res['data']['code']);
        session()->set('flash_notification2',[
            'important'=>true,
            'level'=>'success',
            'message'=>'Email verification sent successfully. Check your email.'
        ]);
        return redirect()->route('users.sign_in');
    }
}
