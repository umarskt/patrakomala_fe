<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
// use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\ResetPasswordRequest;

class PasswordController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    // use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    public function forgotPassword(Request $request){
        if(@$request->get('token')){
            session()->put('reset-password-token',$request->get('token'));
        }
        return redirect()->route('users.reset_password');
    }

    public function resetPassword(Request $request){
        return view('auth.forgot_password');
    }

    public function postForgotPassword(ForgotPasswordRequest $request){
        
        $res = $this->responseJSON2($this->sendRequestToApi('/api/v1/user/reset-password', 'POST', @$request->only('email')));
        session()->set('flash_notification2',[
            'important'=>true,
            'level'=>'success',
            'message'=>'Email reset password sent. Check your email.'
        ]);
        return redirect()->route('users.sign_in');
    }

    public function postResetPassword(ResetPasswordRequest $request){
        $data = [
            'token'   =>  @session()->get('reset-password-token'),
            'password'  =>  @$request->get('password'),
            'pin'  =>  @$request->get('pin')
        ];
        $res = $this->responseJSON2($this->sendRequestToApi('/api/v1/user/change-password', 'POST', $data));
        session()->set('flash_notification2',[
            'important'=>true,
            'level'=>'success',
            'message'=>'Password successfully reset. Try to login.'
        ]);
        return redirect()->route('users.sign_in');
    }
}
