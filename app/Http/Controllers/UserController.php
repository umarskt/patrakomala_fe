<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\NewJobRequest;
use Cookie;

class UserController extends ApiController
{
	public function dashboard(Request $request){
		$data = [];
		// dd(@session()->get('user'));
		$cred = [
			'tenant_id' =>  @session()->get('user')['id']
		];
		if(@session()->get('user')['user_type']=='tenant'){
			$data['profile'] = $this->responseJSON($this->sendRequestToApi('api/v1/tenant/profile-tenant', 'GET',$cred));
			// dd($data);
			if(@$data['profile']){
				$data['shares'] = \Share::load(route('tennants.detail',$data['profile']['id']), 'Kunjungi kami di '.$data['profile']['tenant_name'])->services('facebook', 'gplus', 'twitter','linkedin');
			}
			$view = 'users.tennant.dashboard';
		}
		else{
			$cred = [
				'token' =>  @session()->get('user_token')
			];
			$data['jobs'] = $this->responseJSON($this->sendRequestToApi('api/v1/user/client/list-project', 'POST',$cred));
			// dd($data);
			if(@$data['jobs']['data']){
				$data['jobs'] = $data['jobs']['data'];
			}
			else{
				$data['jobs'] = [];
			}
			// dd($data['jobs']);
			// dd(session()->get('user'));
			$view = 'users.customer.dashboard';
		}
		// dd($data);
		if(@Cookie::get('new-claim')){ //claim!
			$data['claim'] = Cookie::get('new-claim');
			Cookie::queue(
				Cookie::forget('new-claim')
			);
			session()->set('flash_notification2',[
				'important'=>true,
				'level'=>'success',
				'message'=> trans('general.desc.claim.3')
			]);
		}
		// dd($data);
		return view($view,$data);
	}

	public function setting(Request $request){
		$data = [];
		// $data['aboutus'] = $this->responseJSON($this->sendRequestToApi('aboutus', 'GET')->getBody())['data'];
		if(@session()->get('user')['user_type']=='tenant'){
			$data['profile'] = session()->get('user');
			$view = 'users.tennant.settings';
		}
		else{
			$data['profile'] = session()->get('user');
			$view = 'users.customer.settings';
		}
		// dd($data);
		return view($view,$data);
	}

	public function postSetting(Request $request){
		$data = @$request->all();
		// $data['aboutus'] = $this->responseJSON($this->sendRequestToApi('aboutus', 'GET')->getBody())['data'];
		return redirect()->route('users.account.setting');
	}

	public function jobs(Request $request){
		$data = [];
		if(@session()->get('user')['user_type']=='tenant'){
			$cred = [
				'token' =>  @session()->get('user_token')
			];
			$data['jobs'] = $this->responseJSON($this->sendRequestToApi('/api/v1/user/client/list-project', 'POST',$cred));
			$view = 'users.tennant.jobs';
		}
		else{
			$cred = [
				'token' =>  @session()->get('user_token')
			];
			$data['jobs'] = $this->responseJSON($this->sendRequestToApi('/api/v1/user/client/list-project', 'POST',$cred));
			$view = 'users.customer.jobs';
		}
		// dd($data);
		return view($view,$data);   
	}

	public function jobDetail(Request $request){
		$data = [];
		if(@session()->get('user')['user_type']=='tenant'){
			$view = 'users.tennant.job-detail';
		}
		else{
			$view = 'users.customer.job-detail';
		}
		return view($view,$data);   
	}

	public function newJob(Request $request){
		$data = [];
		$data['subsectors'] = $this->responseJSON($this->sendRequestToApi('/api/v1/tenant/list-subsector', 'GET'));
		$view = 'users.customer.new_job';
		return view($view,$data);   
	}

	public function postJob(NewJobRequest $request){
		$cred = @$request->except('_token','new-job-sbm','estimated_costT');
		$cred = array_merge($cred, ['token'=>session()->get('user_token')]);
		foreach ($cred['sub_sectors'] as $key => $value) {
			$cred['sub_sectors'][$key] = (int)$value;
		}
		// dd($cred);
		$res = $this->responseJSON2($this->sendRequestToApi('/api/v1/user/client/post-project', 'POST',$cred));
		if(@$res['status'] == 'success'){
			return redirect()->route('users.account.dashboard');
		}
		else{
			session()->set('flash_notification2',[
				'important'=>true,
				'level'=>'error',
				'message'=> (@$res['message'])?$res['message']:'Internal server error'
			]);
			return redirect()->back()->withInput();
		}
	}

	public function postClaim(Request $request){
		$cred = [
			'token'=>session()->get('user_token'),
			'tenant_id' =>  (int)@$request->get('tenant_id')
		];
		// dd($cred);
		$res = $this->responseJSON2($this->sendRequestToApi('/api/v1/tenant/claim', 'POST',$cred));
		// dd($res);
		if((@$res['status'] != 'failed') && (@$res['status'] != 'error')){
			session()->set('flash_notification2',[
				'important'=>true,
				'level'=>'success',
				'message'=> trans('general.desc.claim.4')
			]);
		}
		else{
			session()->set('flash_notification2',[
				'important'=>true,
				'level'=>'error',
				'message'=> (@$res['message'])?$res['message']:'Claim failed due to some reason'
			]);
		}
		return redirect()->route('users.account.dashboard');
	}

	public function appliance(Request $request){
		$data = [];
			$view = 'users.customer.appliance';
		return view($view,$data);   
	}

	public function updateTenant(Request $request){
		// dd($request->all());
		$data = [
			[
				'name' => 'token',
				'contents'   => @session()->get('user_token')
			]
		];
		if(@$request->file('logo_file') || @$request->get('bg_color')){
			if(@$request->file('logo_file')){
				array_push($data,[
					'name' => 'tenant_logo',
					'contents' => fopen( $request->file('logo_file')->getPathname(), 'r' )
					// 'filename' => $request->file('logo_file')->getClientOriginalName()
				]);
			}
			if(@$request->get('bg_color')){
				array_push($data,[
					'name' => 'tenant_color_background',
					'contents'   => $request->get('bg_color')
				]);
			}
			// dd($data);
			$res = $this->responseJSON2($this->sendRequestToApi('/api/v1/tenant/edit-profile', 'POSTform', $data));
		}

		if(@$request->get('tenant_profile')){
			array_push($data,[
				'name' => 'tenant_profile',
				'contents'   => $request->get('tenant_profile')
			]);
			$res = $this->responseJSON2($this->sendRequestToApi('/api/v1/tenant/edit-profile', 'POSTform', $data));
		}

		if(@$request->get('tenant_intro')){
			array_push($data,[
				'name' => 'tenant_intro',
				'contents'   => $request->get('tenant_intro')
			]);
			$res = $this->responseJSON2($this->sendRequestToApi('/api/v1/tenant/edit-profile', 'POSTform', $data));
		}

		if(@$request->get('tenant_mission')){
			array_push($data,[
				'name' => 'tenant_mission',
				'contents'   => $request->get('tenant_mission')
			]);
			$res = $this->responseJSON2($this->sendRequestToApi('/api/v1/tenant/edit-profile', 'POSTform', $data));
		}

		if(@$request->has('tenant_service')){
			foreach($request->get('tenant_service') as $key => $value){
				array_push($data,[
					'name'  =>  'tenant_service['.$key.']',
					'contents' =>  $value
				]);
			}
			$res = $this->responseJSON2($this->sendRequestToApi('/api/v1/tenant/edit-profile', 'POSTform', $data));
		}

		if(@$request->file('portofolios')){
			// dd($request->all());
			$leng_re = 0;
			if(@$request->get('reportofolios')){
				foreach($request->get('reportofolios') as $key => $value){
					if(@$value){
						array_push($data,[
							'name'  =>  'portofolios['.$key.']',
							'contents' =>  fopen( $value, 'r' )
						]);
						array_push($data,[
							'name'  =>  'portofolio_names['.$key.']',
							'contents' =>  @$request->get('portfolio_name')[$key]
						]);
					}
				}
				$leng_re = count($request->get('reportofolios'));
			}
			foreach($request->file('portofolios') as $key => $value){
				if(@$value){
					array_push($data,[
						'name'  =>  'portofolios['.($key+$leng_re).']',
						'contents' =>  fopen( $value->getPathname(), 'r' ),
						'filename' => $value->getClientOriginalName()
					]);
				}
				array_push($data,[
					'name'  =>  'portofolio_names['.($key+$leng_re).']',
					'contents' => @$request->get('portfolio_name')[($key+$leng_re)]
				]);
			}
			// dd($data);
			$res = $this->responseJSON2($this->sendRequestToApi('/api/v1/tenant/edit-profile', 'POSTform', $data));
		}

		if(@$request->get('address_latitude') && @$request->get('address_longitude')){
			array_push($data,[
				'name' => 'latitude',
				'contents'   => @$request->get('address_latitude')
			]);
			array_push($data,[
				'name' => 'longitude',
				'contents'   => @$request->get('address_longitude')
			]);
			$res = $this->responseJSON2($this->sendRequestToApi('/api/v1/tenant/edit-profile', 'POSTform', $data));
		}

		if(@$request->has('social_site_url') || @$request->has('social_site_type')){
			foreach($request->get('social_site_url') as $key => $value){
				if($value!= "" && @$request->get('social_site_type')[$key] != null){
					array_push($data,[
						'name'  =>  'social_site_type['.$key.']',
						'contents' => @$request->get('social_site_type')[$key]
					]);
				}
				array_push($data,[
					'name'  =>  'social_site_url['.$key.']',
					'contents' =>  $value
				]);
				
			}
			// dd($data);
			$res = $this->responseJSON2($this->sendRequestToApi('/api/v1/tenant/edit-profile', 'POSTform', $data));
		}

		if(@$request->has('client_name')){
			// dd(@$request->all());
			$count = 0;
			if(@$request->get('reclient_img_logo')){
				$context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
				foreach($request->get('reclient_img_logo') as $key => $value){
					if(@$value){
						array_push($data,[
							'name'  =>  'client_img_logo['.$key.']',
							// 'contents' =>  file_get_contents($value)
							'contents' =>  fopen( $value, 'rb')
						]);
					}
				}
				$count = count($request->get('reclient_img_logo'));
			}
			foreach($request->get('client_name') as $key => $value){
				// if($value!= "" && @$request->file('client_img_logo')[$key] != null){
				
					array_push($data,[
						'name'  =>  'client_name['.$key.']',
						'contents' =>  $value
					]);
					if((@$request->get('reclient_img_logo')[$key] == "") || (@$request->get('reclient_img_logo')[$key] == null)){
						array_push($data,[
							'name'  =>  'client_img_logo['.($key).']',
							'contents' =>  fopen( $request->file('client_img_logo')[$key]->getPathname(), 'r' ),
							'filename' => $request->file('client_img_logo')[$key]->getClientOriginalName()
						]);
					}
					else if(@$request->file('client_img_logo')[$key]){
						array_push($data,[
							'name'  =>  'client_img_logo['.($count).']',
							'contents' =>  fopen( $request->file('client_img_logo')[$key]->getPathname(), 'r' ),
							'filename' => $request->file('client_img_logo')[$key]->getClientOriginalName()
						]);
						$count = $count+1;
					}
				// }
			}
			// dd($data);
			$res = $this->responseJSON2($this->sendRequestToApi('/api/v1/tenant/edit-profile', 'POSTform', $data));
		}
		// dd($request->all());
		if(@$request->file('gallery_image')){
			if(@$request->get('id_album')){
				array_push($data,[
					'name'  =>  'id_album',
					'contents' =>  @$request->get('id_album')
				]);
			}
			array_push($data,[
				'name'  =>  'album_title',
				'contents' =>  @$request->get('gallery_title')
			]);
			array_push($data,[
				'name'  =>  'album_description',
				'contents' =>  @$request->get('gallery_description')
			]);
			$count = 0;
			if(@$request->get('regallery_image')){
				foreach($request->get('regallery_image') as $key => $value){
					if(@$value){
						array_push($data,[
							'name'  =>  'gallery_image['.$key.']',
							// 'contents' =>  file_get_contents($value)
							'contents' =>  fopen( $value, 'rb')
						]);
					}
				}
				$count = count($request->get('regallery_image'));
			}
			foreach($request->file('gallery_image') as $key => $value){
				if(@$value){
					array_push($data,[
						'name'  =>  'gallery_image['.$count.']',
						'contents' => fopen( @$value->getPathname(), 'r' ),
						'filename' => @$value->getClientOriginalName()
					]);
					$count = $count+1;
				}
			}
			// dd($data);
			$res = $this->responseJSON2($this->sendRequestToApi('/api/v1/tenant/store-gallery', 'POSTform', $data));
		}

		// dd($data);
		// dd($res);
		if(@$res['status'] == 'success'){
			session()->set('flash_notification2',[
				'important'=>true,
				'level'=>'success',
				'message'=>'Data updated successfully.'
			]);
		}
		else{
			session()->set('flash_notification2',[
				'important'=>true,
				'level'=>'error',
				'message'=>'Failed to update data.'
			]);
		}
		return redirect()->route('users.account.dashboard');
	}

	public function previewPage(Request $request){
		$data = [];
		$data['tenant'] = $this->responseJSON($this->sendRequestToApi('/api/v1/tenant/profile-tenant?tenant_id='.@session()->get('user')['id'], 'GET'));
		// dd($data);
		return view('users.tennant.preview_page.index',$data);
	}

	public function getPartial(Request $request){
		if(@$request->get('partial')){
			$view = 'users.tennant.partials._'.$request->get('partial');
			return view($view);
		}
		else{
			return '';
		}
	}

	public function generateKey(){
		$cred = [
			'token' =>  @session()->get('user_token')
		];
		$res = $this->responseJSON($this->sendRequestToApi('/api/v1/public/generate-key', 'POST',$cred));
		if(@$res['status'] != 'failed'){
			session()->put('generated-key',@$res['key']);
		}
		return redirect()->route('users.account.dashboard');
	}

	public function listEvent(Request $request){
		$data = [];
		$cred = [
			'token' =>  @session()->get('user_token')
		];
		$data['events'] = $this->responseJSON($this->sendRequestToApi('/api/v1/content/list-content', 'POST',$cred));
		$view = 'users.customer.list_event';
		return view($view,$data);
	}

	public function newEvent(Request $request){
		$data = [];
		$view = 'users.customer.new_event';
		// dd(@session()->get('user_token'));
		return view($view,$data);   
	}

	public function postEvent(Request $request){
		$isi = [
			'token' =>  @session()->get('user_token'),
			'title'   =>  @$request->get('title'),
			'start_date'    =>  strtotime(@$request->get('start_date')),
			'end_date'    =>  strtotime(@$request->get('end_date')),
			'content'   =>  @$request->get('description'),
			'place'   =>  @$request->get('building').', '.@$request->get('address'),
			'tags'   =>  @$request->get('tags'),
			'urls'   =>  @$request->get('links')
		];
		// dd(json_encode($isi));
		$res = $this->responseJSON2($this->sendRequestToApi('/api/v1/content/post-content', 'POST',$isi));
		// dd($res);

		if(@$res['status'] == 201){
			$data = [
				[
					'name' => 'token',
					'contents'   => @session()->get('user_token')
				],
				[
					'name' => 'id',
					'contents'   => @$res['data']['event_id']
				]
			];
			if(@$request->file('images')){
				foreach($request->file('images') as $key => $value){
					if(@$value){
						array_push($data,[
							'name'  =>  'images['.($key).']',
							'contents' =>  fopen( $value->getPathname(), 'r' ),
							'filename' => $value->getClientOriginalName()
						]);
					}
				}
			}
			$res2 = $this->responseJSON2($this->sendRequestToApi('/api/v1/content/post-image', 'POSTform',$data));
			session()->set('flash_notification2',[
				'important'=>true,
				'level'=>'success',
				'message'=>'Event created successfully.'
			]);
		}
		else{
			session()->set('flash_notification2',[
				'important'=>true,
				'level'=>'error',
				'message'=>'Failed to save event.'
			]);
			return redirect()->back()->withInput();
		}
		return redirect()->route('users.account.list-event');

	}

	public function editEvent(Request $request,$id){
		$data = [];
		$cred = [
			'token' =>  @session()->get('user_token'),
			'event_id'  =>  (int)@$id
		];
		$res = $this->responseJSON2($this->sendRequestToApi('/api/v1/content/user-content', 'POST',$cred));
		if(@$res['status'] == 200){
			$data['event'] = $res['data'];
		}
		$view = 'users.customer.edit_event';
		// dd($data);
		return view($view,$data);   
	}

	public function updateEvent(Request $request,$id){
		$isi = [
			'token' =>  @session()->get('user_token'),
			'title'   =>  @$request->get('title'),
			'start_date'    =>  strtotime(@$request->get('start_date')),
			'end_date'    =>  strtotime(@$request->get('end_date')),
			'content'   =>  @$request->get('description'),
			'place'   =>  @$request->get('building').', '.@$request->get('address'),
			'tags'   =>  @$request->get('tags'),
			'urls'   =>  @$request->get('links'),
			'type'	=>	'event'
		];
		// dd(json_encode($isi));
		$res = $this->responseJSON2($this->sendRequestToApi('/api/v1/content/'.$id.'/post-content', 'PUT',$isi));
		// dd($res);

		if(@$res['status'] == 202){
			if($request->hasFile('images')){
				$data = [
					[
						'name' => 'token',
						'contents'   => @session()->get('user_token')
					],
					[
						'name' => 'id',
						'contents'   => (int)@$id
					]
				];
				if(@$request->file('images')){
					foreach($request->file('images') as $key => $value){
						if(@$value){
							array_push($data,[
								'name'  =>  'images['.($key).']',
								'contents' =>  fopen( $value->getPathname(), 'r' ),
								'filename' => $value->getClientOriginalName()
							]);
						}
					}
				}
				$res2 = $this->responseJSON2($this->sendRequestToApi('/api/v1/content/post-image', 'POSTform',$data));
			}
			session()->set('flash_notification2',[
				'important'=>true,
				'level'=>'success',
				'message'=>'Event created successfully.'
			]);
		}
		else{
			session()->set('flash_notification2',[
				'important'=>true,
				'level'=>'error',
				'message'=>'Failed to save event.'
			]);
			return redirect()->back()->withInput();
		}
		return redirect()->route('users.account.edit-event',$id);

	}

	public function uploadPicture(Request $request){
		// dd($request->all());
		$CKEditor = $request->get('CKEditor');
		$funcNum  = $request->get('CKEditorFuncNum');
		$message  = $url = '';
		if ($request->hasFile('upload')) {
			$file = $request->file('upload');
			if ($file->isValid()) {
				$filename =rand(1000,9999).$file->getClientOriginalName();
				$file->move(public_path().'/event-tmp/', $filename);
				$url = url('event-tmp/' . $filename);
			} else {
				$message = 'An error occurred while uploading the file.';
			}
		} else {
			$message = 'No file uploaded.';
		}
		$ret = '<script>window.parent.CKEDITOR.tools.callFunction('.$funcNum.', "'.$url.'", "'.$message.'")</script>';
		echo $ret;
	}

	public function getPicture(Request $request){ //unused
		return asset('assets/example-images/cS-1.jpg');
	}
}
