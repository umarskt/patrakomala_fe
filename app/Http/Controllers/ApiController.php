<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Illuminate\Http\Request;
use App;
class ApiController extends Controller
{
	public $client;

    public function __construct()
    {
        $cookie = \Request::cookie('_oAuth');
    	$client = new Client([
    		'base_uri'	=> config('app.api'),
    		'headers'	=> [
    			'content-type'	=>	'application/json',
                'token' => $cookie,
    		]
    	]);
        $lang = session()->get('locale');
        error_log($lang);
        if ($lang != null) App::setLocale($lang);
        error_log(App::getLocale());

    	$this->client = $client;
    }

    public function responseJSON($response)
    {
        // dd($response);
        if(@$response->getStatusCode() == 200){
    	   $res = json_decode($response->getBody(), true);
           if(@$res['data']){
                $res = $res['data'];
           }
           return $res;
        }
        else{
            return null;
        }
    }

    public function responseJSON2($response)
    {
        // dd(json_decode($response->getBody(), true));
        if((@$response->getStatusCode() == 200) || @$response->getStatusCode() == 201){
           return json_decode($response->getBody(), true);
        }
        else{
            if(@$response->getStatusCode() == 400){

            }
            else if(@$response->getStatusCode() == 401){
                \Log::info('ERROR 401');
                \Log::info(json_decode($response->getBody(), true));
            }
            else if(@$response->getStatusCode() == 422){

            }
            else if(@$response->getStatusCode() == 404){

            }
            else if(@$response->getStatusCode() == 500){
                \Log::info('ERROR 500');
                $abc = json_decode($response->getBody(), true);
                \Log::info(json_decode($response->getBody(), true));
                return $abc;
            }
            
            return null;
        }
    }

    public function sendRequestToApi($url, $method, $params = null)
    {
        // dd(config('app.api'));
        $requestClient = new Client([
            'base_uri'  => config('app.api'),
            'headers'   => [
                'Content-Type'  =>  (strtolower($method) == 'postform') ? 'multipart/form-data' : 'application/json',
                // 'Accept-Type' => 'Application/json'
                'api-key'   =>  'CTDGopj48U9wQ9OPZa3rWNxQIx7Dpd'
            ]
        ]);
        try{
            // if ($params) {
            //     $params = array_merge($params, ['api_key' => str_shuffle('HsRkl').base64_encode('anonymous')]);
            // }
            $filter = 'query';
            if(strtolower($method) == 'post'){
                $filter = 'json';
            }
            if(strtolower($method) == 'postform'){
                $filter = 'multipart';
                $method = 'POST';
            }
            return $requestClient->request($method, $url, [$filter => $params]);
        } catch (GuzzleException $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                // dd($e->getResponse());
                if(@$response->getStatusCode() == 401){
                    flash('Email atau password salah.')->error()->important();
                }
                else if(@$response->getStatusCode() == 500){
                    flash('there\'s an internal server error')->error()->important();
                }
                else if(@$response->getStatusCode() == 404){
                    flash('User tidak terdaftar.')->error()->important();
                }
                else if(@$response->getStatusCode() == 402){
                    flash('There is an validation error.')->error()->important();
                }
                else{
                    // flash(@$e->getResponse()->getReasonPhrase())->error()->important();
                }
                // return redirect('home');
                return $response;
            }
        }
    }

}
