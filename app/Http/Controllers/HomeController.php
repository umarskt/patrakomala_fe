<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use App\Http\Requests;
// use Illuminate\Support\Facades\Session;
// use App;
// use Lang;

class HomeController extends ApiController
{
    public function index(Request $request){
        $data = [];
        $params = [
            'filter' => [
                'news'  => (@$request->get('type') == 'news') ? 'latest' : null,
                'event' => (in_array(@$request->get('type'),['ongoing','upcoming','past'])) ? @$request->get('type') : null,
                'keyword' => @$request->get('keyword')
            ],
            'page' => @$request->get('page')
        ];
        try{
            $data['content'] = $this->responseJSON2($this->sendRequestToApi('/api/v1/content/filter-content', 'POST',$params));
            // dd($data);
        }catch(Exception $e){
            $data['content']['data'] = [];
        }
        if($request->ajax()){
            return response()->view('home._list',$data);
        }
    	return view('home.index',$data);
    }
}
