<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\TenantRegisterRequest;
use App\Http\Requests\NewJobRequest;
use Cookie;

class TennantsController extends ApiController
{
    public function index(Request $request){
        $data = [];
        $subdistrict_ids = [];
        if(@$request->get('subdistrict')){
            foreach($request->get('subdistrict') as $key => $value){
                array_push($subdistrict_ids,(int)$value);
            }
        }
        $params_tenant = [
            'page' => $request->get('page'),
            'filter'    => [
                'subsector' => (@$request->get('subsector')) ? [$request->get('subsector')] : [],
                'subdistrict' => $subdistrict_ids,
                'keyword'  =>   $request->get('keyword')
            ]
        ];
        $data['subsectors'] = $this->responseJSON($this->sendRequestToApi('/api/v1/tenant/list-subsector', 'GET'));
        $data['subdistricts'] = $this->responseJSON($this->sendRequestToApi('/api/v1/tenant/list-subdistrict', 'GET'));
        $data['tenants'] = $this->responseJSON2($this->sendRequestToApi('/api/v1/tenant/list-tenant', 'POST', $params_tenant));
        // dd($data);
        if($request->ajax()){
            return response()->view('tennants._list',$data);
        }
    	return view('tennants.index',$data);
    }

    public function detail(Request $request){
        $data = [];
        // $data['aboutus'] = $this->responseJSON($this->sendRequestToApi('aboutus', 'GET')->getBody())['data'];
    	return view('tennants.detail',$data);
    }

    public function listTenant(Request $request){
        $data = [];
        // $data['aboutus'] = $this->responseJSON($this->sendRequestToApi('aboutus', 'GET')->getBody())['data'];
        return response()->json($data);
    }

    public function registration(Request $request){
        $result = \Browser::detect();
        // dd($result);
        $cred = [
            'page_slug' =>  'register-page',
            'ip_address'    =>  $request->getClientIP(true),
            'os'    =>  $result->platformName(),
            'browser'   =>  $result->browserFamily(),
            'device'    =>  ($result->isDesktop()?'desktop':($result->isMobile()?'mobile':($result->isTablet()?'tablet':'unknown')))
        ];
        // dd(json_encode($cred));
        $res = $this->sendRequestToApi('/api/v1/analytic/counter', 'POST',$cred);
        // dd($res);
        $data = [];
        if(@$request->get('subdistrict_id')){
            $data['villages'] = $this->responseJSON($this->sendRequestToApi('/api/v1/tenant/list-villages', 'POST',['subdistrict_id'=>(int)$request->get('subdistrict_id')]));
            if(@$request->get("selected_id")){
                $data['selected_id'] = $request->get("selected_id");
            }
            $view = 'tennants.partials._villages';

        }
        $data['subsectors'] = $this->responseJSON($this->sendRequestToApi('/api/v1/tenant/list-subsector', 'GET'));
        $data['subdistricts'] = $this->responseJSON($this->sendRequestToApi('/api/v1/tenant/list-subdistrict', 'GET'));
        // dd($data);
        // $data['aboutus'] = $this->responseJSON($this->sendRequestToApi('aboutus', 'GET')->getBody())['data'];
        if($request->ajax()){
            return view($view,$data);
        }
    	return view('tennants.regis2',$data);
    }

    public function getPartial(Request $request){
        if(@$request->get('partial')){
            $view = 'tennants.regis._'.$request->get('partial');
            $data = [];
            if(@$request->get('box_children')){
                $data['n'] = $request->get('box_children');
            }
            $data['subsectors'] = $this->responseJSON($this->sendRequestToApi('/api/v1/tenant/list-subsector', 'GET'));
            $data['subdistricts'] = $this->responseJSON($this->sendRequestToApi('/api/v1/tenant/list-subdistrict', 'GET'));
            return view($view,$data);
        }
        else{
            return '';
        }
    }

    public function postRegistration(TenantRegisterRequest $request){
        $data = [];
        $cred = [];
        if(@$request->get('legality_doc_name')){
            $cred['legality_doc_name'] = [];
            $cred['legality_image'] = [];
            foreach($request->get('legality_doc_name') as $key => $value){
                $cred['legality_doc_name'] = array_merge([($value=='sp')? 'Sertifikat Pendirian': (($value=='sku')?'Surat Keterangan Usaha (SKU)':'NPWP & SIUPP')],$cred['legality_doc_name']);
                $cred['legality_image'] = array_merge([$request->file('tenant_legality_images')[$value]],$cred['legality_image']);
            }
        }

        if(@$request->get('fasilitas_toko')){
            foreach($request->get('fasilitas_toko') as $key => $value){
                switch ($request->get('fasilitas_toko')) {
                    case 'toilet':
                        $cred['is_facility_toilet'] = true;
                        break;
                    case 'parkir':
                        $cred['is_facility_parking'] = true;
                        break;
                    case 'mushola':
                        $cred['is_facility_mushola'] = true;
                        break;
                    case 'showroom':
                        $cred['is_facility_showroom'] = true;
                        break;
                    default:
                        break;
                }
            }
        }
        $cred = array_merge([
            'tenant_name'   =>  @$request->get('company_name'),
            'tenant_founding_year'   =>  @$request->get('company_year'),
            'tenant_about'   =>  '',
            'tenant_logo'   =>  null,
            'tenant_email'   =>  @$request->get('tenant_email'),
            'tenant_phone'   =>  @$request->get('tenant_phone'),
            'founder_name'   =>  @$request->get('founder_name'),
            'founder_last_education'   =>  @$request->get('founder_last_education'),
            'founder_last_education_department'   => @$request->get('founder_last_education_department'),
            'founder_gender'   =>  @$request->get('gender'),
            'address_subdistrict_id'   =>  @$request->get('address_subdistrict_id'),
            'village_id'   =>  @$request->get('kelurahan'),
            'address_latitude'   =>  @$request->get('address_latitude'),
            'address_longitude'   =>  @$request->get('address_longitude'),
            'address_street'   =>  @$request->get('address_street'),
            'address_postal_code'   =>  @$request->get('address_postal_code'),
            'company_type'   =>  @$request->get('business_type'),
            'company_hki'   =>  @$request->get('hki'),
            'product'   =>  @$request->get('bentuk_usaha'),
            'product_type'   =>  @$request->get('jenis_produk'),
            'product_character'   =>  @$request->get('karakter_produk'),
            'product_quantity'   =>  @$request->get('quantity'),
            'product_unit'   =>  @$request->get('unit'),
            'tool_name'   =>  @$request->get('equipment_name'),
            'tool_brand_name'   =>  @$request->get('nama_brand'),
            'tool_quantity'   =>  @$request->get('jumlah_mesin'),
            'tool_capacity'   =>  @$request->get('kapasitas_mesin'),
            'tool_price'   =>  @$request->get('harga_mesin'),
            'is_copyright'   =>  false,
            'marketing_is_online'   =>  (int)@$request->get('marketing_type'),
            'social_site_type'   =>  (@$request->get('online_market_type')[0]) ? $request->get('online_market_type') : [],
            'social_site_url'   =>  (@$request->get('online_market_url')[0]) ? $request->get('online_market_url') : [],
            'open_at'   =>  @$request->get('jam_buka'),
            'close_at'   =>  @$request->get('jam_tutup'),
            'surface_area'   =>  @$request->get('luas_area'),
            'employee_male_total'   =>  @$request->get('jumlah_karyawan_pria'),
            'employee_female_total'   =>  @$request->get('jumlah_karyawan_wanita'),
            'employee_general_fee'   =>  @$request->get('minimum_salary'),
            'employee_elementary_school_total'   =>  @$request->get('edu_major')['sd'],
            'employee_junior_high_total'   =>  @$request->get('edu_major')['smp'],
            'employee_high_total'   =>  @$request->get('edu_major')['sma'],
            'employee_d1_total'   =>  @$request->get('edu_major')['d1'],
            'employee_d3_total'   =>  @$request->get('edu_major')['d3'],
            'employee_bacheloor_degree_total'   =>  @$request->get('edu_major')['s1'],
            'employee_post_graduate_total'   =>  @$request->get('edu_major')['s2'],
            'employee_doctoral_degree_total'   =>  @$request->get('edu_major')['s3'],
            'asset'   =>  @$request->get('asset'),
            'daily_omset'   =>  @$request->get('omset')['hari'],
            'monthly_omset'   =>  @$request->get('omset')['bulan'],
            'yearly_omset'   =>  @$request->get('omset')['tahun'],
            'daily_profit'   =>  @$request->get('netto')['hari'],
            'monthly_profit'   =>  @$request->get('netto')['bulan'],
            'yearly_profit'   =>  @$request->get('netto')['tahun'],
            'access_financing'   =>  @$request->get('financing_access'),
            'jenis_usaha_kreatif'   =>  @$request->get('jenis_usaha_kreatif'),
            'investment'   =>  @$request->get('sumber_pembiayaan'),
            'tenant_category'   =>  (@$request->get('tenant_category')[0]) ? $request->get('tenant_category') : [],
            'coaching'   =>  (@$request->get('coach_name')[0]) ? $request->get('coach_name') : [],
            'coaching_type'   =>  (@$request->get('coaching_type')[0]) ? $request->get('coaching_type') : [],
            'coaching_year'   =>  (@$request->get('coaching_year')[0]) ? $request->get('coaching_year') : [],
            'membership_name'   =>  (@$request->get('community_name')[0]) ? $request->get('community_name') : [],
            'business_strength'   =>  (@$request->get('business_strength')[0]) ? $request->get('business_strength') : [],
            'business_opportunity'   =>  (@$request->get('business_opportunity')[0]) ? $request->get('business_opportunity') : []
        ],$cred);
        $cred2 = [];
        foreach($cred as $key => $value){
            if(is_array($value)){
                foreach($value as $k => $v){
                    array_push($cred2,[
                        'name'  =>  $key.'['.$k.']',
                        'contents' =>  $v
                    ]);
                }
            }
            else{
                array_push($cred2,[
                    'name'  =>  $key,
                    'contents' =>  $value
                ]);
            }
        }
// dd($cred2);
        $response = $this->responseJSON2($this->sendRequestToApi('/api/v1/tenant/register-tenant', 'POSTFORM',$cred2));
        if(@$response['status']){
            session()->set('flash_notification2',[
                'important'=>true,
                'level'=>'error',
                'message'=> (@$response['data_error']['Message'])?$response['data_error']['Message']:'Internal server error'
            ]);
            return redirect()->back()->withInput();
        }
        if(@$response['status'] == 'failed'){
            session()->set('flash_notification2',[
                'important'=>true,
                'level'=>'error',
                'message'=> (@$response['message'])?$response['message']:'Internal server error'
            ]);
            return redirect()->back()->withInput();
        }
        return redirect()->route('tennants.sign_up.success');
    }
    
    public function afterRegistration(Request $request){
        $data = [];
        // $data['aboutus'] = $this->responseJSON($this->sendRequestToApi('aboutus', 'GET')->getBody())['data'];
    	return view('tennants.after_signup',$data);
    }

    public function login(Request $request){
        $data = [];
        // $data['aboutus'] = $this->responseJSON($this->sendRequestToApi('aboutus', 'GET')->getBody())['data'];
        return view('tennants.sign_in',$data);
    }

    public function detailTenant(Request $request,$id){
        $data = [];
        $data['tenant'] = $this->responseJSON($this->sendRequestToApi('/api/v1/tenant/profile-tenant?tenant_id='.@$id, 'GET'));

        // dd($data);
        return view('tennants.detail-tenant2',$data);
    }

    public function subsectorDetail(Request $request){
        $data = [];
        if($request->all()){
            $data = $request->all();
        }
        if($request->ajax()){
            return response()->view('tennants._subsektor-detail',$data);
        }
    }

    public function postJob(NewJobRequest $request){
        $data = @$request->except('estimated_costT','new-job-sbm','_token');
        // dd($data);
        if(@session()->get('user_token')){
            $data = array_merge($data,['token'=>@session()->get('user_token')]);
            $res = $this->responseJSON2($this->sendRequestToApi('/api/v1/user/client/post-project', 'POST',$data));
            session()->set('flash_notification2',[
                'important'=>true,
                'level'=>'success',
                'message'=>'Your have successfully save your job.'
            ]);
            return redirect()->route('users.account.jobs');
        }
        else{
            Cookie::queue('new-job', $data, 2880);
            // dd(Cookie::get('new-job'));
            session()->set('flash_notification2',[
                'important'=>true,
                'level'=>'success',
                'message'=>'Your job will be erased after 2 hours. Login to process your new job.'
            ]);
            return redirect()->route('users.sign_in');
        }
    }
}
