<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class LocaleController extends ApiController
{
    public function update(Request $request){
    	$old_locale = Session::get('locale');
        if(@$request->get('language')){

            Session::forget('locale');
            Session::put('locale',$request->get('language'));
        }
        if($request->ajax()){
            return response()->json(@Session::get('locale'), 200);
        }
        if(@session()->get('_previous')['url']){
        	$prev_url = str_replace('/'.$old_locale,'/'.@$request->get('language'), session()->get('_previous')['url']);
        	// dd($prev_url);
        	return redirect()->to($prev_url);
        }
        else{
			return redirect()->route('home');
        }
    }
}
