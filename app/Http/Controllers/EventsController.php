<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class EventsController extends ApiController
{
    public function index(Request $request){
    	$data = [];
    	// $data['events'] = $this->responseJSON($this->sendRequestToApi('events', 'GET')->getBody())['data'];
    	if($request->ajax()){
    		return response()->view('events._list',$data);
    	}
    	return view('events.index',$data);
    }

    public function detail(Request $request,$id){
    	$data = [];
        $res = $this->responseJSON($this->sendRequestToApi('/api/v1/content/detail-content?content_id='.@$id, 'GET'));
        // dd($res);
        if(@$res['status'] != 'failed'){
            $data['event'] = $res;
            return view('events.detail',$data);
        }
        else{
            // session()->set('flash_notification2',[
            //     'important'=>true,
            //     'level'=>'error',
            //     'message'=>(@$res['message'])?$res['message']:'Can not get event detail'
            // ]);
            // return redirect()->route('home');
            return view('errors.404');
        }
    }
}
