<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class FAQsController extends ApiController
{
    public function index(Request $request){
        $data = [];
        // $data['event'] = $this->responseJSON($this->sendRequestToApi('event', 'GET')->getBody())['data'];
        return view('faqs.index',$data);
    }

    public function patrakomala(Request $request){
        $data = [];
        // $data['event'] = $this->responseJSON($this->sendRequestToApi('event', 'GET')->getBody())['data'];
    	return view('faqs.patrakomala',$data);
    }

    public function subsektor(Request $request){
        $data = [];
        // $data['event'] = $this->responseJSON($this->sendRequestToApi('event', 'GET')->getBody())['data'];
    	return view('faqs.subsektor',$data);
    }

    public function jobs(Request $request){
        $data = [];
        // $data['event'] = $this->responseJSON($this->sendRequestToApi('event', 'GET')->getBody())['data'];
    	return view('faqs.jobs',$data);
    }

    public function tourPackages(Request $request){
        $data = [];
        // $data['event'] = $this->responseJSON($this->sendRequestToApi('event', 'GET')->getBody())['data'];
    	return view('faqs.tour_packages',$data);
    }
}
