<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class TourPackagesController extends ApiController
{
    public function index(Request $request){
        $data = [];
        $params_filter = $this->filterParams($request);
        $data['belts'] = $this->responseJSON($this->sendRequestToApi('/api/v1/tour-package/list-belt', 'GET'));
        $data['packages'] = $this->responseJSON($this->sendRequestToApi('/api/v1/tour-package/list-package', 'GET'));
        $data['subsectors'] = $this->responseJSON($this->sendRequestToApi('/api/v1/tenant/list-subsector', 'GET'));
        $data['tenants'] = $this->responseJSON($this->sendRequestToApi('/api/v1/tour-package/filter-package', 'POST'));
        if($request->ajax()){
            $data['filter'] = $this->responseJSON2($this->sendRequestToApi('/api/v1/tour-package/filter-package', 'POST',$params_filter));
        	return response()->json($data['filter']);
        }
        // dd($data);
    	return view('tour_packages.index',$data);
    }

    public function listPackage(Request $request){
        $belt_ids = [];
        if(@$request->get('belt_id')){
            foreach($request->get('belt_id') as $key => $value){
                array_push($belt_ids,(int)$value);
            }
            $params = [
                'belt_id' => json_encode($belt_ids)
            ];
        }
        // return (object)$params;
        $data['packages'] = $this->responseJSON2($this->sendRequestToApi('/api/v1/tour-package/list-package', 'GET',@$params));
        $data['selected_package'] = @$request->get('selected_package');
        // dd($data);
        if($request->ajax()){
            // return response()->json($data);
            return response()->view('tour_packages._list_package',$data);
        }
    }

    public function detailTenant(Request $request){
        $params = [];
        if(@$request->get('id')){
            $params = [
                'tenant_id' =>  @$request->get('id')
            ];
        }
        $detail = $this->responseJSON($this->sendRequestToApi('/api/v1/tour-package/detail-tenant', 'GET', @$params));
        // return $detail;
        if($request->ajax()){
            return response()->json($detail);
        }
        else{
            return $detail;
        }
    }

    protected function filterParams($request){
        return [
            'filter' => [
                'belt' => (@$request->get('belt')) ? $request->get('belt') : null,
                'subsector' => (@$request->get('subsector')) ? $request->get('subsector') : null,
                'package' => (@$request->get('package')) ? $request->get('package') : null,
                'keyword' => (@$request->get('keyword')) ? $request->get('keyword') : ''
            ]
        ];
    }
}
