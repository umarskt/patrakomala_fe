<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function(){
// 	return redirect()->route('home',['lang'=>'en']);
// });
Route::group(['middleware'=>['web','locale']],function($router){
	Route::get('/', [
		'as' 	=> 'home',
		'uses'	=> 'HomeController@index'
	]);
});
Route::group(['middleware'=>['web','locale'],'prefix'=>'about-us'],function($router){
	$router->get('/',[
		'as'	=>	'about_us.index',
		'uses'	=>	'AboutUsController@index'
	]);
});
Route::group(['middleware'=>['web','locale'],'prefix'=>'contact-us'],function($router){
	$router->get('/',[
		'as'	=>	'contact_us.index',
		'uses'	=>	'AboutUsController@contactUs'
	]);
});
Route::group(['middleware'=>['web','locale'],'prefix'=>'terms-and-conditions'],function($router){
	$router->get('/',[
		'as'	=>	'terms_and_conditions.index',
		'uses'	=>	'AboutUsController@termsAndConditions'
	]);
});
Route::group(['middleware'=>['web','locale'],'prefix'=>'faq'],function($router){
	$router->get('/',[
		'as'	=>	'faq.index',
		'uses'	=>	'FAQsController@index'
	]);
	$router->get('/patrakomala',[
		'as'	=>	'faq.patrakomala',
		'uses'	=>	'FAQsController@patrakomala'
	]);
	$router->get('/subsektor',[
		'as'	=>	'faq.subsektor',
		'uses'	=>	'FAQsController@subsektor'
	]);
	$router->get('/jobs',[
		'as'	=>	'faq.jobs',
		'uses'	=>	'FAQsController@jobs'
	]);
	$router->get('/tour-packages',[
		'as'	=>	'faq.tour_packages',
		'uses'	=>	'FAQsController@tourPackages'
	]);
});
Route::group(['middleware'=>['web','locale'],'prefix'=>'events'],function($router){
	// $router->get('/',[					//deleted
	// 	'as'	=>	'events.index',
	// 	'uses'	=>	'EventsController@index'
	// ]);
	$router->get('/{id}',[
		'as'	=>	'events.detail',
		'uses'	=>	'EventsController@detail'
	]);
});
Route::group(['middleware'=>['web','locale'],'prefix'=>'jobs'],function($router){
	$router->get('/',[
		'as'	=>	'jobs.index',
		'uses'	=>	'JobsController@index'
	]);
});
Route::group(['middleware'=>['web','locale'],'prefix'=>'tour-packages'],function($router){
	$router->get('/',[
		'as'	=>	'tour_packages.index',
		'uses'	=>	'TourPackagesController@index'
	]);
	$router->get('/list-tour',function(){
		return view('tour_packages.tour-list');
	});
	$router->get('/list-package',[
		'as'	=>	'tour_packages.list-package',
		'uses'	=>	'TourPackagesController@listPackage'
	]);
	$router->get('/detail',[
		'as'	=>	'tour_packages.detail',
		'uses'	=>	'TourPackagesController@detailTenant'
	]);
});
Route::group(['middleware'=>['web','locale'],'prefix'=>'hackathon'],function($router){
	$router->get('/',[
		'as'	=>	'hackathon.index',
		'uses'	=>	'HackathonController@index'
	]);
	$router->get('/register',[
		'as'	=>	'hackathon.register',
		'uses'	=>	'HackathonController@register'
	]);
	$router->post('/register',[
		'as'	=>	'hackathon.register.post',
		'uses'	=>	'HackathonController@postRegister'
	]);
	$router->get('/thanks',[
		'as'	=>	'hackathon.thanks',
		'uses'	=>	'HackathonController@thanks'
	]);
	$router->get('/list-tour',function(){
		return view('tour_packages.tour-list');
	});
	$router->get('/get-php-info',[
		'as'	=>	'hackathon.php-info',
		'uses'	=>	'HackathonController@phpinfoDD'
	]);
});
Route::group(['middleware'=>['web','locale'],'prefix'=>'tenants'],function($router){
	$router->get('/',[
		'as'	=>	'tennants.index',
		'uses'	=>	'TennantsController@index'
	]);
	$router->get('sign-up',[
		'as'	=>	'tennants.sign_up',
		'uses'	=>	'TennantsController@registration'
	]);
	$router->get('register/partial',[
		'as'	=>	'tennants.sign_up.partial',
		'uses'	=>	'TennantsController@getPartial'
	]);
	$router->get('sign-in',[
		'as'	=>	'tennants.sign_in',
		'uses'	=>	'TennantsController@login'
	]);
	$router->get('sign-up/success',[
		'as'	=>	'tennants.sign_up.success',
		'uses'	=>	'TennantsController@afterRegistration'
	]);
	$router->post('sign-up',[
		'as'	=>	'tennants.sign_up.post',
		'uses'	=>	'TennantsController@postRegistration'
	]);
	$router->post('sign-in',[
		'as'	=>	'tennants.sign_in.post',
		'uses'	=>	'TennantsController@postLogin'
	]);
	// $router->get('/{id}',[
	// 	'as'	=>	'tennants.subsektor.detail',
	// 	'uses'	=>	'TennantsController@detail'
	// ]);
	$router->get('/{id}',[
		'as'	=>	'tennants.detail',
		'uses'	=>	'TennantsController@detailTenant'
	]);
	$router->get('detail/subsector',[
		'as'	=>	'subsector.detail',
		'uses'	=>	'TennantsController@subsectorDetail'
	]);
	$router->post('/post-job',[
		'as'	=>	'tennants.post-job',
		'uses'	=>	'TennantsController@postJob'
	]);
});
Route::group(['middleware'=>['web','locale'],'prefix'=>'users', 'namespace'=> 'Auth'],function($router){
	$router->get('forgot-password',[
		'as'	=>	'users.forgot_password',
		'uses'	=>	'PasswordController@forgotPassword'
	]);
	$router->get('reset-password',[
		'as'	=>	'users.reset_password',
		'uses'	=>	'PasswordController@resetPassword'
	]);
	$router->post('forgot-password',[
		'as'	=>	'users.forgot_password.post',
		'uses'	=>	'PasswordController@postForgotPassword'
	]);
	$router->post('reset-password',[
		'as'	=>	'users.reset_password.post',
		'uses'	=>	'PasswordController@postResetPassword'
	]);
	$router->get('sign-in',[
		'as'	=>	'users.sign_in',
		'uses'	=>	'AuthController@login'
	]);
	$router->post('sign-in',[
		'as'	=>	'users.sign_in.post',
		'uses'	=>	'AuthController@postLogin'
	]);
	$router->get('sign-up',[
		'as'	=>	'users.sign_up',
		'uses'	=>	'AuthController@register'
	]);
	$router->post('sign-up',[
		'as'	=>	'users.sign_up.post',
		'uses'	=>	'AuthController@postRegisterClient'
	]);
	$router->get('sign-out',[
		'as'	=>	'users.sign_out',
		'uses'	=>	'AuthController@logout'
	]);
	$router->get('login/{provider}', [
		'as'	=>	'auth.provider',
		'uses'	=>	'AuthController@redirectToProvider'
	]);
	$router->get('{provider}/callback', [
		'as'	=>	'auth.provider.callback',
		'uses'	=>'AuthController@handleProviderCallback'
	]);
	$router->get('confirmation', [
		'as'	=>	'auth.confirmation',
		'uses'	=>'AuthController@confirmation'
	]);
	$router->post('resend-verify', [
		'as'	=>	'auth.resend-email',
		'uses'	=>'AuthController@resendEmail'
	]);
});
Route::group(['middleware'=>['web','locale','auth'],'prefix'=>'my-account'],function($router){
	$router->get('/you',[
		'as'	=>	'users.account.dashboard',
		'uses'	=>	'UserController@dashboard'
	]);
	$router->post('/',[
		'as'	=>	'users.account.update-tenant',
		'uses'	=>	'UserController@updateTenant'
	]);
	$router->get('/setting',[
		'as'	=>	'users.account.setting',
		'uses'	=>	'UserController@setting'
	]);
	$router->post('/setting',[
		'as'	=>	'users.account.setting.post',
		'uses'	=>	'UserController@postSetting'
	]);
	$router->get('/jobs',[
		'as'	=>	'users.account.jobs',
		'uses'	=>	'UserController@jobs'
	]);
	$router->get('/new-job',[
		'as'	=>	'users.account.new-job',
		'uses'	=>	'UserController@newJob'
	]);
	$router->post('/post-job',[
		'as'	=>	'users.account.post-job',
		'uses'	=>	'UserController@postJob'
	]);
	$router->get('/jobs/{id}',[
		'as'	=>	'users.account.jobs.detail',
		'uses'	=>	'UserController@jobDetail'
	]);
	$router->get('/appliances',[
		'as'	=>	'users.account.appliance',
		'uses'	=>	'UserController@appliance'
	]);
	$router->get('/preview',[
		'as'	=>	'users.account.preview-page',
		'uses'	=>	'UserController@previewPage'
	]);
	$router->post('/post-claim',[
		'as'	=>	'users.claim.post',
		'uses'	=>	'UserController@postClaim'
	]);
	$router->post('/generate-key',[
		'as'	=>	'users.generate-key.post',
		'uses'	=>	'UserController@generateKey'
	]);
	$router->get('/list-event',[
		'as'	=>	'users.account.list-event',
		'uses'	=>	'UserController@listEvent'
	]);
	$router->get('/new-event',[
		'as'	=>	'users.account.new-event',
		'uses'	=>	'UserController@newEvent'
	]);
	$router->post('/post-event',[
		'as'	=>	'users.account.post-event',
		'uses'	=>	'UserController@postEvent'
	]);
	$router->get('/edit-event/{id}',[
		'as'	=>	'users.account.edit-event',
		'uses'	=>	'UserController@editEvent'
	]);
	$router->post('/update-event/{id}',[
		'as'	=>	'users.account.update-event',
		'uses'	=>	'UserController@updateEvent'
	]);

	// AJAX
	$router->get('/get-partial',[
		'as'	=>	'users.account.get-partial',
		'uses'	=>	'UserController@getPartial'
	]);
	$router->post('/upload-picture',[
		'as'	=>	'users.account.upload-picture',
		'uses'	=>	'UserController@uploadPicture'
	]);
	$router->get('/get-picture',[
		'as'	=>	'users.account.get-picture',
		'uses'	=>	'UserController@getPicture'
	]);
});
Route::group(['middleware'=>['web','locale'],'prefix'=>'how-project-work'],function($router){
	$router->get('/',[
		'as'	=>	'how-project-work.index',
		'uses'	=>	'ProjectWorkController@index'
	]);
});
Route::group(['middleware'=>['web','locale'],'prefix'=>'news'],function($router){
	$router->get('/{id}',[
		'as'	=>	'news.detail',
		'uses'	=>	'NewsController@detail'
	]);
});
Route::group(['middleware'=>['web','locale'],'prefix'=>'examples'],function($router){
	$router->get('/geocode',function(){
		return view('examples.geocode');
	});
	$router->get('/lightbox',function(){
		return view('examples.lightbox');
	});
});

$router->get('/change-locale', [
	'as'	=>	'change.locale',
	'uses'	=>	'LocaleController@update'
]);

Route::group(['middleware'=>['web','locale'],'prefix'=>'safari-hki'],function($router){
	$router->get('/',[
		'as'	=>	'safari_hki.index',
		'uses'	=>	'SafariHkiController@index'
	]);
});