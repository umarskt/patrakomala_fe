<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClientRegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_first_name'=>'required',
            'client_last_name'=>'required',
            'client_email'=>'required|email',
            'company_name'=>'required',
            'client_password'=>'required|min:7',
            'client_password_confirmation'=>'required|same:client_password'
        ];
    }

}
