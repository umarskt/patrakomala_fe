<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterHackathonRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'team_name'=>'required',
            // 'description'=>'required',
            'contact_email' => 'required|email',
            'contact_phone' => 'required|regex:/(08)[0-9]{9}/',
            'phone' => 'required|array|min:1',
            'phone.*' => 'required|regex:/(08)[0-9]{9}/',
            'name'=>'required|array|min:1',
            'name.*'    =>  'required|regex:/^[\pL\s\-]+$/u',
            'emergency_name'=>'required|array|min:1',
            'emergency_name.*'  =>  'required|regex:/^[\pL\s\-]+$/u',
            'emergency_address'=>'required|array|min:1',
            'emergency_address.*'=>'required',
            'emergency_phone'=>'required|array|min:1',
            'emergency_phone.*'=>'required|regex:/(08)[0-9]{9}/',
            'address'=>'required|array|min:1',
            'address.*'=>'required',
            'email'=>'required|array|min:1',
            'email.*'=>'required|email',
            'ill' =>  'required|array|min:1',
            'ill.*' =>  'required'
            // 'file'  =>  'required|file|mimes:pdf|max:8000000'

            // 'type_of_content' => 'present',
            // 'choices[]' => 'required_unless:type_of_content,is_information',

            // 'file'  =>  'required'
        ];
    }

    public function messages(){
        return [
            'team_name.required'    =>  'Nama Tim harus diisi',
            'description.required'   =>  'Ide harus diisi',
            'contact_email.required' =>  'Alamat Email harus diisi',
            'contact_phone.required'    =>  'No HP harus diisi',
            'file.required' =>  'File Proposal harus diisi',
            'file.mimes' => 'File Proposal harus PDF',
            'file.max'  =>  'File Proposal tidak lebih dari 10MB',
            'contact_email.email'   =>  'Alamat Email harus valid',
            'email.email'   =>  'Alamat Email harus valid',
            'contact_phone.*.regex'   =>  'No HP tidak valid',
            'phone.*.regex'   =>  'No HP tidak valid',
            'name.*.required'    =>  'Nama harus diisi',
            'email.*.required'    =>  'Email harus diisi',
            'email.*.email'    =>  'Email tidak valid',
            'emergency_name.*.required'    =>  'Nama Kontak Darurat harus diisi',
            'name.*.alpha'    =>  'Nama hanya dapat diisi huruf',
            'emergency_name.*.alpha'    =>  'Nama Kontak Darurat hanya dapat diisi huruf',
            'emergency_phone.*.required'    =>  'Telepon Kontak Darurat harus diisi',
            'emergency_phone.*.regex'    =>  'Telepon Kontak Darurat tidak valid',
            'emergency_address.*.required'    =>  'Alamat Kontak Darurat harus diisi',
            'address.*.required'    =>  'Alamat harus diisi',
            'phone.*.required'    =>  'No HP harus diisi',
            'ill.*.required'    =>  'Riwayat Penyakit harus diisi'
        ];
    }

}
