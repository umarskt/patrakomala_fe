<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NewJobRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required',
            'title'=>'required',
            'description'=>'required',
            'type'=>'required',
            'sub_sectors'=>'required',
            'estimated_cost'=>'required'
        ];
    }

}
