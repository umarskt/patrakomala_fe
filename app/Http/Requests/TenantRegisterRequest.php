<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TenantRegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name'=>'required',
            // 'founder_name'=>'required',
            // 'founder_last_education'=>'required',
            // 'founder_last_education_department'=>'required',
            // 'company_year'=>'required',
            // 'company_month'=>'required',
            'address_street'=>'required',
            'address_subdistrict_id'=>'required',
            'rtrw'=>'required',
            'kelurahan'=>'required',
            'address_postal_code'=>'required',
            'address_latitude'=>'required',
            'address_longitude'=>'required',
            'tenant_email'=>'required',
            'tenant_phone'=>'required',
            // 'hp'=>'required',
            // 'npwp_type'=>'required',
            // 'business_type'=>'required|in:pd,pt,cv',
            // 'legality_doc_name'=>'required',
            // 'tenant_legality_images'=>'required',
            'tenant_category'=>'required',
            // 'bentuk_usaha'=>'required',
            // 'jenis_produk'=>'required',
            // 'karakter_produk'=>'required',
            // 'jumlah_produk_kg'=>'required',
            // 'jumlah_produk_set'=>'required',
            // 'bahan_baku_th'=>'required',
            // 'bahan_baku_rp'=>'required',
            // 'nama_pemasok'=>'required',
            // 'jumlah_mesin'=>'required',
            // 'kapasitas_mesin'=>'required',
            // 'harga_mesin'=>'required',
            // 'nama_brand'=>'required',
            // 'hki'=>'required'
        ];
    }
}
