<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // if (Auth::guard($guard)->guest()) {
        error_log("@session()->get('user_token')");
        error_log(@session()->get('user_token'));
        if (!@session()->get('user_token')) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            }
            else {
                session()->forget('user');
                session()->forget('user_token');
                return redirect()->route('users.sign_in');
            }
        }

        return $next($request);
    }
}
