<?php 

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Session;
use App;
use Lang;

class Language{

    public function __construct(Application $app, Redirector $redirector, Request $request) {
        $this->app = $app;
        $this->redirector = $redirector;
        $this->request = $request;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->isMethod('get')){
            $locale = $request->segment(1);
            if ( ! array_key_exists($locale, $this->app->config->get('app.locales'))) { // all links clicked
                $segments = $request->segments();
                $locale = (@Session::get('locale')) ? Session::get('locale') : $this->app->config->get('app.fallback_locale');
                $locale = (@$request->get('language')) ? $request->get('language') : $locale;
                array_unshift($segments, $locale);
                Session::forget('locale');
                Session::put('locale',$locale);
                App::setLocale($locale);
                $url = implode('/', $segments).(@$request->getQueryString() ? '?'.$request->getQueryString() : '');
            error_log('1');
            error_log(App::getLocale());
                return $this->redirector->to($url);
            }
            else if(($locale != @Session::get('locale')) && array_key_exists($locale, $this->app->config->get('app.locales'))){
                Session::forget('locale');
                Session::put('locale', $locale);
                App::setLocale($locale);
                error_log('2');
                error_log(App::getLocale());
            }
        }
        return $next($request);
    }

}