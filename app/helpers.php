<?php
	if(!function_exists('asset_bg')){
		function asset_bg(){
			$asset_bg = "";
			$route_name = \Request::route()->getName();
			switch ($route_name) {
				case 'home':
					$asset_bg = "background: url('/assets/images/1920/PATRA KOMALA BG.png');";
					$asset_bg .= "background-repeat: no-repeat;background-size: 100% 100%;";
					break;
				case 'tennants.index':
					$asset_bg = "background: url('/assets/images/1920/PATRA KOMALA BG.png');";
					$asset_bg .= "background-repeat: no-repeat;background-size: 100% 100%;";
					break;
				default:
					$asset_bg = "";
					break;
			}
			return $asset_bg;
		}
	}

	if(!function_exists('locale')){
		function locale(){
			// return \App::getLocale();
			return @session()->get('locale');
		}
	}

	if(!function_exists('current_user')){
		function current_user(){
			return @session()->get('user');
		}
	}

	if(!function_exists('current_user_token')){
		function current_user_token(){
			return @session()->get('user_token');
		}
	}

	if (! function_exists('unformatCurrency')) {
	    function UnformatCurrency($value)
	    {
	        $explode = explode('.', $value);

	        $return = implode('', $explode);
	        $return = str_replace(',', '.', $return);
	        return $return;
	    }
	}

	if (! function_exists('currency')) {
	    function currency($value,$dec=0)
	    {
	        return number_format($value,$dec,',','.');
	    }
	}

	if (! function_exists('dateTimeIDN')) {
	    function dateTimeIDN($time)
	    {
	        return date_format(date_create($time),'d M Y');
	    }
	}

?>